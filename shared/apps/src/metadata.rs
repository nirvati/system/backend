// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::BTreeMap;

#[cfg(feature = "graphql")]
use async_graphql::scalar;
use serde::{Deserialize, Serialize};

pub use nirvati::utils::MultiLanguageItem;

use crate::utils::true_default;

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize, Copy, Clone, Default)]
#[cfg_attr(feature = "graphql", derive(async_graphql::Enum))]
pub enum SaasCompatibility {
    Compatible,
    Incompatible,
    #[default]
    Unknown,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, Hash)]
#[serde(untagged)]
pub enum Dependency {
    OneDependency(String),
    AlternativeDependency(Vec<String>),
}

#[derive(Clone, PartialEq, Eq, Debug, Default, Serialize, Deserialize)]
pub struct SvcPorts {
    pub udp: Vec<u16>,
    pub tcp: Vec<u16>,
}

#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq, Eq)]
#[cfg_attr(feature = "graphql", derive(async_graphql::SimpleObject))]
pub struct Permission {
    pub id: String,
    pub name: MultiLanguageItem,
    pub description: MultiLanguageItem,
    /// Other permissions this permission implies
    /// May also contain permissions of other apps
    pub includes: Vec<String>,
    /// Secrets (+ keys) accessible with this permission
    #[cfg_attr(feature = "graphql", graphql(skip))]
    pub secrets: BTreeMap<String, Vec<String>>,
    /// Makes this permission "invisible" (Hidden from the UI) if requested by the apps listed in this field
    /// The * wildcard can be used to hide from all apps
    pub hidden: Vec<String>,
    /// Includes access to certain services and ports on these services
    pub services: BTreeMap<String, SvcPorts>,
    /// For system apps, set this to true to allow all users to access this permission
    /// If false, only other system apps can access this permission
    pub open_to_all_users: bool,
    /// The volumes this permission grants access to
    #[serde(default)]
    pub volumes: Vec<VolumePermission>,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
#[cfg_attr(feature = "graphql", derive(async_graphql::SimpleObject))]
pub struct VolumePermission {
    pub volume: String,
    pub sub_paths: Option<Vec<String>>,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[serde(tag = "type")]
pub enum Setting {
    Enum {
        name: MultiLanguageItem,
        description: MultiLanguageItem,
        values: Vec<String>,
        default: Option<String>,
    },
    String {
        name: MultiLanguageItem,
        description: MultiLanguageItem,
        default: Option<String>,
        max_len: Option<usize>,
        string_type: Option<StringType>,
        #[serde(default = "true_default")]
        required: bool,
        placeholder: Option<MultiLanguageItem>,
    },
    Bool {
        name: MultiLanguageItem,
        description: MultiLanguageItem,
        default: bool,
    },
    Int {
        name: MultiLanguageItem,
        description: MultiLanguageItem,
        default: Option<i64>,
        min: Option<i64>,
        max: Option<i64>,
        step_size: Option<i64>,
    },
    Float {
        name: MultiLanguageItem,
        description: MultiLanguageItem,
        default: Option<f64>,
        min: Option<f64>,
        max: Option<f64>,
        step_size: Option<f64>,
    },
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum StringType {
    #[serde(rename = "password")]
    Password,
    #[serde(rename = "email")]
    Email,
    #[serde(rename = "url")]
    Url,
    #[serde(rename = "ip")]
    Ip,
}

impl Setting {
    pub fn default_value(&self) -> serde_json::Value {
        match self {
            Setting::Enum { default, .. } => match default {
                Some(default) => serde_json::Value::String(default.clone()),
                None => serde_json::Value::Null,
            },
            Setting::String { default, .. } => match default {
                Some(default) => serde_json::Value::String(default.clone()),
                None => serde_json::Value::Null,
            },
            Setting::Bool { default, .. } => serde_json::Value::Bool(*default),
            Setting::Int { default, .. } => match default {
                Some(default) => serde_json::Value::Number(serde_json::Number::from(*default)),
                None => serde_json::Value::Null,
            },
            Setting::Float { default, .. } => match default {
                Some(default) => {
                    serde_json::Value::Number(serde_json::Number::from_f64(*default).unwrap())
                }
                None => serde_json::Value::Null,
            },
        }
    }
}

#[cfg(feature = "graphql")]
scalar!(Setting);

#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq, Eq)]
#[cfg_attr(feature = "graphql", derive(async_graphql::SimpleObject))]
#[cfg_attr(feature = "graphql", graphql(name = "VolumeDefinition"))]
pub struct Volume {
    pub minimum_size: u64,
    pub recommended_size: u64,
    pub name: MultiLanguageItem,
    pub description: MultiLanguageItem,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum Runtime {
    #[serde(rename = "AppYml")]
    Kubernetes,
    Plugin(String),
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
#[serde(rename_all = "camelCase")]
#[cfg_attr(feature = "graphql", derive(async_graphql::SimpleObject))]
pub struct InputDeclaration {
    pub label: MultiLanguageItem,
    pub description: MultiLanguageItem,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
#[serde(rename_all = "camelCase")]
#[cfg_attr(feature = "graphql", derive(async_graphql::SimpleObject))]
pub struct StorePlugin {
    pub name: String,
    pub icon: String,
    pub description: String,
    pub id: String,
    pub inputs: BTreeMap<String, InputDeclaration>,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
#[serde(rename_all = "camelCase")]
#[cfg_attr(feature = "graphql", derive(async_graphql::SimpleObject))]
pub struct UiMenuEntry {
    pub name: MultiLanguageItem,
    // Icon name, must be either a raw SVG or a name of a heroicons icon
    pub icon: String,
    pub path: String,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
#[serde(rename_all = "camelCase")]
#[cfg_attr(feature = "graphql", derive(async_graphql::SimpleObject))]
pub struct UiModule {
    pub menu_entries: Vec<UiMenuEntry>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct Settings(pub BTreeMap<String, Setting>);

#[cfg(feature = "graphql")]
scalar!(Settings);

impl Settings {
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
}

#[derive(Serialize, Deserialize, Clone, Copy, Debug, PartialEq, Eq, Default)]
#[cfg_attr(feature = "graphql", derive(async_graphql::Enum))]
pub enum AppScope {
    #[default]
    User,
    System,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[serde(rename_all = "camelCase")]
#[cfg_attr(feature = "graphql", derive(async_graphql::SimpleObject))]
pub struct AppComponent {
    /// The component id
    pub id: String,
    /// The name of the component
    pub name: MultiLanguageItem,
    /// A description of the component
    pub description: MultiLanguageItem,
    /// Whether the component is required
    pub required: bool,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[serde(rename_all = "camelCase")]
#[cfg_attr(feature = "graphql", derive(async_graphql::SimpleObject))]
pub struct Metadata {
    /// The app id
    pub id: String,
    /// The name of the app
    pub name: MultiLanguageItem,
    /// The version of the app
    #[cfg_attr(feature = "graphql", graphql(skip))]
    pub version: semver::Version,
    /// The version of the app to display
    #[cfg_attr(feature = "graphql", graphql(name = "version"))]
    pub display_version: String,
    /// The category for the app
    pub category: MultiLanguageItem,
    /// A short tagline for the app
    pub tagline: MultiLanguageItem,
    /// Developer name -> their website
    pub developers: BTreeMap<String, String>,
    /// A description of the app
    pub description: MultiLanguageItem,
    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    #[cfg_attr(feature = "graphql", graphql(skip))]
    /// Dependencies the app requires
    pub dependencies: Vec<Dependency>,
    /// Permissions this app has, without including permissions from individual containers
    #[cfg_attr(feature = "graphql", graphql(skip))]
    pub base_permissions: Vec<String>,
    /// Permissions the app has
    /// If a permission is from an app that is not listed in the dependencies, it is considered optional
    #[cfg_attr(feature = "graphql", graphql(skip))]
    pub has_permissions: Vec<String>,
    /// Permissions this app exposes
    pub exposes_permissions: Vec<Permission>,
    /// App repository name -> repo URL
    pub repos: BTreeMap<String, String>,
    /// A support link for the app
    pub support: String,
    /// A list of promo images for the apps
    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub gallery: Vec<String>,
    /// The URL to the app icon
    pub icon: Option<String>,
    /// The path the "Open" link on the dashboard should lead to
    #[serde(skip_serializing_if = "Option::is_none")]
    pub path: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    /// The app's default username
    pub default_username: Option<String>,
    /// The app's default password.
    pub default_password: Option<String>,
    /// For "virtual" apps, the service the app implements
    #[serde(skip_serializing_if = "Option::is_none")]
    pub implements: Option<String>,
    #[serde(
        default,
        skip_serializing_if = "BTreeMap::<String, BTreeMap<String, String>>::is_empty"
    )]
    pub release_notes: BTreeMap<String, BTreeMap<String, String>>,
    /// The SPDX identifier of the app license
    pub license: String,
    /// Available settings for this app
    #[serde(default, skip_serializing_if = "Settings::is_empty")]
    pub settings: Settings,
    /// Volumes this app exposes
    pub volumes: BTreeMap<String, Volume>,
    /// Ports this app uses
    /// Before installing, agent clients need to check if any of these ports are already in use by other apps
    /// If so, the user needs to be notified
    pub ports: Vec<u16>,
    /// Exported data shared with plugins (Map plugin -> data)
    #[serde(skip_serializing_if = "Option::is_none")]
    #[cfg_attr(feature = "graphql", graphql(skip))]
    pub exported_data: Option<BTreeMap<String, BTreeMap<String, serde_json::Value>>>,
    /// The runtime this app uses
    #[cfg_attr(feature = "graphql", graphql(skip))]
    pub runtime: Runtime,
    /// Whether this app is installed system-wide or per user
    #[serde(skip)]
    pub scope: AppScope,
    /// The scopes this app can be installed to
    pub allowed_scopes: Vec<AppScope>,
    /// The default scopes
    pub default_scope: AppScope,
    #[serde(default)]
    pub store_plugins: Vec<StorePlugin>,
    #[serde(default)]
    pub ui_module: Option<UiModule>,
    /// Whether the app supports ingress
    /// This is declared by agent based on whether an app's ingress vec is empty,
    /// setting it during generation has no effect
    #[serde(default = "true_default")]
    pub supports_ingress: bool,
    /// Whether this app can be protected
    /// Please note: If the app type is not "App", this field is ignored and will be treated as if it were false
    #[cfg_attr(feature = "graphql", graphql(skip))]
    pub can_be_protected: bool,
    #[serde(default)]
    pub saas_compatibility: SaasCompatibility,
    /// True if the app receives ingress as TCP (without TLS being handled by Traefik) instead of HTTP
    pub raw_ingress: bool,
    /// URL to redirect to post-install
    pub post_install_redirect: Option<String>,
    #[serde(default)]
    pub components: Vec<AppComponent>,
    /// True if the app is running in the Citadel compat layer
    pub is_citadel: bool,
    #[cfg_attr(feature = "graphql", graphql(skip))]
    pub preload_type: Option<PreloadType>,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum PreloadType {
    Nirvati,
    MainVolumeFromSubdir(String),
}
