// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};

use nirvati::utils::MultiLanguageItem;

#[derive(Debug, Serialize, Deserialize, Clone, Default, PartialEq, Eq)]
#[cfg_attr(feature = "graphql", derive(async_graphql::SimpleObject))]
pub struct StoreMetadata {
    pub name: MultiLanguageItem,
    pub tagline: MultiLanguageItem,
    pub description: MultiLanguageItem,
    pub icon: String,
    // Developer name -> their website
    pub developers: BTreeMap<String, String>,
    pub license: String,
}
