use nirvati_apps::internal::{InternalAppRepresentation, Volume};
use nirvati_apps::metadata::SaasCompatibility;

pub fn saas_compatibility_state(app: &InternalAppRepresentation) -> SaasCompatibility {
    let mut compat = SaasCompatibility::Compatible;

    if !app.plugins.is_empty() {
        compat = SaasCompatibility::Unknown;
    }

    for runnable in app.containers.values() {
        let container = runnable.get_container();
        if container.privileged
            || container.cap_add.iter().any(|cap| {
                let allowed_caps = [
                    "AUDIT_WRITE",
                    "CHOWN",
                    "DAC_OVERRIDE",
                    "FOWNER",
                    "FSETID",
                    "KILL",
                    "MKNOD",
                    "NET_BIND_SERVICE",
                    "SETFCAP",
                    "SETGID",
                    "SETPCAP",
                    "SETUID",
                    "SYS_CHROOT",
                ];
                !allowed_caps.contains(&cap.as_str())
            })
            || container.host_network
            || container
                .volumes
                .iter()
                .any(|v| matches!(v, Volume::Host(_)))
        {
            return SaasCompatibility::Incompatible;
        }
    }

    if app.other.is_empty() && app.custom_resources.is_empty() {
        compat
    } else {
        SaasCompatibility::Unknown
    }
}
