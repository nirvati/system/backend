use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum AppEvent {
    Apply {
        app_id: String,
        version: String,
        settings: serde_json::Value,
    },
    Uninstall {
        app_id: String,
    },
    ChangeSettings {
        app_id: String,
        previous_settings: serde_json::Value,
        new_settings: serde_json::Value,
    },
    Update {
        app_id: String,
        from_version: String,
        to_version: String,
    },
    Pause {
        app_id: String,
    },
    Resume {
        app_id: String,
    },
    Protect {
        app_id: String,
    },
    UnProtect {
        app_id: String,
    },
    AddSource {
        src: String,
    },
    RemoveSource {
        src: String,
    },
    MoveToSystem {
        app_id: String,
    },
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum DomainTarget {
    App(String),
    Proxy(String),
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum HttpsEvent {
    AddDomain {
        target: DomainTarget,
        domain: String,
    },
    RemoveDomain {
        target: DomainTarget,
        domain: String,
    },
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum ProxyEvent {
    Create {
        id: String,
    },
    Remove {
        id: String,
    },
    AddRoute {
        proxy_id: String,
        local_path: String,
        target: String,
    },
    RemoveRoute {
        proxy_id: String,
        local_path: String,
        target: String,
    },
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum UserEvent {
    Login {
        username: String,
    },
    /*Logout {
        username: String,
    },*/
    Create {
        username: String,
        display_name: String,
        avatar: String,
        email: String,
        permissions: Vec<nirvati::permissions::Permission>,
    },
    Delete {
        username: String,
        // Keep track of display name here, so we can show it for deleted users
        display_name: String,
    },
    UpdateProfile {
        username: String,
        display_name: Option<String>,
        avatar: Option<String>,
        previous_display_name: String,
        previous_avatar: String,
    },
    UpdatePassword {
        username: String,
    },
    UpdateEmail {
        username: String,
        email: String,
    },
    UpdateUsername {
        previous_username: String,
        new_username: String,
        display_name: String,
    },
    UpdatePermissions {
        username: String,
        permissions: Vec<nirvati::permissions::Permission>,
        previous_permissions: Vec<nirvati::permissions::Permission>,
    },
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum Event {
    App(AppEvent),
    Https(HttpsEvent),
    Proxy(ProxyEvent),
    User(UserEvent),
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum Method {
    Api { token_audiences: Vec<String> },
    Direct,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct LogEntry {
    pub event_type: Event,
    pub timestamp: chrono::DateTime<chrono::Utc>,
    pub user: String,
    pub method: Method,
}
