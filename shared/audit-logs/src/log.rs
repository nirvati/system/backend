use crate::events::Event;

pub fn log_event(event: Event, user: &str, src: &str) {
    tracing::info!("Logging event from {} via {}: {:?}", user, src, event);
    // TODO: Actually implement logging
}
