<!--
SPDX-FileCopyrightText: 2024 The Nirvati Developers

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Nirvati common components

This repository contains some common components used within the various parts of [Nirvati](https://nirvati.org).
It may be useful if you plan to build a Nirvati plugin.
