// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use serde::{Deserialize, Serialize};

pub mod kubernetes;
pub mod permissions;
pub mod utils;

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct JwtAdditionalClaims {
    pub user_group: String,
    pub permission_set: permissions::PermissionSet,
    pub email: Option<String>,
    pub is_totp_enabled: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_mock_user: Option<bool>,
}
