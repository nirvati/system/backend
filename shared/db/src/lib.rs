// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#[cfg(feature = "migrations")]
pub mod migrations;
pub mod schema;

pub use sea_orm;

pub use schema::prelude::*;
