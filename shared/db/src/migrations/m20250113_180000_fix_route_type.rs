use sea_orm::prelude::*;
use sea_orm_migration::async_trait::async_trait;
use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let sql_query = r#"ALTER TABLE "Route" ALTER COLUMN "id" TYPE UUID USING "id"::UUID;"#;
        let db = manager.get_connection();
        db.execute_unprepared(sql_query).await?;
        Ok(())
    }

    async fn down(&self, _manager: &SchemaManager) -> Result<(), DbErr> {
        // Let's just skip this for now
        Ok(())
    }
}
