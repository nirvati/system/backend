use sea_orm::prelude::*;
use sea_orm_migration::async_trait::async_trait;
use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let do_init_query =
            include_str!("../../prisma/migrations/20241222152109_init/migration.sql");

        let sql_query = format!(
            r#"DO
$do$
BEGIN
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '_prisma_migrations') THEN
    IF EXISTS (SELECT * FROM _prisma_migrations) THEN
        IF EXISTS (SELECT * FROM _prisma_migrations WHERE migration_name = '20241205091519_keep_component_id_for_domains') THEN
            DROP TABLE _prisma_migrations;
            DROP TABLE "_AcmeAccountToDomain";
        ELSE
            RAISE EXCEPTION 'Trying to migrate an old prisma database - please reset your database';
        END IF;
    ELSE
        {do_init_query}
        DROP TABLE _prisma_migrations;
        IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '_AcmeAccountToDomain') THEN
            DROP TABLE "_AcmeAccountToDomain";
        END IF;
    END IF;
ELSE
    {do_init_query}
END IF;
END
$do$"#
        );
        let db = manager.get_connection();
        db.execute_unprepared(&sql_query).await?;
        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let sql_query = r#"
        -- RemoveForeignKey
ALTER TABLE "_AcmeAccountToDomain" DROP CONSTRAINT "_AcmeAccountToDomain_B_fkey";
ALTER TABLE "_AcmeAccountToDomain" DROP CONSTRAINT "_AcmeAccountToDomain_A_fkey";
ALTER TABLE "Snapshot" DROP CONSTRAINT "Snapshot_appInstallationId_fkey";
ALTER TABLE "Service" DROP CONSTRAINT "Service_ownerName_fkey";
ALTER TABLE "Route" DROP CONSTRAINT "Route_serviceId_fkey";
ALTER TABLE "Route" DROP CONSTRAINT "Route_proxyDomain_fkey";
ALTER TABLE "Proxy" DROP CONSTRAINT "Proxy_parentDomainName_fkey";
ALTER TABLE "Proxy" DROP CONSTRAINT "Proxy_acmeAccountId_fkey";
ALTER TABLE "Proxy" DROP CONSTRAINT "Proxy_ownerName_fkey";
ALTER TABLE "TcpProxy" DROP CONSTRAINT "TcpProxy_serviceId_fkey";
ALTER TABLE "TcpProxy" DROP CONSTRAINT "TcpProxy_ownerName_fkey";
ALTER TABLE "AppInstallation" DROP CONSTRAINT "AppInstallation_ownerName_fkey";
ALTER TABLE "AppDomain" DROP CONSTRAINT "AppDomain_acmeAccountId_fkey";
ALTER TABLE "AppDomain" DROP CONSTRAINT "AppDomain_appInstallationId_fkey";
ALTER TABLE "AppDomain" DROP CONSTRAINT "AppDomain_parentDomainName_fkey";
ALTER TABLE "Domain" DROP CONSTRAINT "Domain_ownerName_fkey";
ALTER TABLE "User" DROP CONSTRAINT "User_userGroupId_fkey";
ALTER TABLE "Passkey" DROP CONSTRAINT "Passkey_ownerName_fkey";
ALTER TABLE "AcmeAccount" DROP CONSTRAINT "AcmeAccount_ownerName_fkey";

-- DropIndex
DROP INDEX "_AcmeAccountToDomain_B_index";
DROP INDEX "_AcmeAccountToDomain_AB_unique";
DROP INDEX "Snapshot_id_key";
DROP INDEX "Service_upstream_ownerName_key";
DROP INDEX "Service_id_key";
DROP INDEX "Route_id_key";
DROP INDEX "Proxy_domain_key";
DROP INDEX "TcpProxy_domain_key";
DROP INDEX "UserGroup_name_key";
DROP INDEX "AppInstallation_app_id_ownerName_key";
DROP INDEX "AppInstallation_id_key";
DROP INDEX "AppDomain_name_key";
DROP INDEX "AppDomain_id_key";
DROP INDEX "Domain_name_ownerName_key";
DROP INDEX "Domain_id_key";
DROP INDEX "User_username_key";
DROP INDEX "Passkey_credentialId_key";
DROP INDEX "Passkey_id_key";
DROP INDEX "AcmeAccount_provider_ownerName_key";
DROP INDEX "AcmeAccount_id_key";

-- DropTable
DROP TABLE "_AcmeAccountToDomain";
DROP TABLE "Snapshot";
DROP TABLE "Service";
DROP TABLE "Route";
DROP TABLE "Proxy";
DROP TABLE "TcpProxy";
DROP TABLE "UserGroup";
DROP TABLE "AppInstallation";
DROP TABLE "AppDomain";
DROP TABLE "Domain";
DROP TABLE "User";
DROP TABLE "Passkey";
DROP TABLE "AcmeAccount";

-- DropEnum
DROP TYPE "UpstreamType";
DROP TYPE "DomainType";
DROP TYPE "Protocol";
DROP TYPE "DNSProvider";
DROP TYPE "AcmeProvider";
"#;
        let db = manager.get_connection();
        db.execute_unprepared(sql_query).await?;
        Ok(())
    }
}
