use sea_orm::prelude::*;
use sea_orm_migration::async_trait::async_trait;
use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let sql_query = r#"DO $$
BEGIN
IF EXISTS (SELECT FROM information_schema.tables
               WHERE table_name = '_AcmeAccountToDomain') THEN
DROP TABLE "_AcmeAccountToDomain" CASCADE;
END IF;
END $$;

ALTER TABLE "Passkey" ALTER COLUMN "id" TYPE UUID USING id::UUID, ALTER COLUMN "id" SET DEFAULT gen_random_uuid();
ALTER TABLE "AppDomain" ALTER COLUMN "id" TYPE UUID USING id::UUID, ALTER COLUMN "id" SET DEFAULT gen_random_uuid();
ALTER TABLE "Snapshot" ALTER COLUMN "id" TYPE UUID USING id::UUID, ALTER COLUMN "id" SET DEFAULT gen_random_uuid();

ALTER TABLE "AppDomain" DROP CONSTRAINT "AppDomain_acmeAccountId_fkey";
ALTER TABLE "Proxy" DROP CONSTRAINT "Proxy_acmeAccountId_fkey";
ALTER TABLE "Proxy" DROP CONSTRAINT "Proxy_parentDomainName_fkey";

ALTER TABLE "Route" DROP CONSTRAINT "Route_proxyDomain_fkey";
ALTER TABLE "Route" DROP CONSTRAINT "Route_serviceId_fkey";
ALTER TABLE "TcpProxy" DROP CONSTRAINT "TcpProxy_serviceId_fkey";
ALTER TABLE "AppDomain" DROP CONSTRAINT "AppDomain_appInstallationId_fkey";
ALTER TABLE "AppDomain" DROP CONSTRAINT "AppDomain_parentDomainName_fkey";
ALTER TABLE "Snapshot" DROP CONSTRAINT "Snapshot_appInstallationId_fkey";

ALTER TABLE "AcmeAccount" ALTER COLUMN "id" TYPE UUID USING id::UUID, ALTER COLUMN "id" SET DEFAULT gen_random_uuid();
ALTER TABLE "Domain" ALTER COLUMN "id" TYPE UUID USING id::UUID, ALTER COLUMN "id" SET DEFAULT gen_random_uuid();
ALTER TABLE "AppInstallation" ALTER COLUMN "id" TYPE UUID USING id::UUID, ALTER COLUMN "id" SET DEFAULT gen_random_uuid();
ALTER TABLE "Service" ALTER COLUMN "id" TYPE UUID USING id::UUID, ALTER COLUMN "id" SET DEFAULT gen_random_uuid();
ALTER TABLE "AppDomain" ALTER COLUMN "acmeAccountId" TYPE UUID USING "acmeAccountId"::UUID;
ALTER TABLE "Proxy" ALTER COLUMN "acmeAccountId" TYPE UUID USING "acmeAccountId"::UUID;
ALTER TABLE "AppDomain" ALTER COLUMN "appInstallationId" TYPE UUID USING "appInstallationId"::UUID;
ALTER TABLE "AppDomain" ALTER COLUMN "parentDomainName" TYPE UUID USING "parentDomainName"::UUID;
ALTER TABLE "Snapshot" ALTER COLUMN "appInstallationId" TYPE UUID USING "appInstallationId"::UUID;
ALTER TABLE "Proxy" ALTER COLUMN "parentDomainName" TYPE UUID USING "parentDomainName"::UUID;
ALTER TABLE "Route" ALTER COLUMN "serviceId" TYPE UUID USING "serviceId"::UUID;
ALTER TABLE "TcpProxy" ALTER COLUMN "serviceId" TYPE UUID USING "serviceId"::UUID;

ALTER TABLE "AppDomain" ADD CONSTRAINT "AppDomain_acmeAccountId_fkey" FOREIGN KEY ("acmeAccountId") REFERENCES "AcmeAccount"("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "Proxy" ADD CONSTRAINT "Proxy_acmeAccountId_fkey" FOREIGN KEY ("acmeAccountId") REFERENCES "AcmeAccount"("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "Route" ADD CONSTRAINT "Route_proxyDomain_fkey" FOREIGN KEY ("proxyDomain") REFERENCES "Proxy"("domain") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "Route" ADD CONSTRAINT "Route_serviceId_fkey" FOREIGN KEY ("serviceId") REFERENCES "Service"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE "TcpProxy" ADD CONSTRAINT "TcpProxy_serviceId_fkey" FOREIGN KEY ("serviceId") REFERENCES "Service"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE "AppDomain" ADD CONSTRAINT "AppDomain_appInstallationId_fkey" FOREIGN KEY ("appInstallationId") REFERENCES "AppInstallation"("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "Snapshot" ADD CONSTRAINT "Snapshot_appInstallationId_fkey" FOREIGN KEY ("appInstallationId") REFERENCES "AppInstallation"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE "AppDomain" ADD CONSTRAINT "AppDomain_parentDomainName_fkey" FOREIGN KEY ("parentDomainName") REFERENCES "Domain"("id") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "Proxy" ADD CONSTRAINT "Proxy_parentDomainName_fkey" FOREIGN KEY ("parentDomainName") REFERENCES "Domain"("id") ON DELETE SET NULL ON UPDATE CASCADE;
"#;
        let db = manager.get_connection();
        db.execute_unprepared(sql_query).await?;
        Ok(())
    }

    async fn down(&self, _manager: &SchemaManager) -> Result<(), DbErr> {
        // Let's just skip this for now
        Ok(())
    }
}
