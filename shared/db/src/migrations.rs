use sea_orm_migration::prelude::*;

pub use sea_orm_migration::MigratorTrait;

mod m20241226_180000_init;
mod m20241228_180000_post_prisma_optimizations;
mod m20250102_180000_rename_enums;
mod m20250113_180000_fix_route_type;

pub struct Migrator;

impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20241226_180000_init::Migration),
            Box::new(m20241228_180000_post_prisma_optimizations::Migration),
            Box::new(m20250102_180000_rename_enums::Migration),
            Box::new(m20250113_180000_fix_route_type::Migration),
        ]
    }
}
