//! `SeaORM` Entity, @generated by sea-orm-codegen 1.1.0

use super::sea_orm_active_enums::DomainType;
use sea_orm::entity::prelude::*;

#[derive(Clone, Debug, PartialEq, DeriveEntityModel, Eq)]
#[sea_orm(table_name = "AppDomain")]
pub struct Model {
    #[sea_orm(column_type = "Text", unique)]
    pub name: String,
    #[sea_orm(column_name = "createdAt")]
    pub created_at: DateTime,
    #[sea_orm(column_name = "parentDomainName")]
    pub parent_domain_name: Option<Uuid>,
    #[sea_orm(primary_key, auto_increment = false)]
    pub id: Uuid,
    #[sea_orm(column_name = "appInstallationId")]
    pub app_installation_id: Uuid,
    #[sea_orm(column_name = "acmeAccountId")]
    pub acme_account_id: Option<Uuid>,
    #[sea_orm(column_name = "customPrefix", column_type = "Text", nullable)]
    pub custom_prefix: Option<String>,
    pub r#type: DomainType,
    #[sea_orm(column_name = "appComponentId", column_type = "Text", nullable)]
    pub app_component_id: Option<String>,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "super::acme_account::Entity",
        from = "Column::AcmeAccountId",
        to = "super::acme_account::Column::Id",
        on_update = "Cascade",
        on_delete = "SetNull"
    )]
    AcmeAccount,
    #[sea_orm(
        belongs_to = "super::app_installation::Entity",
        from = "Column::AppInstallationId",
        to = "super::app_installation::Column::Id",
        on_update = "Cascade",
        on_delete = "Cascade"
    )]
    AppInstallation,
    #[sea_orm(
        belongs_to = "super::domain::Entity",
        from = "Column::ParentDomainName",
        to = "super::domain::Column::Id",
        on_update = "Cascade",
        on_delete = "SetNull"
    )]
    Domain,
}

impl Related<super::acme_account::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::AcmeAccount.def()
    }
}

impl Related<super::app_installation::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::AppInstallation.def()
    }
}

impl Related<super::domain::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Domain.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}
