-- Enable pg_trgm
CREATE EXTENSION IF NOT EXISTS pg_trgm;

-- CreateEnum
CREATE TYPE "AcmeProvider" AS ENUM ('LETS_ENCRYPT', 'BUY_PASS');

-- CreateEnum
CREATE TYPE "DNSProvider" AS ENUM ('CLOUDFLARE', 'NIRVATI_ME', 'NONE');

-- CreateEnum
CREATE TYPE "Protocol" AS ENUM ('HTTP', 'HTTPS');

-- CreateEnum
CREATE TYPE "DomainType" AS ENUM ('HTTP', 'HTTPS', 'ONION', 'TCP');

-- CreateEnum
CREATE TYPE "UpstreamType" AS ENUM ('IP', 'DNS');

-- CreateTable
CREATE TABLE "AcmeAccount" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "shareEmail" BOOLEAN NOT NULL DEFAULT false,
    "provider" "AcmeProvider" NOT NULL,
    "ownerName" TEXT NOT NULL,

    CONSTRAINT "AcmeAccount_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Passkey" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "ownerName" TEXT NOT NULL,
    "credentialId" BYTEA NOT NULL,
    "publicKey" BYTEA NOT NULL,
    "displayName" TEXT NOT NULL,
    "rpId" TEXT NOT NULL,
    "signCount" BIGINT NOT NULL,

    CONSTRAINT "Passkey_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "User" (
    "username" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "confirmedEmail" TEXT,
    "email" TEXT,
    "password" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "isEnabled" BOOLEAN NOT NULL DEFAULT false,
    "userGroupId" TEXT NOT NULL,
    "setupFinished" BOOLEAN NOT NULL DEFAULT false,
    "permissions" INTEGER NOT NULL,
    "isPublic" BOOLEAN NOT NULL DEFAULT true,
    "avatar" BYTEA,
    "totpSecret" BYTEA,

    CONSTRAINT "User_pkey" PRIMARY KEY ("username")
);

-- CreateTable
CREATE TABLE "Domain" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "ownerName" TEXT NOT NULL,
    "provider" "DNSProvider" NOT NULL,
    "providerAuth" TEXT,

    CONSTRAINT "Domain_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "AppDomain" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "parentDomainName" TEXT,
    "appInstallationId" TEXT NOT NULL,
    "appComponentId" TEXT,
    "acmeAccountId" TEXT,
    "customPrefix" TEXT,
    "type" "DomainType" NOT NULL,

    CONSTRAINT "AppDomain_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "AppInstallation" (
    "id" TEXT NOT NULL,
    "app_id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "ownerName" TEXT NOT NULL,
    "settings" JSONB NOT NULL,
    "protected" BOOLEAN NOT NULL DEFAULT false,
    "paused" BOOLEAN NOT NULL DEFAULT false,
    "initialDomain" TEXT,
    "currentChartVersion" TEXT NOT NULL,

    CONSTRAINT "AppInstallation_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "UserGroup" (
    "name" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "UserGroup_pkey" PRIMARY KEY ("name")
);

-- CreateTable
CREATE TABLE "TcpProxy" (
    "domain" TEXT NOT NULL,
    "ownerName" TEXT NOT NULL,
    "serviceId" TEXT NOT NULL,

    CONSTRAINT "TcpProxy_pkey" PRIMARY KEY ("domain")
);

-- CreateTable
CREATE TABLE "Proxy" (
    "domain" TEXT NOT NULL,
    "ownerName" TEXT NOT NULL,
    "acmeAccountId" TEXT,
    "parentDomainName" TEXT,

    CONSTRAINT "Proxy_pkey" PRIMARY KEY ("domain")
);

-- CreateTable
CREATE TABLE "Route" (
    "id" TEXT NOT NULL,
    "proxyDomain" TEXT NOT NULL,
    "enableCompression" BOOLEAN NOT NULL,
    "localPath" TEXT NOT NULL,
    "targetPath" TEXT NOT NULL,
    "serviceId" TEXT NOT NULL,
    "targetPort" INTEGER NOT NULL,
    "targetProtocol" "Protocol" NOT NULL,
    "targetSni" TEXT,
    "allowInvalidCert" BOOLEAN NOT NULL,

    CONSTRAINT "Route_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Service" (
    "id" TEXT NOT NULL,
    "upstream" TEXT NOT NULL,
    "type" "UpstreamType" NOT NULL,
    "tcpPorts" INTEGER[],
    "udpPorts" INTEGER[],
    "ownerName" TEXT NOT NULL,

    CONSTRAINT "Service_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Snapshot" (
    "id" TEXT NOT NULL,
    "appInstallationId" TEXT NOT NULL,
    "volumeSnapshots" JSONB NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "reason" TEXT NOT NULL,
    "appChart" BYTEA NOT NULL,

    CONSTRAINT "Snapshot_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_AcmeAccountToDomain" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "AcmeAccount_id_key" ON "AcmeAccount"("id");

-- CreateIndex
CREATE UNIQUE INDEX "AcmeAccount_provider_ownerName_key" ON "AcmeAccount"("provider", "ownerName");

-- CreateIndex
CREATE UNIQUE INDEX "Passkey_id_key" ON "Passkey"("id");

-- CreateIndex
CREATE UNIQUE INDEX "Passkey_credentialId_key" ON "Passkey"("credentialId");

-- CreateIndex
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");

-- CreateIndex
CREATE UNIQUE INDEX "Domain_id_key" ON "Domain"("id");

-- CreateIndex
CREATE UNIQUE INDEX "Domain_name_ownerName_key" ON "Domain"("name", "ownerName");

-- CreateIndex
CREATE UNIQUE INDEX "AppDomain_id_key" ON "AppDomain"("id");

-- CreateIndex
CREATE UNIQUE INDEX "AppDomain_name_key" ON "AppDomain"("name");

-- CreateIndex
CREATE UNIQUE INDEX "AppInstallation_id_key" ON "AppInstallation"("id");

-- CreateIndex
CREATE UNIQUE INDEX "AppInstallation_app_id_ownerName_key" ON "AppInstallation"("app_id", "ownerName");

-- CreateIndex
CREATE UNIQUE INDEX "UserGroup_name_key" ON "UserGroup"("name");

-- CreateIndex
CREATE UNIQUE INDEX "TcpProxy_domain_key" ON "TcpProxy"("domain");

-- CreateIndex
CREATE UNIQUE INDEX "Proxy_domain_key" ON "Proxy"("domain");

-- CreateIndex
CREATE UNIQUE INDEX "Route_id_key" ON "Route"("id");

-- CreateIndex
CREATE UNIQUE INDEX "Service_id_key" ON "Service"("id");

-- CreateIndex
CREATE UNIQUE INDEX "Service_upstream_ownerName_key" ON "Service"("upstream", "ownerName");

-- CreateIndex
CREATE UNIQUE INDEX "Snapshot_id_key" ON "Snapshot"("id");

-- CreateIndex
CREATE UNIQUE INDEX "_AcmeAccountToDomain_AB_unique" ON "_AcmeAccountToDomain"("A", "B");

-- CreateIndex
CREATE INDEX "_AcmeAccountToDomain_B_index" ON "_AcmeAccountToDomain"("B");

-- AddForeignKey
ALTER TABLE "AcmeAccount" ADD CONSTRAINT "AcmeAccount_ownerName_fkey" FOREIGN KEY ("ownerName") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Passkey" ADD CONSTRAINT "Passkey_ownerName_fkey" FOREIGN KEY ("ownerName") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "User" ADD CONSTRAINT "User_userGroupId_fkey" FOREIGN KEY ("userGroupId") REFERENCES "UserGroup"("name") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Domain" ADD CONSTRAINT "Domain_ownerName_fkey" FOREIGN KEY ("ownerName") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "AppDomain" ADD CONSTRAINT "AppDomain_parentDomainName_fkey" FOREIGN KEY ("parentDomainName") REFERENCES "Domain"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "AppDomain" ADD CONSTRAINT "AppDomain_appInstallationId_fkey" FOREIGN KEY ("appInstallationId") REFERENCES "AppInstallation"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "AppDomain" ADD CONSTRAINT "AppDomain_acmeAccountId_fkey" FOREIGN KEY ("acmeAccountId") REFERENCES "AcmeAccount"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "AppInstallation" ADD CONSTRAINT "AppInstallation_ownerName_fkey" FOREIGN KEY ("ownerName") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TcpProxy" ADD CONSTRAINT "TcpProxy_ownerName_fkey" FOREIGN KEY ("ownerName") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TcpProxy" ADD CONSTRAINT "TcpProxy_serviceId_fkey" FOREIGN KEY ("serviceId") REFERENCES "Service"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Proxy" ADD CONSTRAINT "Proxy_ownerName_fkey" FOREIGN KEY ("ownerName") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Proxy" ADD CONSTRAINT "Proxy_acmeAccountId_fkey" FOREIGN KEY ("acmeAccountId") REFERENCES "AcmeAccount"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Proxy" ADD CONSTRAINT "Proxy_parentDomainName_fkey" FOREIGN KEY ("parentDomainName") REFERENCES "Domain"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Route" ADD CONSTRAINT "Route_proxyDomain_fkey" FOREIGN KEY ("proxyDomain") REFERENCES "Proxy"("domain") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Route" ADD CONSTRAINT "Route_serviceId_fkey" FOREIGN KEY ("serviceId") REFERENCES "Service"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Service" ADD CONSTRAINT "Service_ownerName_fkey" FOREIGN KEY ("ownerName") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Snapshot" ADD CONSTRAINT "Snapshot_appInstallationId_fkey" FOREIGN KEY ("appInstallationId") REFERENCES "AppInstallation"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_AcmeAccountToDomain" ADD CONSTRAINT "_AcmeAccountToDomain_A_fkey" FOREIGN KEY ("A") REFERENCES "AcmeAccount"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_AcmeAccountToDomain" ADD CONSTRAINT "_AcmeAccountToDomain_B_fkey" FOREIGN KEY ("B") REFERENCES "Domain"("id") ON DELETE CASCADE ON UPDATE CASCADE;
