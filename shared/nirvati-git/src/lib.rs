// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::{HashMap, VecDeque};
use std::path::Path;

use anyhow::{anyhow, bail, Result};
use gix::bstr::{BStr, BString, ByteSlice, ByteVec};
use gix::{oid, prepare_clone, ObjectId};
use gix_diff::tree::{visit, Visit};
use gix_object::{Find, TreeRefIter};
use gix_refspec::parse::Operation;

pub fn clone(repo_url: gix::Url, branch: &str, target: &Path) -> Result<()> {
    tracing::debug!("Cloning {repo_url:?} into {target:?}...");
    let prepare_clone = prepare_clone(repo_url, target)?;
    let rspec = format!("refs/heads/{}", branch);
    let target_branch_refspec = gix_refspec::parse(BStr::new(&rspec), Operation::Fetch)?.to_owned();
    let (mut prepare_checkout, _) = prepare_clone
        .with_ref_name(Some(branch))?
        .with_fetch_options(gix::remote::ref_map::Options {
            extra_refspecs: vec![target_branch_refspec],
            ..Default::default()
        })
        .fetch_then_checkout(gix::progress::Discard, &gix::interrupt::IS_INTERRUPTED)?;
    let _ =
        prepare_checkout.main_worktree(gix::progress::Discard, &gix::interrupt::IS_INTERRUPTED)?;
    Ok(())
}

/// Gets the currently checked out commit from a repo path
pub fn get_commit(repo_path: &Path) -> Result<String> {
    let repo = gix::discover(repo_path)?;
    let id = repo.head_commit()?.id().to_string();
    Ok(id)
}

fn locate_tree_by_commit<'a>(
    db: &gix::OdbHandle,
    commit: &oid,
    buf: &'a mut Vec<u8>,
) -> Result<TreeRefIter<'a>, gix_object::find::Error> {
    let tree_id = db
        .try_find(commit, buf)?
        .ok_or_else(|| anyhow!("start commit {commit:?} to be present"))?
        .decode()?
        .into_commit()
        .expect("id is actually a commit")
        .tree();

    Ok(db
        .try_find(&tree_id, buf)?
        .expect("main tree present")
        .try_into_tree_iter()
        .expect("id to be a tree"))
}

struct MiniRecorder<'a> {
    app_subdir: &'a str,
    pub changed_apps: &'a mut HashMap<String, String>,
    pub commit_id: &'a str,
    pub valid_apps: &'a [String],
    path_deque: VecDeque<BString>,
    path: BString,
}
/// Access
impl MiniRecorder<'_> {
    pub fn new<'a>(
        app_subdir: &'a str,
        changed_apps: &'a mut HashMap<String, String>,
        commit_id: &'a str,
        valid_apps: &'a [String],
    ) -> MiniRecorder<'a> {
        MiniRecorder {
            app_subdir,
            changed_apps,
            path_deque: VecDeque::new(),
            path: Default::default(),
            commit_id,
            valid_apps,
        }
    }

    /// Return the currently set path.
    pub fn path(&self) -> &BStr {
        self.path.as_ref()
    }

    fn pop_element(&mut self) {
        if let Some(pos) = self.path.rfind_byte(b'/') {
            self.path.resize(pos, 0);
        } else {
            self.path.clear();
        }
    }

    fn push_element(&mut self, name: &BStr) {
        if !self.path.is_empty() {
            self.path.push(b'/');
        }
        self.path.push_str(name);
    }
}

impl Visit for MiniRecorder<'_> {
    fn pop_front_tracked_path_and_set_current(&mut self) {
        self.path = self
            .path_deque
            .pop_front()
            .expect("every parent is set only once");
    }

    fn push_back_tracked_path_component(&mut self, component: &BStr) {
        self.push_element(component);
        self.path_deque.push_back(self.path.clone());
    }

    fn push_path_component(&mut self, component: &BStr) {
        self.push_element(component);
    }

    fn pop_path_component(&mut self) {
        self.pop_element();
    }

    fn visit(&mut self, _change: visit::Change) -> visit::Action {
        let path = self.path();
        if path.starts_with(self.app_subdir.as_bytes()) {
            if path.len() <= self.app_subdir.len() + 1 {
                return visit::Action::Continue;
            }
            let app_name = &mut path[(self.app_subdir.len() + 1)..].splitn(2, |c| *c == b'/');
            let app_name = app_name.next().expect("app name to be present");
            if !self
                .changed_apps
                .contains_key(std::str::from_utf8(app_name).unwrap())
            {
                self.changed_apps.insert(
                    std::str::from_utf8(app_name).unwrap().to_string(),
                    self.commit_id.to_string(),
                );
            }
        }
        if self.changed_apps.len() >= self.valid_apps.len() {
            return visit::Action::Cancel;
        }
        visit::Action::Continue
    }
}

pub fn diff_commits(
    db: &gix::OdbHandle,
    lhs: impl Into<Option<ObjectId>>,
    rhs: &oid,
    subdir: &str,
    changed_apps: &mut HashMap<String, String>,
    valid_apps: &[String],
    commit_id: &str,
) -> Result<()> {
    let mut buf = Vec::new();
    let lhs_tree = lhs
        .into()
        .and_then(|lhs| locate_tree_by_commit(db, &lhs, &mut buf).ok())
        .ok_or(anyhow!("Failed to locate lhs tree"))?;
    let mut buf2 = Vec::new();
    let rhs_tree = locate_tree_by_commit(db, rhs, &mut buf2)
        .map_err(|err| anyhow!("Failed to locate rhs tree: {:?}", err))?;
    let mut recorder = MiniRecorder::new(subdir, changed_apps, commit_id, valid_apps);
    gix_diff::tree(
        lhs_tree,
        rhs_tree,
        gix_diff::tree::State::default(),
        db,
        &mut recorder,
    )
    .or_else(|err| {
        if matches!(err, gix_diff::tree::Error::Cancelled) {
            return Ok(());
        }
        bail!("Failed to diff trees: {:?}", err)
    })?;
    Ok(())
}

pub fn get_latest_commit_for_apps(
    repo_path: &Path,
    app_subdir: &str,
    apps: &[String],
) -> Result<HashMap<String, String>> {
    let mut latest_commits = HashMap::new();
    let repo = gix::discover(repo_path)?;
    let revwalk = repo.rev_walk([repo.head_id()?]).all()?;
    let mut initial_commit_id = String::new();
    for commit in revwalk {
        let commit = commit?;
        // Ignore merge commits (2+ parents) because that's what 'git whatchanged' does.
        // Ignore commit with 0 parents (initial commit) because there's nothing to diff against
        let parents = commit.parent_ids().collect::<Vec<_>>();
        if parents.len() == 1 {
            let prev_commit = parents.into_iter().next().unwrap().detach();
            diff_commits(
                &repo.objects,
                Some(prev_commit),
                &commit.id,
                app_subdir,
                &mut latest_commits,
                apps,
                &commit.id().to_string(),
            )?;
        } else if parents.is_empty() {
            initial_commit_id = commit.id.to_string();
        }
    }
    for app in apps {
        if !latest_commits.contains_key(app) {
            latest_commits.insert(app.clone(), initial_commit_id.clone());
        }
    }
    Ok(latest_commits)
}
