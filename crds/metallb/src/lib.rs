#![allow(clippy::doc_lazy_continuation, unused_imports)]

/// # Kubernetes CRDs for Traefik v3.1.0
///
/// This library provides automatically generated types for the [Traefik v3.1.0 CRD definitions]. It is intended to be used with the [Kube-rs] library.
///
/// [Traefik v3.1.0 CRD definitions]: https://raw.githubusercontent.com/traefik/traefik/v3.1.0/integration/fixtures/k8s/01-traefik-crd.yml
/// [Kube-rs]: https://kube.rs/
pub mod bfdprofiles;
pub use bfdprofiles::*;
pub mod bgpadvertisements;
pub use bgpadvertisements::*;
pub mod bgppeers;
pub use bgppeers::*;
pub mod communities;
pub use communities::*;
pub mod ipaddresspools;
pub use ipaddresspools::*;
pub mod l2advertisements;
pub use l2advertisements::*;
pub mod servicel2statuses;
pub use servicel2statuses::*;
