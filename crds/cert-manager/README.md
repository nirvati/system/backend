# Kubernetes CRDs for cert-manager

This library provides automatically generated types for the [cert-manager 1.15.1 definitions]. It is intended to be used with the [Kube-rs] library.

[cert-manager 1.15.1 definitions]: https://github.com/cert-manager/cert-manager/releases/download/v1.15.1/cert-manager.crds.yaml
[Kube-rs]: https://kube.rs/