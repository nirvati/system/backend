// WARNING: generated by kopium - manual changes will be overwritten
// kopium command: kopium -f publications.yml --schema=derived --docs -b --derive=Default --derive=PartialEq --smart-derive-elision
// kopium version: 0.21.1

#[allow(unused_imports)]
mod prelude {
    pub use kube_derive::CustomResource;
    #[cfg(feature = "schemars")]
    pub use schemars::JsonSchema;
    pub use serde::{Deserialize, Serialize};
    pub use std::collections::BTreeMap;
    #[cfg(feature = "builder")]
    pub use typed_builder::TypedBuilder;
}
use self::prelude::*;

/// PublicationSpec defines the desired state of Publication
#[derive(CustomResource, Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
#[cfg_attr(feature = "builder", derive(TypedBuilder))]
#[cfg_attr(feature = "schemars", derive(JsonSchema))]
#[cfg_attr(not(feature = "schemars"), kube(schema = "disabled"))]
#[kube(
    group = "postgresql.cnpg.io",
    version = "v1",
    kind = "Publication",
    plural = "publications"
)]
#[kube(namespaced)]
#[kube(status = "PublicationStatus")]
#[kube(derive = "Default")]
#[kube(derive = "PartialEq")]
pub struct PublicationSpec {
    /// The name of the PostgreSQL cluster that identifies the "publisher"
    pub cluster: PublicationCluster,
    /// The name of the database where the publication will be installed in
    /// the "publisher" cluster
    pub dbname: String,
    /// The name of the publication inside PostgreSQL
    pub name: String,
    /// Publication parameters part of the `WITH` clause as expected by
    /// PostgreSQL `CREATE PUBLICATION` command
    #[serde(default, skip_serializing_if = "Option::is_none")]
    #[cfg_attr(feature = "builder", builder(default, setter(strip_option)))]
    pub parameters: Option<BTreeMap<String, String>>,
    /// The policy for end-of-life maintenance of this publication
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        rename = "publicationReclaimPolicy"
    )]
    #[cfg_attr(feature = "builder", builder(default, setter(strip_option)))]
    pub publication_reclaim_policy: Option<PublicationPublicationReclaimPolicy>,
    /// Target of the publication as expected by PostgreSQL `CREATE PUBLICATION` command
    pub target: PublicationTarget,
}

/// The name of the PostgreSQL cluster that identifies the "publisher"
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
#[cfg_attr(feature = "builder", derive(TypedBuilder))]
#[cfg_attr(feature = "schemars", derive(JsonSchema))]
pub struct PublicationCluster {
    /// Name of the referent.
    /// This field is effectively required, but due to backwards compatibility is
    /// allowed to be empty. Instances of this type with an empty value here are
    /// almost certainly wrong.
    /// More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
    #[serde(default, skip_serializing_if = "Option::is_none")]
    #[cfg_attr(feature = "builder", builder(default, setter(strip_option)))]
    pub name: Option<String>,
}

/// PublicationSpec defines the desired state of Publication
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[cfg_attr(feature = "schemars", derive(JsonSchema))]
pub enum PublicationPublicationReclaimPolicy {
    #[serde(rename = "delete")]
    Delete,
    #[serde(rename = "retain")]
    Retain,
}

/// Target of the publication as expected by PostgreSQL `CREATE PUBLICATION` command
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
#[cfg_attr(feature = "builder", derive(TypedBuilder))]
#[cfg_attr(feature = "schemars", derive(JsonSchema))]
pub struct PublicationTarget {
    /// Marks the publication as one that replicates changes for all tables
    /// in the database, including tables created in the future.
    /// Corresponding to `FOR ALL TABLES` in PostgreSQL.
    #[serde(default, skip_serializing_if = "Option::is_none", rename = "allTables")]
    #[cfg_attr(feature = "builder", builder(default, setter(strip_option)))]
    pub all_tables: Option<bool>,
    /// Just the following schema objects
    #[serde(default, skip_serializing_if = "Option::is_none")]
    #[cfg_attr(feature = "builder", builder(default, setter(strip_option)))]
    pub objects: Option<Vec<PublicationTargetObjects>>,
}

/// PublicationTargetObject is an object to publish
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
#[cfg_attr(feature = "builder", derive(TypedBuilder))]
#[cfg_attr(feature = "schemars", derive(JsonSchema))]
pub struct PublicationTargetObjects {
    /// Specifies a list of tables to add to the publication. Corresponding
    /// to `FOR TABLE` in PostgreSQL.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    #[cfg_attr(feature = "builder", builder(default, setter(strip_option)))]
    pub table: Option<PublicationTargetObjectsTable>,
    /// Marks the publication as one that replicates changes for all tables
    /// in the specified list of schemas, including tables created in the
    /// future. Corresponding to `FOR TABLES IN SCHEMA` in PostgreSQL.
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        rename = "tablesInSchema"
    )]
    #[cfg_attr(feature = "builder", builder(default, setter(strip_option)))]
    pub tables_in_schema: Option<String>,
}

/// Specifies a list of tables to add to the publication. Corresponding
/// to `FOR TABLE` in PostgreSQL.
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
#[cfg_attr(feature = "builder", derive(TypedBuilder))]
#[cfg_attr(feature = "schemars", derive(JsonSchema))]
pub struct PublicationTargetObjectsTable {
    /// The columns to publish
    #[serde(default, skip_serializing_if = "Option::is_none")]
    #[cfg_attr(feature = "builder", builder(default, setter(strip_option)))]
    pub columns: Option<Vec<String>>,
    /// The table name
    pub name: String,
    /// Whether to limit to the table only or include all its descendants
    #[serde(default, skip_serializing_if = "Option::is_none")]
    #[cfg_attr(feature = "builder", builder(default, setter(strip_option)))]
    pub only: Option<bool>,
    /// The schema name
    #[serde(default, skip_serializing_if = "Option::is_none")]
    #[cfg_attr(feature = "builder", builder(default, setter(strip_option)))]
    pub schema: Option<String>,
}

/// PublicationStatus defines the observed state of Publication
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
#[cfg_attr(feature = "builder", derive(TypedBuilder))]
#[cfg_attr(feature = "schemars", derive(JsonSchema))]
pub struct PublicationStatus {
    /// Applied is true if the publication was reconciled correctly
    #[serde(default, skip_serializing_if = "Option::is_none")]
    #[cfg_attr(feature = "builder", builder(default, setter(strip_option)))]
    pub applied: Option<bool>,
    /// Message is the reconciliation output message
    #[serde(default, skip_serializing_if = "Option::is_none")]
    #[cfg_attr(feature = "builder", builder(default, setter(strip_option)))]
    pub message: Option<String>,
    /// A sequence number representing the latest
    /// desired state that was synchronized
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        rename = "observedGeneration"
    )]
    #[cfg_attr(feature = "builder", builder(default, setter(strip_option)))]
    pub observed_generation: Option<i64>,
}
