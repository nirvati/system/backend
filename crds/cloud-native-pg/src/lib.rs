#![allow(clippy::doc_lazy_continuation, unused_imports)]

/// # Kubernetes CRDs for cloudnative-pg
///
/// This library provides automatically generated types for [CloudNativePG]. It is intended to be used with the [Kube-rs] library.
///
/// [CloudNativePG]: https://cloudnative-pg.io/
/// [Kube-rs]: https://kube.rs/
pub mod backups;
pub use backups::*;
pub mod clusterimagecatalogs;
pub use clusterimagecatalogs::*;
pub mod clusters;
pub use clusters::*;
pub mod databases;
pub use databases::*;
pub mod imagecatalogs;
pub use imagecatalogs::*;
pub mod poolers;
pub use poolers::*;
pub mod publications;
pub use publications::*;
pub mod scheduledbackups;
pub use scheduledbackups::*;
pub mod subscriptions;
pub use subscriptions::*;
