# Kubernetes CRDs for cloudnative-pg

This library provides automatically generated types for [CloudNativePG]. It is intended to be used with the [Kube-rs] library.

[CloudNativePG]: https://cloudnative-pg.io/
[Kube-rs]: https://kube.rs/