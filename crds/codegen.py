#!/usr/bin/env python3

import yaml
import requests
import tempfile
import subprocess
import os


def pascal_to_snake(s):
    return "".join(["_" + c.lower() if c.isupper() else c for c in s]).lstrip("_")


os.chdir(os.path.dirname(os.path.realpath(__file__)))

with open("crds.yaml", "r") as f:
    modules = yaml.safe_load(f)

# If arguments are provided, only generate the specified modules
module_args = os.sys.argv[1:]

for module, module_definition in modules.items():
    if module_args and module not in module_args:
        continue
    print("Generating module", module)
    os.chdir(f"./{module}")
    with open(f"README.md", "r") as f:
        rust_lib = "#![allow(clippy::doc_lazy_continuation, unused_imports)]\n\n" + "\n".join([f"/// {line.strip()}" for line in f.readlines()])

    crds = []

    for crd in module_definition["crds"]:
        r = requests.get(crd)
        r.raise_for_status()
        text = r.text
        if "inputReplacements" in module_definition:
            for original, replacement in module_definition["inputReplacements"].items():
                text = text.replace(original, replacement)
        crd = yaml.safe_load_all(text)
        crds.extend(list(crd))

    sub_suffix_code = {}

    for crd in crds:
        if not crd or crd["kind"] != "CustomResourceDefinition":
            continue
        file_name = crd["metadata"]["name"].removesuffix(
            f".{module_definition['suffix']}"
        ).replace("-", "_")
        full_name = file_name
        if "subSuffixes" in module_definition:
            for subSuffix in module_definition["subSuffixes"]:
                file_name = file_name.removesuffix(f".{subSuffix}")

        if "override" in module_definition and file_name in module_definition["override"]:
            rust_code = module_definition["override"][file_name]
        else:
            with tempfile.NamedTemporaryFile(mode="w") as f:
                yaml.dump(crd, f)
                tmp_file = f.name
                rust_code = subprocess.run(
                    [
                        "kopium",
                        "-f",
                        tmp_file,
                        "--schema=derived",
                        "--docs",
                        "-b",
                        "--derive=Default",
                        "--derive=PartialEq",
                        "--smart-derive-elision",
                    ],
                    capture_output=True,
                )
                if rust_code.returncode != 0:
                    print(rust_code.stderr.decode("utf-8"))
                    exit(1)
                rust_code = rust_code.stdout.decode("utf-8")

        rust_code = rust_code.replace(
            f"// kopium command: kopium -f {tmp_file} --schema=derived --docs -b --derive=Default --derive=PartialEq --smart-derive-elision",
            f"// kopium command: kopium -f {file_name}.yml --schema=derived --docs -b --derive=Default --derive=PartialEq --smart-derive-elision",
        )
        rust_code = "\n".join(
            [
                (
                    line.replace(
                        "#[builder(", '#[cfg_attr(feature = "builder", builder('
                    )
                    .strip()
                    .removesuffix("]")
                    + ")]"
                    if "#[builder(" in line
                    else line
                )
                for line in rust_code.split("\n")
            ]
        )
        rust_code = "\n".join(
            [
                (
                    line.replace(
                        ", TypedBuilder, Default, PartialEq, JsonSchema)]",
                        ', Default, PartialEq)]\n#[cfg_attr(feature = "builder", derive(TypedBuilder))]\n#[cfg_attr(feature = "schemars", derive(JsonSchema))]\n#[cfg_attr(not(feature = "schemars"), kube(schema="disabled"))]',
                    )
                    .replace(
                        ", TypedBuilder, PartialEq, JsonSchema)]",
                        ', PartialEq)]\n#[cfg_attr(feature = "builder", derive(TypedBuilder))]\n#[cfg_attr(feature = "schemars", derive(JsonSchema))]\n#[cfg_attr(not(feature = "schemars"), kube(schema="disabled"))]',
                    )
                    .replace(
                        ", Default, PartialEq, JsonSchema)]",
                        ', Default, PartialEq)]\n#[cfg_attr(feature = "schemars", derive(JsonSchema))]\n#[cfg_attr(not(feature = "schemars"), kube(schema="disabled"))]',
                    )
                    .replace(
                        ", PartialEq, JsonSchema)]",
                        ', PartialEq)]\n#[cfg_attr(feature = "schemars", derive(JsonSchema))]\n#[cfg_attr(not(feature = "schemars"), kube(schema="disabled"))]',
                    )
                    if line.startswith("#[derive(") and "CustomResource" in line
                    else (
                        line.replace(
                            ", TypedBuilder, Default, PartialEq, JsonSchema)]",
                            ', Default, PartialEq)]\n#[cfg_attr(feature = "builder", derive(TypedBuilder))]\n#[cfg_attr(feature = "schemars", derive(JsonSchema))]',
                        )
                        .replace(
                            ", TypedBuilder, PartialEq, JsonSchema)]",
                            ', PartialEq)]\n#[cfg_attr(feature = "builder", derive(TypedBuilder))]\n#[cfg_attr(feature = "schemars", derive(JsonSchema))]',
                        )
                        .replace(
                            ", Default, PartialEq, JsonSchema)]",
                            ', Default, PartialEq)]\n#[cfg_attr(feature = "schemars", derive(JsonSchema))]',
                        )
                        .replace(
                            ", PartialEq, JsonSchema)]",
                            ', PartialEq)]\n#[cfg_attr(feature = "schemars", derive(JsonSchema))]',
                        )
                        if line.startswith("#[derive(")
                        else line
                    )
                )
                for line in rust_code.split("\n")
            ]
        )
        rust_code = (
            rust_code.replace(
                "pub use typed_builder::TypedBuilder;",
                '#[cfg(feature = "builder")]\npub use typed_builder::TypedBuilder;',
            )
            .replace(
                "pub use schemars::JsonSchema;",
                '#[cfg(feature = "schemars")]\npub use schemars::JsonSchema;',
            )
            .replace(
                "pub use kube::CustomResource;", "pub use kube_derive::CustomResource;"
            )
            .replace(
                '#[cfg_attr(feature = "builder", derive(TypedBuilder))]\n#[cfg_attr(feature = "schemars", derive(JsonSchema))]\npub enum',
                '#[cfg_attr(feature = "schemars", derive(JsonSchema))]\npub enum',
            )
        )

        if "replacements" in module_definition:
            for original, replacement in module_definition["replacements"].items():
                rust_code = rust_code.replace(original, replacement)

        is_sub = False
        if (
            "subSuffixes" in module_definition
            and full_name.split(".")[-1] in module_definition["subSuffixes"]
        ):
            is_sub = True
            file_dir = f"./src/{full_name.split(".")[-1]}"
            rust_file = f"{file_dir}/{file_name}.rs"
            os.makedirs(file_dir, exist_ok=True)
            sub_suffix = full_name.split(".")[-1]
            if sub_suffix not in sub_suffix_code:
                sub_suffix_code[sub_suffix] = ""
            sub_suffix_code[sub_suffix] += f"pub mod {file_name};\n"
        else:
            rust_file = f"./src/{file_name}.rs"
        with open(rust_file, "w") as f:
            f.write(rust_code)
        subprocess.Popen(["rustfmt", rust_file])

        if not is_sub:
            # Check if any sub-element of module_definition["feature_gated_crds"] contains the file_name
            had_feature = False
            if "feature_gated_crds" in module_definition:
                for feature, crds in module_definition["feature_gated_crds"].items():
                    if file_name in crds:
                        rust_lib += f"\npub mod {file_name};\npub use {file_name}::*;"
                        had_feature = True
                        break
            if not had_feature:
                rust_lib += f"\npub mod {file_name};\npub use {file_name}::*;"

    if "subSuffixes" in module_definition:
        for subSuffix in module_definition["subSuffixes"]:
            rust_lib += f"\npub mod {subSuffix};\npub use {subSuffix}::*;"
            with open(f"./src/{subSuffix}/mod.rs", "w") as f:
                f.write(sub_suffix_code[subSuffix])
            subprocess.Popen(["rustfmt", f"./src/{subSuffix}/mod.rs"])

    with open("./src/lib.rs", "w") as f:
        f.write(rust_lib)
    subprocess.Popen(["rustfmt", "./src/lib.rs"])

    os.chdir("..")
