#![allow(clippy::doc_lazy_continuation, unused_imports)]

/// # Kubernetes CRDs for Cilium
///
/// This library provides automatically generated types for the various CRDs included in [Cilium]. It is intended to be used with the [Kube-rs] library.
///
/// [Cilium]: https://cilium.io/
/// [Kube-rs]: https://kube.rs/
pub mod ciliumclusterwideenvoyconfigs;
pub use ciliumclusterwideenvoyconfigs::*;
pub mod ciliumclusterwidenetworkpolicies;
pub use ciliumclusterwidenetworkpolicies::*;
pub mod ciliumegressgatewaypolicies;
pub use ciliumegressgatewaypolicies::*;
pub mod ciliumendpoints;
pub use ciliumendpoints::*;
pub mod ciliumenvoyconfigs;
pub use ciliumenvoyconfigs::*;
pub mod ciliumexternalworkloads;
pub use ciliumexternalworkloads::*;
pub mod ciliumidentities;
pub use ciliumidentities::*;
pub mod ciliumlocalredirectpolicies;
pub use ciliumlocalredirectpolicies::*;
pub mod ciliumnetworkpolicies;
pub use ciliumnetworkpolicies::*;
pub mod ciliumnodeconfigs;
pub use ciliumnodeconfigs::*;
pub mod ciliumnodes;
pub use ciliumnodes::*;
pub mod ciliumbgpadvertisements;
pub use ciliumbgpadvertisements::*;
pub mod ciliumbgpclusterconfigs;
pub use ciliumbgpclusterconfigs::*;
pub mod ciliumbgpnodeconfigoverrides;
pub use ciliumbgpnodeconfigoverrides::*;
pub mod ciliumbgpnodeconfigs;
pub use ciliumbgpnodeconfigs::*;
pub mod ciliumbgppeerconfigs;
pub use ciliumbgppeerconfigs::*;
pub mod ciliumbgppeeringpolicies;
pub use ciliumbgppeeringpolicies::*;
pub mod ciliumcidrgroups;
pub use ciliumcidrgroups::*;
pub mod ciliumendpointslices;
pub use ciliumendpointslices::*;
pub mod ciliuml2announcementpolicies;
pub use ciliuml2announcementpolicies::*;
pub mod ciliumloadbalancerippools;
pub use ciliumloadbalancerippools::*;
pub mod ciliumpodippools;
pub use ciliumpodippools::*;
