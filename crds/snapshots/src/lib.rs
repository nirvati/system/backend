#![allow(clippy::doc_lazy_continuation, unused_imports)]

/// # Kubernetes CRDs for Kubernetes snapshots
pub mod snapshot;
pub use snapshot::*;
pub mod groupsnapshot;
pub use groupsnapshot::*;
