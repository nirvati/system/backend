#![allow(clippy::doc_lazy_continuation, unused_imports)]

/// # Kubernetes CRDs for arti-controller
///
/// This library provides automatically generated types for [arti-controller]. It is intended to be used with the [Kube-rs] library.
///
/// [arti-controller]: https://arti-controller.nirvati.org
/// [Kube-rs]: https://kube.rs/
pub mod onionservices;
pub use onionservices::*;
