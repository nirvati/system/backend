use std::{
    collections::HashMap,
    io::{Read, Write},
    path::Path,
};

use rand::RngCore;
use tera::Tera;

use crate::{
    composegenerator::v4::{permissions::ALWAYS_ALLOWED_ENV_VARS, utils::derive_entropy},
    constants::NO_SEED_FOUND_FALLBACK_MSG,
};

use anyhow::{bail, Result};
use sha1::Digest;

// Creates a S2K hash like used by Tor
fn tor_hash(input: &str, salt: [u8; 8]) -> String {
    let mut bytes = Vec::new();
    bytes.extend_from_slice(&salt);
    bytes.extend_from_slice(input.as_bytes());
    let mut hash = sha1::Sha1::new();
    while bytes.len() < 0x10000 {
        bytes.extend_from_slice(&salt);
        bytes.extend_from_slice(input.as_bytes());
    }
    bytes.truncate(0x10000);
    hash.update(&bytes);
    let hash = hash.finalize();
    let mut hash_str = String::new();
    for byte in hash {
        hash_str.push_str(&format!("{byte:02X}"));
    }
    format!(
        "16:{}60{}",
        hex::encode(salt).to_uppercase(),
        hash_str.to_uppercase()
    )
}

fn random_hex_string(len: usize) -> String {
    let mut rng = rand::thread_rng();
    let mut bytes = vec![0u8; len];
    rng.fill_bytes(&mut bytes);
    hex::encode(bytes)
}

pub fn convert_app_yml(
    app_path: &Path,
    services: &[String],
    env_vars: &HashMap<String, String>,
    citadel_seed: &Option<String>,
) -> Result<()> {
    let app_yml_jinja = app_path.to_path_buf().join("app.yml.jinja");
    if app_yml_jinja.exists() {
        convert_app_yml_internal(
            &app_yml_jinja,
            app_path.file_name().unwrap().to_str().unwrap(),
            services,
            env_vars,
            citadel_seed.to_owned(),
        )?;
    }
    Ok(())
}

fn convert_app_yml_internal(
    jinja_file: &Path,
    app_id: &str,
    services: &[String],
    env_vars: &HashMap<String, String>,
    citadel_seed: Option<String>,
) -> Result<()> {
    let mut context = tera::Context::new();
    context.insert("services", services);
    context.insert("app_name", app_id);
    let mut tmpl = String::new();
    std::fs::File::open(jinja_file)?.read_to_string(&mut tmpl)?;
    let mut tera = Tera::default();
    let app_id = app_id.to_string();
    let app_id_clone = app_id.clone();
    for (key, val) in env_vars {
        // We can't know the permissions at this stage, so we only allow the env vars here that are always allowed
        if ALWAYS_ALLOWED_ENV_VARS.contains(&key.as_str()) {
            context.insert(key, &val);
        }
    }
    tera.register_function(
        "derive_entropy",
        move |args: &HashMap<String, serde_json::Value>| -> Result<tera::Value, tera::Error> {
            let identifier = if args.contains_key("id") {
                args.get("id")
            } else {
                args.get("identifier")
            };
            let Some(identifier) = identifier else {
                return Err(tera::Error::msg("Missing identifier"));
            };

            let Some(identifier) = identifier.as_str() else {
                return Err(tera::Error::msg("Identifier must be a string"));
            };

            if let Some(citadel_seed) = &citadel_seed {
                Ok(tera::to_value(derive_entropy(
                    citadel_seed,
                    format!("app-{}-{}", app_id.replace('-', "_"), identifier).as_str(),
                ))
                .expect("Failed to serialize value"))
            } else {
                Ok(tera::to_value(NO_SEED_FOUND_FALLBACK_MSG).expect("Failed to serialize value"))
            }
        },
    );
    tera.register_filter(
        "tor_hash",
        |val: &tera::Value,
         _args: &HashMap<String, tera::Value>|
         -> Result<tera::Value, tera::Error> {
            let Some(input) = val.as_str() else {
                return Err(tera::Error::msg("Identifier must be a string"));
            };
            let mut salt = [0u8; 8];
            rand::thread_rng().fill_bytes(&mut salt);
            Ok(tera::to_value(tor_hash(input, salt)).expect("Failed to serialize value"))
        },
    );
    tera.register_function(
        "gen_seed",
        |args: &HashMap<String, tera::Value>| -> Result<tera::Value, tera::Error> {
            let Some(len) = args.get("len") else {
                return Err(tera::Error::msg("Length must be defined"));
            };
            let Some(len) = len.as_u64() else {
                return Err(tera::Error::msg("Length must be a number"));
            };
            Ok(tera::to_value(random_hex_string(len as usize)).expect("Failed to serialize value"))
        },
    );
    let tmpl_result = tera.render_str(tmpl.as_str(), &context);
    if let Err(e) = tmpl_result {
        bail!("Error processing template {}: {}", jinja_file.display(), e);
    }
    let tmpl_result = tmpl_result.unwrap();
    if tmpl_result.contains(NO_SEED_FOUND_FALLBACK_MSG) {
        bail!(
            "App {} uses APP_SEED in a Jinja file, it can't be processed yet.",
            app_id_clone
        );
    }
    let mut writer = std::fs::File::create(jinja_file.to_path_buf().with_extension(""))?;
    writer.write_all(tmpl_result.as_bytes())?;
    Ok(())
}

#[cfg(test)]
mod test {
    use super::tor_hash;

    #[test]
    fn hash_matches_tor() {
        assert_eq!(
            tor_hash("test123", [0x3E, 0x6B, 0xF3, 0xDC, 0xEC, 0x50, 0xFE, 0x51]),
            "16:3E6BF3DCEC50FE5160DBD0C3A9132DB0118AFA5104FE8DA29ADC20A65E"
        );
    }
}
