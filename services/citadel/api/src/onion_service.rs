use k8s_crds_arti_controller::OnionService;
use kube::{Api, Client};

pub async fn get_onion_service(client: Client) -> anyhow::Result<String> {
    let api: Api<OnionService> = Api::namespaced(
        client,
        &format!(
            "{}-lnd",
            std::env::var("EXPECTED_USERNAME").expect("EXPECTED_USERNAME must be set")
        ),
    );
    let svc = api.get("p2p").await?;
    Ok(svc
        .status
        .ok_or(anyhow::anyhow!("Failed to get onion domain"))?
        .onion_name)
}

pub async fn save_onion_service(client: Client) -> anyhow::Result<()> {
    let service_name = get_onion_service(client).await?;
    let mut cfg = ini::Ini::load_from_file("/config/lnd.conf")?;
    let section = cfg
        .section_mut(Some("Application Options"))
        .ok_or(anyhow::anyhow!("Failed to get section"))?;
    section.append("externalip", service_name);
    cfg.write_to_file("/config/lnd.conf")?;
    Ok(())
}
