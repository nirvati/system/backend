// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{EmptySubscription, Schema};
use citadel_api::graphql::{Mutation, Query};
use corepc_client::client_sync::Auth;

fn main() {
    let schema = Schema::build(
        Query::new(
            "placeholder-url",
            Auth::UserPass("placeholder".to_string(), "placeholder".to_string()),
        ),
        Mutation::default(),
        EmptySubscription,
    )
    .finish();
    let spdx_note = "# SPDX-FileCopyrightText: 2024 The Nirvati Developers\n#\n# SPDX-License-Identifier: AGPL-3.0-or-later\n\n".to_string();
    std::fs::write("schema.gql", spdx_note + &schema.sdl()).expect("Failed to save schema");
}
