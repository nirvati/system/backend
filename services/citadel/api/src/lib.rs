// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

pub mod graphql;
pub mod onion_service;

pub async fn restart_deployment(client: kube::Client, deployment: &str) -> anyhow::Result<()> {
    let api: kube::Api<k8s_openapi::api::apps::v1::Deployment> = kube::Api::namespaced(
        client,
        &format!(
            "{}-lnd",
            std::env::var("EXPECTED_USERNAME").expect("EXPECTED_USERNAME must be set")
        ),
    );
    api.restart(deployment).await?;
    Ok(())
}

pub async fn restart_lnd(client: kube::Client) -> anyhow::Result<()> {
    restart_deployment(client, "main").await
}

pub async fn restart_plugin(client: kube::Client) -> anyhow::Result<()> {
    restart_deployment(client, "plugin").await
}
