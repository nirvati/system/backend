// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::sync::OnceLock;

use actix_web::{
    guard, http::header::HeaderMap, web, App, HttpRequest, HttpResponse, HttpServer, Responder,
};
use actix_web_prom::PrometheusMetricsBuilder;
use anyhow::{anyhow, bail};
use async_graphql::{EmptySubscription, Schema};
use async_graphql_actix_web::{GraphQLRequest, GraphQLResponse};
use base64::Engine;
use biscuit::jwa::SignatureAlgorithm;
use biscuit::jws::Secret;
use biscuit::{ClaimPresenceOptions, Presence, Validation, ValidationOptions, JWT};
use citadel_api::graphql::auth_ctx::UserCtx;
use citadel_api::graphql::{AppSchema, Mutation, Query};
use citadel_api::onion_service::save_onion_service;
use citadel_api::restart_lnd;
use corepc_client::client_sync::Auth;
use nirvati::JwtAdditionalClaims;
use rsa::pkcs1::EncodeRsaPublicKey;
use rsa::pkcs8::DecodePublicKey;

static SECRET: OnceLock<Secret> = OnceLock::new();

fn get_user_ctx_from_headers(headers: &HeaderMap) -> anyhow::Result<UserCtx> {
    let auth_header = headers
        .get("Authorization")
        .ok_or(anyhow!("Authorization header not found"))?
        .to_str()?;
    let auth_header = auth_header.split("Bearer ").collect::<Vec<_>>();
    if auth_header.len() != 2 {
        bail!("Invalid Authorization header");
    }
    let token = auth_header[1];
    let jwt_secret = SECRET.get().ok_or(anyhow!("JWT secret not set"))?;
    let token = JWT::<JwtAdditionalClaims, biscuit::Empty>::new_encoded(token);
    let options = ClaimPresenceOptions {
        expiry: Presence::Required,
        subject: Presence::Required,
        audience: Presence::Required,
        ..Default::default()
    };
    let token = token.into_decoded(jwt_secret, SignatureAlgorithm::RS512)?;
    token.validate(ValidationOptions {
        claim_presence_options: options,
        expiry: Validation::Validate(()),
        ..Default::default()
    })?;
    let (_, claims) = token.unwrap_decoded();
    let username = claims.registered.subject.unwrap();
    let expected_username =
        std::env::var("EXPECTED_USERNAME").expect("EXPECTED_USERNAME must be set");
    if username != expected_username {
        bail!(
            "Username mismatch: expected {}, got {}",
            expected_username,
            username
        );
    }
    let audience: Vec<_> = claims
        .registered
        .audience
        .unwrap()
        .iter()
        .cloned()
        .collect();
    if !audience.contains(&"app-citadel".to_string()) {
        bail!("Invalid token audience");
    }
    Ok(UserCtx {
        // Existence of subject is already validated
        username,
        permissions: claims.private.permission_set,
        user_group: claims.private.user_group,
        email: claims.private.email,
        audience,
    })
}

async fn options_main(_req: HttpRequest) -> impl Responder {
    HttpResponse::NoContent()
}

async fn graphql_main(
    schema: web::Data<AppSchema>,
    req: HttpRequest,
    gql_request: GraphQLRequest,
) -> GraphQLResponse {
    let mut request = gql_request.into_inner();
    match get_user_ctx_from_headers(req.headers()) {
        Ok(token) => request = request.data(token),
        Err(e) => {
            tracing::error!("Failed to get user context: {:#}", e);
        }
    }
    schema.execute(request).await.into()
}

/*async fn index_ws(
    schema: web::Data<AppSchema>,
    req: HttpRequest,
    payload: web::Payload,
) -> Result<HttpResponse> {
    GraphQLSubscription::new(Schema::clone(&*schema))
        .on_connection_init(on_connection_init)
        .start(&req, payload)
}*/

async fn health() -> HttpResponse {
    HttpResponse::Ok().finish()
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    tracing_subscriber::fmt::init();
    let rsa_key = std::env::var("RSA_KEY").expect("DATABASE_URL must be set");
    let bind_address = std::env::var("BIND_ADDRESS").expect("BIND_ADDRESS must be set");
    let bitcoin_rpc_url = std::env::var("BITCOIN_RPC_URL").expect("BITCOIN_RPC_URL must be set");
    let bitcoin_rpc_user = std::env::var("BITCOIN_RPC_USER").expect("BITCOIN_RPC_USER must be set");
    let bitcoin_rpc_password =
        std::env::var("BITCOIN_RPC_PASSWORD").expect("BITCOIN_RPC_PASSWORD must be set");

    let rsa_key = base64::engine::general_purpose::STANDARD
        .decode(rsa_key.as_bytes())
        .expect("Failed to decode RSA key");

    let parsed_key =
        rsa::RsaPublicKey::from_public_key_der(&rsa_key).expect("Failed to parse RSA key");

    let der = parsed_key.to_pkcs1_der().expect("Failed to encode RSA key");

    let secret = Secret::PublicKey(der.as_bytes().to_vec());

    if SECRET.set(secret.clone()).is_err() {
        panic!("Failed to set JWT secret");
    }

    let schema = Schema::build(
        Query::new(
            &bitcoin_rpc_url,
            Auth::UserPass(bitcoin_rpc_user, bitcoin_rpc_password),
        ),
        Mutation::default(),
        EmptySubscription,
    )
    .limit_recursive_depth(10)
    .data(secret)
    .finish();

    let prometheus = PrometheusMetricsBuilder::new("api")
        .endpoint("/metrics")
        .build()
        .unwrap();

    let kube_client = kube::Client::try_default()
        .await
        .expect("Failed to create kube client");

    save_onion_service(kube_client.clone())
        .await
        .expect("Failed to insert onion service into LND config");
    restart_lnd(kube_client)
        .await
        .expect("Failed to restart LND");

    HttpServer::new(move || {
        let app = App::new();
        app.wrap(prometheus.clone())
            .app_data(web::Data::new(schema.clone()))
            .service(
                web::resource("/v0/graphql")
                    .guard(guard::Post())
                    .to(graphql_main),
            )
            .service(
                web::resource("/v0/graphql")
                    .guard(guard::Options())
                    .to(options_main),
            )
            .service(web::resource("/health").to(health))
    })
    .bind(&bind_address)?
    .run()
    .await
}
