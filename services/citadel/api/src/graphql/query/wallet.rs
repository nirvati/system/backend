// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Object, Result};
use lnd_grpc_rust::lnrpc::GenSeedRequest;
use std::path::Path;

#[derive(Default)]
pub struct Wallet;

#[Object]
impl Wallet {
    pub async fn is_initialized(&self) -> Result<bool> {
        let path = "/data/data/chain/bitcoin/".to_owned()
            + &std::env::var("BTC_NETWORK").expect("Network not set!")
            + "/admin.macaroon";
        let path = Path::new(&path);
        Ok(path.exists())
    }

    pub async fn random_seed(&self) -> Result<Vec<String>> {
        let cert_bytes = std::fs::read("/data/tls.cert").expect("FailedToReadTlsCertFile");
        // Convert the bytes to a hex string
        let cert = hex::encode(&cert_bytes);
        let mut lnd_client =
            lnd_grpc_rust::connect_wallet_unlocker(cert.clone(), std::env::var("LND_URL").unwrap())
                .await
                .map_err(|e| async_graphql::Error::new(e.to_string()))?;
        let seed = lnd_client
            .gen_seed(GenSeedRequest {
                aezeed_passphrase: vec![],
                seed_entropy: vec![],
            })
            .await?;
        Ok(seed.into_inner().cipher_seed_mnemonic)
    }
}
