// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::graphql::auth_ctx::UserCtx;
use async_graphql::{Context, Error, Object, Result};

#[derive(Default)]
pub struct User;

#[Object]
impl User {
    pub async fn username(&self, ctx: &Context<'_>) -> Result<String> {
        let user_ctx = ctx
            .data::<UserCtx>()
            .map_err(|_| Error::from("No user in context"))?;
        Ok(user_ctx.username.clone())
    }

    pub async fn email(&self, ctx: &Context<'_>) -> Result<Option<String>> {
        let user_ctx = ctx
            .data::<UserCtx>()
            .map_err(|_| Error::from("No user in context"))?;
        Ok(user_ctx.email.clone())
    }
}
