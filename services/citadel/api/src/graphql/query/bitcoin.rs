use async_graphql::{Object, Result, SimpleObject};
use corepc_client::client_sync::v28::Client;
use corepc_client::client_sync::Auth;

#[derive(SimpleObject)]
pub struct SyncState {
    pub synced_to: u64,
    pub chain_tip: u64,
    pub verification_process: f64,
}

pub struct Bitcoin {
    client: Client,
}

impl Bitcoin {
    pub fn new(url: &str, auth: Auth) -> Self {
        Self {
            client: Client::new_with_auth(url, auth).expect("Failed to create client"),
        }
    }
}

#[Object]
impl Bitcoin {
    pub async fn sync_state(&self) -> Result<SyncState> {
        let blockchain_info = self.client.get_blockchain_info()?;
        Ok(SyncState {
            synced_to: blockchain_info.blocks as u64,
            chain_tip: blockchain_info.headers as u64,
            verification_process: blockchain_info.verification_progress,
        })
    }
}
