// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Error, Object, Result};
use k8s_openapi::serde_json;
use std::collections::HashMap;

#[derive(Default)]
pub struct Config;

#[Object]
impl Config {
    pub async fn raw(&self) -> Result<String> {
        let path = "/config/lnd.conf";
        let data = std::fs::read_to_string(path).map_err(|e| Error::from(e.to_string()))?;
        Ok(data)
    }

    pub async fn parsed(&self) -> Result<HashMap<String, HashMap<String, serde_json::Value>>> {
        let config =
            ini::Ini::load_from_file("/config/lnd.conf").map_err(|e| Error::from(e.to_string()))?;
        let mut result = HashMap::new();
        for (section_key, props) in config.into_iter() {
            let mut map: HashMap<String, serde_json::Value> = HashMap::new();
            for (prop_id, prop_val) in props.into_iter() {
                if let std::collections::hash_map::Entry::Vacant(e) = map.entry(prop_id.clone()) {
                    e.insert(serde_json::Value::String(prop_val));
                } else if let Some(existing_prop_val) = map.get_mut(&prop_id) {
                    if let Some(existing_prop_val_inner) = existing_prop_val.as_array_mut() {
                        existing_prop_val_inner.push(serde_json::Value::String(prop_val));
                    } else if let Some(existing_prop_val_inner) = existing_prop_val.as_str() {
                        *existing_prop_val = serde_json::Value::Array(vec![
                            serde_json::Value::String(existing_prop_val_inner.to_string()),
                            serde_json::Value::String(prop_val),
                        ]);
                    } else {
                        *existing_prop_val = serde_json::Value::Array(vec![
                            existing_prop_val.clone(),
                            serde_json::Value::String(prop_val),
                        ]);
                    }
                }
            }
            result.insert(
                section_key.unwrap_or_else(|| "__no_section".to_string()),
                map,
            );
        }
        Ok(result)
    }
}
