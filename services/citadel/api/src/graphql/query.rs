// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::graphql::auth_guard::IsLoggedInGuard;
use async_graphql::Object;
use corepc_client::client_sync::Auth;

mod bitcoin;
mod config;
mod user;
mod wallet;

pub struct Query {
    pub bitcoin: bitcoin::Bitcoin,
}

impl Query {
    pub fn new(url: &str, auth: Auth) -> Self {
        Self {
            bitcoin: bitcoin::Bitcoin::new(url, auth),
        }
    }
}

#[Object]
impl Query {
    #[graphql(guard = "IsLoggedInGuard::new()")]
    async fn user(&self) -> user::User {
        user::User
    }

    #[graphql(guard = "IsLoggedInGuard::new()")]
    async fn wallet(&self) -> wallet::Wallet {
        wallet::Wallet
    }

    #[graphql(guard = "IsLoggedInGuard::new()")]
    async fn config(&self) -> config::Config {
        config::Config
    }

    #[graphql(guard = "IsLoggedInGuard::new()")]
    async fn bitcoin(&self) -> &bitcoin::Bitcoin {
        &self.bitcoin
    }
}
