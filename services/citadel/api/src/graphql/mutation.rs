// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::MergedObject;

mod config;
mod manage;
mod wallet;

#[derive(MergedObject, Default)]
pub struct Mutation(wallet::Wallet, config::Config, manage::Manage);
