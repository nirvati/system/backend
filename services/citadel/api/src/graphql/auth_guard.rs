// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Context, Guard, Result};

use crate::graphql::auth_ctx::UserCtx;

pub struct IsLoggedInGuard;

impl IsLoggedInGuard {
    pub fn new() -> Self {
        Self {}
    }
}

impl Guard for IsLoggedInGuard {
    async fn check(&self, ctx: &Context<'_>) -> Result<()> {
        if ctx.data_opt::<UserCtx>().is_some() {
            Ok(())
        } else {
            Err("Forbidden".into())
        }
    }
}
