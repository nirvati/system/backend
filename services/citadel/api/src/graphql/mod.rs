// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{EmptySubscription, Schema};

pub use mutation::Mutation;
pub use query::Query;

pub mod auth_ctx;
mod auth_guard;
mod mutation;
pub(crate) mod query;

pub type AppSchema = Schema<Query, Mutation, EmptySubscription>;
