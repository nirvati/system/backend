// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use nirvati::permissions::PermissionSet;

pub struct UserCtx {
    pub username: String,
    pub permissions: PermissionSet,
    pub user_group: String,
    // Include this because it's needed in a few places, and we don't want to always get it from the DB
    pub email: Option<String>,
    pub audience: Vec<String>,
}
