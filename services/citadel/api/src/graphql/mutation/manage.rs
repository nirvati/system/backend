// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Error, Object, Result};

#[derive(Default)]
pub struct Manage;

#[Object(name = "ManageMutations")]
impl Manage {
    async fn restart_lnd(&self) -> Result<bool> {
        let client = kube::Client::try_default()
            .await
            .map_err(|e| Error::from(e.to_string()))?;
        crate::restart_lnd(client)
            .await
            .map_err(|e| Error::from(e.to_string()))?;
        Ok(true)
    }
}
