// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Error, Object, Result};

#[derive(Default)]
pub struct Config;

#[Object(name = "ConfigMutations")]
impl Config {
    async fn config_add(&self, section: String, key: String, value: String) -> Result<bool> {
        let mut config =
            ini::Ini::load_from_file("/config/lnd.conf").map_err(|e| Error::from(e.to_string()))?;
        let section = config
            .entry(Some(section))
            .or_insert_with(ini::Properties::new);
        section.append(key, value);
        config
            .write_to_file("/config/lnd.conf")
            .map_err(|e| Error::from(e.to_string()))?;
        Ok(true)
    }

    async fn config_set(&self, section: String, key: String, value: String) -> Result<bool> {
        let mut config =
            ini::Ini::load_from_file("/config/lnd.conf").map_err(|e| Error::from(e.to_string()))?;
        let section = config
            .entry(Some(section))
            .or_insert_with(ini::Properties::new);
        section.insert(key, value);
        config
            .write_to_file("/config/lnd.conf")
            .map_err(|e| Error::from(e.to_string()))?;
        Ok(true)
    }

    async fn config_remove(&self, section: String, key: String) -> Result<bool> {
        let mut config =
            ini::Ini::load_from_file("/config/lnd.conf").map_err(|e| Error::from(e.to_string()))?;
        if let Some(section) = config.section_mut(Some(section)) {
            section.remove(key);
        } else {
            return Err(Error::from("Section not found"));
        }
        config
            .write_to_file("/config/lnd.conf")
            .map_err(|e| Error::from(e.to_string()))?;
        Ok(true)
    }
}
