// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::restart_plugin;
use async_graphql::{Error, Object};
use lnd_grpc_rust::lnrpc::InitWalletRequest;

#[derive(Default)]
pub struct Wallet;

#[Object(name = "WalletMutations")]
impl Wallet {
    async fn init_wallet(&self, seed: Vec<String>) -> async_graphql::Result<bool> {
        let cert_bytes = std::fs::read("/data/tls.cert").expect("FailedToReadTlsCertFile");
        // Convert the bytes to a hex string
        let cert = hex::encode(&cert_bytes);
        let mut lnd_client =
            lnd_grpc_rust::connect_wallet_unlocker(cert.clone(), std::env::var("LND_URL").unwrap())
                .await
                .map_err(|e| async_graphql::Error::new(e.to_string()))?;
        lnd_client
            .init_wallet(InitWalletRequest {
                wallet_password: "freesoftware".as_bytes().to_vec(),
                cipher_seed_mnemonic: seed,
                aezeed_passphrase: vec![],
                recovery_window: 0,
                channel_backups: None,
                stateless_init: false,
                extended_master_key: "".to_string(),
                extended_master_key_birthday_timestamp: 0,
                watch_only: None,
                macaroon_root_key: vec![],
            })
            .await?;
        let client = kube::Client::try_default()
            .await
            .map_err(|e| Error::from(e.to_string()))?;
        restart_plugin(client)
            .await
            .map_err(|e| Error::from(e.to_string()))?;
        Ok(true)
    }
}
