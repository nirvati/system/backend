fn main() {
    let version = std::env::var("NIRVATI_VERSION").unwrap();
    let id = std::env::var("INSTALL_ID").unwrap();
    let json = serde_json::json!({
        "version": version,
        "id": id,
    });
    let client = reqwest::blocking::Client::new();
    let response = client
        .post("https://telemetry.nirvati.org/v0/ping")
        .json(&json)
        .send()
        .unwrap();
    let status = response.status();
    let body = response.text().unwrap();
    if status.is_success() {
        println!("Telemetry pinged successfully");
    } else {
        eprintln!("Failed to ping telemetry: {}", body);
    }
}
