// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    tracing::info!("No migrations to run");
}
