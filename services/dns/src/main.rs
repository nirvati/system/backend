use std::sync::OnceLock;

use anyhow::{anyhow, Result};
use cached::proc_macro::cached;
use dns_server::{DnsQuestion, DnsRecord, DnsType};
use itertools::Itertools;
use k8s_openapi::api::core::v1::Service;
use kube::{Api, Client};
use nirvati_db::schema::app_installation;
use nirvati_db::sea_orm::prelude::*;
use nirvati_db::sea_orm::{Database, DatabaseConnection};
use permit::Permit;
use signal_hook::consts::{SIGHUP, SIGINT, SIGQUIT, SIGTERM};
use signal_hook::iterator::Signals;

static DB_CLIENT: OnceLock<DatabaseConnection> = OnceLock::new();

#[cached(result = true, time = 60, sync_writes = true)]
fn load_registry(user: String) -> Result<Vec<nirvati_apps::metadata::Metadata>> {
    let registry_path = format!("/apps/{}/registry.json", user);
    let registry = std::fs::read_to_string(registry_path)?;
    let registry: Vec<nirvati_apps::metadata::Metadata> = serde_json::from_str(&registry)?;
    Ok(registry)
}

#[cached(result = true, time = 60, sync_writes = true)]
fn get_app(user: String, app: String) -> Result<nirvati_apps::metadata::Metadata> {
    let registry = load_registry(user)?;
    registry
        .into_iter()
        .find(|m| m.id == app)
        .ok_or_else(|| anyhow!("App not found"))
}

#[cached(result = true, time = 60, sync_writes = true)]
fn get_implementations(
    user: String,
    virtual_app: String,
) -> Result<Vec<nirvati_apps::metadata::Metadata>> {
    let registry = load_registry(user)?;
    Ok(registry
        .into_iter()
        .filter(|m| m.implements.as_ref().is_some_and(|i| i == &virtual_app))
        .collect())
}

#[cached(result = true, time = 15, sync_writes = true)]
async fn get_installed_apps(user: String) -> Result<Vec<String>> {
    let result = app_installation::Entity::find()
        .filter(app_installation::Column::OwnerName.eq(user))
        .all(DB_CLIENT.get().unwrap())
        .await?
        .into_iter()
        .map(|app| app.app_id)
        .collect();
    Ok(result)
}

#[cached(result = true, time = 60, sync_writes = true)]
async fn get_installed_impl(user: String, virtual_app: String) -> Result<String> {
    let installed_apps = get_installed_apps(user.clone()).await?;
    let implementations = get_implementations(user, virtual_app)?;
    let installed_impl = installed_apps
        .into_iter()
        .find(|app| implementations.iter().any(|i| &i.id == app))
        .ok_or_else(|| anyhow!("No installed implementation found"))?;
    Ok(installed_impl)
}

async fn resolve_to_service_and_ns(q: &DnsQuestion) -> Result<Option<(String, String)>> {
    let apps = if q.name.inner().split('.').count() == 3 {
        q.name
            .inner()
            .splitn(3, '.')
            .collect_tuple()
            .map(|(svc, app, tld)| (svc, app, "system", tld))
    } else {
        q.name.inner().splitn(4, '.').collect_tuple()
    };
    let Some((svc, app, user, tld)) = apps else {
        return Ok(None);
    };
    if tld != "nirvati" {
        return Ok(None);
    }
    let installed_apps = get_installed_apps(user.to_string()).await?;
    if installed_apps.contains(&app.to_string()) {
        if user == "system" {
            Ok(Some((svc.to_string(), app.to_string())))
        } else {
            Ok(Some((svc.to_string(), format!("{}-{}", user, app))))
        }
    } else {
        let app_impl = get_installed_impl(user.to_string(), app.to_string()).await?;
        Ok(Some((svc.to_string(), format!("{}-{}", user, app_impl))))
    }
}

async fn resolve_to_cname(q: &DnsQuestion) -> Result<Option<String>> {
    resolve_to_service_and_ns(q)
        .await
        .map(|svc| svc.map(|(svc, ns)| format!("{}.{}.svc.cluster.local", svc, ns)))
}

async fn get_service_ip(kube_client: Client, svc: &str, ns: &str) -> Result<String> {
    let svc_api: Api<Service> = Api::namespaced(kube_client, ns);
    let svc = svc_api.get(svc).await?;
    svc.spec
        .ok_or(anyhow!("Service spec not found"))?
        .cluster_ip
        .ok_or(anyhow!("Service IP not found"))
}

async fn resolve_to_a(kube_client: Client, q: &DnsQuestion) -> Result<Option<String>> {
    let Some((svc, ns)) = resolve_to_service_and_ns(q).await? else {
        return Ok(None);
    };
    let svc_ip = get_service_ip(kube_client, &svc, &ns).await?;
    Ok(Some(svc_ip))
}

fn main() {
    tracing_subscriber::fmt::init();
    let top_permit = Permit::new();
    let permit = top_permit.new_sub();
    std::thread::spawn(move || {
        Signals::new([SIGHUP, SIGINT, SIGQUIT, SIGTERM])
            .unwrap()
            .forever()
            .next();
        drop(top_permit);
    });
    let rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap();
    let kube_client = rt.block_on(kube::Client::try_default()).unwrap();
    let db_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let db = rt
        .block_on(Database::connect(db_url))
        .expect("Failed to create database client");
    DB_CLIENT.set(db).unwrap();

    dns_server::Builder::new_port(53)
        .unwrap()
        .with_permit(permit)
        .serve(&|q: &DnsQuestion| {
            tracing::debug!("Received query: {:#?}", q);
            if q.typ == DnsType::CNAME || q.typ == DnsType::ANY {
                let Ok(Some(cname_target)) = rt.block_on(resolve_to_cname(q)) else {
                    return vec![];
                };
                vec![DnsRecord::new_cname(q.name.inner(), &cname_target).unwrap()]
            } else if q.typ == DnsType::A {
                match rt.block_on(resolve_to_a(kube_client.clone(), q)) {
                    Ok(Some(a_target)) => {
                        vec![DnsRecord::new_a(q.name.inner(), &a_target).unwrap()]
                    }
                    Ok(None) => {
                        vec![]
                    }
                    Err(e) => {
                        eprintln!("Error resolving A record: {:#?}", e);
                        vec![]
                    }
                }
            } else {
                vec![]
            }
        })
        .unwrap();
}
