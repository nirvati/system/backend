// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::api::manager_client::ManagerClient;
use crate::api::{
    download_apps_request::Download, DownloadAppsRequest, GenerateFilesRequest, UserState,
};
use nirvati_db::schema::app_installation;
use nirvati_db::sea_orm::prelude::*;
use std::collections::HashMap;
use tonic::Request;

mod api {
    tonic::include_proto!("nirvati_agent");
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let db_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let agent_endpoint = std::env::var("AGENT_ENDPOINT").expect("AGENT_ENDPOINT must be set");
    let endpoint = tonic::transport::Endpoint::from_shared(agent_endpoint)
        .expect("Failed to create apps endpoint");
    let channel = endpoint.connect().await.expect("Failed to connect to core");

    let db = nirvati_db::sea_orm::Database::connect(db_url)
        .await
        .expect("Could not connect to database");
    let mut nirvati_agent = ManagerClient::new(channel);
    let users = nirvati_db::schema::user::Entity::find()
        .find_with_related(app_installation::Entity)
        .all(&db)
        .await
        .expect("Failed to find user");
    for (user, app_installs) in users {
        if user.username == "system" {
            continue;
        }
        let app_settings: HashMap<String, String> = app_installs
            .into_iter()
            .map(|app| (app.app_id, serde_json::to_string(&app.settings).unwrap()))
            .collect();
        let apps = app_settings.keys().cloned().collect();
        let user_state = UserState {
            user_id: user.username.clone(),
            installed_apps: apps,
            app_settings,
            user_display_name: user.name,
        };
        if let Err(e) = nirvati_agent
            .download(Request::new(DownloadAppsRequest {
                user_state: Some(user_state.clone()),
                download: Some(Download::NewOrNotInstalledOnly(true)),
            }))
            .await
        {
            tracing::error!("Failed to download apps for user {}: {}", user.username, e);
            continue;
        }
        if let Err(e) = nirvati_agent
            .generate_files(Request::new(GenerateFilesRequest {
                user_state: Some(user_state.clone()),
            }))
            .await
        {
            tracing::error!("Failed to generate files for user {}: {}", user.username, e);
        };
    }
}
