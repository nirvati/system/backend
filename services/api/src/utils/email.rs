// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use fancy_regex::Regex;
use std::sync::LazyLock;

pub static EMAIL_REGEX: LazyLock<Regex> =
    LazyLock::new(|| Regex::new(r"^(?=[\w\-\.]+).{1,64}@([\w-]{1,63}\.)+[\w\-]{2,63}$").unwrap());

pub fn validate_email(email: &str) -> bool {
    // Does not allow quoted strings, comments, or unicode characters, and does not check for .. in the domain name
    // Early check to avoid regex
    if email.len() > 254 || !email.contains('@') || !email.contains('.') {
        return false;
    }
    EMAIL_REGEX.is_match(email).unwrap_or(false)
}

#[cfg(test)]
mod tests {
    use crate::utils::email::validate_email;

    #[test]
    fn basic_email() {
        assert!(validate_email("info@nirvati.org"));
    }

    #[test]
    fn invalid_email() {
        assert!(!validate_email("info@nirvati"));
    }

    #[test]
    fn invalid_email_2() {
        assert!(!validate_email("info@nirvati."));
    }
}
