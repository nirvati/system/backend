// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::Context;

use nirvati_db::schema::{app_installation, user};
use nirvati_db::sea_orm::prelude::*;

use crate::api::UserState;
use crate::graphql::auth_ctx::UserCtx;

pub async fn get_user_state(ctx: &Context<'_>) -> UserState {
    let user = ctx.data_unchecked::<UserCtx>().username.clone();
    get_user_state_by_username(ctx, user).await
}

pub async fn get_user_state_by_username(ctx: &Context<'_>, username: String) -> UserState {
    let db_client = ctx.data_unchecked::<DatabaseConnection>();
    let details = user::Entity::find()
        .filter(user::Column::Username.contains(username.clone()))
        .find_with_related(app_installation::Entity)
        .all(db_client)
        .await
        .expect("Failed to get user");
    if details.len() != 1 {
        panic!("Unexpected length {}", details.len());
    }
    let (user, apps) = details.into_iter().next().unwrap();
    let settings = apps
        .into_iter()
        // Serializing can basically never fail, because settings has a JSON type in the DB and is a serde_json::Value
        .map(|app| {
            (
                app.app_id,
                serde_json::to_string(&app.settings).expect("Failed to serialize app settings"),
            )
        })
        .collect::<std::collections::HashMap<_, _>>();

    UserState {
        user_id: username,
        installed_apps: settings.keys().cloned().collect(),
        app_settings: settings,
        user_display_name: user.name,
    }
}
