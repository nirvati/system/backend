// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use serde_aux::prelude::*;
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ApiVersion {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Schema {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    #[serde(
        rename = "collectionActions",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub collection_actions: HashMap<String, String>,
    #[serde(
        rename = "collectionFields",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub collection_fields: HashMap<String, String>,
    #[serde(
        rename = "collectionFilters",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub collection_filters: HashMap<String, String>,
    #[serde(
        rename = "collectionMethods",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub collection_methods: Vec<String>,
    pub id: String,
    #[serde(
        rename = "includeableLinks",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub includeable_links: Vec<String>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    #[serde(rename = "pluralName")]
    pub plural_name: String,
    #[serde(
        rename = "resourceActions",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub resource_actions: HashMap<String, String>,
    #[serde(
        rename = "resourceFields",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub resource_fields: HashMap<String, String>,
    #[serde(
        rename = "resourceMethods",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub resource_methods: Vec<String>,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Error {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub code: String,
    pub detail: String,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub message: String,
    pub status: i32,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AttachInput {
    #[serde(rename = "attachedBy")]
    pub attached_by: String,
    #[serde(rename = "attacherType")]
    pub attacher_type: String,
    #[serde(rename = "attachmentID")]
    pub attachment_i_d: String,
    #[serde(rename = "disableFrontend")]
    pub disable_frontend: bool,
    #[serde(rename = "hostId")]
    pub host_id: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DetachInput {
    #[serde(rename = "attachmentID")]
    pub attachment_i_d: String,
    #[serde(rename = "forceDetach")]
    pub force_detach: bool,
    #[serde(rename = "hostId")]
    pub host_id: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SnapshotInput {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub labels: HashMap<String, String>,
    pub name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SnapshotCRInput {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub labels: HashMap<String, String>,
    pub name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Backup {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    #[serde(rename = "compressionMethod")]
    pub compression_method: String,
    pub created: String,
    pub error: String,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub labels: HashMap<String, String>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub messages: HashMap<String, String>,
    pub name: String,
    pub progress: i32,
    pub size: String,
    #[serde(rename = "snapshotCreated")]
    pub snapshot_created: String,
    #[serde(rename = "snapshotName")]
    pub snapshot_name: String,
    pub state: String,
    pub r#type: String,
    pub url: String,
    #[serde(rename = "volumeBackingImageName")]
    pub volume_backing_image_name: String,
    #[serde(rename = "volumeCreated")]
    pub volume_created: String,
    #[serde(rename = "volumeName")]
    pub volume_name: String,
    #[serde(rename = "volumeSize")]
    pub volume_size: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackupInput {
    pub name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackupStatus {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    #[serde(rename = "backupURL")]
    pub backup_url: Option<String>,
    pub error: Option<String>,
    pub id: Option<String>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub progress: Option<i32>,
    pub replica: Option<String>,
    pub size: Option<String>,
    pub snapshot: Option<String>,
    pub state: Option<String>,
    pub r#type: Option<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SyncBackupResource {
    #[serde(rename = "syncAllBackupTargets")]
    pub sync_all_backup_targets: bool,
    #[serde(rename = "syncAllBackupVolumes")]
    pub sync_all_backup_volumes: bool,
    #[serde(rename = "syncBackupTarget")]
    pub sync_backup_target: bool,
    #[serde(rename = "syncBackupVolume")]
    pub sync_backup_volume: bool,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Orphan {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub name: String,
    #[serde(rename = "nodeID")]
    pub node_i_d: String,
    #[serde(rename = "orphanType")]
    pub orphan_type: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub parameters: HashMap<String, String>,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RestoreStatus {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    #[serde(rename = "backupURL")]
    pub backup_url: Option<String>,
    pub error: Option<String>,
    pub filename: Option<String>,
    pub id: Option<String>,
    #[serde(rename = "isRestoring")]
    pub is_restoring: Option<bool>,
    #[serde(rename = "lastRestored")]
    pub last_restored: Option<String>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub progress: Option<i32>,
    pub replica: Option<String>,
    pub state: Option<String>,
    pub r#type: Option<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PurgeStatus {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub error: Option<String>,
    pub id: Option<String>,
    #[serde(rename = "isPurging")]
    pub is_purging: Option<bool>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub progress: Option<i32>,
    pub replica: Option<String>,
    pub state: Option<String>,
    pub r#type: Option<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RebuildStatus {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub error: Option<String>,
    #[serde(rename = "fromReplica")]
    pub from_replica: Option<String>,
    pub id: Option<String>,
    #[serde(rename = "isRebuilding")]
    pub is_rebuilding: Option<bool>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub progress: Option<i32>,
    pub replica: Option<String>,
    pub state: Option<String>,
    pub r#type: Option<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ReplicaRemoveInput {
    pub name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SalvageInput {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub names: Vec<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ActivateInput {
    pub frontend: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ExpandInput {
    pub size: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EngineUpgradeInput {
    pub image: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Replica {
    pub address: String,
    #[serde(rename = "currentImage")]
    pub current_image: String,
    #[serde(rename = "dataEngine")]
    pub data_engine: String,
    #[serde(rename = "dataPath")]
    pub data_path: String,
    #[serde(rename = "diskID")]
    pub disk_i_d: String,
    #[serde(rename = "diskPath")]
    pub disk_path: String,
    #[serde(rename = "failedAt")]
    pub failed_at: String,
    #[serde(rename = "hostId")]
    pub host_id: String,
    pub image: String,
    #[serde(rename = "instanceManagerName")]
    pub instance_manager_name: String,
    pub mode: String,
    pub name: String,
    pub running: bool,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Controller {
    #[serde(rename = "actualSize")]
    pub actual_size: String,
    pub address: String,
    #[serde(rename = "currentImage")]
    pub current_image: String,
    pub endpoint: String,
    #[serde(rename = "hostId")]
    pub host_id: String,
    pub image: String,
    #[serde(rename = "instanceManagerName")]
    pub instance_manager_name: String,
    #[serde(rename = "isExpanding")]
    pub is_expanding: bool,
    #[serde(rename = "lastExpansionError")]
    pub last_expansion_error: String,
    #[serde(rename = "lastExpansionFailedAt")]
    pub last_expansion_failed_at: String,
    #[serde(rename = "lastRestoredBackup")]
    pub last_restored_backup: String,
    pub name: String,
    #[serde(rename = "requestedBackupRestore")]
    pub requested_backup_restore: String,
    pub running: bool,
    pub size: String,
    #[serde(rename = "unmapMarkSnapChainRemovedEnabled")]
    pub unmap_mark_snap_chain_removed_enabled: bool,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DiskUpdate {
    #[serde(rename = "allowScheduling")]
    pub allow_scheduling: bool,
    #[serde(rename = "diskType")]
    pub disk_type: String,
    #[serde(rename = "evictionRequested")]
    pub eviction_requested: bool,
    pub path: String,
    #[serde(rename = "storageReserved")]
    pub storage_reserved: i32,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub tags: Vec<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateReplicaCountInput {
    #[serde(rename = "replicaCount")]
    pub replica_count: i32,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateReplicaAutoBalanceInput {
    #[serde(rename = "replicaAutoBalance")]
    pub replica_auto_balance: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateDataLocalityInput {
    #[serde(rename = "dataLocality")]
    pub data_locality: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateAccessModeInput {
    #[serde(rename = "accessMode")]
    pub access_mode: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateSnapshotDataIntegrityInput {
    #[serde(rename = "snapshotDataIntegrity")]
    pub snapshot_data_integrity: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateSnapshotMaxCountInput {
    #[serde(rename = "snapshotMaxCount")]
    pub snapshot_max_count: i32,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateSnapshotMaxSizeInput {
    #[serde(rename = "snapshotMaxSize")]
    pub snapshot_max_size: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateOfflineReplicaRebuildingInput {
    #[serde(rename = "offlineReplicaRebuilding")]
    pub offline_replica_rebuilding: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateBackupCompressionInput {
    #[serde(rename = "backupCompressionMethod")]
    pub backup_compression_method: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateUnmapMarkSnapChainRemovedInput {
    #[serde(rename = "unmapMarkSnapChainRemoved")]
    pub unmap_mark_snap_chain_removed: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateReplicaSoftAntiAffinityInput {
    #[serde(rename = "replicaSoftAntiAffinity")]
    pub replica_soft_anti_affinity: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateReplicaZoneSoftAntiAffinityInput {
    #[serde(rename = "replicaZoneSoftAntiAffinity")]
    pub replica_zone_soft_anti_affinity: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateReplicaDiskSoftAntiAffinityInput {
    #[serde(rename = "replicaDiskSoftAntiAffinity")]
    pub replica_disk_soft_anti_affinity: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WorkloadStatus {
    #[serde(rename = "podName")]
    pub pod_name: Option<String>,
    #[serde(rename = "podStatus")]
    pub pod_status: Option<String>,
    #[serde(rename = "workloadName")]
    pub workload_name: Option<String>,
    #[serde(rename = "workloadType")]
    pub workload_type: Option<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CloneStatus {
    pub snapshot: Option<String>,
    #[serde(rename = "sourceVolume")]
    pub source_volume: Option<String>,
    pub state: Option<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Empty {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VolumeRecurringJob {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub id: String,
    #[serde(rename = "isGroup")]
    pub is_group: bool,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub name: String,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VolumeRecurringJobInput {
    #[serde(rename = "isGroup")]
    pub is_group: bool,
    pub name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PVCreateInput {
    #[serde(rename = "fsType")]
    pub fs_type: String,
    #[serde(rename = "pvName")]
    pub pv_name: String,
    #[serde(rename = "secretName")]
    pub secret_name: String,
    #[serde(rename = "secretNamespace")]
    pub secret_namespace: String,
    #[serde(rename = "storageClassName")]
    pub storage_class_name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PVCCreateInput {
    pub namespace: String,
    #[serde(rename = "pvcName")]
    pub pvc_name: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SettingDefinition {
    pub category: String,
    pub default: String,
    pub description: String,
    #[serde(rename = "displayName")]
    pub display_name: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub options: Vec<String>,
    #[serde(rename = "readOnly")]
    pub read_only: bool,
    pub required: bool,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VolumeCondition {
    #[serde(rename = "lastProbeTime")]
    pub last_probe_time: String,
    #[serde(rename = "lastTransitionTime")]
    pub last_transition_time: String,
    pub message: String,
    pub reason: String,
    pub status: String,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct NodeCondition {
    #[serde(rename = "lastProbeTime")]
    pub last_probe_time: String,
    #[serde(rename = "lastTransitionTime")]
    pub last_transition_time: String,
    pub message: String,
    pub reason: String,
    pub status: String,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DiskCondition {
    #[serde(rename = "lastProbeTime")]
    pub last_probe_time: String,
    #[serde(rename = "lastTransitionTime")]
    pub last_transition_time: String,
    pub message: String,
    pub reason: String,
    pub status: String,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LonghornCondition {
    #[serde(rename = "lastProbeTime")]
    pub last_probe_time: String,
    #[serde(rename = "lastTransitionTime")]
    pub last_transition_time: String,
    pub message: String,
    pub reason: String,
    pub status: String,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Event {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub event: k8s_openapi::api::core::v1::Event,
    #[serde(rename = "eventType")]
    pub event_type: String,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SupportBundle {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    #[serde(rename = "errorMessage")]
    pub error_message: String,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub name: String,
    #[serde(rename = "nodeID")]
    pub node_i_d: String,
    #[serde(rename = "progressPercentage")]
    pub progress_percentage: i32,
    pub state: String,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SupportBundleInitateInput {
    pub description: String,
    #[serde(rename = "issueURL")]
    pub issue_url: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Tag {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub name: String,
    #[serde(rename = "tagType")]
    pub tag_type: String,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InstanceManager {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    #[serde(rename = "currentState")]
    pub current_state: String,
    #[serde(rename = "dataEngine")]
    pub data_engine: String,
    pub id: String,
    pub image: String,
    #[serde(
        rename = "instanceEngines",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub instance_engines: HashMap<String, String>,
    #[serde(
        rename = "instanceReplicas",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub instance_replicas: HashMap<String, String>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub instances: HashMap<String, String>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    #[serde(rename = "managerType")]
    pub manager_type: String,
    pub name: String,
    #[serde(rename = "nodeID")]
    pub node_i_d: String,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackingImageDiskFileStatus {
    #[serde(rename = "lastStateTransitionTime")]
    pub last_state_transition_time: Option<String>,
    pub message: Option<String>,
    pub progress: Option<i32>,
    pub state: Option<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackingImageCleanupInput {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub disks: Vec<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Attachment {
    #[serde(rename = "attachmentID")]
    pub attachment_i_d: String,
    #[serde(rename = "attachmentType")]
    pub attachment_type: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub conditions: Vec<LonghornCondition>,
    #[serde(rename = "nodeID")]
    pub node_i_d: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub parameters: HashMap<String, String>,
    pub satisfied: bool,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VolumeAttachment {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub attachments: HashMap<String, Attachment>,
    pub volume: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Volume {
    #[serde(rename = "accessMode")]
    pub access_mode: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    #[serde(rename = "backingImage")]
    pub backing_image: String,
    #[serde(rename = "backupCompressionMethod")]
    pub backup_compression_method: String,
    #[serde(
        rename = "backupStatus",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub backup_status: Vec<BackupStatus>,
    #[serde(rename = "cloneStatus")]
    pub clone_status: CloneStatus,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub conditions: HashMap<String, VolumeCondition>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub controllers: Vec<Controller>,
    pub created: String,
    #[serde(rename = "currentImage")]
    pub current_image: String,
    #[serde(rename = "dataEngine")]
    pub data_engine: String,
    #[serde(rename = "dataLocality")]
    pub data_locality: String,
    #[serde(rename = "dataSource")]
    pub data_source: String,
    #[serde(rename = "disableFrontend")]
    pub disable_frontend: bool,
    #[serde(
        rename = "diskSelector",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub disk_selector: Vec<String>,
    pub encrypted: bool,
    #[serde(rename = "fromBackup")]
    pub from_backup: String,
    pub frontend: String,
    pub id: String,
    pub image: String,
    #[serde(rename = "kubernetesStatus")]
    pub kubernetes_status: KubernetesStatus,
    #[serde(rename = "lastAttachedBy")]
    pub last_attached_by: String,
    #[serde(rename = "lastBackup")]
    pub last_backup: String,
    #[serde(rename = "lastBackupAt")]
    pub last_backup_at: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub migratable: bool,
    pub name: String,
    #[serde(
        rename = "nodeSelector",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub node_selector: Vec<String>,
    #[serde(rename = "numberOfReplicas")]
    pub number_of_replicas: i32,
    #[serde(rename = "offlineReplicaRebuilding")]
    pub offline_replica_rebuilding: Option<String>,
    #[serde(rename = "offlineReplicaRebuildingRequired")]
    pub offline_replica_rebuilding_required: Option<bool>,
    #[serde(
        rename = "purgeStatus",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub purge_status: Vec<PurgeStatus>,
    pub ready: bool,
    #[serde(
        rename = "rebuildStatus",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub rebuild_status: Vec<RebuildStatus>,
    #[serde(
        rename = "recurringJobSelector",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub recurring_job_selector: Vec<VolumeRecurringJob>,
    #[serde(rename = "replicaAutoBalance")]
    pub replica_auto_balance: String,
    #[serde(rename = "replicaDiskSoftAntiAffinity")]
    pub replica_disk_soft_anti_affinity: String,
    #[serde(rename = "replicaSoftAntiAffinity")]
    pub replica_soft_anti_affinity: String,
    #[serde(rename = "replicaZoneSoftAntiAffinity")]
    pub replica_zone_soft_anti_affinity: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub replicas: Vec<Replica>,
    #[serde(rename = "restoreInitiated")]
    pub restore_initiated: bool,
    #[serde(rename = "restoreRequired")]
    pub restore_required: bool,
    #[serde(
        rename = "restoreStatus",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub restore_status: Vec<RestoreStatus>,
    #[serde(rename = "restoreVolumeRecurringJob")]
    pub restore_volume_recurring_job: String,
    #[serde(rename = "revisionCounterDisabled")]
    pub revision_counter_disabled: bool,
    pub robustness: String,
    #[serde(rename = "shareEndpoint")]
    pub share_endpoint: String,
    #[serde(rename = "shareState")]
    pub share_state: String,
    pub size: String,
    #[serde(rename = "snapshotDataIntegrity")]
    pub snapshot_data_integrity: String,
    #[serde(rename = "snapshotMaxCount")]
    pub snapshot_max_count: i32,
    #[serde(rename = "snapshotMaxSize")]
    pub snapshot_max_size: String,
    #[serde(rename = "staleReplicaTimeout")]
    pub stale_replica_timeout: i32,
    pub standby: bool,
    pub state: String,
    pub r#type: String,
    #[serde(rename = "unmapMarkSnapChainRemoved")]
    pub unmap_mark_snap_chain_removed: String,
    #[serde(rename = "volumeAttachment")]
    pub volume_attachment: VolumeAttachment,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Snapshot {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub checksum: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub children: HashMap<String, bool>,
    pub created: String,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub labels: HashMap<String, String>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub name: String,
    pub parent: String,
    pub removed: bool,
    pub size: String,
    pub r#type: String,
    pub usercreated: bool,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SnapshotCR {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub checksum: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub children: HashMap<String, bool>,
    #[serde(rename = "crCreationTime")]
    pub cr_creation_time: String,
    #[serde(rename = "createSnapshot")]
    pub create_snapshot: bool,
    #[serde(rename = "creationTime")]
    pub creation_time: String,
    pub error: String,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub labels: HashMap<String, String>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    #[serde(rename = "markRemoved")]
    pub mark_removed: bool,
    pub name: String,
    #[serde(rename = "ownerID")]
    pub owner_i_d: String,
    pub parent: String,
    #[serde(rename = "readyToUse")]
    pub ready_to_use: bool,
    #[serde(rename = "restoreSize")]
    pub restore_size: i32,
    pub size: i32,
    pub r#type: String,
    #[serde(rename = "userCreated")]
    pub user_created: bool,
    pub volume: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackupTarget {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub available: bool,
    #[serde(rename = "backupTargetURL")]
    pub backup_target_url: String,
    #[serde(rename = "credentialSecret")]
    pub credential_secret: String,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub message: String,
    pub name: String,
    #[serde(rename = "pollInterval")]
    pub poll_interval: String,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackupVolume {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    #[serde(rename = "backingImageChecksum")]
    pub backing_image_checksum: String,
    #[serde(rename = "backingImageName")]
    pub backing_image_name: String,
    pub created: String,
    #[serde(rename = "dataStored")]
    pub data_stored: String,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub labels: HashMap<String, String>,
    #[serde(rename = "lastBackupAt")]
    pub last_backup_at: String,
    #[serde(rename = "lastBackupName")]
    pub last_backup_name: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub messages: HashMap<String, String>,
    pub name: String,
    pub size: String,
    #[serde(rename = "storageClassName")]
    pub storage_class_name: String,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Setting {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub definition: SettingDefinition,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub name: String,
    pub r#type: String,
    pub value: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RecurringJob {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub concurrency: i32,
    pub cron: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub groups: Vec<String>,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub labels: HashMap<String, String>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub name: String,
    pub retain: i32,
    pub task: String,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EngineImage {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    #[serde(rename = "buildDate")]
    pub build_date: String,
    #[serde(rename = "cliAPIMinVersion")]
    pub cli_a_p_i_min_version: i32,
    #[serde(rename = "cliAPIVersion")]
    pub cli_a_p_i_version: i32,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub conditions: Vec<String>,
    #[serde(rename = "controllerAPIMinVersion")]
    pub controller_a_p_i_min_version: i32,
    #[serde(rename = "controllerAPIVersion")]
    pub controller_a_p_i_version: i32,
    #[serde(rename = "dataFormatMinVersion")]
    pub data_format_min_version: i32,
    #[serde(rename = "dataFormatVersion")]
    pub data_format_version: i32,
    pub default: bool,
    #[serde(rename = "gitCommit")]
    pub git_commit: String,
    pub id: String,
    pub image: String,
    pub incompatible: bool,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub name: String,
    #[serde(rename = "noRefSince")]
    pub no_ref_since: String,
    #[serde(
        rename = "nodeDeploymentMap",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub node_deployment_map: HashMap<String, String>,
    #[serde(rename = "ownerID")]
    pub owner_i_d: String,
    #[serde(rename = "refCount")]
    pub ref_count: i32,
    pub state: String,
    pub r#type: String,
    pub version: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackingImage {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    #[serde(rename = "currentChecksum")]
    pub current_checksum: String,
    #[serde(rename = "deletionTimestamp")]
    pub deletion_timestamp: String,
    #[serde(
        rename = "diskFileStatusMap",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub disk_file_status_map: HashMap<String, BackingImageDiskFileStatus>,
    #[serde(rename = "expectedChecksum")]
    pub expected_checksum: String,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub name: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub parameters: HashMap<String, String>,
    pub size: i32,
    #[serde(rename = "sourceType")]
    pub source_type: String,
    pub r#type: String,
    pub uuid: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Node {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    pub address: String,
    #[serde(rename = "allowScheduling")]
    pub allow_scheduling: bool,
    #[serde(rename = "autoEvicting")]
    pub auto_evicting: bool,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub conditions: HashMap<String, NodeCondition>,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub disks: HashMap<String, DiskInfo>,
    #[serde(rename = "evictionRequested")]
    pub eviction_requested: bool,
    pub id: String,
    #[serde(rename = "instanceManagerCPURequest")]
    pub instance_manager_c_p_u_request: i32,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub name: String,
    pub region: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub tags: Vec<String>,
    pub r#type: String,
    pub zone: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DiskUpdateInput {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub disks: Vec<DiskUpdate>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DiskInfo {
    #[serde(rename = "allowScheduling")]
    pub allow_scheduling: bool,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub conditions: HashMap<String, DiskCondition>,
    #[serde(rename = "diskType")]
    pub disk_type: String,
    #[serde(rename = "diskUUID")]
    pub disk_u_u_i_d: String,
    #[serde(rename = "evictionRequested")]
    pub eviction_requested: bool,
    pub path: String,
    #[serde(
        rename = "scheduledReplica",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub scheduled_replica: HashMap<String, String>,
    #[serde(rename = "storageAvailable")]
    pub storage_available: i32,
    #[serde(rename = "storageMaximum")]
    pub storage_maximum: i32,
    #[serde(rename = "storageReserved")]
    pub storage_reserved: i32,
    #[serde(rename = "storageScheduled")]
    pub storage_scheduled: i32,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub tags: Vec<String>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct KubernetesStatus {
    #[serde(rename = "lastPVCRefAt")]
    pub last_p_v_c_ref_at: Option<String>,
    #[serde(rename = "lastPodRefAt")]
    pub last_pod_ref_at: Option<String>,
    pub namespace: Option<String>,
    #[serde(rename = "pvName")]
    pub pv_name: Option<String>,
    #[serde(rename = "pvStatus")]
    pub pv_status: Option<String>,
    #[serde(rename = "pvcName")]
    pub pvc_name: Option<String>,
    #[serde(
        rename = "workloadsStatus",
        deserialize_with = "deserialize_default_from_null"
    )]
    pub workloads_status: Option<Vec<WorkloadStatus>>,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackupTargetListOutput {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub data: Vec<BackupTarget>,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackupVolumeListOutput {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub data: Vec<BackupVolume>,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BackupListOutput {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub data: Vec<Backup>,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SnapshotListOutput {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub data: Vec<Snapshot>,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SystemBackup {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    #[serde(rename = "createdAt")]
    pub created_at: String,
    pub error: String,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    #[serde(rename = "managerImage")]
    pub manager_image: String,
    pub name: String,
    pub state: String,
    pub r#type: String,
    pub version: String,
    #[serde(rename = "volumeBackupPolicy")]
    pub volume_backup_policy: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SystemRestore {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub actions: HashMap<String, String>,
    #[serde(rename = "createdAt")]
    pub created_at: String,
    pub error: String,
    pub id: String,
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub links: HashMap<String, String>,
    pub name: String,
    pub state: String,
    #[serde(rename = "systemBackup")]
    pub system_backup: String,
    pub r#type: String,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SnapshotCRListOutput {
    #[serde(deserialize_with = "deserialize_default_from_null")]
    pub data: Vec<SnapshotCR>,
    pub r#type: String,
}
