// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::sync::{Arc, OnceLock};

use actix_web::{
    guard, http::header::HeaderMap, web, App, HttpRequest, HttpResponse, HttpServer, Responder,
};
use actix_web_prom::PrometheusMetricsBuilder;
use async_graphql::{EmptySubscription, Schema};
use async_graphql_actix_web::{GraphQLRequest, GraphQLResponse};
use biscuit::jwa::SignatureAlgorithm;
use biscuit::jws::Secret;
use biscuit::{ClaimPresenceOptions, Presence, Validation, ValidationOptions, JWT};
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use k8s_openapi::ByteString;
use kube::api::PostParams;
use kube::{Api, Client};
use openssl::rsa::Rsa;

use nirvati::JwtAdditionalClaims;
use nirvati_api::graphql::auth_ctx::UserCtx;
use nirvati_api::graphql::{AppSchema, Mutation, Query};
use nirvati_db::migrations::{Migrator, MigratorTrait};
use nirvati_db::sea_orm::Database;

static SECRET: OnceLock<Secret> = OnceLock::new();

fn get_user_ctx_from_headers(headers: &HeaderMap) -> Option<UserCtx> {
    let auth_header = headers.get("Authorization")?.to_str().ok()?;
    let auth_header = auth_header.split("Bearer ").collect::<Vec<_>>();
    if auth_header.len() != 2 {
        return None;
    }
    let token = auth_header[1];
    let jwt_secret = SECRET.get().expect("JWT secret not set");
    let token = JWT::<JwtAdditionalClaims, biscuit::Empty>::new_encoded(token);
    let options = ClaimPresenceOptions {
        expiry: Presence::Required,
        subject: Presence::Required,
        audience: Presence::Required,
        ..Default::default()
    };
    let token = token
        .into_decoded(jwt_secret, SignatureAlgorithm::RS512)
        .ok()?;
    token
        .validate(ValidationOptions {
            claim_presence_options: options,
            expiry: Validation::Validate(()),
            ..Default::default()
        })
        .ok()?;
    let (_, claims) = token.unwrap_decoded();
    Some(UserCtx {
        // Existence of subject is already validated
        username: claims.registered.subject.unwrap(),
        permissions: claims.private.permission_set,
        user_group: claims.private.user_group,
        email: claims.private.email,
        is_mock_user: claims.private.is_mock_user.is_some_and(|x| x),
        audience: claims
            .registered
            .audience
            .unwrap()
            .iter()
            .cloned()
            .collect(),
        is_totp_session: claims.private.is_totp_enabled,
    })
}

async fn options_main(_req: HttpRequest) -> impl Responder {
    HttpResponse::NoContent()
}

async fn graphql_main(
    schema: web::Data<AppSchema>,
    req: HttpRequest,
    gql_request: GraphQLRequest,
) -> GraphQLResponse {
    let mut request = gql_request.into_inner();
    if let Some(token) = get_user_ctx_from_headers(req.headers()) {
        request = request.data(token);
    }
    schema.execute(request).await.into()
}

/*async fn index_ws(
    schema: web::Data<AppSchema>,
    req: HttpRequest,
    payload: web::Payload,
) -> Result<HttpResponse> {
    GraphQLSubscription::new(Schema::clone(&*schema))
        .on_connection_init(on_connection_init)
        .start(&req, payload)
}*/

async fn load_or_gen_keys() -> anyhow::Result<ring::rsa::KeyPair> {
    let client = Client::try_default().await?;
    let api: Api<k8s_openapi::api::core::v1::Secret> = Api::namespaced(client, "nirvati");

    // Check if  a secret already exists
    let secret = api.get("jwt-keys").await;
    let (_pubkey_der, privkey_der) = if let Ok(secret) = secret {
        let data = secret.data.unwrap();
        let pubkey_der = data.get("public_key").unwrap().clone();
        let privkey_der = data.get("private_key").unwrap().clone();
        (pubkey_der.0, privkey_der.0)
    } else {
        let rsa = Rsa::generate(4096)?;

        // Extract the private key in DER format
        let private_key_der = rsa.private_key_to_der()?;

        // Extract the public key in DER format
        let public_key_der = rsa.public_key_to_der()?;

        // Save these as a Kubernetes secret
        let pp = PostParams::default();
        api.create(
            &pp,
            &k8s_openapi::api::core::v1::Secret {
                data: Some(
                    vec![
                        ("public_key".to_string(), ByteString(public_key_der.clone())),
                        (
                            "private_key".to_string(),
                            ByteString(private_key_der.clone()),
                        ),
                    ]
                    .into_iter()
                    .collect(),
                ),
                metadata: ObjectMeta {
                    name: Some("jwt-keys".to_string()),
                    ..Default::default()
                },
                ..Default::default()
            },
        )
        .await?;

        (public_key_der, private_key_der)
    };

    let keypair = ring::rsa::KeyPair::from_der(&privkey_der)?;

    Ok(keypair)
}

async fn health() -> HttpResponse {
    HttpResponse::Ok().finish()
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    #[cfg(feature = "dotenvy")]
    dotenvy::dotenv().expect("Failed to load .env file");
    tracing_subscriber::fmt::init();
    let db_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let redis_url = std::env::var("REDIS_URL").expect("REDIS_URL must be set");
    let bind_address = std::env::var("BIND_ADDRESS").expect("BIND_ADDRESS must be set");
    let agent_endpoint = std::env::var("AGENT_ENDPOINT").expect("AGENT_ENDPOINT must be set");
    let client = Database::connect(db_url)
        .await
        .expect("Failed to create database client");

    Migrator::up(&client, None)
        .await
        .expect("Failed to run migrations");

    let rsa_key = load_or_gen_keys()
        .await
        .expect("Failed to load or generate keys");
    let secret = Secret::RsaKeyPair(Arc::new(rsa_key));

    let agent_endpoint = tonic::transport::Endpoint::from_shared(agent_endpoint)
        .expect("Failed to create agent endpoint");
    let agent_channel = agent_endpoint
        .connect()
        .await
        .expect("Failed to connect to agent");

    if SECRET.set(secret.clone()).is_err() {
        panic!("Failed to set JWT secret");
    }

    let kube_client = kube::Client::try_default()
        .await
        .expect("Failed to create kube client");
    let longhorn_client = nirvati_api::longhorn::Client::new(
        std::env::var("LONGHORN_ENDPOINT")
            .expect("LONGHORN_ENDPOINT must be set")
            .as_str(),
    );
    let redis_client = redis::Client::open(redis_url).expect("Failed to create redis client");
    let schema = Schema::build(Query, Mutation::default(), EmptySubscription)
        .limit_recursive_depth(10)
        .data(agent_channel)
        .data(client)
        .data(secret)
        .data(kube_client)
        .data(longhorn_client)
        .data(redis_client)
        .finish();

    let prometheus = PrometheusMetricsBuilder::new("api")
        .endpoint("/metrics")
        .build()
        .unwrap();

    HttpServer::new(move || {
        let app = App::new();
        #[cfg(feature = "__development")]
        let app = app.wrap(actix_cors::Cors::permissive());
        app.wrap(prometheus.clone())
            .app_data(web::Data::new(schema.clone()))
            .service(
                web::resource("/v0/graphql")
                    .guard(guard::Post())
                    .to(graphql_main),
            )
            .service(
                web::resource("/v0/graphql")
                    .guard(guard::Options())
                    .to(options_main),
            )
            .service(web::resource("/health").to(health))
    })
    .bind(&bind_address)?
    .run()
    .await
}
