// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use fancy_regex::Regex;
use std::sync::LazyLock;

pub(crate) mod api {
    tonic::include_proto!("nirvati_agent");
}
pub(crate) mod saas {
    tonic::include_proto!("nirvati_saas");
}
pub(crate) mod apps;
pub mod graphql;
pub mod longhorn;
pub(crate) mod passkeys;
pub mod utils;

static USERNAME_REGEX: LazyLock<Regex> = LazyLock::new(|| {
    Regex::new(r#"[a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*"#).unwrap()
});
