use anyhow::{bail, Result};
use base64::engine::general_purpose::URL_SAFE;
use base64::Engine;
use nirvati_db::schema::passkey;
use nirvati_db::sea_orm::prelude::*;
use nirvati_db::sea_orm::ActiveValue::Set;
use nirvati_db::sea_orm::IntoActiveModel;
use openssl::pkey::Id;
use redis::{AsyncCommands, RedisResult};

pub async fn create_new_challenge(redis_client: &redis::Client, username: &str) -> Result<String> {
    let mut conn = redis_client.get_multiplexed_tokio_connection().await?;
    let mut rand_bytes = [0u8; 32];
    openssl::rand::rand_bytes(&mut rand_bytes)?;
    let challenge_str = Engine::encode(&URL_SAFE, rand_bytes);
    let _: () = conn.set_ex(challenge_str.trim_end_matches('='), username, 300)
        .await?;
    Ok(challenge_str)
}

#[derive(serde::Deserialize)]
struct ClientData {
    r#type: String,
    challenge: String,
    origin: String,
}

pub async fn verify_challenge(
    db: &DatabaseConnection,
    redis_client: &redis::Client,
    client_data_json: &str,
    authenticator_data: &str,
    key_id: &str,
    signature: &str,
) -> Result<Option<String>> {
    let client_data_json = Engine::decode(&URL_SAFE, client_data_json.as_bytes())?;
    let authenticator_data = Engine::decode(&URL_SAFE, authenticator_data.as_bytes())?;
    let client_data: ClientData = serde_json::from_slice(&client_data_json)?;
    let challenge = client_data.challenge;
    if client_data.r#type != "webauthn.get" {
        bail!("Invalid client data type");
    };

    let mut conn = redis_client.get_multiplexed_tokio_connection().await?;

    let username: RedisResult<String> = conn.get(challenge.trim_end_matches('=')).await;
    if let Ok(username) = username {
        let _: () = conn.del(&challenge).await?;
        let decoded_key_id = Engine::decode(&URL_SAFE, key_id.as_bytes())?;
        let credential = passkey::Entity::find()
            .filter(passkey::Column::CredentialId.eq(decoded_key_id))
            .filter(passkey::Column::OwnerName.eq(username.clone()))
            .one(db)
            .await?;
        if let Some(credential) = credential {
            let credential_pubkey = openssl::pkey::PKey::public_key_from_raw_bytes(
                &credential.public_key,
                Id::ED25519,
            )?;

            let rp_id_hash = &authenticator_data[0..32];
            let mut hasher = openssl::sha::Sha256::new();
            hasher.update(credential.rp_id.as_bytes());
            let rp_id_hash_expected = hasher.finish();
            if rp_id_hash != rp_id_hash_expected {
                bail!("Invalid rp_id_hash");
            }

            // Check if client_data.origin matches the rp_id
            let relevant_origin = url::Url::parse(&client_data.origin)?;
            if relevant_origin.host_str() != Some(&credential.rp_id) {
                bail!("Invalid origin");
            }

            let sign_count = &authenticator_data[33..37];
            let sign_count =
                u32::from_be_bytes([sign_count[0], sign_count[1], sign_count[2], sign_count[3]]);
            if sign_count != 0 {
                if sign_count <= credential.sign_count as u32 {
                    bail!("Invalid sign count");
                }
                let mut existing_passkey = passkey::Entity::find()
                    .filter(passkey::Column::Id.eq(credential.id))
                    .one(db)
                    .await?
                    .ok_or_else(|| anyhow::anyhow!("Credential not found"))?
                    .into_active_model();
                existing_passkey.sign_count = Set(sign_count as i64);
                passkey::Entity::update(existing_passkey).exec(db).await?;
            }

            let mut hasher = openssl::sha::Sha256::new();
            hasher.update(&client_data_json);
            let client_data_hash = hasher.finish();

            let mut message = Vec::with_capacity(authenticator_data.len() + client_data_json.len());
            message.extend_from_slice(&authenticator_data);
            message.extend_from_slice(&client_data_hash);

            let signature = Engine::decode(&URL_SAFE, signature.as_bytes())?;

            let mut verifier = openssl::sign::Verifier::new_without_digest(&credential_pubkey)?;

            Ok(match verifier.verify_oneshot(&signature, &message) {
                Ok(true) => Some(username),
                Ok(false) => None,
                Err(e) => {
                    tracing::error!("Error verifying response: {:?}", e);
                    return Err(e.into());
                }
            })
        } else {
            tracing::debug!("No credential found for key id {}", key_id);
            Ok(None)
        }
    } else {
        tracing::debug!("No username or key id found for challenge {}!", challenge);
        Ok(None)
    }
}
