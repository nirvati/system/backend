// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::sync::OnceLock;

use async_graphql::{SimpleObject, Union};
use serde::{Deserialize, Serialize};

use nirvati::utils::MultiLanguageItem;
use nirvati_apps::metadata::Metadata;

use crate::graphql::query::apps::app::AppUpdate;

#[derive(SimpleObject, Clone, Debug, PartialEq)]
pub struct AlternativeDependency {
    pub(crate) options: Vec<AppMetadata>,
}

#[derive(Clone, Debug, PartialEq, Union)]
#[graphql(name = "Dependency")]
pub enum OutputDependency {
    OneDependency(AppMetadata),
    AlternativeDependency(AlternativeDependency),
}

#[derive(SimpleObject, Deserialize, Clone, Debug, PartialEq)]
#[serde(rename_all = "camelCase")]
#[graphql(complex)]
pub struct AppMetadata {
    #[graphql(flatten)]
    #[serde(flatten)]
    pub metadata: Metadata,
    #[graphql(skip)]
    #[serde(skip)]
    pub(crate) owner: String,
    #[serde(skip_deserializing)]
    pub installed: bool,
    #[serde(skip)]
    pub protected: bool,
    #[serde(skip)]
    pub paused: bool,
    #[serde(skip)]
    #[graphql(skip)]
    pub latest_version: OnceLock<Option<AppUpdate>>,
    #[serde(skip_deserializing)]
    #[graphql(name = "dependencies")]
    pub dependencies_resolved: Vec<OutputDependency>,
    #[graphql(name = "permissions")]
    #[serde(skip_deserializing)]
    pub permissions_resolved: Vec<Permission>,
}

#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq, Eq, SimpleObject)]
pub struct AppPermission {
    pub app_name: MultiLanguageItem,
    pub perm_name: MultiLanguageItem,
    pub description: MultiLanguageItem,
    pub hidden: Vec<String>,
}

#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq, Eq, SimpleObject)]
pub struct BuiltinPermission {
    pub permission: String,
}

#[derive(Union, Deserialize, Clone, Debug, PartialEq, Eq)]
#[graphql(name = "AppRequestedPermission")]
pub enum Permission {
    App(AppPermission),
    Builtin(BuiltinPermission),
}
