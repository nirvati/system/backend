// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::api::{LoadAppMetadataRequest, LoadRegistryRequest, LoadStoresRequest, UserState};
use crate::apps::types::{AppMetadata, OutputDependency};
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::clients::{apps_client, manage_client};
use crate::graphql::query::apps::store::AppStore;
use crate::utils::user_state::{get_user_state, get_user_state_by_username};
use async_graphql::{Context, Error};
use itertools::Itertools;
use nirvati::permissions::Permission;
use nirvati_apps::metadata::{AppScope, Dependency};
use nirvati_db::schema::app_installation;
use nirvati_db::schema::prelude::*;
use nirvati_db::sea_orm::prelude::*;
use tonic::Request;

pub mod types;

fn finalize_app(
    mut app: AppMetadata,
    current_user: &str,
    finalize_args: &FinalizeArgs,
    apps_by_id: &std::collections::HashMap<String, AppMetadata>,
    previously_visited: &mut Vec<String>,
) -> Option<AppMetadata> {
    // The app manager should have already checked that there are no circular dependencies, so this should be safe
    // But we have a check here just in case
    if previously_visited.contains(&app.metadata.id) {
        return None;
    }
    previously_visited.push(app.metadata.id.clone());
    app.installed = finalize_args.installed_user_apps.contains(&app.metadata.id)
        || finalize_args.installed_sys_apps.contains(&app.metadata.id);
    app.metadata.scope = if finalize_args.installed_sys_apps.contains(&app.metadata.id) {
        AppScope::System
    } else if finalize_args.installed_user_apps.contains(&app.metadata.id) {
        AppScope::User
    } else {
        app.metadata.default_scope
    };
    app.owner = match app.metadata.scope {
        AppScope::System => "system".to_string(),
        AppScope::User => current_user.to_string(),
    };
    app.protected = finalize_args.protected_apps.contains(&app.metadata.id);
    app.paused = finalize_args.paused_apps.contains(&app.metadata.id);
    app.dependencies_resolved = app
        .metadata
        .dependencies
        .iter()
        .filter_map(|dep| {
            Some(match dep {
                Dependency::OneDependency(id) => {
                    let target_app = apps_by_id.get(id)?.clone();
                    if app.metadata.scope == AppScope::System
                        && target_app.metadata.scope != AppScope::System
                    {
                        // System apps can't depend on non-system apps
                        return None;
                    }
                    OutputDependency::OneDependency(finalize_app(
                        target_app,
                        current_user,
                        finalize_args,
                        apps_by_id,
                        previously_visited,
                    )?)
                }
                Dependency::AlternativeDependency(ids) => {
                    OutputDependency::AlternativeDependency(types::AlternativeDependency {
                        options: ids
                            .iter()
                            .filter_map(|id| {
                                let target_app = apps_by_id.get(id)?.clone();
                                if app.metadata.scope == AppScope::System
                                    && target_app.metadata.scope != AppScope::System
                                {
                                    return None;
                                }
                                finalize_app(
                                    target_app,
                                    current_user,
                                    finalize_args,
                                    apps_by_id,
                                    previously_visited,
                                )
                            })
                            .collect(),
                    })
                }
            })
        })
        .collect();
    if app.dependencies_resolved.len() != app.metadata.dependencies.len() {
        return None;
    }
    if app
        .dependencies_resolved
        .iter()
        .filter_map(|dep| match dep {
            OutputDependency::OneDependency(_) => None,
            OutputDependency::AlternativeDependency(ids) => Some(&ids.options),
        })
        .any(|ids| ids.is_empty())
    {
        return None;
    };
    for dep in app.dependencies_resolved.iter_mut() {
        if let OutputDependency::AlternativeDependency(alt_dep) = dep {
            if alt_dep.options.len() == 1 {
                *dep = OutputDependency::OneDependency(alt_dep.options.remove(0));
            }
        }
    }

    app.permissions_resolved = app
        .metadata
        .has_permissions
        .iter()
        .filter_map(|perm| {
            if perm.starts_with("builtin/") {
                Some(types::Permission::Builtin(types::BuiltinPermission {
                    permission: perm.strip_prefix("builtin/").unwrap().to_string(),
                }))
            } else {
                let (app, perm_id) = perm.splitn(2, '/').collect_tuple()?;
                let app = apps_by_id.get(app)?;
                let perm = app
                    .metadata
                    .exposes_permissions
                    .iter()
                    .find(|perm| perm.id == perm_id)?;
                Some(types::Permission::App(types::AppPermission {
                    app_name: app.metadata.name.clone(),
                    perm_name: perm.name.clone(),
                    description: perm.description.clone(),
                    hidden: perm.hidden.clone(),
                }))
            }
        })
        .collect();
    Some(app)
}

/// Read the app registry
/// include_system sets whether to show system apps from the currently running user,
/// installed system apps are always included
async fn read_app_registry(
    ctx: &Context<'_>,
    user_state: UserState,
    finalize_args: &FinalizeArgs,
    include_system: bool,
) -> anyhow::Result<Vec<AppMetadata>> {
    let user = user_state.user_id.clone();
    let mut manage_client = manage_client(ctx).or(Err(anyhow::anyhow!("No manage client")))?;
    let app_registry: Vec<AppMetadata> = if user != "system" {
        let registry = manage_client
            .load_registry(Request::new(LoadRegistryRequest {
                user_state: Some(user_state),
            }))
            .await?
            .into_inner()
            .registry;
        serde_json::from_str(&registry)?
    } else {
        vec![]
    };
    let mut apps_by_id = app_registry
        .iter()
        .map(|app| (app.metadata.id.clone(), app.clone()))
        .collect::<std::collections::HashMap<_, _>>();
    let system_user_state = get_user_state_by_username(ctx, "system".to_string()).await;
    let system_app_registry = manage_client
        .load_registry(Request::new(LoadRegistryRequest {
            user_state: Some(system_user_state),
        }))
        .await?
        .into_inner()
        .registry;
    let system_app_registry: Vec<AppMetadata> = serde_json::from_str(&system_app_registry)?;
    for mut app in system_app_registry {
        app.metadata.scope = AppScope::System;
        apps_by_id.insert(app.metadata.id.clone(), app);
    }
    let app_registry = apps_by_id
        .values()
        .cloned()
        .filter_map(|app| {
            let app = finalize_app(app, &user, finalize_args, &apps_by_id, &mut vec![]);
            if include_system
                || app
                    .as_ref()
                    .is_some_and(|app| app.metadata.scope != AppScope::System || app.installed)
            {
                app
            } else {
                None
            }
        })
        .collect();
    Ok(app_registry)
}

struct FinalizeArgs {
    installed_user_apps: Vec<String>,
    installed_sys_apps: Vec<String>,
    protected_apps: Vec<String>,
    paused_apps: Vec<String>,
}

async fn prepare_finalize(
    ctx: &Context<'_>,
    username: String,
) -> async_graphql::Result<FinalizeArgs> {
    let client = ctx.data_unchecked::<DatabaseConnection>();
    let installed_user_apps = if username != "system" {
        AppInstallation::find()
            .filter(app_installation::Column::OwnerName.eq(username))
            .all(client)
            .await?
    } else {
        vec![]
    };
    let installed_sys_apps = AppInstallation::find()
        .filter(app_installation::Column::OwnerName.eq("system"))
        .all(client)
        .await?;
    let protected_apps = installed_user_apps
        .iter()
        .chain(installed_sys_apps.iter())
        .filter(|app| app.protected)
        .map(|app| app.app_id.clone())
        .collect::<Vec<String>>();
    let paused_apps = installed_user_apps
        .iter()
        .chain(installed_sys_apps.iter())
        .filter(|app| app.paused)
        .map(|app| app.app_id.clone())
        .collect::<Vec<String>>();
    let installed_user_apps = installed_user_apps
        .into_iter()
        .map(|app| app.app_id)
        .collect::<Vec<String>>();
    let installed_sys_apps = installed_sys_apps
        .into_iter()
        .map(|app| app.app_id)
        .collect::<Vec<String>>();
    Ok(FinalizeArgs {
        installed_user_apps,
        installed_sys_apps,
        protected_apps,
        paused_apps,
    })
}
pub async fn get_app_registry(ctx: &Context<'_>) -> async_graphql::Result<Vec<AppMetadata>> {
    let user_state = get_user_state(ctx).await;
    get_app_registry_for_user(ctx, user_state).await
}

pub async fn get_app_registry_for_user(
    ctx: &Context<'_>,
    user_state: UserState,
) -> async_graphql::Result<Vec<AppMetadata>> {
    let user_ctx = ctx.data_unchecked::<UserCtx>();
    let finalize_args = prepare_finalize(ctx, user_state.user_id.clone()).await?;
    Ok(read_app_registry(
        ctx,
        user_state,
        &finalize_args,
        user_ctx.permissions.has(Permission::ManageSysComponents) || user_ctx.is_mock_user,
    )
    .await?)
}

/// TODO: Add username parameter
pub async fn get_app(ctx: &Context<'_>, id: &str) -> async_graphql::Result<AppMetadata> {
    let app_registry = get_app_registry(ctx).await?;
    let app = app_registry
        .into_iter()
        .find(|app| app.metadata.id == id)
        .ok_or_else(|| Error::new("App not found"))?;
    Ok(app)
}

pub async fn get_app_with_settings(
    ctx: &Context<'_>,
    id: &str,
    settings: Option<serde_json::Value>,
    initial_domain: Option<String>,
) -> async_graphql::Result<AppMetadata> {
    let user_ctx = ctx.data_unchecked::<UserCtx>();
    let mut apps_client = apps_client(ctx)?;
    let mut user_state = get_user_state_by_username(ctx, user_ctx.username.clone()).await;
    if let Some(settings) = settings {
        user_state
            .app_settings
            .insert(id.to_string(), serde_json::to_string(&settings).unwrap());
    }
    let resp = apps_client
        .load_app_metadata(Request::new(LoadAppMetadataRequest {
            id: id.to_string(),
            user_state: Some(user_state.clone()),
            initial_domain,
        }))
        .await?
        .into_inner();
    let parsed_app: AppMetadata = serde_json::from_str(&resp.metadata)?;
    let finalize_args = prepare_finalize(ctx, user_ctx.username.clone()).await?;
    let app_registry_map = read_app_registry(
        ctx,
        user_state,
        &finalize_args,
        user_ctx.permissions.has(Permission::ManageSysComponents) || user_ctx.is_mock_user,
    )
    .await?
    .into_iter()
    .map(|app| (app.metadata.id.clone(), app))
    .collect();
    let app = finalize_app(
        parsed_app,
        &user_ctx.username,
        &finalize_args,
        &app_registry_map,
        &mut vec![],
    )
    .ok_or_else(|| Error::new("Failed to load app with these settings"))?;
    Ok(app)
}

pub async fn get_stores_yml(
    ctx: &Context<'_>,
    user_state: UserState,
) -> anyhow::Result<Vec<AppStore>> {
    let mut manage_client = manage_client(ctx).or(Err(anyhow::anyhow!("No manage client")))?;
    let user = user_state.user_id.clone();
    let stores_yml = manage_client
        .load_stores(Request::new(LoadStoresRequest {
            user_state: Some(user_state),
        }))
        .await?
        .into_inner()
        .stores;
    let mut result: Vec<AppStore> = serde_yaml::from_str(&stores_yml)?;
    for store in result.iter_mut() {
        store.owner = user.clone();
    }
    Ok(result)
}
