// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::MergedObject;

mod apps;
mod https;
mod network;
mod proxies;
mod services;
mod setup;
mod storage;
mod tor;
mod upgrades;
mod user;

#[derive(MergedObject, Default)]
pub struct Mutation(
    setup::Setup,
    user::User,
    upgrades::Upgrades,
    network::Network,
    apps::Apps,
    https::Https,
    storage::Storage,
    services::Services,
    proxies::Proxies,
    tor::Tor,
);
