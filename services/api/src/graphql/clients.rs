// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Context, Error};
use tonic::transport::Channel;

use crate::api::apps_client::AppsClient;
use crate::api::citadel_compat_client::CitadelCompatClient;
use crate::api::entropy_client::EntropyClient;
use crate::api::https_client::HttpsClient;
use crate::api::ingress_client::IngressClient;
use crate::api::manager_client::ManagerClient;
use crate::api::middlewares_client::MiddlewaresClient;
use crate::api::node_client::NodeClient;
use crate::api::services_client::ServicesClient;
use crate::api::setup_client::SetupClient;
use crate::api::tailscale_client::TailscaleClient;
use crate::api::upgrades_client::UpgradesClient;
use crate::api::users_client::UsersClient;

pub fn apps_client(ctx: &Context<'_>) -> async_graphql::Result<AppsClient<Channel>> {
    let channel = ctx
        .data_opt::<Channel>()
        .ok_or::<Error>("No communication channel found".into())?
        .clone();
    Ok(AppsClient::new(channel))
}

pub fn ingress_client(ctx: &Context<'_>) -> async_graphql::Result<IngressClient<Channel>> {
    let channel = ctx
        .data_opt::<Channel>()
        .ok_or::<Error>("No communication channel found".into())?
        .clone();
    Ok(IngressClient::new(channel))
}

pub fn manage_client(ctx: &Context<'_>) -> async_graphql::Result<ManagerClient<Channel>> {
    let channel = ctx
        .data_opt::<Channel>()
        .ok_or::<Error>("No communication channel found".into())?
        .clone();
    Ok(ManagerClient::new(channel))
}

pub fn citadel_compat_client(
    ctx: &Context<'_>,
) -> async_graphql::Result<CitadelCompatClient<Channel>> {
    let channel = ctx
        .data_opt::<Channel>()
        .ok_or::<Error>("No communication channel found".into())?
        .clone();
    Ok(CitadelCompatClient::new(channel))
}

pub fn https_client(ctx: &Context<'_>) -> async_graphql::Result<HttpsClient<Channel>> {
    let channel = ctx
        .data_opt::<Channel>()
        .ok_or::<Error>("No communication channel found".into())?
        .clone();
    Ok(HttpsClient::new(channel))
}

pub fn node_client(ctx: &Context<'_>) -> async_graphql::Result<NodeClient<Channel>> {
    let channel = ctx
        .data_opt::<Channel>()
        .ok_or::<Error>("No communication channel found".into())?
        .clone();
    Ok(NodeClient::new(channel))
}

pub fn tailscale_client(ctx: &Context<'_>) -> async_graphql::Result<TailscaleClient<Channel>> {
    let channel = ctx
        .data_opt::<Channel>()
        .ok_or::<Error>("No communication channel found".into())?
        .clone();
    Ok(TailscaleClient::new(channel))
}

pub fn upgrades_client(ctx: &Context<'_>) -> async_graphql::Result<UpgradesClient<Channel>> {
    let channel = ctx
        .data_opt::<Channel>()
        .ok_or::<Error>("No communication channel found".into())?
        .clone();
    Ok(UpgradesClient::new(channel))
}

pub fn entropy_client(ctx: &Context<'_>) -> async_graphql::Result<EntropyClient<Channel>> {
    let channel = ctx
        .data_opt::<Channel>()
        .ok_or::<Error>("No communication channel found".into())?
        .clone();
    Ok(EntropyClient::new(channel))
}

pub fn setup_client(ctx: &Context<'_>) -> async_graphql::Result<SetupClient<Channel>> {
    let channel = ctx
        .data_opt::<Channel>()
        .ok_or::<Error>("No communication channel found".into())?
        .clone();
    Ok(SetupClient::new(channel))
}

pub fn users_client(ctx: &Context<'_>) -> async_graphql::Result<UsersClient<Channel>> {
    let channel = ctx
        .data_opt::<Channel>()
        .ok_or::<Error>("No communication channel found".into())?
        .clone();
    Ok(UsersClient::new(channel))
}

pub fn services_client(ctx: &Context<'_>) -> async_graphql::Result<ServicesClient<Channel>> {
    let channel = ctx
        .data_opt::<Channel>()
        .ok_or::<Error>("No communication channel found".into())?
        .clone();
    Ok(ServicesClient::new(channel))
}

pub fn proxies_client(
    ctx: &Context<'_>,
) -> async_graphql::Result<crate::api::proxies_client::ProxiesClient<Channel>> {
    let channel = ctx
        .data_opt::<Channel>()
        .ok_or::<Error>("No communication channel found".into())?
        .clone();
    Ok(crate::api::proxies_client::ProxiesClient::new(channel))
}

pub fn middlewares_client(ctx: &Context<'_>) -> async_graphql::Result<MiddlewaresClient<Channel>> {
    let channel = ctx
        .data_opt::<Channel>()
        .ok_or::<Error>("No communication channel found".into())?
        .clone();
    Ok(MiddlewaresClient::new(channel))
}
