// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Error, Object};
use k8s_openapi::api::core::v1::PersistentVolumeClaim;
use kube::Api;

use nirvati_apps::metadata::AppScope;
use nirvati_db::schema::app_installation;
use nirvati_db::sea_orm::prelude::*;

use crate::apps::get_app;
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::longhorn::Client as LonghornClient;
use nirvati::permissions::Permission;

#[derive(Default)]
pub struct Storage;

#[Object(name = "StorageMutations")]
impl Storage {
    #[graphql(guard = "PermissionGuard::new(Permission::ManageAppStorage)")]
    pub async fn set_replica_count(
        &self,
        ctx: &async_graphql::Context<'_>,
        app_id: String,
        volume_id: String,
        replicas: u8,
    ) -> async_graphql::Result<bool> {
        let user = ctx.data::<UserCtx>()?;
        let db_client = ctx.data_unchecked::<DatabaseConnection>();
        if replicas == 0 {
            return Err(Error::new("Replicas must be greater than 0"));
        }
        let app = get_app(ctx, &app_id).await?;
        let is_installed = app_installation::Entity::find()
            .filter(app_installation::Column::AppId.eq(app_id.clone()))
            .filter(app_installation::Column::OwnerName.eq(
                if app.metadata.scope == AppScope::System {
                    "system".to_string()
                } else {
                    user.username.clone()
                },
            ))
            .count(db_client)
            .await
            .map(|count| count > 0)
            .unwrap_or(false);
        if !is_installed {
            return Err(Error::new("App not found"));
        }
        let kube_client = ctx.data_unchecked::<kube::Client>().clone();
        let longhorn_client = ctx.data_unchecked::<LonghornClient>();
        let app_ns = if app.metadata.scope == AppScope::System {
            app_id
        } else {
            format!("{}-{}", &user.username, app_id)
        };
        let volumes_api: Api<PersistentVolumeClaim> = Api::namespaced(kube_client, &app_ns);
        let volume = volumes_api
            .get(&volume_id)
            .await
            .map_err(|_| Error::new("Failed to get volume"))?;
        let spec = volume.spec.ok_or(Error::new("Failed to get volume spec"))?;
        let longhorn_id = spec
            .volume_name
            .ok_or(Error::new("Failed to get volume name"))?;
        longhorn_client
            .update_replica_count(&longhorn_id, replicas)
            .await?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageAppStorage)")]
    pub async fn resize_volume(
        &self,
        ctx: &async_graphql::Context<'_>,
        app_id: String,
        volume_id: String,
        size: u64,
    ) -> async_graphql::Result<bool> {
        let user = ctx.data::<UserCtx>()?;
        let db_client = ctx.data_unchecked::<DatabaseConnection>();
        let is_installed = app_installation::Entity::find()
            .filter(app_installation::Column::AppId.eq(app_id.clone()))
            .filter(app_installation::Column::OwnerName.eq(user.username.clone()))
            .count(db_client)
            .await
            .map(|count| count > 0)
            .unwrap_or(false);
        if !is_installed {
            return Err(Error::new("App not found"));
        }
        let app = get_app(ctx, &app_id).await?;
        let app_volume = app
            .metadata
            .volumes
            .get(&volume_id)
            .ok_or(Error::new("Volume not found"))?;
        if size < app_volume.minimum_size {
            return Err(Error::new("Size too small"));
        };
        let kube_client = ctx.data_unchecked::<kube::Client>().clone();
        let longhorn_client = ctx.data_unchecked::<LonghornClient>();
        let app_ns = format!("{}-{}", &user.username, app_id);
        let volumes_api: Api<PersistentVolumeClaim> = Api::namespaced(kube_client, &app_ns);
        let volume = volumes_api
            .get(&volume_id)
            .await
            .map_err(|_| Error::new("Failed to get volume"))?;
        let spec = volume.spec.ok_or(Error::new("Failed to get volume spec"))?;
        let longhorn_id = spec
            .volume_name
            .ok_or(Error::new("Failed to get volume name"))?;
        let current_vol = longhorn_client
            .get_volume(&longhorn_id)
            .await
            .map_err(|_| Error::new("Failed to get volume"))?;
        let current_size: u64 = current_vol
            .size
            .parse()
            .map_err(|_| Error::new("Failed to parse volume size"))?;
        if current_size >= size {
            return Err(Error::new("Size too small"));
        }
        longhorn_client.expand(&longhorn_id, size).await?;
        Ok(true)
    }
}
