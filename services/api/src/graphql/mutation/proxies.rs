// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Object, Result};
use tonic::Request;

use crate::api::{AddProxyRequest, HttpsOptions, RemoveProxyRequest, Route};
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::proxies_client;
use crate::graphql::query::https::acme_provider::AcmeProvider;
use crate::graphql::query::https::proxy::Protocol;
use nirvati::permissions::Permission;
use nirvati_db::schema::{acme_account, domain, prelude::*, proxy, route, service};
use nirvati_db::sea_orm::prelude::*;
use nirvati_db::sea_orm::ActiveValue::Set;

#[derive(Default)]
pub struct Proxies;

fn proto_to_int(p: nirvati_db::schema::sea_orm_active_enums::Protocol) -> i32 {
    match p {
        nirvati_db::schema::sea_orm_active_enums::Protocol::Http => 0,
        nirvati_db::schema::sea_orm_active_enums::Protocol::Https => 1,
    }
}

async fn recreate_proxy(
    ctx: &async_graphql::Context<'_>,
    user: String,
    domain: String,
    routes: Vec<nirvati_db::schema::route::Model>,
    parent: Option<String>,
    acme_provider: AcmeProvider,
) -> Result<()> {
    let mut proxies_client =
        proxies_client(ctx).map_err(|_| "Failed to create https client".to_string())?;
    proxies_client
        .add_proxy(Request::new(AddProxyRequest {
            domain,
            namespace: user.clone(),
            routes: routes
                .into_iter()
                .map(|r| Route {
                    target_svc: format!("svc-{}", r.service_id),
                    remote_prefix: r.target_path,
                    local_prefix: r.local_path,
                    enable_compression: r.enable_compression,
                    protocol: proto_to_int(r.target_protocol),
                    port: r.target_port,
                    https: Some(HttpsOptions {
                        insecure_skip_verify: r.allow_invalid_cert,
                        sni: r.target_sni,
                    }),
                })
                .collect(),
            cert_user: user,
            parent,
            acme_provider: Some(acme_provider.into()),
        }))
        .await
        .map_err(|_| "Failed to create proxy in Kubernetes".to_string())?;
    Ok(())
}

#[Object(name = "ProxiesMutations")]
impl Proxies {
    #[graphql(guard = "PermissionGuard::new(Permission::ManageProxies)")]
    async fn create_route(
        &self,
        ctx: &async_graphql::Context<'_>,
        proxy_domain: String,
        local_path: String,
        target_path: String,
        protocol: Protocol,
        target_port: u16,
        enable_compression: bool,
        service_id: String,
        allow_invalid_cert: bool,
        target_sni: Option<String>,
    ) -> Result<String> {
        let proxy_domain = proxy_domain.trim().to_lowercase();
        let db = ctx.data_unchecked::<DatabaseConnection>();
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let service = service::Entity::find()
            .filter(service::Column::Id.eq(service_id.clone()))
            .one(db)
            .await?
            .ok_or("Failed to find service in database".to_string())?;
        if service.owner_name != username {
            return Err("Failed to find service in database".to_string().into());
        }
        let (proxy, proxy_parent) = proxy::Entity::find()
            .filter(proxy::Column::Domain.eq(proxy_domain.clone()))
            .find_also_related(domain::Entity)
            .one(db)
            .await?
            .ok_or("Failed to find proxy in database".to_string())?;
        let route = route::Entity::insert(route::ActiveModel {
            proxy_domain: Set(proxy_domain.clone()),
            enable_compression: Set(enable_compression),
            local_path: Set(local_path.clone()),
            target_path: Set(target_path.clone()),
            service_id: Set(service_id.clone().parse()?),
            target_port: Set(target_port as i32),
            target_protocol: Set(protocol.into()),
            target_sni: Set(target_sni),
            allow_invalid_cert: Set(allow_invalid_cert),
            ..Default::default()
        })
        .exec(db)
        .await?;
        let all_routes = route::Entity::find()
            .filter(route::Column::ProxyDomain.eq(proxy_domain.clone()))
            .all(db)
            .await?;
        let acme_provider = proxy
            .find_related(AcmeAccount)
            .one(db)
            .await?
            .ok_or("Failed to find acme provider in database".to_string())?
            .provider;
        recreate_proxy(
            ctx,
            service.owner_name,
            proxy_domain,
            all_routes,
            proxy_parent.map(|p| p.id.to_string()),
            acme_provider.into(),
        )
        .await
        .map_err(|_| "Failed to recreate proxy in Kubernetes".to_string())?;
        Ok(route.last_insert_id.to_string())
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageProxies)")]
    async fn delete_route(
        &self,
        ctx: &async_graphql::Context<'_>,
        route_id: String,
    ) -> Result<bool> {
        let db = ctx.data_unchecked::<DatabaseConnection>();
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let route_id: Uuid = route_id
            .trim()
            .to_string()
            .parse()
            .map_err(|_| "Invalid service ID".to_string())?;
        let (route, target_service) = route::Entity::find_by_id(route_id)
            .find_also_related(service::Entity)
            .one(db)
            .await?
            .ok_or("Failed to find route in database".to_string())?;
        let Some(target_service) = target_service else {
            return Err("Failed to find route target service in database"
                .to_string()
                .into());
        };
        if target_service.owner_name != username {
            return Err("Failed to find service in database".to_string().into());
        }
        let Some(proxy) = route.find_related(Proxy).one(db).await? else {
            return Err("Failed to find proxy in database".to_string().into());
        };
        route.delete(db).await?;
        let all_domain_routes = route::Entity::find()
            .filter(route::Column::ProxyDomain.eq(proxy.domain.clone()))
            .all(db)
            .await?;
        let acme_account = proxy
            .find_related(AcmeAccount)
            .one(db)
            .await?
            .ok_or("Failed to find acme provider in database".to_string())?;
        let parent_domain_name = proxy.find_related(Domain).one(db).await?.map(|d| d.id);
        recreate_proxy(
            ctx,
            target_service.owner_name,
            proxy.domain,
            all_domain_routes,
            parent_domain_name.map(|p| p.to_string()),
            acme_account.provider.into(),
        )
        .await
        .map_err(|_| "Failed to recreate proxy in Kubernetes".to_string())?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageProxies)")]
    async fn create_proxy(
        &self,
        ctx: &async_graphql::Context<'_>,
        domain: String,
        acme_provider: AcmeProvider,
    ) -> Result<bool> {
        let db = ctx.data_unchecked::<DatabaseConnection>();
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let acme_account =
            acme_account::Entity::find()
                .filter(
                    acme_account::Column::Provider
                        .eq::<nirvati_db::schema::sea_orm_active_enums::AcmeProvider>(
                        acme_provider.into(),
                    ),
                )
                .one(db)
                .await
                .map_err(|_| "Failed to find ACME provider in database".to_string())?
                .ok_or("Failed to find ACME provider in database".to_string())?;
        let sld = domain.split('.').rev().take(2).collect::<Vec<_>>();
        let sld = sld.into_iter().rev().collect::<Vec<_>>().join(".");
        let mut potential_parents = domain::Entity::find()
            .filter(domain::Column::OwnerName.eq(username.clone()))
            .filter(domain::Column::Name.ends_with(sld.clone()))
            .all(db)
            .await?;
        potential_parents.sort_by(|potential_parent_a, potential_parent_b| {
            potential_parent_a
                .name
                .len()
                .cmp(&potential_parent_b.name.len())
        });
        let parent = potential_parents.first();
        proxy::Entity::insert(proxy::ActiveModel {
            domain: Set(domain.clone()),
            owner_name: Set(username.clone()),
            acme_account_id: Set(Some(acme_account.id)),
            parent_domain_name: Set(parent.map(|p| p.id)),
        })
        .exec(db)
        .await
        .map_err(|_| "Failed to create proxy in database".to_string())?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageProxies)")]
    async fn delete_proxy(&self, ctx: &async_graphql::Context<'_>, domain: String) -> Result<bool> {
        let db = ctx.data_unchecked::<DatabaseConnection>();
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let proxy = proxy::Entity::find()
            .filter(proxy::Column::Domain.eq(domain.clone()))
            .one(db)
            .await?
            .ok_or("Failed to find proxy in database".to_string())?;
        if proxy.owner_name != username {
            return Err("Failed to find proxy in database".to_string().into());
        }
        proxy.delete(db).await?;
        let mut proxies_client =
            proxies_client(ctx).map_err(|_| "Failed to create https client".to_string())?;
        proxies_client
            .remove_proxy(Request::new(RemoveProxyRequest {
                domain,
                namespace: username.clone(),
            }))
            .await
            .map_err(|_| "Failed to delete route in Kubernetes".to_string())?;
        Ok(true)
    }
}
