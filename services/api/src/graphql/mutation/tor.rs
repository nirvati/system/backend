use crate::api::{AddDomainRequest, DomainType};
use crate::apps::get_app;
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::ingress_client;
use crate::utils::user_state::{get_user_state, get_user_state_by_username};
use async_graphql::{Context, Object};
use nirvati::permissions::Permission;
use nirvati_apps::metadata::AppScope;
use nirvati_db::schema::{app_domain, app_installation};
use nirvati_db::sea_orm::prelude::*;
use nirvati_db::sea_orm::ActiveValue::Set;
use tonic::Request;

#[derive(Default)]
pub struct Tor;

#[Object(name = "TorMutations")]
impl Tor {
    #[graphql(guard = "PermissionGuard::new(Permission::ManageDomains)")]
    async fn add_onion_domain(
        &self,
        ctx: &Context<'_>,
        app_id: String,
        app_component: Option<String>,
    ) -> async_graphql::Result<String> {
        let user_ctx = ctx.data_unchecked::<UserCtx>();
        let db_client = ctx.data_unchecked::<DatabaseConnection>();
        let mut ingress_client = ingress_client(ctx)?;
        let app = get_app(ctx, &app_id).await?.metadata;
        let user_state = if app.scope == AppScope::System {
            get_user_state_by_username(ctx, "system".to_string()).await
        } else {
            get_user_state(ctx).await
        };
        let installation = app_installation::Entity::find()
            .filter(app_installation::Column::AppId.eq(app_id.clone()))
            .filter(app_installation::Column::OwnerName.eq(user_ctx.username.clone()))
            .one(db_client)
            .await?
            .ok_or_else(|| async_graphql::Error::new("App not found"))?;
        let new_domain = ingress_client
            .add_domain(Request::new(AddDomainRequest {
                id: app_id.clone(),
                domain: None,
                user_state: Some(user_state.clone()),
                protect: false,
                custom_prefix: None,
                r#type: DomainType::DomainOnion as i32,
                parent: None,
                acme_provider: None,
                cert_user: None,
                app_component: app_component.clone(),
            }))
            .await?
            .into_inner()
            .domain;
        app_domain::Entity::insert(app_domain::ActiveModel {
            name: Set(new_domain.clone()),
            app_installation_id: Set(installation.id),
            app_component_id: Set(app_component.clone()),
            acme_account_id: Default::default(),
            r#type: Set(nirvati_db::schema::sea_orm_active_enums::DomainType::Onion),
            ..Default::default()
        })
        .exec(db_client)
        .await?;
        Ok(new_domain)
    }
}
