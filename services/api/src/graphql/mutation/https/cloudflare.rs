use cloudflare::endpoints::dns;
use cloudflare::endpoints::dns::DnsRecord;
use cloudflare::framework::response::ApiResponse;
use cloudflare::framework::HttpApiClient;

pub fn create_txt_record(
    zone_identifier: &str,
    name: &str,
    content: String,
    api_client: &HttpApiClient,
) -> ApiResponse<DnsRecord> {
    let endpoint = dns::CreateDnsRecord {
        zone_identifier,
        params: dns::CreateDnsRecordParams {
            name,
            content: dns::DnsContent::TXT { content },
            priority: None,
            proxied: None,
            ttl: None,
        },
    };
    api_client.request(&endpoint)
}
