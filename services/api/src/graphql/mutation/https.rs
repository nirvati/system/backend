// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use ::cloudflare::endpoints::zone::{ListZones, ListZonesParams};
use ::cloudflare::framework::auth::Credentials;
use ::cloudflare::framework::{Environment, HttpApiClient, HttpApiClientConfig};
use async_graphql::{Context, Error, Object, Result};
use hickory_resolver::error::ResolveErrorKind;
use tonic::Request;

use nirvati_apps::metadata::AppScope;

use nirvati_db::schema::app_installation;
use nirvati_db::sea_orm::prelude::*;
use nirvati_db::sea_orm::DatabaseConnection;

use crate::api;
use crate::api::{AddDomainRequest, DomainType, EntropyRequest, RemoveDomainRequest};
use crate::api::{BuiltInAcmeProvider, DnsProviderConfiguration};
use crate::apps::get_app;
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::{entropy_client, https_client, ingress_client};
use crate::graphql::mutation::apps::install_app;
use crate::graphql::mutation::https::cloudflare::create_txt_record;
use crate::graphql::query::https::acme_provider::AcmeProvider;
use crate::graphql::query::https::user_domain::DnsProvider;
use crate::graphql::query::nirvati_me;
use crate::utils::user_state::{get_user_state, get_user_state_by_username};
use nirvati::permissions::Permission;
use nirvati_db::schema::{acme_account, app_domain, domain};
use nirvati_db::sea_orm::ActiveValue::Set;

mod cloudflare;

#[derive(Default)]
pub struct Https;

impl From<nirvati_db::schema::sea_orm_active_enums::AcmeProvider> for BuiltInAcmeProvider {
    fn from(value: nirvati_db::schema::sea_orm_active_enums::AcmeProvider) -> Self {
        match value {
            nirvati_db::schema::sea_orm_active_enums::AcmeProvider::LetsEncrypt => {
                BuiltInAcmeProvider::LetsEncrypt
            }
            nirvati_db::schema::sea_orm_active_enums::AcmeProvider::BuyPass => {
                BuiltInAcmeProvider::BuyPass
            }
        }
    }
}

impl From<nirvati_db::schema::sea_orm_active_enums::DomainType> for DomainType {
    fn from(value: nirvati_db::schema::sea_orm_active_enums::DomainType) -> Self {
        match value {
            nirvati_db::schema::sea_orm_active_enums::DomainType::Http => DomainType::DomainHttp,
            nirvati_db::schema::sea_orm_active_enums::DomainType::Https => DomainType::DomainHttps,
            nirvati_db::schema::sea_orm_active_enums::DomainType::Onion => DomainType::DomainOnion,
            nirvati_db::schema::sea_orm_active_enums::DomainType::Tcp => DomainType::DomainTcp,
        }
    }
}

impl From<DomainType> for nirvati_db::schema::sea_orm_active_enums::DomainType {
    fn from(value: DomainType) -> Self {
        match value {
            DomainType::DomainHttp => nirvati_db::schema::sea_orm_active_enums::DomainType::Http,
            DomainType::DomainHttps => nirvati_db::schema::sea_orm_active_enums::DomainType::Https,
            DomainType::DomainOnion => nirvati_db::schema::sea_orm_active_enums::DomainType::Onion,
            DomainType::DomainTcp => nirvati_db::schema::sea_orm_active_enums::DomainType::Tcp,
        }
    }
}

pub async fn add_app_domain(
    ctx: &Context<'_>,
    app_id: String,
    domain: String,
    acme_provider: AcmeProvider,
    custom_prefix: Option<String>,
    force_http: bool,
    app_component: Option<String>,
) -> Result<bool> {
    let app = get_app(ctx, &app_id).await?.metadata;
    let user_name = ctx.data_unchecked::<UserCtx>().username.clone();
    let owner_name = if app.scope == AppScope::System {
        "system".to_string()
    } else {
        user_name.clone()
    };
    let client = ctx.data_unchecked::<DatabaseConnection>();
    let installation = app_installation::Entity::find()
        .filter(app_installation::Column::AppId.eq(app_id.clone()))
        .filter(app_installation::Column::OwnerName.eq(owner_name.clone()))
        .one(client)
        .await?
        .ok_or(Error::new("Failed to find app installation"))?;
    if !app.supports_ingress {
        return Err(Error::new(
            "Cannot add domain to an app that doesn't support ingress",
        ));
    }
    let mut ingress_client = ingress_client(ctx)?;
    let sld = domain.split('.').rev().take(2).collect::<Vec<_>>();
    let sld = sld.into_iter().rev().collect::<Vec<_>>().join(".");
    let mut potential_parents = domain::Entity::find()
        .filter(domain::Column::Name.ends_with(sld))
        .filter(domain::Column::OwnerName.eq(user_name.clone()))
        .all(client)
        .await
        .map_err::<Error, _>(|_| "Failed to get potential parent domains from DB".into())?;
    if potential_parents.is_empty()
        && !ctx
            .data_unchecked::<UserCtx>()
            .permissions
            .has(Permission::AddTrustedDomains)
    {
        return Err(Error::new("You do not have the permission to add domains whose ownership you have not verified to apps"));
    }
    potential_parents.sort_by(|potential_parent_a, potential_parent_b| {
        potential_parent_a
            .name
            .len()
            .cmp(&potential_parent_b.name.len())
    });
    let user_state = if app.scope == AppScope::System {
        get_user_state_by_username(ctx, "system".to_string()).await
    } else {
        get_user_state(ctx).await
    };
    let domain_type = if force_http {
        DomainType::DomainHttp
    } else if app.raw_ingress {
        DomainType::DomainTcp
    } else {
        DomainType::DomainHttps
    };
    let acme_account = acme_account::Entity::find()
        .filter(acme_account::Column::OwnerName.eq(user_name.clone()))
        .filter(
            acme_account::Column::Provider
                .eq::<nirvati_db::schema::sea_orm_active_enums::AcmeProvider>(acme_provider.into()),
        )
        .one(client)
        .await?
        .ok_or(Error::new("Failed to find ACME account"))?;
    let chosen_parent = potential_parents.into_iter().last();
    ingress_client
        .add_domain(Request::new(AddDomainRequest {
            user_state: Some(user_state),
            id: app_id.clone(),
            domain: Some(domain.clone()),
            protect: installation.protected,
            custom_prefix: custom_prefix.clone(),
            acme_provider: Some(acme_provider.into()),
            parent: chosen_parent.as_ref().map(|p| p.name.clone()),
            r#type: domain_type as i32,
            cert_user: Some(user_name.clone()),
            app_component: app_component.clone(),
        }))
        .await?;
    app_domain::Entity::insert(app_domain::ActiveModel {
        name: Set(domain),
        // TODO: Rename this DB col to parent_domain_id
        parent_domain_name: Set(chosen_parent.map(|parent| parent.id)),
        app_installation_id: Set(installation.id),
        app_component_id: Set(app_component),
        acme_account_id: Set(Some(acme_account.id)),
        custom_prefix: Set(custom_prefix),
        r#type: Set(nirvati_db::schema::sea_orm_active_enums::DomainType::Https),
        ..Default::default()
    })
    .exec(client)
    .await?;
    Ok(true)
}

#[Object(name = "HttpsMutations")]
impl Https {
    #[graphql(guard = "PermissionGuard::new(Permission::ManageIssuers)")]
    async fn create_acme_issuer(
        &self,
        ctx: &Context<'_>,
        provider: AcmeProvider,
        share_email: Option<bool>,
    ) -> Result<bool> {
        let user_id = ctx.data_unchecked::<UserCtx>().username.clone();
        let client = ctx.data_unchecked::<DatabaseConnection>();
        let share_email = share_email.unwrap_or(false);
        if share_email && ctx.data_unchecked::<UserCtx>().email.is_none() {
            return Err(Error::new(
                "You must have set an email address to share it with ACME providers",
            ));
        };
        if provider == AcmeProvider::BuyPass && !share_email {
            return Err(Error::new(
                "You must share your email address with BuyPass to use it as an ACME provider",
            ));
        }
        let mut https_client = https_client(ctx)?;
        https_client
            .add_issuer(Request::new(api::AddUserIssuerRequest {
                user: user_id.clone(),
                provider: Some(provider.into()),
                email: if share_email {
                    ctx.data_unchecked::<UserCtx>().email.clone()
                } else {
                    None
                },
            }))
            .await
            .map_err::<Error, _>(|_| "Failed to add issuer".into())?;
        acme_account::Entity::insert(acme_account::ActiveModel {
            provider: Set(provider.into()),
            owner_name: Set(user_id.clone()),
            share_email: Set(share_email),
            ..Default::default()
        })
        .exec(client)
        .await?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageIssuers)")]
    async fn delete_acme_issuer(&self, ctx: &Context<'_>, provider: AcmeProvider) -> Result<bool> {
        let user_id = ctx.data_unchecked::<UserCtx>().username.clone();
        let client = ctx.data_unchecked::<DatabaseConnection>();
        acme_account::Entity::delete_many()
            .filter(acme_account::Column::OwnerName.eq(user_id.clone()))
            .filter(
                acme_account::Column::Provider
                    .eq::<nirvati_db::schema::sea_orm_active_enums::AcmeProvider>(provider.into()),
            )
            .exec(client)
            .await?;
        let mut https_client = https_client(ctx)?;
        https_client
            .delete_issuer(Request::new(api::DeleteUserIssuerRequest {
                user: user_id,
                provider: Some(provider.into()),
            }))
            .await
            .map_err::<Error, _>(|_| "Failed to add issuer".into())?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn add_app_domain(
        &self,
        ctx: &Context<'_>,
        app: String,
        domain: String,
        acme_provider: AcmeProvider,
        custom_prefix: Option<String>,
        force_http_only: bool,
        app_component: Option<String>,
    ) -> Result<bool> {
        add_app_domain(
            ctx,
            app,
            domain,
            acme_provider,
            custom_prefix,
            force_http_only,
            app_component,
        )
        .await
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn remove_app_domain(
        &self,
        ctx: &Context<'_>,
        domain: String,
        app_component: Option<String>,
    ) -> Result<bool> {
        let user_id = ctx.data_unchecked::<UserCtx>().username.clone();
        let client = ctx.data_unchecked::<DatabaseConnection>();
        let mut ingress_client = ingress_client(ctx)?;
        let (domain_info, app_install) = app_domain::Entity::find()
            .filter(app_domain::Column::Name.eq(domain.clone()))
            .find_also_related(app_installation::Entity)
            .one(client)
            .await?
            .ok_or::<Error>("Domain not found".into())?;
        let Some(app_install) = app_install else {
            return Err(Error::new("Domain not found"));
        };
        if app_install.owner_name != user_id
            && (app_install.owner_name != "system"
                || !ctx
                    .data_unchecked::<UserCtx>()
                    .permissions
                    .has(Permission::ManageSysComponents))
        {
            return Err(Error::new("Domain not found"));
        };
        let domain_type: DomainType = domain_info.r#type.clone().into();
        domain_info.delete(client).await?;
        let user_state = get_user_state_by_username(ctx, app_install.owner_name).await;
        ingress_client
            .remove_domain(Request::new(RemoveDomainRequest {
                user_state: Some(user_state),
                id: app_install.app_id,
                domain,
                r#type: domain_type as i32,
                app_component,
            }))
            .await
            .map_err::<Error, _>(|_| "Failed to remove domain from app".into())?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageDomains)")]
    async fn add_domain(
        &self,
        ctx: &Context<'_>,
        domain: String,
        dns_provider: Option<DnsProvider>,
        dns_auth: Option<String>,
        // TODO: This option was never handled properly, we need to re-add it in a future release
        //acme_providers: Vec<AcmeProvider>,
    ) -> Result<bool> {
        let user = ctx.data_unchecked::<UserCtx>();
        let user_id = user.username.clone();
        let user_email = user.email.clone();
        let db = ctx.data_unchecked::<DatabaseConnection>();
        if dns_provider == Some(DnsProvider::NirvatiMe) {
            let has_nirvati_me = app_installation::Entity::find()
                .filter(app_installation::Column::AppId.eq("nirvati-me".to_string()))
                .filter(app_installation::Column::OwnerName.eq("system".to_string()))
                .one(db)
                .await?
                .is_some();
            if !has_nirvati_me {
                if ctx
                    .data_unchecked::<UserCtx>()
                    .permissions
                    .has(Permission::ManageApps)
                {
                    install_app(
                        ctx,
                        "nirvati-me".into(),
                        serde_json::Value::Null,
                        None,
                        None,
                        true,
                        false,
                    )
                    .await?;
                } else {
                    return Err(Error::new("You must have the Nirvati.me integration installed to use it as a DNS provider"));
                }
            };
            let mut client = entropy_client(ctx)?;
            let username = client
                .derive_entropy(Request::new(EntropyRequest {
                    input: format!("{}:nirvati.me-username", user_id),
                }))
                .await
                .map(|response| response.into_inner().entropy)
                .map_err::<Error, _>(|_| Error::new("Failed to derive entropy"))?;
            let password = client
                .derive_entropy(Request::new(EntropyRequest {
                    input: format!("{}:nirvati.me-password", user_id),
                }))
                .await
                .map(|response| response.into_inner().entropy)
                .map_err::<Error, _>(|_| Error::new("Failed to derive entropy"))?;
            let Ok(result) = nirvati_me::get_details(&username, &password).await else {
                return Err(Error::new("Failed to verify your nirvati.me account!"));
            };
            if domain != format!("{}.nirvati.me", result.name) {
                return Err(Error::new("Tried to add an invalid domain!"));
            };
        } else if !user.permissions.has(Permission::AddTrustedDomains) {
            let mut client = entropy_client(ctx)?;
            let verification_code = client
                .derive_entropy(Request::new(EntropyRequest {
                    input: format!("{}:domain:{}", &user_id, domain),
                }))
                .await
                .map(|response| response.into_inner().entropy)
                .map_err::<Error, _>(|_| Error::new("Failed to derive entropy"))?;
            let resolver = hickory_resolver::AsyncResolver::tokio_from_system_conf()
                .map_err::<Error, _>(|_| Error::new("Failed to create DNS resolver"))?;
            let value = resolver
                .txt_lookup(&domain)
                .await
                .map_err::<Error, _>(|e| match e.kind() {
                    ResolveErrorKind::NoRecordsFound { .. } => Error::new(
                        "Failed to query TXT verification record: Record does not exist!",
                    ),
                    _ => Error::new(format!("Failed to query TXT verification record: {:#?}", e)),
                })?;
            let verification_code = format!("nirvati-verification={}", verification_code);
            if !value.iter().any(|val| val.to_string() == verification_code) {
                tracing::debug!(
                    "{}",
                    value
                        .iter()
                        .map(|val| val.to_string())
                        .collect::<Vec<_>>()
                        .join("; ")
                );
                tracing::debug!("Expected {}", verification_code);
                return Err(Error::new("Failed to verify domain ownership"));
            };
        };
        domain::Entity::insert(domain::ActiveModel {
            name: Set(domain.clone()),
            owner_name: Set(user_id.clone()),
            provider: Set(dns_provider
                .map(|provider| provider.into())
                .unwrap_or(nirvati_db::schema::sea_orm_active_enums::DnsProvider::None)),
            provider_auth: Set(dns_auth.clone()),
            ..Default::default()
        })
        .exec(db)
        .await?;
        tracing::info!("Created domain");
        let acme_accounts = acme_account::Entity::find()
            .filter(acme_account::Column::OwnerName.eq(user_id.clone()))
            .all(db)
            .await?;
        let mut https_client = https_client(ctx)?;
        let dns_provider: Option<api::DnsProvider> = dns_provider.map(|provider| provider.into());
        for acme_account in acme_accounts {
            let api_acme_provider: BuiltInAcmeProvider = acme_account.provider.into();
            let share_email = acme_account.share_email;
            if share_email && user_email.is_none() {
                return Err(Error::new(
                    "You must have set an email address to share it with ACME providers",
                ));
            };
            if dns_provider.is_some() && dns_auth.is_none() {
                return Err(Error::new(
                    "You must provide authentication for the DNS provider",
                ));
            }
            https_client
                .add_domain_issuer(Request::new(api::CreateDomainIssuerRequest {
                    user: user_id.clone(),
                    domain: domain.clone(),
                    dns_provider: dns_provider.map(|dns_provider| DnsProviderConfiguration {
                        dns_provider: dns_provider.into(),
                        auth: dns_auth.clone().unwrap(),
                    }),
                    email: if share_email {
                        user_email.clone()
                    } else {
                        None
                    },
                    acme_provider: Some(api::AcmeProvider {
                        provider: Some(api::acme_provider::Provider::BuiltIn(
                            api_acme_provider.into(),
                        )),
                    }),
                }))
                .await
                .map_err::<Error, _>(|_| "Failed to add domain issuer".into())?;
        }
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageDomains)")]
    async fn remove_domain(&self, ctx: &Context<'_>, domain: String) -> Result<bool> {
        let user_id = ctx.data_unchecked::<UserCtx>().username.clone();
        let db = ctx.data_unchecked::<DatabaseConnection>();
        let acme_accounts = acme_account::Entity::find()
            .filter(acme_account::Column::OwnerName.eq(user_id.clone()))
            .all(db)
            .await?;
        domain::Entity::delete_many()
            .filter(domain::Column::Name.eq(domain.clone()))
            .filter(domain::Column::OwnerName.eq(user_id.clone()))
            .exec(db)
            .await?;
        let mut https_client = https_client(ctx)?;
        for acme_account in acme_accounts {
            let api_acme_provider: BuiltInAcmeProvider = acme_account.provider.into();
            if let Err(e) = https_client
                .remove_domain_issuer(Request::new(api::DeleteDomainIssuerRequest {
                    user: user_id.clone(),
                    domain: domain.clone(),
                    acme_provider: Some(api::AcmeProvider {
                        provider: Some(api::acme_provider::Provider::BuiltIn(
                            api_acme_provider.into(),
                        )),
                    }),
                }))
                .await
            {
                tracing::error!(
                    "Failed to remove domain issuer for {} with provider {:?}: {:?}",
                    domain,
                    api_acme_provider,
                    e
                );
            }
        }
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageDomains)")]
    async fn add_verification_txt(
        &self,
        ctx: &Context<'_>,
        domain: String,
        dns_provider: DnsProvider,
        dns_auth: String,
    ) -> Result<bool> {
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let mut client = entropy_client(ctx)?;
        let verification_code = client
            .derive_entropy(Request::new(EntropyRequest {
                input: format!("{}:domain:{}", &username, domain),
            }))
            .await
            .map(|response| response.into_inner().entropy)
            .map_err::<Error, _>(|_| Error::new("Failed to derive entropy"))?;
        let verification_code = format!("nirvati-verification={}", verification_code);
        match dns_provider {
            DnsProvider::Cloudflare => {
                let api_client = HttpApiClient::new(
                    Credentials::UserAuthToken { token: dns_auth },
                    HttpApiClientConfig::default(),
                    Environment::Production,
                )?;
                let Some(real_domain) = psl::domain(domain.as_ref()) else {
                    return Err(Error::new("Failed to parse domain"));
                };
                let real_domain_str =
                    String::from_utf8(real_domain.trim().as_bytes().to_vec())
                        .map_err::<Error, _>(|_| Error::new("Failed to parse domain"))?;
                let zones_endpoint = ListZones {
                    params: ListZonesParams {
                        name: Some(real_domain_str.clone()),
                        ..Default::default()
                    },
                };
                let zones = api_client.request(&zones_endpoint)?.result;
                if zones.is_empty() {
                    return Err(Error::new("Failed to find zone"));
                }
                let zone = &zones[0];
                let mut subdomain = domain
                    .trim_end_matches(&format!(".{}", real_domain_str))
                    .trim_end_matches('.')
                    .to_string();
                if subdomain == real_domain_str {
                    subdomain = "@".to_string();
                };
                let resp = create_txt_record(&zone.id, &subdomain, verification_code, &api_client)?;
                if !resp.errors.is_empty() {
                    tracing::error!("Failed to add DNS record: {:?}", resp.errors);
                };
                Ok(true)
            }
            DnsProvider::NirvatiMe => Ok(true),
        }
    }
}
