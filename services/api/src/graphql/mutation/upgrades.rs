// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Context, Object, Result};

use crate::api::VersionInfo;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::upgrades_client;
use nirvati::permissions::Permission;

#[derive(Default)]
pub struct Upgrades;

#[Object(name = "UpgradeMutations")]
impl Upgrades {
    #[graphql(guard = "PermissionGuard::new(Permission::ManageSysComponents)")]
    async fn upgrade_kube(&self, ctx: &Context<'_>, target_version: String) -> Result<bool> {
        let mut client = upgrades_client(ctx)?;
        client
            .update_kube(tonic::Request::new(VersionInfo {
                version: target_version,
            }))
            .await
            .map(|_| true)
            .map_err(|err| err.to_string().into())
    }
}
