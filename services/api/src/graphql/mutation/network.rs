// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::api::Empty;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::tailscale_client;
use async_graphql::{Context, Object, Result};
use nirvati::permissions::Permission;

#[derive(Default)]
pub struct Network;

#[Object(name = "NetworkMutations")]
impl Network {
    #[graphql(guard = "PermissionGuard::new(Permission::ConfigureNetwork)")]
    async fn start_tailscale_login(&self, ctx: &Context<'_>) -> Result<bool> {
        let mut client = tailscale_client(ctx)?;
        client
            .start_login(tonic::Request::new(Empty {}))
            .await
            .map(|_| true)
            .map_err(|err| {
                let msg = err.message();
                async_graphql::Error::new(msg)
            })
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ConfigureNetwork)")]
    async fn start_tailscale(&self, ctx: &Context<'_>) -> Result<bool> {
        let mut client = tailscale_client(ctx)?;
        client.add_ip_to_metallb(Empty {}).await.map_err(|err| {
            let msg = err.message();
            async_graphql::Error::new(msg)
        })?;
        client
            .start(tonic::Request::new(Empty {}))
            .await
            .map(|_| true)
            .map_err(|err| {
                let msg = err.message();
                async_graphql::Error::new(msg)
            })
    }
}
