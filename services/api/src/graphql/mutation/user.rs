// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Context, Error, Object, Result};
use base64::{engine::general_purpose::URL_SAFE, Engine};
use biscuit::jwa::*;
use biscuit::jws::*;
use biscuit::*;
use tonic::Request;

use nirvati_db::sea_orm::prelude::*;

use crate::api::{
    acme_provider, AcmeProvider, AddUserAuthRequest, AddUserNsRequest, DeleteUserAuthRequest,
};
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::{IsLoggedInGuard, PermissionGuard};
use crate::graphql::clients::{https_client, manage_client, middlewares_client, users_client};
use crate::passkeys::{create_new_challenge, verify_challenge};
use crate::utils::email::validate_email;
use crate::USERNAME_REGEX;
use nirvati::permissions::{Permission, PermissionSet};
use nirvati::JwtAdditionalClaims;
use nirvati_db::schema::{app_installation, domain, passkey, prelude::*, user, user_group};
use nirvati_db::sea_orm::ActiveValue::Set;
use nirvati_db::sea_orm::{EntityOrSelect, IntoActiveModel};

#[derive(Default)]
pub struct User;

async fn issue_token(
    secret: &Secret,
    username: String,
    permissions: u16,
    user_group: String,
    email: Option<String>,
    is_totp_enabled: bool,
) -> Result<String> {
    let claims = ClaimsSet::<JwtAdditionalClaims> {
        registered: RegisteredClaims {
            issuer: Some("nirvati".to_string()),
            subject: Some(username),
            issued_at: Some(chrono::Utc::now().into()),
            expiry: Some((chrono::Utc::now() + chrono::Duration::minutes(10)).into()),
            audience: Some(SingleOrMultiple::Multiple(vec![
                "api".to_string(),
                "dashboard".to_string(),
            ])),
            ..Default::default()
        },
        private: JwtAdditionalClaims {
            user_group,
            permission_set: permissions.into(),
            email,
            is_totp_enabled,
            is_mock_user: None,
        },
    };
    let token = JWT::new_decoded(
        From::from(RegisteredHeader {
            algorithm: SignatureAlgorithm::RS512,
            ..Default::default()
        }),
        claims.clone(),
    );

    let token = token
        .into_encoded(secret)
        .map_err(|_| Error::new("Failed to encode token"))?;
    Ok(token.unwrap_encoded().to_string())
}

#[Object(name = "UserMutations")]
impl User {
    #[graphql(guard = "PermissionGuard::new(Permission::ManageUsers)")]
    async fn create_user(
        &self,
        ctx: &Context<'_>,
        username: String,
        password: String,
        name: String,
        email: Option<String>,
        permissions: Option<Vec<Permission>>,
        group: Option<String>,
        show_on_login_screen: bool,
        avatar: Option<String>,
    ) -> Result<bool> {
        // Reserve some dangerous namespaces and always-installed system components
        if username == "default"
            || username.starts_with("kube-")
            || username == "system"
            || username == "nirvati"
            || username == "coredns"
            || username == "longhorn"
            || username == "longhorn-system"
            || username == "cert-manager"
            || username == "system-upgrade-controller"
            || username == "nirvati-telemetry"
            || username == "metallb"
            || username == "local-path-provisioner"
        {
            return Err(Error::new("Username is reserved"));
        };
        if !USERNAME_REGEX.is_match(&username)? {
            return Err(Error::new(
                "Username must only consist of lowercase alphanumeric characters, '-' or '.'",
            ));
        }
        let user = ctx.data::<UserCtx>()?;
        if !user.permissions.has(Permission::ManageGroups)
            && group.as_ref().is_some_and(|g| g != &user.user_group)
        {
            return Err(Error::new("Forbidden"));
        }
        // TODO: Technically, ManageUsers allows privilege escalation by changing the password of a user with higher permissions
        // But we're not going to worry about that for now
        for permission in &permissions.clone().unwrap_or_default() {
            if !user.permissions.has(*permission) {
                return Err(Error::new("Forbidden"));
            }
        }
        let user_group = group.unwrap_or_else(|| user.user_group.clone());
        let client = ctx.data::<DatabaseConnection>()?;
        let has_existing_user = user::Entity::find_by_id(&username).count(client).await? > 0;
        if has_existing_user {
            return Err(Error::new("User already exists"));
        }
        let has_sys_app_with_same_name = app_installation::Entity::find()
            .filter(app_installation::Column::AppId.eq(&username))
            .filter(app_installation::Column::OwnerName.eq("system"))
            .count(client)
            .await?
            > 0;
        if has_sys_app_with_same_name {
            return Err(Error::new("App with same name already exists"));
        };
        if let Some(email) = &email {
            let is_valid = validate_email(email);
            if !is_valid {
                return Err(Error::new("Invalid email"));
            }
        }
        user::Entity::insert(user::ActiveModel {
            username: Set(username.clone()),
            name: Set(name.clone()),
            email: Set(email.clone()),
            password: Set(bcrypt::hash(password, bcrypt::DEFAULT_COST)
                .map_err(|_| Error::new("Failed to hash password"))?),
            is_enabled: Set(true),
            user_group_id: Set(user_group.clone()),
            permissions: Set(permissions
                .map(|permissions| {
                    Into::<u16>::into(Into::<PermissionSet>::into(permissions.as_slice())) as i32
                })
                .unwrap_or(0)),
            is_public: Set(show_on_login_screen),
            avatar: Set(avatar.and_then(|avatar| {
                URL_SAFE
                    .decode(avatar.as_bytes())
                    .map_err(|_| Error::new("Failed to decode avatar"))
                    .ok()
            })),
            ..Default::default()
        })
        .exec(client)
        .await?;
        let mut users_client = users_client(ctx)?;
        users_client
            .create_user_namespace(Request::new(AddUserNsRequest {
                user: username.clone(),
            }))
            .await
            .map_err(|_| Error::new("Failed to create user in manager"))?;
        let mut manage_client = manage_client(ctx)?;
        manage_client
            .init_user(Request::new(crate::api::InitUserRequest {
                user_id: username,
                user_display_name: name,
            }))
            .await
            .map_err(|_| Error::new("Failed to preload apps"))?;
        Ok(true)
    }

    #[graphql(guard = "IsLoggedInGuard::new()")]
    async fn update_user(
        &self,
        ctx: &Context<'_>,
        username: Option<String>,
        password: Option<String>,
        name: Option<String>,
        email: Option<String>,
        erase_email: Option<bool>,
        permissions: Option<Vec<Permission>>,
        group: Option<String>,
        show_on_login_screen: Option<bool>,
        avatar: Option<String>,
    ) -> Result<bool> {
        let user = ctx.data::<UserCtx>()?;
        let username = username.unwrap_or(user.username.clone());
        if username != user.username && !user.permissions.has(Permission::ManageUsers) {
            return Err(Error::new("Forbidden"));
        }
        if username != user.username
            && !user.permissions.has(Permission::ManageGroups)
            && group.as_ref().is_some_and(|g| g != &user.user_group)
        {
            return Err(Error::new("Forbidden"));
        }
        let client = ctx.data::<DatabaseConnection>()?;
        let current_user = user.get_details(client).await?;
        if current_user.user_group_id != user.user_group
            && !user.permissions.has(Permission::ManageGroups)
        {
            return Err(Error::new("Forbidden"));
        }
        let mut settings: user::ActiveModel = current_user.into_active_model();

        if let Some(name) = name {
            settings.name = Set(name);
        }
        if (erase_email.unwrap_or(false) && email.is_none())
            || email.as_ref().is_some_and(|email| email.trim().is_empty())
        {
            settings.email = Set(None);
        } else if let Some(email) = email {
            let is_valid = validate_email(&email);
            if !is_valid {
                return Err(Error::new("Invalid email"));
            }
            settings.email = Set(Some(email));
        }

        if let Some(password) = password {
            settings.password = Set(bcrypt::hash(&password, bcrypt::DEFAULT_COST)
                .map_err(|_| Error::new("Failed to hash password"))?);
            let mut mw_client = middlewares_client(ctx)?;
            mw_client
                .add_user_auth(Request::new(AddUserAuthRequest {
                    user: username.clone(),
                    password,
                }))
                .await?;
        }
        if let Some(permissions) = permissions {
            for permission in &permissions {
                if !user.permissions.has(*permission) && !user.permissions.has(Permission::Escalate)
                {
                    return Err(Error::new("Forbidden"));
                }
            }
            settings.permissions =
                Set(Into::<u16>::into(Into::<PermissionSet>::into(permissions.as_slice())) as i32);
        }
        if let Some(group) = group {
            settings.user_group_id = Set(group);
        }

        if let Some(show_on_login_screen) = show_on_login_screen {
            settings.is_public = Set(show_on_login_screen);
        }

        if let Some(avatar) = avatar {
            settings.avatar = Set(Some(
                URL_SAFE
                    .decode(avatar.as_bytes())
                    .map_err(|_| Error::new("Failed to decode avatar"))?,
            ));
        }

        user::Entity::update(settings).exec(client).await?;

        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageUsers)")]
    async fn delete_user(&self, ctx: &Context<'_>, username: String) -> Result<bool> {
        let user = ctx.data::<UserCtx>()?;
        let client = ctx.data::<DatabaseConnection>()?;
        let current_user = user.get_details(client).await?;
        if current_user.user_group_id != user.user_group
            && !user.permissions.has(Permission::ManageGroups)
        {
            return Err(Error::new("Forbidden"));
        }
        let user_issuers = Domain::find()
            .filter(domain::Column::OwnerName.eq(username.clone()))
            .select()
            .all(client)
            .await?;
        for issuer in user_issuers {
            let mut https_client = https_client(ctx)?;
            https_client
                .remove_domain_issuer(crate::api::DeleteDomainIssuerRequest {
                    domain: issuer.name.clone(),
                    user: username.clone(),
                    // TODO: Store issuer again
                    acme_provider: Some(AcmeProvider {
                        provider: Some(acme_provider::Provider::BuiltIn(
                            crate::api::BuiltInAcmeProvider::LetsEncrypt as i32,
                        )),
                    }),
                })
                .await?;
            issuer.delete(client).await?;
        }
        user::Entity::delete_by_id(username.clone())
            .exec(client)
            .await
            .map_err(|_| Error::new("Failed to delete user"))?;

        let mut users_client = users_client(ctx)?;
        users_client
            .delete_user_namespace(Request::new(crate::api::DeleteUserNsRequest {
                user: username.clone(),
            }))
            .await
            .map_err(|_| Error::new("Failed to delete user in manager"))?;
        let mut mw_client = middlewares_client(ctx)?;
        mw_client
            .remove_user_auth(Request::new(DeleteUserAuthRequest {
                user: username.clone(),
            }))
            .await?;
        let mut manage_client = manage_client(ctx)?;
        manage_client
            .delete_user(Request::new(crate::api::DeleteUserRequest {
                user: username,
            }))
            .await
            .map_err(|_| Error::new("Failed to delete user in manager"))?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageGroups)")]
    async fn create_user_group(&self, ctx: &Context<'_>, name: String) -> Result<bool> {
        let client = ctx.data::<DatabaseConnection>()?;
        UserGroup::insert(user_group::ActiveModel {
            name: Set(name),
            ..Default::default()
        })
        .exec(client)
        .await?;
        Ok(true)
    }

    async fn login(
        &self,
        ctx: &Context<'_>,
        username: String,
        password: String,
    ) -> Result<Option<String>> {
        let client = ctx.data::<DatabaseConnection>()?;
        let user = user::Entity::find_by_id(&username)
            .one(client)
            .await
            .map_err(|_| Error::new("Failed to find user"))?
            .ok_or(Error::new("Failed to find user"))?;
        if bcrypt::verify(password, &user.password)
            .map_err(|_| Error::new("Failed to verify password"))?
        {
            if user.totp_secret.is_some() {
                Ok(None)
            } else {
                let secret = ctx.data_unchecked::<Secret>();
                Ok(Some(
                    issue_token(
                        secret,
                        username,
                        user.permissions as u16,
                        user.user_group_id,
                        user.email,
                        false,
                    )
                    .await?,
                ))
            }
        } else {
            Err(Error::new("Invalid password"))
        }
    }

    async fn create_challenge(&self, ctx: &Context<'_>, username: String) -> Result<String> {
        let redis_client = ctx.data_unchecked::<redis::Client>();
        Ok(create_new_challenge(redis_client, &username).await?)
    }

    async fn login_with_passkey(
        &self,
        ctx: &Context<'_>,
        client_data_json: String,
        authenticator_data: String,
        signature: String,
        key_id: String,
    ) -> Result<String> {
        let db_client = ctx.data::<DatabaseConnection>()?;
        let redis_client = ctx.data_unchecked::<redis::Client>();
        let username = verify_challenge(
            db_client,
            redis_client,
            &client_data_json,
            &authenticator_data,
            &key_id,
            &signature,
        )
        .await?;
        if let Some(username) = username {
            let user = user::Entity::find_by_id(&username)
                .one(db_client)
                .await
                .map_err(|_| Error::new("Failed to find user"))?
                .ok_or(Error::new("Failed to find user"))?;
            let secret = ctx.data_unchecked::<Secret>();
            issue_token(
                secret,
                username,
                user.permissions as u16,
                user.user_group_id,
                user.email,
                // Passkey bypasses TOTP currently
                true,
            )
            .await
        } else {
            Err(Error::new("Invalid signature"))
        }
    }

    #[graphql(guard = "IsLoggedInGuard::new()")]
    async fn add_pubkey(
        &self,
        ctx: &Context<'_>,
        display_name: String,
        key_id: String,
        pubkey_der: String,
        rp_id: String,
    ) -> Result<bool> {
        let db_client = ctx.data::<DatabaseConnection>()?;
        let user = ctx.data::<UserCtx>()?;

        let decoded_id = URL_SAFE.decode(key_id)?;
        let decoded_key = URL_SAFE.decode(pubkey_der)?;
        let key = openssl::pkey::PKey::public_key_from_der(&decoded_key)?;
        let raw_key = key.raw_public_key()?;
        passkey::Entity::insert(passkey::ActiveModel {
            credential_id: Set(decoded_id.clone()),
            owner_name: Set(user.username.clone()),
            public_key: Set(raw_key),
            display_name: Set(display_name),
            rp_id: Set(rp_id),
            sign_count: Set(0),
            ..Default::default()
        })
        .exec(db_client)
        .await?;
        Ok(true)
    }

    #[graphql(guard = "IsLoggedInGuard::new()")]
    async fn remove_pubkey(&self, ctx: &Context<'_>, key_id: String) -> Result<bool> {
        let db_client = ctx.data::<DatabaseConnection>()?;
        let user = ctx.data::<UserCtx>()?;
        let decoded_id = URL_SAFE.decode(key_id)?;
        passkey::Entity::delete_many()
            .filter(passkey::Column::CredentialId.eq(decoded_id))
            .filter(passkey::Column::OwnerName.eq(user.username.clone()))
            .exec(db_client)
            .await?;
        Ok(true)
    }

    #[graphql(guard = "IsLoggedInGuard::new()")]
    async fn generate_totp_url(&self, ctx: &Context<'_>) -> Result<String> {
        let user = ctx.data::<UserCtx>()?;
        let secret = totp_rs::Secret::generate_secret();
        let totp = totp_rs::TOTP::new(
            totp_rs::Algorithm::SHA256,
            6,
            1,
            30,
            secret.to_bytes().unwrap(),
            Some("nirvati".to_string()),
            user.username.clone(),
        )?;
        Ok(totp.get_url())
    }

    #[graphql(guard = "IsLoggedInGuard::new()")]
    async fn save_totp(&self, ctx: &Context<'_>, secret: String, code: String) -> Result<bool> {
        let user = ctx.data::<UserCtx>()?;
        let db_client = ctx.data::<DatabaseConnection>()?;
        if user.get_details(db_client).await?.totp_secret.is_some() {
            return Err(Error::new("This account already uses 2FA"));
        }
        let secret_bytes = totp_rs::Secret::Encoded(secret).to_bytes()?;
        let totp = totp_rs::TOTP::new(
            totp_rs::Algorithm::SHA256,
            6,
            1,
            30,
            secret_bytes.clone(),
            Some("nirvati".to_string()),
            user.username.clone(),
        )?;
        let timestamp_now = std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)?
            .as_secs();
        if totp.check(&code, timestamp_now) {
            let db_client = ctx.data::<DatabaseConnection>()?;
            let existing_user = user::Entity::find()
                .filter(user::Column::Username.eq(user.username.clone()))
                .one(db_client)
                .await
                .map_err(|_| Error::new("Failed to find user"))?
                .ok_or(Error::new("Failed to find user"))?;
            let mut existing_user = existing_user.into_active_model();
            existing_user.totp_secret = Set(Some(secret_bytes));
            user::Entity::update(existing_user).exec(db_client).await?;
            Ok(true)
        } else {
            Err(Error::new("Invalid code"))
        }
    }

    #[graphql(guard = "IsLoggedInGuard::new()")]
    async fn remove_totp(&self, ctx: &Context<'_>, code: String) -> Result<bool> {
        let user = ctx.data::<UserCtx>()?;
        let db_client = ctx.data::<DatabaseConnection>()?;
        let Some(secret_bytes) = user.get_details(db_client).await?.totp_secret else {
            return Err(Error::new("This account already does not use 2FA"));
        };
        let totp = totp_rs::TOTP::new(
            totp_rs::Algorithm::SHA256,
            6,
            1,
            30,
            secret_bytes.clone(),
            Some("nirvati".to_string()),
            user.username.clone(),
        )?;
        let timestamp_now = std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)?
            .as_secs();
        if totp.check(&code, timestamp_now) {
            let mut existing_user = user::Entity::find()
                .filter(user::Column::Username.eq(user.username.clone()))
                .one(db_client)
                .await
                .map_err(|_| Error::new("Failed to find user"))?
                .ok_or(Error::new("Failed to find user"))?
                .into_active_model();
            existing_user.totp_secret = Set(None);
            user::Entity::update(existing_user).exec(db_client).await?;
            Ok(true)
        } else {
            Err(Error::new("Invalid code"))
        }
    }
    async fn login_with_totp(
        &self,
        ctx: &Context<'_>,
        username: String,
        password: String,
        code: String,
    ) -> Result<String> {
        let client = ctx.data::<DatabaseConnection>()?;
        let user = user::Entity::find_by_id(&username)
            .one(client)
            .await
            .map_err(|_| Error::new("Failed to find user"))?
            .ok_or(Error::new("Failed to find user"))?;
        if bcrypt::verify(password, &user.password)
            .map_err(|_| Error::new("Failed to verify password"))?
        {
            let Some(totp_secret) = user.totp_secret else {
                return Err(Error::new("This account does not use 2FA"));
            };
            let secret = ctx.data_unchecked::<Secret>();
            let totp = totp_rs::TOTP::new(
                totp_rs::Algorithm::SHA256,
                6,
                1,
                30,
                totp_secret,
                Some("nirvati".to_string()),
                user.username.clone(),
            )?;
            let timestamp_now = std::time::SystemTime::now()
                .duration_since(std::time::UNIX_EPOCH)?
                .as_secs();
            if totp.check(&code, timestamp_now) {
                issue_token(
                    secret,
                    username,
                    user.permissions as u16,
                    user.user_group_id,
                    user.email,
                    true,
                )
                .await
            } else {
                Err(Error::new("Invalid code"))
            }
        } else {
            Err(Error::new("Invalid password"))
        }
    }
}
