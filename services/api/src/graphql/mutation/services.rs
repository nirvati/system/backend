// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Object, Result};
use tonic::Request;

use crate::api::add_service_request::Remote;
use crate::api::{AddServiceRequest, RemoveServiceRequest};
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::services_client;
use nirvati::permissions::Permission;
use nirvati_db::schema::prelude::Service;
use nirvati_db::schema::sea_orm_active_enums::UpstreamType;
use nirvati_db::schema::service;
use nirvati_db::sea_orm::prelude::*;
use nirvati_db::sea_orm::ActiveValue::{NotSet, Set};
use nirvati_db::sea_orm::IntoActiveModel;

#[derive(Default)]
pub struct Services;

#[Object(name = "ServicesMutations")]
impl Services {
    #[graphql(guard = "PermissionGuard::new(Permission::ManageProxies)")]
    async fn create_service(
        &self,
        ctx: &async_graphql::Context<'_>,
        ip: Option<String>,
        dns_name: Option<String>,
        tcp_ports: Vec<u16>,
        udp_ports: Vec<u16>,
    ) -> Result<String> {
        let db = ctx.data_unchecked::<DatabaseConnection>();
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let mut services_client =
            services_client(ctx).map_err(|_| "Failed to create services client".to_string())?;
        if ip.is_none() && dns_name.is_none() {
            return Err("Either ip or dnsName must be provided".into());
        }
        let service = service::Entity::insert(service::ActiveModel {
            upstream: Set(if let Some(ref ip) = ip {
                ip.clone()
            } else {
                dns_name.clone().unwrap()
            }),
            r#type: Set(if dns_name.is_none() {
                UpstreamType::Ip
            } else {
                UpstreamType::Dns
            }),
            tcp_ports: Set(Some(
                tcp_ports
                    .iter()
                    .map(|port| *port as i32)
                    .collect::<Vec<_>>(),
            )),
            udp_ports: Set(Some(
                udp_ports
                    .iter()
                    .map(|port| *port as i32)
                    .collect::<Vec<_>>(),
            )),
            owner_name: Set(username.clone()),
            id: NotSet,
        })
        .exec(db)
        .await
        .map_err(|_| "Failed to create service in database".to_string())?;
        services_client
            .add_service(Request::new(AddServiceRequest {
                namespace: username,
                name: format!("svc-{}", service.last_insert_id),
                tcp_ports: tcp_ports.into_iter().map(|port| port as u32).collect(),
                udp_ports: udp_ports.into_iter().map(|port| port as u32).collect(),
                remote: Some(if let Some(dns_name) = dns_name {
                    Remote::DnsName(dns_name)
                } else {
                    Remote::Ip(ip.unwrap())
                }),
            }))
            .await
            .map_err(|e| {
                tracing::error!("{:#?}", e);
                "Failed to create service in Kubernetes".to_string()
            })?;
        Ok(service.last_insert_id.to_string())
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageProxies)")]
    async fn add_service_ports(
        &self,
        ctx: &async_graphql::Context<'_>,
        id: String,
        tcp_ports: Vec<u16>,
        udp_ports: Vec<u16>,
    ) -> Result<bool> {
        let db = ctx.data_unchecked::<DatabaseConnection>();
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let mut services_client =
            services_client(ctx).map_err(|_| "Failed to create services client".to_string())?;
        let id: Uuid = id.parse().map_err(|_| "Invalid service ID".to_string())?;
        let found_service = service::Entity::find_by_id(id)
            .one(db)
            .await
            .map_err(|_| "Failed to find service in database".to_string())?
            .ok_or("Service not found")?;
        if found_service.owner_name != username {
            return Err("Service not found".into());
        }
        let new_tcp_ports: Vec<_> = found_service
            .tcp_ports
            .as_ref()
            .cloned()
            .unwrap_or_default()
            .into_iter()
            .chain(tcp_ports.iter().map(|port| *port as i32))
            .collect();
        let new_udp_ports: Vec<_> = found_service
            .udp_ports
            .as_ref()
            .cloned()
            .unwrap_or_default()
            .into_iter()
            .chain(udp_ports.iter().map(|port| *port as i32))
            .collect();
        let upstream = found_service.upstream.clone();
        let mut service = found_service.into_active_model();
        service.tcp_ports = Set(Some(new_tcp_ports.clone()));
        service.udp_ports = Set(Some(new_udp_ports.clone()));
        service::Entity::update(service)
            .exec(db)
            .await
            .map_err(|_| "Failed to update service in database".to_string())?;
        services_client
            .remove_service(Request::new(RemoveServiceRequest {
                namespace: username.clone(),
                name: format!("svc-{}", id),
            }))
            .await
            .map_err(|_| "Failed to delete service in Kubernetes".to_string())?;
        services_client
            .add_service(Request::new(AddServiceRequest {
                namespace: username,
                name: format!("svc-{}", id),
                tcp_ports: new_tcp_ports.into_iter().map(|port| port as u32).collect(),
                udp_ports: new_udp_ports.into_iter().map(|port| port as u32).collect(),
                remote: Some(Remote::Ip(upstream)),
            }))
            .await
            .map_err(|e| {
                tracing::error!("{:#?}", e);
                "Failed to create service in Kubernetes".to_string()
            })?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageProxies)")]
    async fn delete_service(&self, ctx: &async_graphql::Context<'_>, id: String) -> Result<bool> {
        let db = ctx.data_unchecked::<DatabaseConnection>();
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let mut services_client =
            services_client(ctx).map_err(|_| "Failed to create services client".to_string())?;
        let id: Uuid = id.parse().map_err(|_| "Invalid service ID".to_string())?;
        let found_service = Service::find_by_id(id)
            .one(db)
            .await
            .map_err(|_| "Failed to find service in database".to_string())?
            .ok_or("Service not found")?;
        if found_service.owner_name != username {
            return Err("Service not found".into());
        }
        found_service.delete(db).await?;
        services_client
            .remove_service(Request::new(RemoveServiceRequest {
                namespace: username,
                name: format!("svc-{}", id),
            }))
            .await
            .map_err(|_| "Failed to delete service in Kubernetes".to_string())?;
        Ok(true)
    }
}
