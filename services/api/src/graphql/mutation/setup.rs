// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Context, Error, Object, Result};
use tonic::Request;

#[cfg(not(feature = "__development"))]
use crate::api::Empty;
use crate::api::GenerateFilesRequest;
use crate::api::{AddUserAuthRequest, AddUserNsRequest};
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::IsLoggedInGuard;
use crate::graphql::auth_guard::PermissionGuard;
#[cfg(not(feature = "__development"))]
use crate::graphql::clients::setup_client;
use crate::graphql::clients::{manage_client, middlewares_client, users_client};
use crate::graphql::mutation::apps::install_app;
use crate::utils::user_state::get_user_state_by_username;
use crate::USERNAME_REGEX;
use nirvati::permissions::{Permission, PermissionSet};
use nirvati_db::schema::{user, user_group};
use nirvati_db::sea_orm::prelude::*;
use nirvati_db::sea_orm::ActiveValue::Set;
use nirvati_db::sea_orm::IntoActiveModel;

#[derive(Default)]
pub struct Setup;

#[Object(name = "SetupMutations")]
impl Setup {
    async fn create_initial_user(
        &self,
        ctx: &Context<'_>,
        username: String,
        password: String,
        name: String,
        email: Option<String>,
        is_public_name: bool,
    ) -> Result<bool> {
        // We can skip the db lookup for installed apps here, because we know what apps can be installed at this stage
        if username == "default"
            || username.starts_with("kube-")
            || username == "system"
            || username == "nirvati"
            || username == "coredns"
            || username == "longhorn"
            || username == "longhorn-system"
            || username == "cert-manager"
            || username == "system-upgrade-controller"
            || username == "nirvati-telemetry"
            || username == "metallb"
            || username == "local-path-provisioner"
        {
            return Err(Error::new("Username is reserved"));
        };
        if !USERNAME_REGEX.is_match(&username)? {
            return Err(Error::new(
                "Username must only consist of lowercase alphanumeric characters, '-' or '.'",
            ));
        }
        let client = ctx.data::<DatabaseConnection>()?;
        let user_count = user::Entity::find().count(client).await?;
        if user_count > 0 {
            return Err(Error::new("Initial user already exists"));
        }
        user_group::Entity::insert(user_group::ActiveModel {
            name: Set("main".to_string()),
            ..Default::default()
        })
        .exec(client)
        .await?;
        user::Entity::insert(user::ActiveModel {
            username: Set("system".to_string()),
            name: Set("Nirvati System".to_string()),
            password: Set("ACCOUNT_DISABLED".to_string()),
            user_group_id: Set("main".to_string()),
            permissions: Set(Into::<u16>::into(PermissionSet::full()) as i32),
            is_public: Set(false),
            ..Default::default()
        })
        .exec(client)
        .await?;
        user::Entity::insert(user::ActiveModel {
            username: Set(username.clone()),
            name: Set(name.clone()),
            password: Set(bcrypt::hash(&password, bcrypt::DEFAULT_COST)
                .map_err(|_| Error::new("Failed to hash password"))?),
            user_group_id: Set("main".to_string()),
            permissions: Set(Into::<u16>::into(PermissionSet::full()) as i32),
            is_public: Set(is_public_name),
            email: Set(email.clone()),
            ..Default::default()
        })
        .exec(client)
        .await?;
        let mut mw_client = middlewares_client(ctx)?;
        mw_client
            .add_user_auth(Request::new(AddUserAuthRequest {
                user: username.clone(),
                password,
            }))
            .await?;

        let mut users_client = users_client(ctx)?;
        users_client
            .create_user_namespace(Request::new(AddUserNsRequest {
                user: username.clone(),
            }))
            .await
            .map_err(|_| Error::new("Failed to create user in manager"))?;

        let mut manage_client = manage_client(ctx)?;
        manage_client
            .init_user(Request::new(crate::api::InitUserRequest {
                user_id: username.clone(),
                user_display_name: name,
            }))
            .await
            .map_err(|_| Error::new("Failed to init user"))?;

        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageSysComponents)")]
    async fn install_required_system_apps(&self, ctx: &Context<'_>) -> Result<bool> {
        for app in [
            "cert-manager",
            "coredns",
            "system-upgrade-controller",
            "longhorn",
            #[cfg(not(feature = "__development"))]
            "nirvati",
            "metallb",
        ] {
            install_app(
                ctx,
                app.to_string(),
                serde_json::Value::Null,
                None,
                None,
                true,
                false,
            )
            .await
            .map_err(|e| Error::new(format!("Failed to install app {}: {:#?}", app, e)))?;
        }

        let mut client = manage_client(ctx)?;
        client
            .generate_files(Request::new(GenerateFilesRequest {
                user_state: Some(get_user_state_by_username(ctx, "system".to_string()).await),
            }))
            .await
            .map_err(|_| Error::new("Failed to generate system apps"))?;
        Ok(true)
    }

    #[graphql(guard = "IsLoggedInGuard::new()")]
    pub async fn finish_setup(&self, ctx: &Context<'_>) -> Result<bool> {
        let db_client = ctx.data_unchecked::<DatabaseConnection>();
        #[cfg(not(feature = "__development"))]
        {
            let users_with_finished_setup = user::Entity::find()
                .filter(user::Column::SetupFinished.eq(true))
                .count(db_client)
                .await
                .map_err(|_| Error::new("Failed to find initial user"))?;
            let is_initial_setup = users_with_finished_setup == 0;
            if is_initial_setup {
                let mut setup_client = setup_client(ctx)?;
                setup_client
                    .delete_nirvati_ingress(Request::new(Empty {}))
                    .await?;
            }
        }
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let user = user::Entity::find()
            .filter(user::Column::Username.eq(username.clone()))
            .one(db_client)
            .await
            .map_err(|_| Error::new("Failed to find user"))?
            .ok_or_else(|| Error::new("User not found"))?;
        let mut user = user.into_active_model();
        user.setup_finished = Set(true);
        user::Entity::update(user)
            .exec(db_client)
            .await
            .map_err(|_| Error::new("Failed to finish setup"))?;
        Ok(true)
    }
}
