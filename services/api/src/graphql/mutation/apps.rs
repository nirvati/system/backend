// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::HashMap;

use async_graphql::{Context, Error, Object, Result, SimpleObject};
use tonic::Request;

use nirvati_apps::metadata::AppScope;
use nirvati_db::schema::prelude::*;
use nirvati_db::sea_orm::prelude::*;
use nirvati_db::sea_orm::{DatabaseConnection, IntoActiveModel, QueryOrder};

use crate::api::download_apps_request::Download;
use crate::api::{
    AddDomainRequest, AddStoreRequest, DownloadAppsRequest, GenerateFilesRequest,
    InstallCitadelAppsRequest, InstallRequest, RemoveStoreRequest, SetDomainProtectRequest,
    UninstallRequest,
};
use crate::apps::get_app;
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::{apps_client, citadel_compat_client, ingress_client, manage_client};
use crate::graphql::mutation::https::add_app_domain;
use crate::graphql::query::apps::app::get_conflicting_users;
use crate::graphql::query::apps::store::StoreType;
use crate::graphql::query::https::acme_provider::AcmeProvider;
use crate::utils::user_state::{get_user_state, get_user_state_by_username};
use nirvati::permissions::Permission;
use nirvati_db::schema::{app_domain, app_installation};
use nirvati_db::sea_orm::ActiveValue::Set;

#[derive(SimpleObject)]
pub struct CitadelImportResult {
    pub failed_apps: Vec<String>,
}

#[derive(Default)]
pub struct Apps;

pub async fn install_app(
    ctx: &Context<'_>,
    app_id: String,
    settings: serde_json::Value,
    initial_domain: Option<String>,
    acme_provider: Option<AcmeProvider>,
    is_system_library: bool,
    is_protected_initially: bool,
) -> Result<bool> {
    // We can safely unwrap here because we know the user is logged in
    let user = ctx.data_unchecked::<UserCtx>().username.clone();
    let mut client = apps_client(ctx)?;
    let db = ctx.data_unchecked::<DatabaseConnection>();
    let mut user_state = if is_system_library {
        get_user_state_by_username(ctx, "system".to_string()).await
    } else {
        get_user_state(ctx).await
    };
    user_state
        .app_settings
        .insert(app_id.clone(), settings.to_string());
    let resp = client
        .apply(Request::new(InstallRequest {
            id: app_id.clone(),
            user_state: Some(user_state.clone()),
            initial_domain: initial_domain.clone(),
            src_user: if is_system_library {
                Some(user.to_string())
            } else {
                None
            },
            is_existing_on_citadel: None,
        }))
        .await?;
    let chart_version = resp.into_inner().chart_version;
    AppInstallation::insert(app_installation::ActiveModel {
        app_id: Set(app_id.clone()),
        owner_name: Set(if is_system_library {
            "system".to_string()
        } else {
            user.clone()
        }),
        settings: Set(settings.clone()),
        protected: Set(is_protected_initially),
        initial_domain: Set(initial_domain.clone()),
        current_chart_version: Set(chart_version.clone()),
        ..Default::default()
    })
    .exec(db)
    .await?;
    if let (Some(initial_domain), Some(acme_provider)) = (initial_domain, acme_provider) {
        let app = get_app(ctx, &app_id).await?.metadata;
        add_app_domain(
            ctx,
            app_id.clone(),
            initial_domain,
            acme_provider,
            None,
            app.raw_ingress,
            None,
        )
        .await?;
    }
    Ok(true)
}

#[Object(name = "AppMutations")]
impl Apps {
    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn install_app(
        &self,
        ctx: &Context<'_>,
        app_id: String,
        settings: serde_json::Value,
        initial_domain: Option<String>,
        acme_provider: Option<AcmeProvider>,
        protect: Option<bool>,
    ) -> Result<bool> {
        let perms = ctx.data_unchecked::<UserCtx>().permissions;
        let app = get_app(ctx, &app_id).await?;
        let conflicts = get_conflicting_users(
            app_id.clone(),
            if app.metadata.scope == AppScope::System {
                "system".to_string()
            } else {
                ctx.data_unchecked::<UserCtx>().username.clone()
            },
            ctx.data_unchecked::<DatabaseConnection>(),
        )
        .await?;
        if !conflicts.is_empty() {
            return Err(Error::new(format!(
                "The app is already installed by the following users: {}",
                conflicts.join(", ")
            )));
        }
        if app.metadata.scope == AppScope::System {
            let db_client = ctx.data_unchecked::<DatabaseConnection>();
            let user_with_same_name = User::find_by_id(&app_id).count(db_client).await? > 0;
            if user_with_same_name {
                return Err(Error::new(
                    "An account with the same name as the app already exists",
                ));
            }
        }

        if app.metadata.supports_ingress && (initial_domain.is_none() || acme_provider.is_none()) {
            return Err(Error::new("Apps with ingress require a domain to be set"));
        };
        if !app.metadata.supports_ingress && (initial_domain.is_some() || acme_provider.is_some()) {
            return Err(Error::new(
                "Apps that do not support ingress cannot have a domain set",
            ));
        };
        if app.metadata.scope == AppScope::System && !perms.has(Permission::ManageSysComponents) {
            return Err(Error::new(
                "You do not have permission to install system libraries",
            ));
        };
        if !perms.has(Permission::InstallRootApps)
            && (app
                .metadata
                .has_permissions
                .contains(&"builtin/root".to_string())
                || app
                    .metadata
                    .has_permissions
                    .contains(&"builtin/kube-api/full".to_string())
                || app
                    .metadata
                    .has_permissions
                    .iter()
                    .any(|perm| perm.starts_with("builtin/network")))
        {
            return Err(Error::new(
                "You do not have permission to install apps that require root access",
            ));
        }
        install_app(
            ctx,
            app_id,
            settings,
            initial_domain,
            acme_provider,
            app.metadata.scope == AppScope::System,
            protect.unwrap_or(false),
        )
        .await?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn uninstall_app(&self, ctx: &Context<'_>, app_id: String) -> Result<bool> {
        // We can safely unwrap here because we know the user is logged in
        let user = ctx.data_unchecked::<UserCtx>().username.clone();
        let app = get_app(ctx, &app_id).await?;
        let mut client = apps_client(ctx)?;
        let db = ctx.data_unchecked::<DatabaseConnection>();
        let owner_name = if app.metadata.scope == AppScope::System {
            "system".to_string()
        } else {
            user.clone()
        };
        client
            .uninstall(Request::new(UninstallRequest {
                id: app_id.clone(),
                user_state: if app.metadata.scope == AppScope::System {
                    Some(get_user_state_by_username(ctx, "system".to_string()).await)
                } else {
                    Some(get_user_state(ctx).await)
                },
            }))
            .await
            .map_err(|_| Error::new("Failed to uninstall app"))?;
        AppInstallation::delete_many()
            .filter(app_installation::Column::AppId.eq(app_id.clone()))
            .filter(app_installation::Column::OwnerName.eq(owner_name))
            .exec(db)
            .await?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn update_app_settings(
        &self,
        ctx: &Context<'_>,
        app_id: String,
        settings: serde_json::Value,
    ) -> Result<bool> {
        let perms = ctx.data_unchecked::<UserCtx>().permissions;
        let app = get_app(ctx, &app_id).await?;
        if app.metadata.scope == AppScope::System && !perms.has(Permission::ManageSysComponents) {
            return Err(Error::new(
                "You do not have permission to install system libraries",
            ));
        };
        let mut client = apps_client(ctx)?;
        let db = ctx.data_unchecked::<DatabaseConnection>();
        let user_state = if app.metadata.scope == AppScope::System {
            get_user_state_by_username(ctx, "system".to_string()).await
        } else {
            get_user_state(ctx).await
        };
        let app_db_entry = AppInstallation::update_many()
            .filter(app_installation::Column::AppId.eq(app_id.clone()))
            .filter(app_installation::Column::OwnerName.eq(
                if app.metadata.scope == AppScope::System {
                    "system".to_string()
                } else {
                    ctx.data_unchecked::<UserCtx>().username.clone()
                },
            ))
            .col_expr(app_installation::Column::Settings, Expr::value(settings))
            .exec_with_returning(db)
            .await?;
        if app_db_entry.is_empty() {
            return Err(Error::new("Failed to update app installation"));
        }
        let app_db_entry = app_db_entry.into_iter().next().unwrap();
        client
            .apply(Request::new(InstallRequest {
                id: app_id,
                user_state: Some(user_state.clone()),
                initial_domain: app_db_entry.initial_domain,
                // The app is already present for the system user here
                src_user: None,
                is_existing_on_citadel: None,
            }))
            .await
            .map_err(|_| Error::new("Failed to install app"))?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn set_app_protected(
        &self,
        ctx: &Context<'_>,
        app_id: String,
        protected: bool,
    ) -> Result<bool> {
        let user_perms = ctx.data_unchecked::<UserCtx>().permissions;
        if !user_perms.has(Permission::ManageApps) {
            return Err(Error::new("You do not have permission to lock apps"));
        };
        let db_client = ctx.data_unchecked::<DatabaseConnection>();
        let app = get_app(ctx, &app_id).await?;
        if app.metadata.scope != AppScope::User {
            return Err(Error::new("Only user-scoped apps can be locked currently."));
        };
        let user = ctx.data_unchecked::<UserCtx>().username.clone();
        let app_install = AppInstallation::find()
            .filter(app_installation::Column::AppId.eq(app_id.clone()))
            .filter(app_installation::Column::OwnerName.eq(user.clone()))
            .one(db_client)
            .await?
            .ok_or(Error::new("Failed to find app installation"))?;
        let mut app_install = app_install.into_active_model();
        app_install.protected = Set(protected);
        AppInstallation::update(app_install)
            .exec(db_client)
            .await
            .map_err(|_| Error::new("Failed to update app installation"))?;
        // Get the app domains
        let app_domains = AppDomain::find()
            .find_also_related(AppInstallation)
            .filter(app_installation::Column::AppId.eq(app_id.clone()))
            .filter(app_installation::Column::OwnerName.eq(user))
            .order_by_asc(app_domain::Column::CreatedAt)
            .all(db_client)
            .await?;
        let mut client = ingress_client(ctx)?;
        for (domain, _) in app_domains {
            let is_tor = domain.name.ends_with(".onion");
            client
                .set_domain_protect(Request::new(SetDomainProtectRequest {
                    user_state: Some(get_user_state(ctx).await),
                    id: app_id.clone(),
                    domain: domain.name,
                    protect: protected,
                    custom_prefix: domain.custom_prefix,
                    r#type: if is_tor {
                        crate::api::DomainType::DomainOnion as i32
                    } else {
                        crate::api::DomainType::DomainHttps as i32
                    },
                    app_component: domain.app_component_id,
                }))
                .await
                .map_err(|_| Error::new("Failed to update app ingress"))?;
        }
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn generate(&self, ctx: &Context<'_>) -> Result<bool> {
        let mut client = manage_client(ctx)?;
        client
            .generate_files(Request::new(GenerateFilesRequest {
                user_state: Some(get_user_state(ctx).await),
            }))
            .await
            .map_err(|_| Error::new("Failed to generate apps"))?;
        let user_ctx = ctx.data_unchecked::<UserCtx>();
        if user_ctx.permissions.has(Permission::ManageSysComponents) {
            client
                .generate_files(Request::new(GenerateFilesRequest {
                    user_state: Some(get_user_state_by_username(ctx, "system".to_string()).await),
                }))
                .await
                .map_err(|_| Error::new("Failed to generate system apps"))?;
        }
        Ok(true)
    }

    async fn refresh_sources(&self, ctx: &Context<'_>) -> Result<bool> {
        let mut client = manage_client(ctx)?;
        client
            .download(Request::new(DownloadAppsRequest {
                user_state: Some(get_user_state(ctx).await),
                download: Some(Download::NewOrNotInstalledOnly(true)),
            }))
            .await
            .map_err(|_| Error::new("Failed to refresh sources"))
            .map(|_| true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageAppStores)")]
    async fn add_app_store(
        &self,
        ctx: &Context<'_>,
        src: HashMap<String, String>,
        store_type: StoreType,
        store_plugin: Option<String>,
    ) -> Result<bool> {
        let mut client = manage_client(ctx)?;
        let store_type: crate::api::StoreType = store_type.into();
        client
            .add_store(Request::new(AddStoreRequest {
                src,
                user_state: Some(get_user_state(ctx).await),
                r#type: store_type as i32,
                store_plugin,
            }))
            .await
            .map_err(|_| Error::new("Failed to add app store"))
            .map(|_| true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageAppStores)")]
    async fn remove_app_store(
        &self,
        ctx: &Context<'_>,
        src: HashMap<String, String>,
    ) -> Result<bool> {
        let mut client = manage_client(ctx)?;
        client
            .remove_store(Request::new(RemoveStoreRequest {
                src,
                user_state: Some(get_user_state(ctx).await),
            }))
            .await
            .map_err(|_| Error::new("Failed to remove app store"))
            .map(|_| true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn update_app(
        &self,
        ctx: &Context<'_>,
        app_id: String,
        skip_download: Option<bool>,
    ) -> Result<bool> {
        let user_perms = ctx.data_unchecked::<UserCtx>().permissions;
        if !user_perms.has(Permission::ManageApps) {
            return Err(Error::new("You do not have permission to update apps"));
        }
        let app = get_app(ctx, &app_id).await?;
        let user_state = if app.metadata.scope == AppScope::System {
            Some(get_user_state_by_username(ctx, "system".to_string()).await)
        } else {
            Some(get_user_state(ctx).await)
        };
        let mut client = manage_client(ctx)?;
        if app.metadata.scope == AppScope::System
            && !user_perms.has(Permission::ManageSysComponents)
        {
            return Err(Error::new(
                "You do not have permission to update system libraries",
            ));
        }
        if !skip_download.unwrap_or(false) {
            client
                .download(Request::new(DownloadAppsRequest {
                    user_state: user_state.clone(),
                    download: Some(Download::App(app_id.clone())),
                }))
                .await
                .map_err(|_| Error::new("Failed to download new app"))?;
        }
        client
            .generate_files(Request::new(GenerateFilesRequest {
                user_state: user_state.clone(),
            }))
            .await
            .map_err(|_| Error::new("Failed to regenerate files"))?;
        let db = ctx.data_unchecked::<DatabaseConnection>();
        let user = ctx.data_unchecked::<UserCtx>().username.clone();
        let app_user = if app.metadata.scope == AppScope::System {
            "system".to_string()
        } else {
            user.clone()
        };
        let app_domains = AppInstallation::find()
            .find_with_related(AppDomain)
            .filter(app_installation::Column::AppId.eq(app_id.clone()))
            .filter(app_installation::Column::OwnerName.eq(app_user.clone()))
            .all(db)
            .await?;
        let (app_install, app_domains) = app_domains
            .into_iter()
            .next()
            .ok_or_else(|| Error::new("Failed to find app installation for updated app"))?;
        let mut apps_client = apps_client(ctx)?;
        let mut ingress_client = ingress_client(ctx)?;
        for domain in app_domains {
            let is_tor = domain.name.ends_with(".onion");
            let parent = domain.find_related(Domain).one(db).await?;
            let acme_account = domain.find_related(AcmeAccount).one(db).await?;
            if !is_tor && acme_account.is_none() {
                return Err(Error::new("App domain is missing an ACME account"));
            }
            ingress_client
                .add_domain(Request::new(AddDomainRequest {
                    user_state: user_state.clone(),
                    id: app_id.clone(),
                    domain: if is_tor { None } else { Some(domain.name) },
                    protect: app_install.protected,
                    custom_prefix: domain.custom_prefix,
                    r#type: if is_tor {
                        crate::api::DomainType::DomainOnion as i32
                    } else {
                        crate::api::DomainType::DomainHttps as i32
                    },
                    parent: parent.map(|p| p.name),
                    acme_provider: acme_account.map(|a| match a.provider {
                        nirvati_db::schema::sea_orm_active_enums::AcmeProvider::LetsEncrypt => {
                            crate::api::AcmeProvider {
                                provider: Some(crate::api::acme_provider::Provider::BuiltIn(
                                    crate::api::BuiltInAcmeProvider::LetsEncrypt as i32,
                                )),
                            }
                        }
                        nirvati_db::schema::sea_orm_active_enums::AcmeProvider::BuyPass => {
                            crate::api::AcmeProvider {
                                provider: Some(crate::api::acme_provider::Provider::BuiltIn(
                                    crate::api::BuiltInAcmeProvider::BuyPass as i32,
                                )),
                            }
                        }
                    }),
                    cert_user: Some(user.clone()),
                    app_component: domain.app_component_id,
                }))
                .await
                .map_err(|_| Error::new("Failed to update app ingress"))?;
        }
        let resp = apps_client
            .apply(Request::new(InstallRequest {
                user_state: user_state.clone(),
                id: app_id.clone(),
                initial_domain: app_install.initial_domain.clone(),
                // In this case, the update has already been downloaded to the system user,
                // so we don't need to specify a source user to copy from
                src_user: None,
                is_existing_on_citadel: None,
            }))
            .await
            .map_err(|_| Error::new("Failed to download new app"))?;
        let chart_version = resp.into_inner().chart_version;
        let mut app_install = app_install.into_active_model();
        app_install.current_chart_version = Set(chart_version);
        AppInstallation::update(app_install)
            .exec(db)
            .await
            .map_err(|_| Error::new("Failed to update app installation"))?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn pause_app(&self, ctx: &Context<'_>, app_id: String) -> Result<bool> {
        let user_perms = ctx.data_unchecked::<UserCtx>().permissions;
        if !user_perms.has(Permission::ManageApps) {
            return Err(Error::new("You do not have permission to pause apps"));
        }
        let app = get_app(ctx, &app_id).await?;
        let mut apps_client = apps_client(ctx)?;
        let mut ingress_client = ingress_client(ctx)?;
        let db = ctx.data_unchecked::<DatabaseConnection>();
        let user = ctx.data_unchecked::<UserCtx>().username.clone();
        let user_state = if app.metadata.scope == AppScope::System {
            get_user_state_by_username(ctx, "system".to_string()).await
        } else {
            get_user_state(ctx).await
        };
        let app_user = if app.metadata.scope == AppScope::System {
            "system".to_string()
        } else {
            user.clone()
        };
        let domains = AppDomain::find()
            .find_also_related(AppInstallation)
            .filter(app_installation::Column::AppId.eq(app_id.clone()))
            .filter(app_installation::Column::OwnerName.eq(app_user.clone()))
            .all(db)
            .await?;
        for (domain, _) in domains {
            ingress_client
                .pause_app_domain(Request::new(crate::api::PauseAppDomainRequest {
                    user_state: Some(user_state.clone()),
                    app: app_id.clone(),
                    domain: domain.name,
                    app_component: domain.app_component_id,
                }))
                .await
                .map_err(|_| Error::new("Failed to pause app domain"))?;
        }
        let current_app = AppInstallation::find()
            .filter(app_installation::Column::AppId.eq(app_id.clone()))
            .filter(app_installation::Column::OwnerName.eq(app_user.clone()))
            .one(db)
            .await?
            .ok_or(Error::new("Failed to find app installation"))?;
        apps_client
            .pause_app(Request::new(crate::api::PauseAppRequest {
                user_state: Some(user_state),
                app: app_id.clone(),
            }))
            .await
            .map_err(|_| Error::new("Failed to pause app"))?;
        let mut app_install = current_app.into_active_model();
        app_install.paused = Set(true);
        AppInstallation::update(app_install).exec(db).await?;
        Ok(true)
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageApps)")]
    async fn unpause_app(&self, ctx: &Context<'_>, app_id: String) -> Result<bool> {
        let user_perms = ctx.data_unchecked::<UserCtx>().permissions;
        if !user_perms.has(Permission::ManageApps) {
            return Err(Error::new("You do not have permission to unpause apps"));
        }
        let app = get_app(ctx, &app_id).await?;
        let mut apps_client = apps_client(ctx)?;
        let mut ingress_client = ingress_client(ctx)?;
        let db = ctx.data_unchecked::<DatabaseConnection>();
        let user = ctx.data_unchecked::<UserCtx>().username.clone();
        let user_state = if app.metadata.scope == AppScope::System {
            get_user_state_by_username(ctx, "system".to_string()).await
        } else {
            get_user_state(ctx).await
        };
        let app_user = if app.metadata.scope == AppScope::System {
            "system".to_string()
        } else {
            user.clone()
        };
        let domains = AppDomain::find()
            .find_also_related(AppInstallation)
            .filter(app_installation::Column::AppId.eq(app_id.clone()))
            .filter(app_installation::Column::OwnerName.eq(app_user.clone()))
            .all(db)
            .await?;
        for (domain, _) in domains {
            let is_tor = domain.name.ends_with(".onion");
            ingress_client
                .resume_app_domain(Request::new(crate::api::ResumeAppDomainRequest {
                    user_state: Some(user_state.clone()),
                    app: app_id.clone(),
                    domain: domain.name,
                    custom_prefix: domain.custom_prefix,
                    protect: false, // TODO: Remember protection status
                    r#type: if is_tor {
                        crate::api::DomainType::DomainOnion as i32
                    } else {
                        crate::api::DomainType::DomainHttps as i32
                    },
                    app_component: domain.app_component_id,
                }))
                .await
                .map_err(|_| Error::new("Failed to unpause app domain"))?;
        }
        apps_client
            .resume_app(Request::new(crate::api::ResumeAppRequest {
                user_state: Some(user_state),
                app: app_id.clone(),
            }))
            .await
            .map_err(|_| Error::new("Failed to resume app"))?;
        let app_installation = AppInstallation::find()
            .filter(app_installation::Column::AppId.eq(app_id.clone()))
            .filter(app_installation::Column::OwnerName.eq(app_user.clone()))
            .one(db)
            .await?
            .ok_or(Error::new("Failed to find app installation"))?;
        let mut app_installation = app_installation.into_active_model();
        app_installation.paused = Set(false);
        AppInstallation::update(app_installation).exec(db).await?;
        Ok(true)
    }

    async fn finish_citadel_import(&self, ctx: &Context<'_>) -> Result<CitadelImportResult> {
        let user_perms = ctx.data_unchecked::<UserCtx>().permissions;
        if !user_perms.has(Permission::ManageSysComponents) {
            return Err(Error::new(
                "You do not have permission to finish citadel import",
            ));
        }
        let mut client = citadel_compat_client(ctx)?;
        let system_user_state = get_user_state_by_username(ctx, "system".to_string()).await;
        let user_state = get_user_state(ctx).await;
        let response = client
            .install_citadel_apps(Request::new(InstallCitadelAppsRequest {
                user_state: Some(user_state),
                system_user_state: Some(system_user_state),
            }))
            .await
            .map_err(|_| Error::new("Failed to finish citadel import"))?;
        Ok(CitadelImportResult {
            failed_apps: response.into_inner().failed_apps,
        })
    }
}
