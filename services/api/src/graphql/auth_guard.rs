// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Context, Guard, Result};

use crate::graphql::auth_ctx::UserCtx;
use nirvati::permissions::Permission;

pub struct PermissionGuard {
    required: Permission,
}

impl PermissionGuard {
    pub fn new(required: Permission) -> Self {
        Self { required }
    }
}

impl Guard for PermissionGuard {
    async fn check(&self, ctx: &Context<'_>) -> Result<()> {
        let user_ctx = ctx.data_opt::<UserCtx>();
        if user_ctx.is_some_and(|ctx| {
            ctx.audience.contains(&"api".to_string()) && ctx.permissions.has(self.required)
        }) {
            Ok(())
        } else {
            Err("Forbidden".into())
        }
    }
}

pub struct IsLoggedInGuard;

impl IsLoggedInGuard {
    pub fn new() -> Self {
        Self {}
    }
}

impl Guard for IsLoggedInGuard {
    async fn check(&self, ctx: &Context<'_>) -> Result<()> {
        if ctx.data_opt::<UserCtx>().is_some() {
            Ok(())
        } else {
            Err("Forbidden".into())
        }
    }
}
