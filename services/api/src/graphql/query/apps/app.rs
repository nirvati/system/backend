// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::{BTreeMap, HashMap};

use async_graphql::{ComplexObject, Context, Error, Json, SimpleObject};
use k8s_openapi::api::core::v1::PersistentVolumeClaim;
use kube::Api;

use nirvati::utils::MultiLanguageItem;
use nirvati_apps::metadata::Volume as VolumeDefinition;
use nirvati_db::schema::{app_domain, app_installation};
use nirvati_db::sea_orm::prelude::*;

use crate::apps::types::AppMetadata;
use crate::graphql::clients::manage_client;
use crate::graphql::query::apps::installable::AppInstallable;
use crate::graphql::query::https::app_domain::AppDomain;
use crate::graphql::query::users::User;
use crate::longhorn::Client as LonghornClient;
use crate::utils::user_state::get_user_state_by_username;

#[derive(SimpleObject, Clone, Debug, PartialEq, Eq)]
pub struct Volume {
    pub id: String,
    pub size: u64,
    pub replica_count: u32,
    pub actual_size: u64,
    pub human_readable_id: String,
    pub volume_definition: VolumeDefinition,
}

#[derive(SimpleObject, Clone, Debug, PartialEq, Eq)]
pub struct AppUpdate {
    latest_version: String,
    releases_notes: Json<HashMap<String, MultiLanguageItem>>,
    new_permissions: Vec<String>,
    removed_permissions: Vec<String>,
}

impl From<crate::api::AppUpdate> for AppUpdate {
    fn from(update: crate::api::AppUpdate) -> Self {
        Self {
            latest_version: update.latest,
            releases_notes: Json(
                update
                    .releases_notes
                    .into_iter()
                    .map(|(k, v)| (k, MultiLanguageItem(v.notes.into_iter().collect())))
                    .collect(),
            ),
            new_permissions: update.new_permissions,
            removed_permissions: update.removed_permissions,
        }
    }
}

pub async fn get_conflicting_users(
    app_id: String,
    owner: String,
    db_client: &DatabaseConnection,
) -> async_graphql::Result<Vec<String>> {
    let other_apps_with_same_id = app_installation::Entity::find()
        .filter(if owner == "system" {
            app_installation::Column::OwnerName.ne("system")
        } else {
            app_installation::Column::OwnerName.eq("system")
        })
        .filter(app_installation::Column::AppId.eq(app_id))
        .all(db_client)
        .await
        .map_err(|_| Error::new("Failed to get app installations"))?;
    Ok(other_apps_with_same_id
        .into_iter()
        .map(|app| app.owner_name)
        .collect())
}

#[ComplexObject]
impl AppMetadata {
    async fn owner(&self) -> User {
        User::from_username(self.owner.clone())
    }
    async fn latest_version(&self, ctx: &Context<'_>) -> async_graphql::Result<Option<AppUpdate>> {
        if let Some(latest_version) = self.latest_version.get() {
            Ok(latest_version.clone())
        } else {
            // Basically unreachable, but if this becomes reachable in the future, prevent a crash
            let mut client = manage_client(ctx)?;
            let response = client
                .get_updates(tonic::Request::new(crate::api::GetAppUpdateRequest {
                    app: Some(self.metadata.id.clone()),
                    user_state: Some(get_user_state_by_username(ctx, self.owner.clone()).await),
                }))
                .await
                .map_err(|_| Error::new("Failed to get app updates"))?
                .into_inner();
            if let Some(update) = response.updates.get(&self.metadata.id) {
                if update.current == update.latest {
                    Ok(None)
                } else {
                    Ok(Some(update.clone().into()))
                }
            } else {
                Ok(None)
            }
        }
    }

    async fn domains(&self, ctx: &Context<'_>) -> async_graphql::Result<Vec<AppDomain>> {
        let db_client = ctx.data_unchecked::<DatabaseConnection>();
        let domains = app_domain::Entity::find()
            .find_also_related(app_installation::Entity)
            .filter(app_installation::Column::OwnerName.eq(self.owner.clone()))
            .filter(app_installation::Column::AppId.eq(self.metadata.id.clone()))
            .all(db_client)
            .await
            .map_err(|_| Error::new("Failed to get app domains"))?;
        Ok(domains
            .into_iter()
            .map(|(app_domain, _)| AppDomain {
                name: app_domain.name,
                created_at: app_domain.created_at.and_utc(),
                app_id: self.metadata.id.clone(),
                owner_name: self.owner.clone(),
                parent: app_domain.parent_domain_name,
            })
            .collect())
    }

    async fn developers(&self) -> Json<BTreeMap<String, String>> {
        Json(self.metadata.developers.clone())
    }

    async fn running_volumes(&self, ctx: &Context<'_>) -> async_graphql::Result<Vec<Volume>> {
        let kube_client = ctx.data_unchecked::<kube::Client>().clone();
        let longhorn_client = ctx.data_unchecked::<LonghornClient>();
        let app_ns = format!("{}-{}", self.owner, self.metadata.id);
        let volumes_api: Api<PersistentVolumeClaim> = Api::namespaced(kube_client, &app_ns);
        let volumes = volumes_api.list(&Default::default()).await.map_err(|err| {
            tracing::error!("Failed to get app volumes: {:?}", err);
            Error::new("Failed to get app volumes")
        })?;
        let mut result = vec![];
        for volume in volumes {
            let Some(ref name) = volume.metadata.name else {
                continue;
            };
            let Some(main_definition) = self.metadata.volumes.get(name) else {
                continue;
            };
            let Some(spec) = volume.spec else {
                continue;
            };
            let Some(inner) = spec.volume_name else {
                continue;
            };
            let details = longhorn_client.get_volume(&inner).await?;
            result.push(Volume {
                id: inner,
                size: details
                    .size
                    .parse()
                    .map_err(|_| Error::new("Failed to parse volume size"))?,
                replica_count: details.number_of_replicas as u32,
                actual_size: details
                    .controllers
                    .first()
                    .map(|c| c.actual_size.clone())
                    .unwrap_or("0".to_string())
                    .parse()
                    .map_err(|_| Error::new("Failed to parse volume size"))?,
                human_readable_id: name.clone(),
                volume_definition: main_definition.clone(),
            });
        }
        Ok(result)
    }

    async fn installable(&self, ctx: &Context<'_>) -> async_graphql::Result<AppInstallable> {
        let db_client = ctx.data_unchecked::<DatabaseConnection>();
        let conflicting_users =
            get_conflicting_users(self.metadata.id.clone(), self.owner.clone(), db_client).await?;
        Ok(AppInstallable {
            can_install: conflicting_users.is_empty(),
            conflicting_users,
        })
    }
}
