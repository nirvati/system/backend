use async_graphql::SimpleObject;

#[derive(SimpleObject, Clone, Debug, PartialEq, Eq)]
pub struct AppInstallable {
    pub can_install: bool,
    pub conflicting_users: Vec<String>,
}
