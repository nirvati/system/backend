// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::{BTreeMap, HashMap};

use async_graphql::{ComplexObject, Context, Enum, Error, Result, SimpleObject};
use serde::{Deserialize, Serialize};

use nirvati_apps::stores::StoreMetadata;

use crate::apps::get_app_registry_for_user;
use crate::apps::types::AppMetadata;
use crate::graphql::clients::manage_client;
use crate::graphql::query::apps::app::AppUpdate;
use crate::graphql::query::users::User;
use crate::utils::user_state::{get_user_state, get_user_state_by_username};

#[derive(Enum, Clone, Copy, Serialize, Deserialize, Debug, PartialEq, Eq, Default)]
#[graphql(remote = "crate::api::StoreType")]
pub enum StoreType {
    #[default]
    // Rename for backwards compatibility
    #[serde(rename = "Nirvati")]
    Git,
    Plugin,
}

#[derive(SimpleObject, Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
#[graphql(complex)]
pub struct AppStore {
    pub id: String,
    #[graphql(flatten)]
    #[serde(flatten)]
    pub metadata: StoreMetadata,
    #[serde(default)]
    pub r#type: StoreType,
    pub src: HashMap<String, String>,
    #[graphql(skip)]
    pub apps: Vec<String>,

    #[graphql(skip)]
    #[serde(skip)]
    pub owner: String,

    // Data specific to the app store type
    #[graphql(skip)]
    pub provider_data: serde_yaml::Value,
}

#[ComplexObject]
impl AppStore {
    async fn apps(&self, ctx: &Context<'_>) -> Result<Vec<AppMetadata>> {
        let user_state = get_user_state_by_username(ctx, self.owner.clone()).await;
        let mut registry = get_app_registry_for_user(ctx, user_state).await?;
        let mut manage_client = manage_client(ctx)?;
        let response = manage_client
            .get_updates(tonic::Request::new(crate::api::GetAppUpdateRequest {
                app: None,
                user_state: Some(get_user_state(ctx).await),
            }))
            .await
            .map_err(|_| Error::new("Failed to get app updates"))?
            .into_inner();
        let mut updates: BTreeMap<String, AppUpdate> = BTreeMap::new();
        for (app_id, update) in response.updates {
            if update.current != update.latest {
                updates.insert(app_id, update.into());
            }
        }
        for app in &mut registry {
            app.latest_version
                .set(updates.remove(&app.metadata.id))
                .map_err::<Error, _>(|_| Error::new("Failed to set latest version"))?;
        }
        Ok(registry
            .into_iter()
            .filter(|app| self.apps.contains(&app.metadata.id))
            .collect())
    }

    async fn owner(&self) -> Result<User> {
        Ok(User::from_username(self.owner.clone()))
    }
}
