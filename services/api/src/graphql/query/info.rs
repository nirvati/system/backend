// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Context, Error, Object, Result};
use nirvati_db::schema::prelude::*;
use nirvati_db::schema::user;
use nirvati_db::sea_orm::prelude::*;

#[derive(Default)]
pub struct Info;

#[Object]
impl Info {
    pub async fn is_user_setup(&self, ctx: &Context<'_>) -> Result<bool> {
        let db_client = ctx.data_unchecked::<DatabaseConnection>();
        let count = User::find().count(db_client).await?;
        Ok(count > 0)
    }

    pub async fn is_setup_finished(&self, ctx: &Context<'_>) -> Result<bool> {
        let db_client = ctx.data_unchecked::<DatabaseConnection>();
        User::find()
            .filter(user::Column::SetupFinished.eq(true))
            .count(db_client)
            .await
            .map(|count| count > 0)
            .map_err(|_| Error::new("Failed to check if setup is finished"))
    }

    pub async fn is_citadel_compat(&self) -> Result<bool> {
        Ok(std::env::var("IS_CITADEL_COMPAT").is_ok())
    }
}
