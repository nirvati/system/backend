// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Context, Error, Object, Result};

use crate::api::{Empty, KubeDistro};
use crate::graphql::clients::upgrades_client;

pub struct KubeInfo;

#[Object]
impl KubeInfo {
    async fn installed_version(&self, ctx: &Context<'_>) -> Result<String> {
        let mut client = upgrades_client(ctx)?;
        client
            .get_kube_version(tonic::Request::new(Empty {}))
            .await
            .map(|data| data.into_inner().version)
            .map_err(|err| err.to_string().into())
    }

    /// Returns the latest version of Kubernetes that we can safely upgrade to, taking version skew into account.
    async fn latest_version(&self, ctx: &Context<'_>) -> Result<Option<String>> {
        let mut client = upgrades_client(ctx)?;
        let is_known_distro = client
            .get_kube_version(tonic::Request::new(Empty {}))
            .await
            .map(|data| data.into_inner().distro)
            .map_err::<Error, _>(|err| err.to_string().into())
            .is_ok_and(|distro| distro != KubeDistro::Unknown as i32);
        if is_known_distro {
            client
                .get_latest_kube_version(tonic::Request::new(Empty {}))
                .await
                .map(|data| Some(data.into_inner().version))
                .map_err::<Error, _>(|err| err.to_string().into())
        } else {
            Ok(None)
        }
    }
}
