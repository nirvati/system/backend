// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::Object;

mod kube;

pub struct Upgrades;

#[Object]
impl Upgrades {
    async fn kube(&self) -> kube::KubeInfo {
        kube::KubeInfo
    }
}
