// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{ComplexObject, Context, Error, Result, SimpleObject};
use nirvati_db::schema::domain;
use nirvati_db::schema::prelude::*;
use nirvati_db::sea_orm::prelude::*;
use tonic::Request;

use crate::api::GetCertificateStatusRequest;
use crate::apps::get_app;
use crate::apps::types::AppMetadata;
use crate::graphql::clients::https_client;
use crate::graphql::query::https::user_domain::{dns_provider_from_db, UserDomain};
use crate::graphql::query::users::User;

#[derive(SimpleObject)]
#[graphql(complex)]
pub struct AppDomain {
    pub name: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    #[graphql(skip)]
    pub app_id: String,
    #[graphql(skip)]
    pub owner_name: String,
    #[graphql(skip)]
    pub parent: Option<Uuid>,
}

#[ComplexObject]
impl AppDomain {
    async fn parent_domain(&self, ctx: &Context<'_>) -> Result<Option<UserDomain>> {
        let Some(parent) = self.parent else {
            return Ok(None);
        };
        let db_client = ctx.data::<DatabaseConnection>()?;
        let parent = Domain::find()
            .filter(domain::Column::Name.eq(parent))
            .filter(domain::Column::OwnerName.eq(self.owner_name.clone()))
            .one(db_client)
            .await?
            .ok_or::<Error>("Parent domain not found".into())?;
        Ok(Some(UserDomain {
            created_at: parent.created_at.and_utc(),
            domain: parent.name,
            dns_provider: dns_provider_from_db(parent.provider),
            dns_provider_auth: parent.provider_auth,
            owner_name: parent.owner_name,
            domain_id: parent.id,
        }))
    }
    async fn user(&self) -> Result<User> {
        let user_id = self.owner_name.clone();
        Ok(User::from_username(user_id))
    }

    async fn ready(&self, ctx: &Context<'_>) -> Result<bool> {
        let mut https_client = https_client(ctx)?;
        https_client
            .get_certificate_status(Request::new(GetCertificateStatusRequest {
                namespace: if &self.owner_name == "system" {
                    self.app_id.clone()
                } else {
                    format!("{}-{}", self.owner_name, self.app_id)
                },
                domain: self.name.clone(),
            }))
            .await
            .map_err::<Error, _>(|err| err.to_string().into())
            .map(|response| response.into_inner().ready)
    }

    async fn app(&self, ctx: &Context<'_>) -> Result<AppMetadata> {
        get_app(ctx, &self.app_id).await
    }
}
