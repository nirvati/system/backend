// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::graphql::query::https::app_domain::AppDomain;
use crate::graphql::query::users::User;
use async_graphql::{ComplexObject, Context, Enum, Error, Result, SimpleObject};
use nirvati_db::schema::prelude::*;
use nirvati_db::sea_orm::prelude::*;

#[derive(Enum, PartialEq, Eq, Copy, Clone, Debug)]
pub enum DnsProvider {
    Cloudflare,
    NirvatiMe,
}

impl From<crate::api::DnsProvider> for DnsProvider {
    fn from(provider: crate::api::DnsProvider) -> Self {
        match provider {
            crate::api::DnsProvider::Cloudflare => DnsProvider::Cloudflare,
            crate::api::DnsProvider::NirvatiMe => DnsProvider::NirvatiMe,
        }
    }
}

impl From<DnsProvider> for crate::api::DnsProvider {
    fn from(provider: DnsProvider) -> Self {
        match provider {
            DnsProvider::Cloudflare => crate::api::DnsProvider::Cloudflare,
            DnsProvider::NirvatiMe => crate::api::DnsProvider::NirvatiMe,
        }
    }
}

impl From<DnsProvider> for nirvati_db::schema::sea_orm_active_enums::DnsProvider {
    fn from(provider: DnsProvider) -> Self {
        match provider {
            DnsProvider::Cloudflare => {
                nirvati_db::schema::sea_orm_active_enums::DnsProvider::Cloudflare
            }
            DnsProvider::NirvatiMe => {
                nirvati_db::schema::sea_orm_active_enums::DnsProvider::NirvatiMe
            }
        }
    }
}

pub fn dns_provider_from_db(
    provider: nirvati_db::schema::sea_orm_active_enums::DnsProvider,
) -> Option<DnsProvider> {
    match provider {
        nirvati_db::schema::sea_orm_active_enums::DnsProvider::Cloudflare => {
            Some(DnsProvider::Cloudflare)
        }
        nirvati_db::schema::sea_orm_active_enums::DnsProvider::NirvatiMe => {
            Some(DnsProvider::NirvatiMe)
        }
        nirvati_db::schema::sea_orm_active_enums::DnsProvider::None => None,
    }
}

#[derive(SimpleObject)]
#[graphql(complex)]
pub struct UserDomain {
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub domain: String,
    pub dns_provider: Option<DnsProvider>,
    pub dns_provider_auth: Option<String>,
    #[graphql(skip)]
    pub owner_name: String,
    #[graphql(skip)]
    pub domain_id: Uuid,
}

#[ComplexObject]
impl UserDomain {
    async fn user(&self, ctx: &Context<'_>) -> Result<User> {
        let user_id = self.owner_name.clone();
        let db_client = ctx.data::<DatabaseConnection>()?;
        let user = nirvati_db::schema::prelude::User::find_by_id(user_id.clone())
            .one(db_client)
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            .ok_or::<Error>("User not found".into())?;
        Ok(user.into())
    }

    async fn app_domains(&self, ctx: &Context<'_>) -> Result<Vec<AppDomain>> {
        let db_client = ctx.data::<DatabaseConnection>()?;
        Ok(nirvati_db::schema::prelude::AppDomain::find()
            .filter(
                nirvati_db::schema::app_domain::Column::ParentDomainName.eq(Some(self.domain_id)),
            )
            .find_also_related(AppInstallation)
            .all(db_client)
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            .into_iter()
            .filter_map(|(app_domain, app_install)| {
                let app_install = app_install?;
                Some(AppDomain {
                    name: app_domain.name,
                    created_at: app_domain.created_at.and_utc(),
                    app_id: app_install.app_id,
                    owner_name: app_install.owner_name,
                    parent: app_domain.parent_domain_name,
                })
            })
            .collect())
    }
}
