// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{ComplexObject, Context, Enum, Error, SimpleObject};

use crate::graphql::query::users::User;
use nirvati_db::schema::route;
use nirvati_db::sea_orm::prelude::*;

#[derive(Enum, Copy, Clone, PartialEq, Eq, Debug)]
#[graphql(remote = "nirvati_db::schema::sea_orm_active_enums::Protocol")]
pub enum Protocol {
    Http,
    Https,
}

#[derive(Enum, Copy, Clone, PartialEq, Eq, Debug)]
#[graphql(remote = "nirvati_db::schema::sea_orm_active_enums::UpstreamType")]
pub enum UpstreamType {
    Dns,
    Ip,
}

#[derive(SimpleObject)]
#[graphql(complex)]
pub struct Proxy {
    pub domain: String,
    #[graphql(skip)]
    pub owner: String,
}

#[derive(SimpleObject)]
#[graphql(complex)]
pub struct TcpProxy {
    pub domain: String,
    #[graphql(skip)]
    pub owner: String,
    #[graphql(skip)]
    pub service_id: Uuid,
    pub target_port: u16,
}

#[derive(SimpleObject)]
#[graphql(complex)]
pub struct Route {
    pub id: String,
    pub enable_compression: bool,
    pub local_path: String,
    pub target_path: String,
    #[graphql(skip)]
    pub service_id: Uuid,
    #[graphql(skip)]
    pub owner: String,
    pub protocol: Protocol,
    pub target_port: u16,
}

#[derive(SimpleObject)]
#[graphql(complex)]
pub struct Service {
    pub id: String,
    pub upstream: String,
    pub upstream_type: UpstreamType,
    #[graphql(skip)]
    pub owner: String,
    pub tcp_ports: Vec<u16>,
    pub udp_ports: Vec<u16>,
}

#[ComplexObject]
impl Proxy {
    async fn owner(&self, ctx: &Context<'_>) -> async_graphql::Result<User> {
        let user_id = self.owner.clone();
        let db_client = ctx.data::<DatabaseConnection>()?;
        let user = nirvati_db::schema::prelude::User::find_by_id(user_id)
            .one(db_client)
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            // Should be basically impossible to happen
            .ok_or::<Error>("User not found".into())?;
        Ok(user.into())
    }

    async fn routes(&self, ctx: &Context<'_>) -> async_graphql::Result<Vec<Route>> {
        let db_client = ctx.data::<DatabaseConnection>()?;
        let routes = nirvati_db::schema::prelude::Route::find()
            .filter(route::Column::ProxyDomain.eq(&self.domain))
            .all(db_client)
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?;
        Ok(routes
            .into_iter()
            .map(|route| Route {
                enable_compression: route.enable_compression,
                local_path: route.local_path,
                target_path: route.target_path,
                service_id: route.service_id,
                owner: self.owner.clone(),
                protocol: route.target_protocol.into(),
                target_port: route.target_port as u16,
                id: route.id.to_string(),
            })
            .collect())
    }
}

#[ComplexObject]
impl Route {
    async fn service(&self, ctx: &Context<'_>) -> async_graphql::Result<Service> {
        let db_client = ctx.data::<DatabaseConnection>()?;
        let service = nirvati_db::schema::prelude::Service::find_by_id(self.service_id)
            .one(db_client)
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            // Should be basically impossible to happen
            .ok_or::<Error>("Service not found".into())?;
        Ok(Service {
            id: service.id.to_string(),
            owner: service.owner_name,
            upstream: service.upstream,
            upstream_type: service.r#type.into(),
            tcp_ports: service
                .tcp_ports
                .unwrap_or_default()
                .into_iter()
                .map(|p| p as u16)
                .collect(),
            udp_ports: service
                .udp_ports
                .unwrap_or_default()
                .into_iter()
                .map(|p| p as u16)
                .collect(),
        })
    }

    async fn owner(&self, ctx: &Context<'_>) -> async_graphql::Result<User> {
        let user_id = self.owner.clone();
        let db_client = ctx.data::<DatabaseConnection>()?;
        let user = nirvati_db::schema::prelude::User::find_by_id(user_id)
            .one(db_client)
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            // Should be basically impossible to happen
            .ok_or::<Error>("User not found".into())?;
        Ok(user.into())
    }
}

#[ComplexObject]
impl Service {
    async fn owner(&self, ctx: &Context<'_>) -> async_graphql::Result<User> {
        let user_id = self.owner.clone();
        let db_client = ctx.data::<DatabaseConnection>()?;
        let user = nirvati_db::schema::prelude::User::find_by_id(user_id)
            .one(db_client)
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            // Should be basically impossible to happen
            .ok_or::<Error>("User not found".into())?;
        Ok(user.into())
    }
}

#[ComplexObject]
impl TcpProxy {
    async fn owner(&self, ctx: &Context<'_>) -> async_graphql::Result<User> {
        let user_id = self.owner.clone();
        let db_client = ctx.data::<DatabaseConnection>()?;
        let user = nirvati_db::schema::prelude::User::find_by_id(user_id)
            .one(db_client)
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            // Should be basically impossible to happen
            .ok_or::<Error>("User not found".into())?;
        Ok(user.into())
    }

    async fn service(&self, ctx: &Context<'_>) -> async_graphql::Result<Service> {
        let db_client = ctx.data::<DatabaseConnection>()?;
        let service = nirvati_db::schema::prelude::Service::find_by_id(self.service_id)
            .one(db_client)
            .await
            .map_err::<Error, _>(|err| err.to_string().into())?
            // Should be basically impossible to happen
            .ok_or::<Error>("Service not found".into())?;
        Ok(Service {
            id: service.id.to_string(),
            owner: service.owner_name,
            upstream: service.upstream,
            upstream_type: service.r#type.into(),
            tcp_ports: service
                .tcp_ports
                .unwrap_or_default()
                .into_iter()
                .map(|p| p as u16)
                .collect(),
            udp_ports: service
                .udp_ports
                .unwrap_or_default()
                .into_iter()
                .map(|p| p as u16)
                .collect(),
        })
    }
}
