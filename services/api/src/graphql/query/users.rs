#![allow(non_camel_case_types)]

// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Context, Error, Object, Result, SimpleObject};
use base64::Engine;

use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::{IsLoggedInGuard, PermissionGuard};
pub use crate::graphql::query::users::user::User;
use nirvati::permissions::Permission;
use nirvati_db::sea_orm::prelude::*;
use nirvati_db::sea_orm::sea_query::extension::postgres::PgBinOper;
use nirvati_db::sea_orm::sea_query::BinOper;

pub mod user;

#[derive(SimpleObject)]
pub struct LoginUser {
    pub username: String,
    pub name: String,
    pub avatar: Option<String>,
}
pub struct Users;

#[Object]
impl Users {
    #[graphql(guard = "IsLoggedInGuard::new()")]
    async fn me(&self, ctx: &Context<'_>) -> Result<User> {
        Ok(User::from_username(ctx.data::<UserCtx>()?.username.clone()))
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageUsers)")]
    async fn user(&self, ctx: &Context<'_>, username: String) -> Result<User> {
        let user_ctx = ctx.data::<UserCtx>()?;
        let db = ctx.data::<DatabaseConnection>()?;
        let user_restriction = user_ctx.get_restricted_to_group(None)?;
        let mut users_query = nirvati_db::User::find()
            .filter(nirvati_db::schema::user::Column::Username.eq(username.clone()));
        if let Some(user_restriction) = user_restriction {
            users_query = users_query
                .filter(nirvati_db::schema::user::Column::UserGroupId.eq(user_restriction));
        }
        let user = users_query
            .one(db)
            .await?
            .ok_or(Error::new("User not found"))?;
        Ok(user.into())
    }

    async fn login_usernames(&self, ctx: &Context<'_>) -> Result<Vec<LoginUser>> {
        let db = ctx.data::<DatabaseConnection>()?;
        let users = nirvati_db::schema::user::Entity::find()
            .filter(nirvati_db::schema::user::Column::IsPublic.eq(true))
            .all(db)
            .await?;

        Ok(users
            .into_iter()
            .map(|user| LoginUser {
                username: user.username,
                name: user.name,
                avatar: user
                    .avatar
                    .map(|bytes| base64::engine::general_purpose::URL_SAFE.encode(bytes)),
            })
            .collect())
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageUsers)")]
    async fn count(&self, ctx: &Context<'_>) -> Result<usize> {
        let user_ctx = ctx.data::<UserCtx>()?;
        let user_restriction = user_ctx.get_restricted_to_group(None)?;
        let db = ctx.data::<DatabaseConnection>()?;
        let mut users_query = nirvati_db::User::find();
        if let Some(user_restriction) = user_restriction {
            users_query = users_query
                .filter(nirvati_db::schema::user::Column::UserGroupId.eq(user_restriction));
        }
        let users = users_query.count(db).await?;

        Ok(users as usize)
    }
    #[graphql(guard = "PermissionGuard::new(Permission::ManageUsers)")]
    async fn users(
        &self,
        ctx: &Context<'_>,
        search_name_id: Option<String>,
        user_group: Option<String>,
        email: Option<String>,
    ) -> Result<Vec<User>> {
        let user_ctx = ctx.data::<UserCtx>()?;
        let user_restriction = user_ctx.get_restricted_to_group(user_group)?;
        let mut users_query = nirvati_db::User::find();
        if let Some(user_restriction) = user_restriction {
            users_query = users_query
                .filter(nirvati_db::schema::user::Column::UserGroupId.eq(user_restriction));
        }
        if let Some(ref search_name) = search_name_id {
            users_query = users_query.filter(
                Expr::col(nirvati_db::schema::user::Column::Username)
                    .binary(
                        BinOper::PgOperator(PgBinOper::Similarity),
                        Expr::value(search_name.clone()),
                    )
                    .or(Expr::col(nirvati_db::schema::user::Column::Name).binary(
                        BinOper::PgOperator(PgBinOper::Similarity),
                        Expr::value(search_name.clone()),
                    )),
            );
        }
        if let Some(ref email) = email {
            users_query =
                users_query.filter(nirvati_db::schema::user::Column::Email.contains(email));
        };
        let db = ctx.data::<DatabaseConnection>()?;
        let users = users_query.all(db).await?;
        Ok(users.into_iter().map(|user| user.into()).collect())
    }
}
