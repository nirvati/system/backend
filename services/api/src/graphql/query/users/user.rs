// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{ComplexObject, Context, Error, Result, SimpleObject};
use base64::Engine;
use biscuit::jwa::SignatureAlgorithm;
use biscuit::jws::{RegisteredHeader, Secret};
use biscuit::{ClaimsSet, RegisteredClaims, SingleOrMultiple, JWT};
use hickory_resolver::error::ResolveErrorKind;
use nirvati::permissions::PermissionSet;
use nirvati::JwtAdditionalClaims;
use nirvati_apps::metadata::AppScope;
use tokio::sync::OnceCell;

use crate::api::EntropyRequest;
use crate::apps::types::AppMetadata;
use crate::apps::{get_app, get_app_registry_for_user, get_app_with_settings, get_stores_yml};
use crate::graphql::auth_ctx::UserCtx;
use crate::graphql::auth_guard::PermissionGuard;
use crate::graphql::clients::{entropy_client, manage_client};
use crate::graphql::query::apps::store::AppStore;
use crate::graphql::query::https::acme_provider::AcmeProvider;
use crate::graphql::query::https::app_domain::AppDomain;
use crate::graphql::query::https::proxy::{Proxy, Service};
use crate::graphql::query::https::user_domain::{dns_provider_from_db, UserDomain};
use crate::graphql::query::nirvati_me;
use crate::graphql::query::nirvati_me::NirvatiMeDetails;
use crate::utils::user_state::get_user_state_by_username;
use nirvati_db::schema::prelude::*;
use nirvati_db::schema::{acme_account, app_installation, domain, passkey, proxy, service, user};
use nirvati_db::sea_orm::prelude::*;

use super::Permission;

impl From<user::Model> for UserCache {
    fn from(data: user::Model) -> Self {
        UserCache {
            name: data.name,
            permissions: (data.permissions as u16).into(),
            email: data.email,
            last_verified_email: data.confirmed_email,
            created_at: data.created_at.and_utc(),
            enabled: data.is_enabled,
            is_public: data.is_public,
            acme_providers: OnceCell::new(),
            avatar: data
                .avatar
                .map(|byte| base64::engine::general_purpose::URL_SAFE.encode(byte)),
            setup_finished: data.setup_finished,
            passkeys: /*data.passkeys.into_iter().map(|passkey| PassKey {
                display_name: passkey.display_name,
                key_id_base64: base64::engine::general_purpose::URL_SAFE.encode(passkey.credential_id),
                created_at: passkey.created_at.into(),
                rp_id: passkey.rp_id,
            }).collect()*/ OnceCell::new(),
        }
    }
}

impl From<user::Model> for User {
    fn from(data: user::Model) -> Self {
        User {
            username: data.username.to_owned(),
            _cache: OnceCell::new_with(Some(data.into())),
        }
    }
}

#[derive(Clone, SimpleObject)]
pub struct PassKey {
    pub display_name: String,
    pub key_id_base64: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub rp_id: String,
}

#[derive(Clone)]
pub struct UserCache {
    pub name: String,
    pub permissions: PermissionSet,
    pub email: Option<String>,
    pub last_verified_email: Option<String>,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub enabled: bool,
    pub acme_providers: OnceCell<Vec<AcmeProvider>>,
    pub avatar: Option<String>,
    pub is_public: bool,
    pub setup_finished: bool,
    pub passkeys: OnceCell<Vec<PassKey>>,
}

#[derive(Clone, SimpleObject)]
#[graphql(complex)]
pub struct User {
    pub username: String,
    #[graphql(skip)]
    pub _cache: OnceCell<UserCache>,
}

#[ComplexObject]
impl User {
    #[graphql(skip)]
    pub fn from_username(name: String) -> Self {
        Self {
            username: name,
            _cache: OnceCell::new(),
        }
    }

    // To get access to a user object, the authorization is checked already, so we don't need a check here
    async fn domains(&self, ctx: &Context<'_>) -> Result<Vec<UserDomain>> {
        let user_domains = Domain::find()
            .filter(domain::Column::OwnerName.eq(self.username.clone()))
            .all(ctx.data_unchecked::<DatabaseConnection>())
            .await?;
        Ok(user_domains
            .into_iter()
            .map(|domain| UserDomain {
                created_at: domain.created_at.and_utc(),
                domain: domain.name,
                dns_provider: dns_provider_from_db(domain.provider),
                dns_provider_auth: domain.provider_auth,
                owner_name: domain.owner_name,
                domain_id: domain.id,
            })
            .collect())
    }

    async fn app_domains(&self, ctx: &Context<'_>) -> Result<Vec<AppDomain>> {
        let db = ctx.data_unchecked::<DatabaseConnection>();
        let domains = nirvati_db::schema::app_domain::Entity::find()
            .find_also_related(AppInstallation)
            .filter(app_installation::Column::OwnerName.eq(self.username.clone()))
            .all(db)
            .await?;
        Ok(domains
            .into_iter()
            .map(|(domain, app_install)| {
                let install = app_install.unwrap();
                AppDomain {
                    name: domain.name,
                    app_id: install.app_id,
                    owner_name: install.owner_name,
                    created_at: domain.created_at.and_utc(),
                    parent: domain.parent_domain_name,
                }
            })
            .collect())
    }

    #[graphql(skip)]
    async fn cache(&self, ctx: &Context<'_>) -> Result<&UserCache> {
        self._cache
            .get_or_try_init(|| async {
                let db = ctx.data_unchecked::<DatabaseConnection>();
                let user = user::Entity::find_by_id(self.username.clone())
                    .one(db)
                    .await
                    .map_err(|_| Error::new("Failed to find user"))?
                    .ok_or(Error::new("Failed to find user"))?;
                Ok(user.into())
            })
            .await
    }

    async fn name(&self, ctx: &Context<'_>) -> Result<String> {
        Ok(self.cache(ctx).await?.name.clone())
    }

    async fn permissions(&self, ctx: &Context<'_>) -> Result<Vec<Permission>> {
        Ok(self.cache(ctx).await?.permissions.into_iter().collect())
    }
    async fn email(&self, ctx: &Context<'_>) -> Result<Option<String>> {
        Ok(self.cache(ctx).await?.email.clone())
    }
    async fn last_verified_email(&self, ctx: &Context<'_>) -> Result<Option<String>> {
        Ok(self.cache(ctx).await?.last_verified_email.clone())
    }
    async fn created_at(&self, ctx: &Context<'_>) -> Result<chrono::DateTime<chrono::Utc>> {
        Ok(self.cache(ctx).await?.created_at)
    }
    async fn enabled(&self, ctx: &Context<'_>) -> Result<bool> {
        Ok(self.cache(ctx).await?.enabled)
    }
    async fn is_public(&self, ctx: &Context<'_>) -> Result<bool> {
        Ok(self.cache(ctx).await?.is_public)
    }
    async fn setup_finished(&self, ctx: &Context<'_>) -> Result<bool> {
        Ok(self.cache(ctx).await?.setup_finished)
    }
    async fn acme_providers(&self, ctx: &Context<'_>) -> Result<Vec<AcmeProvider>> {
        let cache = self.cache(ctx).await?;
        Ok(cache
            .acme_providers
            .get_or_try_init(|| async {
                let db = ctx.data_unchecked::<DatabaseConnection>();
                Ok::<_, Error>(
                    AcmeAccount::find()
                        .filter(acme_account::Column::OwnerName.eq(self.username.clone()))
                        .all(db)
                        .await
                        .map_err(|_| Error::new("Failed to find acme providers"))?
                        .into_iter()
                        .map(|acme_account| acme_account.provider.into())
                        .collect(),
                )
            })
            .await?
            .clone())
    }

    async fn passkeys(&self, ctx: &Context<'_>) -> Result<Vec<PassKey>> {
        let cache = self.cache(ctx).await?;
        Ok(cache
            .passkeys
            .get_or_try_init(|| async {
                let db = ctx.data_unchecked::<DatabaseConnection>();
                Ok::<_, Error>(
                    Passkey::find()
                        .filter(passkey::Column::OwnerName.eq(self.username.clone()))
                        .all(db)
                        .await
                        .map_err(|_| Error::new("Failed to find acme providers"))?
                        .into_iter()
                        .map(|passkey| PassKey {
                            display_name: passkey.display_name,
                            key_id_base64: base64::engine::general_purpose::URL_SAFE
                                .encode(&passkey.credential_id),
                            created_at: passkey.created_at.and_utc(),
                            rp_id: passkey.rp_id,
                        })
                        .collect(),
                )
            })
            .await?
            .clone())
    }

    async fn avatar(&self, ctx: &Context<'_>) -> Result<Option<String>> {
        Ok(self.cache(ctx).await?.avatar.clone())
    }

    async fn stores(&self, ctx: &Context<'_>) -> Result<Vec<AppStore>> {
        let user_state = get_user_state_by_username(ctx, self.username.clone()).await;
        get_stores_yml(ctx, user_state)
            .await
            .map_err::<Error, _>(|err| {
                Error::new(format!(
                    "Failed to get stores for user {}: {}",
                    self.username, err
                ))
            })
    }

    async fn store(&self, ctx: &Context<'_>, id: String) -> Result<AppStore> {
        let user_state = get_user_state_by_username(ctx, self.username.clone()).await;
        get_stores_yml(ctx, user_state)
            .await
            .map_err::<Error, _>(|err| {
                Error::new(format!(
                    "Failed to get stores for user {}: {}",
                    self.username, err
                ))
            })?
            .into_iter()
            .find(|store| store.id == id)
            .ok_or(Error::new("Store not found"))
    }

    async fn apps(
        &self,
        ctx: &Context<'_>,
        installed_only: Option<bool>,
    ) -> Result<Vec<AppMetadata>> {
        let user_state = get_user_state_by_username(ctx, self.username.clone()).await;
        let installed_only = installed_only.unwrap_or(false);
        let mut app_registry = get_app_registry_for_user(ctx, user_state).await?;
        if ctx.look_ahead().field("latestVersion").exists() {
            let mut manage_client = manage_client(ctx)?;
            let mut all_updates = manage_client
                .get_updates(tonic::Request::new(crate::api::GetAppUpdateRequest {
                    app: None,
                    user_state: Some(get_user_state_by_username(ctx, self.username.clone()).await),
                }))
                .await
                .map_err(|_| Error::new("Failed to get app updates"))?
                .into_inner()
                .updates;
            if ctx
                .data_unchecked::<UserCtx>()
                .permissions
                .has(Permission::ManageSysComponents)
            {
                let all_sys_updates = manage_client
                    .get_updates(tonic::Request::new(crate::api::GetAppUpdateRequest {
                        app: None,
                        user_state: Some(
                            get_user_state_by_username(ctx, "system".to_string()).await,
                        ),
                    }))
                    .await
                    .map_err(|_| Error::new("Failed to get app updates"))?
                    .into_inner()
                    .updates;
                all_updates.extend(all_sys_updates);
            }
            for app in &mut app_registry {
                if app.metadata.scope == AppScope::User {
                    app.latest_version
                        .set(
                            all_updates
                                .get(&app.metadata.id)
                                .cloned()
                                .map(|update| update.into()),
                        )
                        .map_err::<Error, _>(|_| Error::new("Failed to set latest version"))?;
                }
            }
        }
        if installed_only {
            Ok(app_registry
                .into_iter()
                .filter(|app| app.installed)
                .collect())
        } else {
            Ok(app_registry)
        }
    }

    async fn app(
        &self,
        ctx: &Context<'_>,
        app_id: String,
        settings: Option<serde_json::Value>,
        initial_domain: Option<String>,
    ) -> Result<AppMetadata> {
        if settings.is_some() || initial_domain.is_some() {
            get_app_with_settings(ctx, &app_id, settings, initial_domain).await
        } else {
            get_app(ctx, &app_id).await
        }
    }

    async fn jwt(&self, ctx: &Context<'_>, audience: Option<Vec<String>>) -> Result<String> {
        let client = ctx.data::<DatabaseConnection>()?;
        // We do another call to find to make sure a deleted user can not keep issuing JWTs for themselves
        // This can be bypassed by Instance admins, who can currently issue JWTs for other admins
        // We will fix that long-term, but for now it's not a big issue considering that instance admin permissions are not given out lightly
        let user = user::Entity::find_by_id(self.username.clone())
            .one(client)
            .await
            .map_err(|_| Error::new("Failed to find user"))?
            .ok_or(Error::new("Failed to find user"))?;
        let secret = ctx.data_unchecked::<Secret>();
        let user_ctx = ctx.data_unchecked::<UserCtx>();
        if user.totp_secret.is_some() && !user_ctx.is_totp_session {
            return Err(Error::new(
                "TOTP was recently enabled - Please log in again",
            ));
        }
        if audience.is_some() && !user_ctx.audience.contains(&"dashboard".to_string()) {
            return Err(Error::new("Setting the audience is not allowed"));
        }
        let claims = ClaimsSet::<JwtAdditionalClaims> {
            registered: RegisteredClaims {
                issuer: Some("nirvati".to_string()),
                subject: Some(self.username.clone()),
                issued_at: Some(chrono::Utc::now().into()),
                expiry: Some((chrono::Utc::now() + chrono::Duration::minutes(10)).into()),
                audience: Some(SingleOrMultiple::Multiple(
                    audience.unwrap_or(user_ctx.audience.clone()),
                )),
                ..Default::default()
            },
            private: JwtAdditionalClaims {
                user_group: user.user_group_id,
                permission_set: (user.permissions as u16).into(),
                email: user.email,
                is_totp_enabled: user_ctx.is_totp_session,
                is_mock_user: None,
            },
        };
        let token = JWT::new_decoded(
            From::from(RegisteredHeader {
                algorithm: SignatureAlgorithm::RS512,
                ..Default::default()
            }),
            claims.clone(),
        );

        let token = token
            .into_encoded(secret)
            .map_err(|_| Error::new("Failed to encode token"))?;
        Ok(token.unwrap_encoded().to_string())
    }

    pub async fn entropy(&self, ctx: &Context<'_>, identifier: String) -> Result<String> {
        let mut client = entropy_client(ctx)?;
        client
            .derive_entropy(tonic::Request::new(EntropyRequest {
                input: format!("{}:{}", self.username, identifier),
            }))
            .await
            .map(|response| response.into_inner().entropy)
            .map_err::<Error, _>(|_| Error::new("Failed to derive entropy"))
    }

    async fn domain_verification_code(&self, ctx: &Context<'_>, domain: String) -> Result<String> {
        let mut client = entropy_client(ctx)?;
        client
            .derive_entropy(tonic::Request::new(EntropyRequest {
                input: format!("{}:domain:{}", self.username, domain),
            }))
            .await
            .map(|response| response.into_inner().entropy)
            .map_err::<Error, _>(|_| Error::new("Failed to derive entropy"))
    }

    async fn verify_txt(&self, ctx: &Context<'_>, domain: String) -> Result<bool> {
        let username = ctx.data_unchecked::<UserCtx>().username.clone();
        let mut client = entropy_client(ctx)?;
        let verification_code = client
            .derive_entropy(tonic::Request::new(EntropyRequest {
                input: format!("{}:domain:{}", &username, domain),
            }))
            .await
            .map(|response| response.into_inner().entropy)
            .map_err::<Error, _>(|_| Error::new("Failed to derive entropy"))?;
        let verification_code = format!("nirvati-verification={}", verification_code);
        let resolver = hickory_resolver::AsyncResolver::tokio_from_system_conf()
            .map_err::<Error, _>(|_| Error::new("Failed to create DNS resolver"))?;
        let value = resolver
            .txt_lookup(&domain)
            .await
            .map_err::<Error, _>(|e| match e.kind() {
                ResolveErrorKind::NoRecordsFound { .. } => {
                    Error::new("Failed to query TXT verification record: Record does not exist!")
                }
                _ => Error::new(format!("Failed to query TXT verification record: {:#?}", e)),
            })?;
        Ok(value.iter().any(|val| val.to_string() == verification_code))
    }

    #[graphql(guard = "PermissionGuard::new(Permission::UseNirvatiMe)")]
    async fn nirvati_me(&self, ctx: &Context<'_>) -> Result<Option<NirvatiMeDetails>> {
        let mut client = entropy_client(ctx)?;
        let username = client
            .derive_entropy(tonic::Request::new(EntropyRequest {
                input: format!("{}:nirvati.me-username", self.username),
            }))
            .await
            .map(|response| response.into_inner().entropy)
            .map_err::<Error, _>(|_| Error::new("Failed to derive entropy"))?;
        let password = client
            .derive_entropy(tonic::Request::new(EntropyRequest {
                input: format!("{}:nirvati.me-password", self.username),
            }))
            .await
            .map(|response| response.into_inner().entropy)
            .map_err::<Error, _>(|_| Error::new("Failed to derive entropy"))?;
        let result = nirvati_me::get_details(&username, &password).await;
        if let Err(err) = &result {
            tracing::info!("Failed to get Nirvati.me details: {}", err);
            Ok(None)
        } else {
            Ok(nirvati_me::get_details(&username, &password).await.ok())
        }
    }

    async fn proxies(&self, ctx: &Context<'_>) -> Result<Vec<Proxy>> {
        let db_client = ctx.data_unchecked::<DatabaseConnection>();
        proxy::Entity::find()
            .filter(proxy::Column::OwnerName.eq(self.username.clone()))
            .all(db_client)
            .await
            .map_err(|_| Error::new("Failed to find proxies"))
            .map(|proxies| {
                proxies
                    .into_iter()
                    .map(|proxy| Proxy {
                        domain: proxy.domain,
                        owner: proxy.owner_name,
                    })
                    .collect()
            })
    }

    async fn proxy(&self, ctx: &Context<'_>, domain: String) -> Result<Proxy> {
        let db_client = ctx.data_unchecked::<DatabaseConnection>();
        let proxy = proxy::Entity::find()
            .filter(proxy::Column::OwnerName.eq(self.username.clone()))
            .filter(proxy::Column::Domain.eq(domain))
            .one(db_client)
            .await
            .map_err(|_| Error::new("Failed to find proxy"))?
            .ok_or(Error::new("Failed to find proxy"))?;
        Ok(Proxy {
            domain: proxy.domain,
            owner: proxy.owner_name,
        })
    }

    async fn services(&self, ctx: &Context<'_>) -> Result<Vec<Service>> {
        let db_client = ctx.data_unchecked::<DatabaseConnection>();
        service::Entity::find()
            .filter(service::Column::OwnerName.eq(self.username.clone()))
            .all(db_client)
            .await
            .map_err(|_| Error::new("Failed to find services"))
            .map(|services| {
                services
                    .into_iter()
                    .map(|service| Service {
                        id: service.id.to_string(),
                        owner: service.owner_name,
                        upstream: service.upstream,
                        upstream_type: service.r#type.into(),
                        tcp_ports: service
                            .tcp_ports
                            .unwrap_or_default()
                            .into_iter()
                            .map(|p| p as u16)
                            .collect(),
                        udp_ports: service
                            .udp_ports
                            .unwrap_or_default()
                            .into_iter()
                            .map(|p| p as u16)
                            .collect(),
                    })
                    .collect()
            })
    }

    async fn service(&self, ctx: &Context<'_>, id: String) -> Result<Service> {
        let db_client = ctx.data_unchecked::<DatabaseConnection>();
        service::Entity::find()
            .filter(service::Column::OwnerName.eq(self.username.clone()))
            .filter(service::Column::Id.eq(id.clone()))
            .one(db_client)
            .await
            .map_err(|_| Error::new("Failed to find service"))?
            .map(|service| Service {
                id: service.id.to_string(),
                owner: service.owner_name,
                upstream: service.upstream,
                upstream_type: service.r#type.into(),
                tcp_ports: service
                    .tcp_ports
                    .unwrap_or_default()
                    .into_iter()
                    .map(|p| p as u16)
                    .collect(),
                udp_ports: service
                    .udp_ports
                    .unwrap_or_default()
                    .into_iter()
                    .map(|p| p as u16)
                    .collect(),
            })
            .ok_or(Error::new("Failed to find service"))
    }
}
