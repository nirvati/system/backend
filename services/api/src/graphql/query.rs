// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::Object;

use nirvati::permissions::Permission;

use super::auth_guard::PermissionGuard;

pub(crate) mod apps;
pub(crate) mod https;
mod info;
mod network;
pub(crate) mod nirvati_me;
mod storage;
mod upgrades;
pub(crate) mod users;

pub struct Query;

#[Object]
impl Query {
    async fn network(&self) -> network::Network {
        network::Network
    }
    async fn info(&self) -> info::Info {
        info::Info
    }

    #[graphql(guard = "PermissionGuard::new(Permission::ManageSysComponents)")]
    async fn upgrades(&self) -> upgrades::Upgrades {
        upgrades::Upgrades
    }

    /*async fn https(&self) -> https::Https {
        https::Https
    }*/

    async fn users(&self) -> users::Users {
        users::Users
    }
}
