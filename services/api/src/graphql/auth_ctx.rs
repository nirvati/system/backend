// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use async_graphql::{Error, Result};

use nirvati::permissions::{Permission, PermissionSet};
use nirvati_db::schema::prelude::*;
use nirvati_db::schema::user;
use nirvati_db::sea_orm::prelude::*;

pub struct UserCtx {
    pub username: String,
    pub permissions: PermissionSet,
    pub user_group: String,
    // Include this because it's needed in a few places, and we don't want to always get it from the DB
    pub email: Option<String>,
    pub audience: Vec<String>,
    pub is_totp_session: bool,
    pub is_mock_user: bool,
}

impl UserCtx {
    pub fn get_restricted_to_group(
        &self,
        requested_group: Option<String>,
    ) -> Result<Option<String>> {
        Ok(if self.permissions.has(Permission::ManageGroups) {
            requested_group
        } else if let Some(user_group) = requested_group {
            if user_group != self.user_group {
                return Err(Error::new("Forbidden"));
            };
            Some(user_group)
        } else {
            Some(self.user_group.clone())
        })
    }

    pub async fn get_details(&self, db: &DatabaseConnection) -> Result<user::Model> {
        User::find_by_id(self.username.clone())
            .one(db)
            .await
            .map_err(|_| Error::new("User not found"))?
            .ok_or(Error::new("User not found"))
    }
}
