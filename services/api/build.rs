// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

pub fn main() {
    tonic_build::configure()
        .protoc_arg("--experimental_allow_proto3_optional")
        .compile_protos(
            &[
                "../agent/protos/apps.proto",
                "../agent/protos/ingress.proto",
                "../agent/protos/core.proto",
                "../agent/protos/https.proto",
                "../agent/protos/setup.proto",
                "../agent/protos/citadel-compat.proto",
                "../../shared/saas/protos/saas.proto",
            ],
            &["../agent/protos", "../../shared/saas/protos"],
        )
        .unwrap();
}
