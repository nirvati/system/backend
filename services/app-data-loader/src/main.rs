use nirvati_apps::metadata::PreloadType;
use std::path::Path;
use tonic::Request;

mod api {
    tonic::include_proto!("nirvati_public");
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let app_id = std::env::var("APP_ID").expect("APP_ID must be set");
    let user_id = std::env::var("USER_ID").expect("USER_ID must be set");
    let target_endpoint = std::env::var("TARGET_ENDPOINT").expect("TARGET_ENDPOINT must be set");
    let mut client = api::apps_public_client::AppsPublicClient::connect(target_endpoint)
        .await
        .unwrap();
    let app_data = client
        .get_app_dir(Request::new(api::GetAppDirRequest {
            app_id: app_id.clone(),
            user_id: user_id.clone(),
        }))
        .await
        .unwrap()
        .into_inner()
        .app_dir;
    let volumes = std::env::var("VOLUMES").expect("VOLUMES must be set");
    let preload_type = std::env::var("PRELOAD_TYPE").expect("PRELOAD_TYPE must be set");
    let preload_type: nirvati_apps::metadata::PreloadType =
        serde_json::from_str(&preload_type).expect("Invalid preload type");
    let volumes: Vec<_> = volumes.split(',').collect();
    // Use flate2 and tar to extract the app data to /tmp/app
    let archive = flate2::read::GzDecoder::new(std::io::Cursor::new(app_data));
    let mut archive = tar::Archive::new(archive);
    archive.unpack("/tmp/app").unwrap();
    let tmp_dir = Path::new("/tmp/app/data");
    for volume in volumes {
        let volume_preload_dir = match preload_type {
            PreloadType::Nirvati => tmp_dir.join(volume),
            PreloadType::MainVolumeFromSubdir(ref subdir) => tmp_dir.join("..").join(subdir),
        };
        let volume_path = Path::new("/volumes").join(volume);
        if volume_preload_dir.exists() {
            fs_extra::dir::copy(
                volume_preload_dir,
                volume_path,
                &fs_extra::dir::CopyOptions::new()
                    .copy_inside(true)
                    .content_only(true),
            )
            .expect("Failed to preload data");
        }
    }
}
