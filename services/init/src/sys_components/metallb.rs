// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::path::Path;

use anyhow::{bail, Result};
use k8s_crds_metallb::{IPAddressPool, L2Advertisement};
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use kube::Client;
use nirvati::kubernetes::{apply_with_ns, get_node_ip};
use nirvati_agent::apps::generator::helm::generate_chart;
use nirvati_agent::apps::generator::internal::ResolverCtx;
use nirvati_agent::apps::generator::kubernetes::generate_kubernetes_config;
use nirvati_agent::plugins::api::UserState;

pub async fn install_metallb(
    kube_client: Client,
    fake_global_apps_root: &Path,
    fake_user_apps_root: &Path,
    nirvati_seed: &str,
    user_state: &UserState,
    resolver_ctx: &ResolverCtx,
) -> Result<()> {
    let metallb_app = nirvati_agent::apps::parser::load_app(
        kube_client.clone(),
        fake_global_apps_root,
        "metallb",
        &fake_user_apps_root.join("metallb"),
        user_state.clone(),
        nirvati_seed,
        None,
        resolver_ctx,
    )
    .await
    .expect("Failed to load MetalLB app");
    let metadata = metallb_app.get_metadata().clone();
    let kube_config =
        generate_kubernetes_config(kube_client, metallb_app, user_state, resolver_ctx)
            .await
            .expect("Failed to generate Kubernetes config");
    let (helm_chart, _) = generate_chart(kube_config, metadata, "system", "default")
        .expect("Failed to turn app into Helm chart");
    let chart_dir = temp_dir::TempDir::new().expect("Failed to create temp dir");
    let chart_file_path = chart_dir.path().join("chart.tgz");
    std::fs::write(&chart_file_path, helm_chart).expect("Failed to write chart to file");
    let helm_install = tokio::process::Command::new("helm")
        .args([
            "upgrade",
            "metallb",
            chart_file_path
                .to_str()
                .expect("Failed to convert chart file path to string"),
            "--namespace",
            "metallb-system",
            "--create-namespace",
            "--install",
            "--force",
        ])
        .output()
        .await
        .expect("Failed to run helm install");
    if !helm_install.status.success() {
        bail!(
            "Failed to install MetalLB app: {}",
            String::from_utf8_lossy(&helm_install.stderr)
        );
    }
    Ok(())
}

pub async fn configure_metallb(client: Client) -> Result<()> {
    let ip = get_node_ip(&client).await?;
    let pool = IPAddressPool {
        metadata: ObjectMeta {
            name: Some("default".to_string()),
            ..Default::default()
        },
        spec: k8s_crds_metallb::IPAddressPoolSpec {
            addresses: vec![if ip.contains(":") {
                format!("{}/128", ip)
            } else {
                format!("{}/32", ip)
            }],
            ..Default::default()
        },
    };
    apply_with_ns(client.clone(), &[pool], "metallb-system").await?;
    let l2 = L2Advertisement {
        metadata: ObjectMeta {
            name: Some("default".to_string()),
            ..Default::default()
        },
        spec: k8s_crds_metallb::L2AdvertisementSpec {
            ..Default::default()
        },
    };
    apply_with_ns(client, &[l2], "metallb-system").await?;
    Ok(())
}
