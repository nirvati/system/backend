// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::path::Path;

use anyhow::Result;
use kube::Client;
use nirvati_agent::apps::generator::helm::generate_chart;
use nirvati_agent::apps::generator::internal::ResolverCtx;
use nirvati_agent::apps::generator::kubernetes::generate_kubernetes_config;
use nirvati_agent::plugins::api::UserState;

pub async fn install_coredns(
    kube_client: Client,
    fake_global_apps_root: &Path,
    fake_user_apps_root: &Path,
    nirvati_seed: &str,
    user_state: &UserState,
    resolver_ctx: &ResolverCtx,
) -> Result<()> {
    let coredns_app = nirvati_agent::apps::parser::load_app(
        kube_client.clone(),
        fake_global_apps_root,
        "coredns",
        &fake_user_apps_root.join("coredns"),
        user_state.clone(),
        nirvati_seed,
        None,
        resolver_ctx,
    )
    .await
    .expect("Failed to load CoreDNS app");
    let metadata = coredns_app.get_metadata().clone();
    let kube_config =
        generate_kubernetes_config(kube_client, coredns_app, user_state, resolver_ctx)
            .await
            .expect("Failed to generate Kubernetes config");
    let (helm_chart, _) = generate_chart(kube_config, metadata, "system", "default")
        .expect("Failed to turn app into Helm chart");
    let chart_dir = temp_dir::TempDir::new().expect("Failed to create temp dir");
    let chart_file_path = chart_dir.path().join("chart.tgz");
    std::fs::write(&chart_file_path, helm_chart).expect("Failed to write chart to file");
    let helm_install = tokio::process::Command::new("helm")
        .args([
            "upgrade",
            "coredns",
            chart_file_path
                .to_str()
                .expect("Failed to convert chart file path to string"),
            "--namespace",
            "coredns",
            "--create-namespace",
            "--install",
            "--force",
        ])
        .output()
        .await
        .expect("Failed to run helm install");
    if !helm_install.status.success() {
        tracing::error!(
            "Failed to install Coredns app: {}",
            String::from_utf8_lossy(&helm_install.stderr)
        );
        std::process::exit(1);
    }
    Ok(())
}
