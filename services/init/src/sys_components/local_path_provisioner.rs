// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::path::Path;

use anyhow::Result;
use kube::{Api, Client};

use nirvati_agent::apps::generator::helm::generate_chart;
use nirvati_agent::apps::generator::internal::ResolverCtx;
use nirvati_agent::apps::generator::kubernetes::generate_kubernetes_config;
use nirvati_agent::plugins::api::UserState;

pub async fn install(
    kube_client: &Client,
    fake_global_apps_root: &Path,
    fake_user_apps_root: &Path,
    nirvati_seed: &str,
    user_state: &UserState,
    resolver_ctx: &ResolverCtx,
) -> Result<()> {
    let local_path_provisioner_app = nirvati_agent::apps::parser::load_app(
        kube_client.clone(),
        fake_global_apps_root,
        "local-path-provisioner",
        &fake_user_apps_root.join("local-path-provisioner"),
        user_state.clone(),
        nirvati_seed,
        None,
        resolver_ctx,
    )
    .await
    .expect("Failed to load local path provisioner");
    let metadata = local_path_provisioner_app.get_metadata().clone();
    let kube_config = generate_kubernetes_config(
        kube_client.clone(),
        local_path_provisioner_app,
        user_state,
        resolver_ctx,
    )
    .await
    .expect("Failed to generate Kubernetes config");
    let (helm_chart, _) = generate_chart(kube_config, metadata, "system", "default")
        .expect("Failed to turn app into Helm chart");
    let chart_dir = temp_dir::TempDir::new().expect("Failed to create temp dir");
    let chart_file_path = chart_dir.path().join("chart.tgz");
    std::fs::write(&chart_file_path, helm_chart).expect("Failed to write chart to file");
    let helm_install = tokio::process::Command::new("helm")
        .args([
            "upgrade",
            "local-path-provisioner",
            chart_file_path
                .to_str()
                .expect("Failed to convert chart file path to string"),
            "--namespace",
            "local-path-provisioner",
            "--create-namespace",
            "--install",
        ])
        .output()
        .await
        .expect("Failed to run helm install");
    if !helm_install.status.success() {
        tracing::error!(
            "Failed to install Local path provisioner: {}",
            String::from_utf8_lossy(&helm_install.stderr)
        );
        std::process::exit(1);
    }
    Ok(())
}

pub async fn is_installed(client: &Client) -> Result<bool> {
    let api: Api<k8s_openapi::api::core::v1::Namespace> = Api::all(client.clone());
    let namespaces = api.list(&Default::default()).await?;
    let has_ns = namespaces
        .iter()
        .any(|ns| ns.metadata.name == Some("local-path-provisioner".into()));
    Ok(has_ns)
}
