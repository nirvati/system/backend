// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use k8s_crds_helm_controller::*;
use k8s_crds_traefik::*;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;

pub fn compress_mw() -> Middleware {
    Middleware {
        metadata: k8s_meta::ObjectMeta {
            name: Some("compress".to_string()),
            namespace: Some("nirvati".to_string()),
            ..Default::default()
        },
        spec: MiddlewareSpec {
            compress: Some(MiddlewareCompress {
                ..Default::default()
            }),
            ..Default::default()
        },
    }
}

pub fn https_redirect_mw() -> Middleware {
    Middleware {
        metadata: k8s_meta::ObjectMeta {
            name: Some("https-redirect".to_string()),
            namespace: Some("nirvati".to_string()),
            ..Default::default()
        },
        spec: MiddlewareSpec {
            redirect_scheme: Some(MiddlewareRedirectScheme {
                permanent: Some(true),
                scheme: Some("https".to_string()),
                ..Default::default()
            }),
            ..Default::default()
        },
    }
}

pub fn traefik_helm_config() -> HelmChartConfig {
    HelmChartConfig {
        metadata: k8s_meta::ObjectMeta {
            name: Some("traefik".to_string()),
            namespace: Some("kube-system".to_string()),
            ..Default::default()
        },
        spec: HelmChartConfigSpec {
            values_content: Some(
                r#"globalArguments:
- "--providers.kubernetescrd.allowCrossNamespace=true"
- "--providers.kubernetescrd.allowExternalNameServices=true"
service:
  annotations:
    metallb.universe.tf/allow-shared-ip: "ip_set_main"
"#
                .to_string(),
            ),
            ..Default::default()
        },
    }
}

pub fn cilium_helm_config() -> HelmChartConfig {
    HelmChartConfig {
        metadata: k8s_meta::ObjectMeta {
            name: Some("rke2-cilium".to_string()),
            namespace: Some("kube-system".to_string()),
            ..Default::default()
        },
        spec: HelmChartConfigSpec {
            values_content: Some(
                r#"kubeProxyReplacement: true
nodeIPAM:
    enabled: true"#
                    .to_string(),
            ),
            ..Default::default()
        },
    }
}
