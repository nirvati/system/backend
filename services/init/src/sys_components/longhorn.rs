// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::path::Path;

use anyhow::Result;
use k8s_crds_longhorn::{Node, NodeDisks, NodeDisksDiskType, Setting};
use k8s_openapi::api::core::v1::ConfigMap;
use k8s_openapi::api::storage::v1::StorageClass;
use kube::api::{Patch, PatchParams};
use kube::{Api, Client};
use slugify::slugify;

use nirvati_agent::apps::generator::helm::generate_chart;
use nirvati_agent::apps::generator::internal::ResolverCtx;
use nirvati_agent::apps::generator::kubernetes::generate_kubernetes_config;
use nirvati_agent::plugins::api::UserState;

use crate::{disk, StorageLayer};

pub async fn is_installed(client: &Client) -> Result<bool> {
    // Check whether the cert-manager namespace exists
    let api: Api<k8s_openapi::api::core::v1::Namespace> = Api::all(client.clone());
    let namespaces = api.list(&Default::default()).await?;
    let has_ns = namespaces
        .iter()
        .any(|ns| ns.metadata.name == Some("longhorn-system".into()));
    Ok(has_ns)
}

pub async fn set_default_replica_count(client: &Client, replicas: u8) -> Result<()> {
    let pp = PatchParams {
        force: true,
        field_manager: Some("nirvati".to_string()),
        ..Default::default()
    };
    {
        set_setting(client, "default-replica-count", &replicas.to_string()).await?;
    }
    {
        let config_api: Api<ConfigMap> = Api::namespaced(client.clone(), "longhorn-system");
        let mut config = config_api.get("longhorn-storageclass").await?;
        config.metadata.managed_fields = None;
        let storage_class_data = config
            .data
            .clone()
            .ok_or(anyhow::anyhow!("No data in configmap"))?;
        let storage_class_yml = storage_class_data
            .get("storageclass.yaml")
            .ok_or(anyhow::anyhow!("No storageclass.yaml in configmap"))?;
        let mut storage_class_yml: serde_yaml::Value = serde_yaml::from_str(storage_class_yml)?;
        storage_class_yml
            .get_mut("parameters")
            .ok_or(anyhow::anyhow!("No parameters in storage-class.yaml"))?
            .as_mapping_mut()
            .ok_or(anyhow::anyhow!(
                "parameters is not a mapping in storage-class.yaml"
            ))?
            .insert(
                "numberOfReplicas".into(),
                serde_yaml::Value::String(replicas.to_string()),
            );
        config
            .data
            .as_mut()
            .ok_or(anyhow::anyhow!("No data in configmap"))?
            .insert(
                "storageclass.yaml".into(),
                serde_yaml::to_string(&storage_class_yml)?,
            );
        config_api
            .patch("longhorn-storageclass", &pp, &Patch::Apply(config))
            .await?;
    }
    let storage_class_api: Api<StorageClass> = Api::all(client.clone());
    let mut storage_class = storage_class_api.get("longhorn").await?;
    while !storage_class.parameters.is_some_and(|p| {
        p.get("numberOfReplicas")
            .map(|v| v == &replicas.to_string())
            .unwrap_or(false)
    }) {
        tracing::info!("Waiting for longhorn storage class to be updated");
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        storage_class = storage_class_api.get("longhorn").await?;
    }

    Ok(())
}
pub async fn enable_v2_data_engine(client: &Client) -> Result<()> {
    let pp = PatchParams {
        force: true,
        field_manager: Some("nirvati".to_string()),
        ..Default::default()
    };
    let config_api: Api<ConfigMap> = Api::namespaced(client.clone(), "longhorn-system");
    let settings_api: Api<Setting> = Api::namespaced(client.clone(), "longhorn-system");
    let mut setting = settings_api.get("v1-data-engine").await?;
    setting.metadata.managed_fields = None;
    setting.value = "true".to_string();
    settings_api
        .patch("v1-data-engine", &pp, &Patch::Apply(setting))
        .await?;
    let mut setting = settings_api.get("v2-data-engine").await?;
    setting.metadata.managed_fields = None;
    setting.value = "true".to_string();
    settings_api
        .patch("v2-data-engine", &pp, &Patch::Apply(setting))
        .await?;
    let mut config = config_api.get("longhorn-storageclass").await?;
    config.metadata.managed_fields = None;
    let storage_class_data = config
        .data
        .clone()
        .ok_or(anyhow::anyhow!("No data in configmap"))?;
    let storage_class_yml = storage_class_data
        .get("storageclass.yaml")
        .ok_or(anyhow::anyhow!("No storageclass.yaml in configmap"))?;
    let mut storage_class_yml: serde_yaml::Value = serde_yaml::from_str(storage_class_yml)?;
    storage_class_yml
        .get_mut("parameters")
        .ok_or(anyhow::anyhow!("No parameters in storage-class.yaml"))?
        .as_mapping_mut()
        .ok_or(anyhow::anyhow!(
            "parameters is not a mapping in storage-class.yaml"
        ))?
        .insert("dataEngine".into(), serde_yaml::Value::String("v2".into()));
    config
        .data
        .as_mut()
        .ok_or(anyhow::anyhow!("No data in configmap"))?
        .insert(
            "storageclass.yaml".into(),
            serde_yaml::to_string(&storage_class_yml)?,
        );
    config_api
        .patch("longhorn-storageclass", &pp, &Patch::Apply(config))
        .await?;
    let storage_class_api: Api<StorageClass> = Api::all(client.clone());
    let mut storage_class = storage_class_api.get("longhorn").await?;
    while !storage_class
        .parameters
        .is_some_and(|p| p.get("dataEngine").map(|v| v == "v2").unwrap_or(false))
    {
        tracing::info!("Waiting for longhorn storage class to be updated");
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        storage_class = storage_class_api.get("longhorn").await?;
    }

    Ok(())
}

async fn get_node(client: &Client) -> Result<Node> {
    let api: Api<Node> = Api::namespaced(client.clone(), "longhorn-system");
    let node = api
        .list(&Default::default())
        .await?
        .items
        .into_iter()
        .next()
        .unwrap();
    Ok(node)
}

pub async fn has_v2_disk(client: &Client) -> Result<bool> {
    let node = get_node(client).await?;
    let disks = node.spec.disks.unwrap_or_default();
    Ok(disks.values().any(|d| {
        d.disk_type
            .as_ref()
            .is_some_and(|disk_type| disk_type == &NodeDisksDiskType::Block)
    }))
}

pub async fn remove_all_disks(client: &Client) -> Result<()> {
    let api: Api<Node> = Api::namespaced(client.clone(), "longhorn-system");
    let pp = PatchParams {
        force: true,
        field_manager: Some("nirvati".to_string()),
        ..Default::default()
    };
    {
        let mut node = get_node(client).await?;
        let mut disks = node.spec.clone().disks.unwrap_or_default();
        for disk in disks.values_mut() {
            disk.allow_scheduling = Some(false);
        }
        node.spec.disks = Some(disks);
        node.metadata.managed_fields = None;
        node.metadata.uid = None;
        api.patch(
            &node.metadata.name.clone().unwrap(),
            &pp,
            &Patch::Apply(node),
        )
        .await?;
    }
    {
        let mut node = get_node(client).await?;
        tokio::time::sleep(tokio::time::Duration::from_secs(5)).await;
        node.spec.disks = Some(Default::default());
        node.metadata.managed_fields = None;
        node.metadata.uid = None;
        api.patch(
            &node.metadata.name.clone().unwrap(),
            &pp,
            &Patch::Apply(node),
        )
        .await?;
    }
    Ok(())
}
pub async fn add_disks(client: &Client, disk_paths: &[String]) -> Result<()> {
    let api: Api<Node> = Api::namespaced(client.clone(), "longhorn-system");
    let mut node = get_node(client).await?;
    let mut disks = node.spec.clone().disks.unwrap_or_default();
    for disk_path in disk_paths {
        if !disks.values().any(|d| d.path == Some(disk_path.clone())) {
            disks.insert(
                slugify!(disk_path),
                NodeDisks {
                    allow_scheduling: Some(true),
                    disk_type: Some(NodeDisksDiskType::Block),
                    eviction_requested: None,
                    path: Some(disk_path.clone()),
                    storage_reserved: None,
                    tags: None,
                    disk_driver: None,
                },
            );
        }
    }
    let pp = PatchParams {
        force: true,
        field_manager: Some("nirvati".to_string()),
        ..Default::default()
    };
    node.spec.disks = Some(disks);
    node.metadata.managed_fields = None;
    node.metadata.uid = None;
    api.patch(
        &node.metadata.name.clone().unwrap(),
        &pp,
        &Patch::Apply(node),
    )
    .await?;
    Ok(())
}

pub async fn set_setting(client: &Client, setting: &str, value: &str) -> Result<()> {
    let pp = PatchParams {
        force: true,
        field_manager: Some("nirvat".to_string()),
        ..Default::default()
    };
    let api: Api<Setting> = Api::namespaced(client.clone(), "longhorn-system");
    let mut setting = api.get(setting).await?;
    setting.metadata.managed_fields = None;
    setting.value = value.to_string();
    api.patch(
        setting.metadata.name.clone().unwrap().as_str(),
        &pp,
        &Patch::Apply(setting),
    )
    .await?;
    Ok(())
}

pub async fn configure(client: &Client, longhorn_version: StorageLayer) -> Result<()> {
    if longhorn_version == StorageLayer::LonghornV1 {
        // Default to one replica
        set_default_replica_count(client, 1).await?;
    } else if !has_v2_disk(client).await? {
        enable_v2_data_engine(client).await?;
        let disks = disk::initialize_disks()?;
        if disks.is_empty() {
            tracing::error!("No disks found!");
            std::process::exit(1);
        }
        loop {
            if !disks.iter().any(|(_, is_new)| *is_new) {
                if let Err(err) = remove_all_disks(client).await {
                    tracing::warn!("Failed to remove all disks: {:#?}", err);
                    tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
                    continue;
                }
            }
            if let Err(err) = add_disks(
                client,
                &disks
                    .iter()
                    .filter(|(_, is_new)| *is_new)
                    .map(|(disk, _)| disk.clone())
                    .collect::<Vec<_>>(),
            )
            .await
            {
                tracing::warn!("Failed to add disks: {:#?}", err);
                tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
                continue;
            }
            if let Err(err) = set_default_replica_count(client, disks.len() as u8).await {
                tracing::warn!("Failed to set number of replicas: {:#?}", err);
                tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
                continue;
            }
            break;
        }
    }
    set_setting(client, "node-drain-policy", "allow-if-replica-is-stopped").await?;
    set_setting(client, "storage-over-provisioning-percentage", "1000").await?;
    set_setting(client, "replica-auto-balance", "best-effort").await?;
    Ok(())
}

pub async fn install_longhorn(
    kube_client: Client,
    fake_global_apps_root: &Path,
    fake_apps_root: &Path,
    nirvati_seed: &str,
    user_state: &UserState,
    resolver_ctx: &ResolverCtx,
) -> Result<()> {
    let longhorn_app = nirvati_agent::apps::parser::load_app(
        kube_client.clone(),
        fake_global_apps_root,
        "longhorn",
        &fake_apps_root.join("longhorn"),
        user_state.clone(),
        nirvati_seed,
        None,
        resolver_ctx,
    )
    .await
    .expect("Failed to load Longhorn app");
    let metadata = longhorn_app.get_metadata().clone();
    let kube_config =
        generate_kubernetes_config(kube_client, longhorn_app, user_state, resolver_ctx)
            .await
            .expect("Failed to generate Kubernetes config");
    let (helm_chart, _) = generate_chart(kube_config, metadata, "system", "default")
        .expect("Failed to turn app into Helm chart");
    let chart_dir = temp_dir::TempDir::new().expect("Failed to create temp dir");
    let chart_file_path = chart_dir.path().join("chart.tgz");
    std::fs::write(&chart_file_path, helm_chart).expect("Failed to write chart to file");
    let helm_install = tokio::process::Command::new("helm")
        .args([
            "upgrade",
            "longhorn",
            chart_file_path
                .to_str()
                .expect("Failed to convert chart file path to string"),
            "--namespace",
            "longhorn-system",
            "--create-namespace",
            "--install",
        ])
        .output()
        .await
        .expect("Failed to run helm install");
    if !helm_install.status.success() {
        tracing::error!(
            "Failed to install Longhorn app: {}",
            String::from_utf8_lossy(&helm_install.stderr)
        );
        std::process::exit(1);
    }
    Ok(())
}
