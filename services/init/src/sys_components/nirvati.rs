// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::path::Path;

use anyhow::Result;
use k8s_crds_traefik::IngressRoute;
use kube::api::PostParams;
use kube::Api;

use nirvati_agent::apps::generator::helm::generate_chart;
use nirvati_agent::apps::generator::internal::ResolverCtx;
use nirvati_agent::apps::generator::kubernetes::generate_kubernetes_config;
use nirvati_agent::apps::generator::kubernetes::ingress::get_app_ingress;
use nirvati_agent::plugins::api::UserState;

pub async fn install_nirvati(
    kube_client: &kube::Client,
    fake_global_apps_root: &Path,
    fake_user_apps_root: &Path,
    nirvati_seed: &str,
    user_state: &UserState,
    resolver_ctx: &ResolverCtx,
    add_ingress: bool,
) -> Result<()> {
    let nirvati_app = nirvati_agent::apps::parser::load_app(
        kube_client.clone(),
        fake_global_apps_root,
        "nirvati",
        &fake_user_apps_root.join("nirvati"),
        user_state.clone(),
        nirvati_seed,
        None,
        resolver_ctx,
    )
    .await
    .expect("Failed to load Nirvati app");
    let metadata = nirvati_app.get_metadata().clone();
    let ingress = get_app_ingress(
        None,
        nirvati_app.get_ingress(),
        vec!["web".to_string(), "websecure".to_string()],
        false,
        "system",
        false,
        None,
        None,
    );
    let kube_config =
        generate_kubernetes_config(kube_client.clone(), nirvati_app, user_state, resolver_ctx)
            .await
            .expect("Failed to generate Kubernetes config");
    let (helm_chart, _) = generate_chart(kube_config, metadata, "system", "default")
        .expect("Failed to turn app into Helm chart");
    let chart_dir = temp_dir::TempDir::new().expect("Failed to create temp dir");
    let chart_file_path = chart_dir.path().join("chart.tgz");
    std::fs::write(&chart_file_path, helm_chart).expect("Failed to write chart to file");
    let helm_install = tokio::process::Command::new("helm")
        .args([
            "upgrade",
            "nirvati",
            chart_file_path
                .to_str()
                .expect("Failed to convert chart file path to string"),
            "--namespace",
            "nirvati",
            "--create-namespace",
            "--install",
        ])
        .output()
        .await
        .expect("Failed to run helm install");
    if !helm_install.status.success() {
        tracing::error!(
            "Failed to install Nirvati app: {}",
            String::from_utf8_lossy(&helm_install.stderr)
        );
        std::process::exit(1);
    }
    if add_ingress {
        let ingress_api: Api<IngressRoute> = Api::namespaced(kube_client.clone(), "nirvati");
        let pp = PostParams::default();
        if let Err(err) = ingress_api.create(&pp, &ingress).await {
            tracing::warn!("Failed to create ingress: {}", err);
        };
    }
    Ok(())
}
