// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::{BTreeMap, HashMap};
use std::str::FromStr;
use std::time::Duration;

use crate::nirvati_agent_api::InitUserRequest;
use crate::sys_components::builtins::*;
use crate::sys_components::coredns::install_coredns;
use crate::sys_components::metallb::{configure_metallb, install_metallb};
use crate::sys_components::nirvati::install_nirvati;
use crate::sys_components::{local_path_provisioner, longhorn};
use k8s_openapi::api::core::v1::Secret;
use kube::{Api, Client, CustomResourceExt};
use nirvati::kubernetes::{apply, apply_with_ns, create_secret};
use nirvati_agent::apps::files::write_stores_yml;
use nirvati_agent::apps::generator::internal::ResolverCtx;
use nirvati_agent::apps::repos::types::AppStore;
use nirvati_agent::plugins::api::UserState;
use nirvati_agent::plugins::crd::{ClusterPlugin, Plugin};
use rand::Rng;
use temp_dir::TempDir;

mod dbus;
mod disk;

pub mod sys_components;

mod nirvati_agent_api {
    tonic::include_proto!("nirvati_agent");
}

impl From<UserState> for nirvati_agent_api::UserState {
    fn from(value: UserState) -> Self {
        Self {
            user_id: value.user_id,
            installed_apps: value.installed_apps,
            app_settings: value.app_settings,
            user_display_name: value.user_display_name,
        }
    }
}

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum StorageLayer {
    LocalPathProvisioner,
    LonghornV1,
    LonghornSpdk,
}

impl StorageLayer {
    pub fn is_longhorn(&self) -> bool {
        matches!(self, Self::LonghornV1 | Self::LonghornSpdk)
    }
}

impl FromStr for StorageLayer {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "disabled" | "none" | "local-path-provisioner" => {
                Ok(StorageLayer::LocalPathProvisioner)
            }
            "v1" | "1" => Ok(StorageLayer::LonghornV1),
            "v2" | "2" | "spdk" => Ok(StorageLayer::LonghornSpdk),
            _ => Err(()),
        }
    }
}

pub async fn get_or_create_secret(
    kube_client: &Client,
    namespace: &str,
    name: &str,
    key: String,
    value: String,
) -> anyhow::Result<String> {
    let api: Api<Secret> = Api::namespaced(kube_client.clone(), namespace);
    let secret = api.get(name).await;
    let create_and_return = || async {
        create_secret(
            kube_client,
            namespace,
            name,
            &BTreeMap::from([(key.clone(), value.clone())]),
        )
        .await
        .expect("Failed to create secret");
        Ok(value)
    };
    if let Ok(secret) = secret {
        if let Some(data) = secret.data {
            if let Some(v) = data.get(&key) {
                Ok(String::from_utf8(v.0.clone())?)
            } else {
                create_and_return().await
            }
        } else {
            create_and_return().await
        }
    } else {
        create_and_return().await
    }
}

pub async fn prepare_fake_app_root(kube_client: Client) -> anyhow::Result<TempDir> {
    let fake_global_apps_root = TempDir::new()?;
    let fake_apps_root = fake_global_apps_root.path().join("system");
    if !fake_apps_root.exists() {
        std::fs::create_dir_all(&fake_apps_root)?;
    }
    let init_stores_yml = if std::env::var("IS_CITADEL").is_err() {
        vec![AppStore {
            id: uuid::Uuid::new_v4(),
            src: HashMap::from([
                (
                    "repo_url".to_string(),
                    "https://gitlab.com/nirvati/apps/essentials.git".to_string(),
                ),
                (
                    "branch".to_string(),
                    std::env::var("NIRVATI_APP_STORE_VERSION").unwrap_or("stable".to_string()),
                ),
            ]),
            ..Default::default()
        }]
    } else {
        vec![AppStore {
            id: uuid::Uuid::new_v4(),
            src: HashMap::from([
                (
                    "repo_url".to_string(),
                    "https://gitlab.com/nirvati/citadel/lts/apps.git".to_string(),
                ),
                ("branch".to_string(), "hybrid".to_string()),
            ]),
            ..Default::default()
        }]
    };

    write_stores_yml(&fake_apps_root, &init_stores_yml)?;

    let user_state = UserState {
        user_id: "system".to_string(),
        ..Default::default()
    };

    let app_sources_yml = fake_global_apps_root.path().join("app-sources.yml");
    std::fs::write(&app_sources_yml, "")?;

    nirvati_agent::apps::repos::download_apps(
        kube_client,
        fake_global_apps_root.path(),
        false,
        user_state.clone(),
    )
    .await?;
    Ok(fake_global_apps_root)
}

pub async fn main() -> anyhow::Result<()> {
    let kube_client = Client::try_default().await?;
    apply(kube_client.clone(), &[ClusterPlugin::crd(), Plugin::crd()]).await?;
    let longhorn_version: StorageLayer = std::env::var("STORAGE_LAYER")
        .or_else(|_| std::env::var("LONGHORN_VERSION"))
        .map(|v| v.parse().expect("Failed to parse Longhorn version"))
        .unwrap_or(StorageLayer::LonghornV1);

    let mut rng = rand::thread_rng();
    let mut entropy = [0u8; 64];
    rng.fill(&mut entropy);
    let nirvati_seed = get_or_create_secret(
        &kube_client,
        "nirvati",
        "nirvati-seed",
        "seed".to_string(),
        hex::encode(entropy),
    )
    .await?;
    let middlewares_nirvati = [compress_mw(), https_redirect_mw()];
    let version = kube_client.apiserver_version().await?;
    if version.git_version.contains("k3s") {
        apply_with_ns(kube_client.clone(), &[traefik_helm_config()], "kube-system").await?;
    } else if version.git_version.contains("rke2") {
        apply_with_ns(kube_client.clone(), &[cilium_helm_config()], "kube-system").await?;
    }

    apply_with_ns(kube_client.clone(), &middlewares_nirvati, "nirvati").await?;

    let mut retry_counter = 0;
    let fake_global_apps_root;
    loop {
        match prepare_fake_app_root(kube_client.clone()).await {
            Ok(fake_global_apps_root_new) => {
                fake_global_apps_root = fake_global_apps_root_new;
                break;
            }
            Err(e) => {
                tracing::error!("Failed to prepare fake app root: {}", e);
                retry_counter += 1;
                if retry_counter > 60 {
                    let _ = dbus::reboot().await;
                    // Try to chroot /host-root reboot
                    std::process::Command::new("chroot")
                        .args(["/host-root", "bash", "-c", "reboot"])
                        .output()
                        .expect("Failed to chroot /host-root reboot");
                    std::process::exit(1);
                }
                // Reboot
                tokio::time::sleep(Duration::from_secs(1)).await;
            }
        };
    }
    let fake_apps_root = fake_global_apps_root.path().join("system");

    let user_state = UserState {
        user_id: "system".to_string(),
        ..Default::default()
    };

    let resolver_ctx = ResolverCtx::new(
        fake_global_apps_root.path().to_path_buf(),
        user_state.clone(),
        nirvati_seed.clone(),
        kube_client.clone(),
    );

    if longhorn_version.is_longhorn() && !longhorn::is_installed(&kube_client).await? {
        longhorn::install_longhorn(
            kube_client.clone(),
            fake_global_apps_root.path(),
            &fake_apps_root,
            &nirvati_seed,
            &user_state,
            &resolver_ctx,
        )
        .await?;
        tracing::info!("Waiting 30 seconds for Longhorn to install.");
        let mut i = 0;
        while i < 30 {
            match longhorn::configure(&kube_client, longhorn_version).await {
                Ok(_) => break,
                Err(e) => {
                    tracing::warn!("Failed to configure Longhorn: {}", e);
                    tokio::time::sleep(Duration::from_secs(1)).await;
                    i += 1;
                }
            }
            tokio::time::sleep(Duration::from_secs(1)).await;
            i += 1;
        }
        if i == 30 {
            tracing::error!("Failed to configure Longhorn after 30 seconds");
            std::process::exit(1);
        }
    };
    if longhorn_version == StorageLayer::LocalPathProvisioner
        && !local_path_provisioner::is_installed(&kube_client).await?
    {
        local_path_provisioner::install(
            &kube_client,
            fake_global_apps_root.path(),
            &fake_apps_root,
            &nirvati_seed,
            &user_state,
            &resolver_ctx,
        )
        .await?;
    }
    install_coredns(
        kube_client.clone(),
        fake_global_apps_root.path(),
        &fake_apps_root,
        &nirvati_seed,
        &user_state,
        &resolver_ctx,
    )
    .await?;
    install_metallb(
        kube_client.clone(),
        fake_global_apps_root.path(),
        &fake_apps_root,
        &nirvati_seed,
        &user_state,
        &resolver_ctx,
    )
    .await?;
    let mut i = 0;
    loop {
        match configure_metallb(kube_client.clone()).await {
            Ok(_) => break,
            Err(e) => {
                tracing::warn!("Failed to configure MetalLB: {}", e);
                tokio::time::sleep(Duration::from_secs(1)).await;
                i += 1;
                if i == 30 {
                    tracing::error!("Failed to configure MetalLB after 30 seconds");
                    std::process::exit(1);
                }
            }
        }
    }

    #[cfg(not(feature = "__development"))]
    install_nirvati(
        &kube_client,
        fake_global_apps_root.path(),
        &fake_apps_root,
        &nirvati_seed,
        &user_state,
        &resolver_ctx,
        true,
    )
    .await?;

    // Wait until we can connect to the agent API
    let mut manager_client = None;
    for _ in 0..240 {
        if let Ok(client) = nirvati_agent_api::manager_client::ManagerClient::connect(
            #[cfg(feature = "__development")]
            "http://localhost:3001",
            #[cfg(not(feature = "__development"))]
            "http://agent.nirvati.svc.cluster.local:8080",
        )
        .await
        {
            manager_client = Some(client);
            break;
        }
        tracing::warn!("Waiting for agent API to be reachable");
        tokio::time::sleep(Duration::from_secs(1)).await;
    }
    let Some(mut manager_client) = manager_client else {
        tracing::error!("Failed to connect to agent API");
        std::process::exit(1);
    };

    manager_client
        .init_user(InitUserRequest {
            user_id: "system".to_string(),
            user_display_name: "Nirvati System".to_string(),
        })
        .await?;

    Ok(())
}
