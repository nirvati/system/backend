#[tokio::main]
async fn main() {
    #[cfg(feature = "__development")]
    dotenvy::dotenv().expect("Failed to load .env file");
    tracing_subscriber::fmt::init();
    nirvati_init::main().await.expect("Failed to init");
}
