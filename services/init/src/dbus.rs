use zbus::proxy;
use zbus::{fdo::Result, Connection};

// Define a proxy for the systemd-logind interface, which manages power-related tasks.
#[proxy(
    default_service = "org.freedesktop.login1",
    interface = "org.freedesktop.login1.Manager",
    default_path = "/org/freedesktop/login1"
)]
trait LoginManager {
    fn reboot(&self, interactive: bool) -> Result<()>;
}

pub async fn reboot() -> Result<()> {
    // Connect to the system bus.
    let connection = Connection::system().await?;

    // Create a proxy to interact with the org.freedesktop.login1.Manager interface.
    let proxy = LoginManagerProxy::new(&connection).await?;

    // Call the reboot method (interactive = false means no user interaction).
    proxy.reboot(false).await?;

    println!("Reboot initiated successfully.");

    Ok(())
}
