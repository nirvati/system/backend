// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

pub fn main() {
    tonic_build::configure()
        .protoc_arg("--experimental_allow_proto3_optional")
        .compile_protos(
            &[
                "protos/apps.proto",
                "protos/ingress.proto",
                "protos/core.proto",
                "protos/https.proto",
                "protos/setup.proto",
                "protos/public-api.proto",
                "protos/citadel-compat.proto",
            ],
            &["protos"],
        )
        .unwrap();
}
