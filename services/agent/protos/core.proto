// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

syntax = "proto3";

package nirvati_agent;
import "shared.proto";

message AddNirvatiDomainRequest {
  string user = 1;
  string domain = 2;
}

message DeleteNirvatiDomainRequest {
  string user = 1;
  string domain = 2;
}

message AddUserAuthRequest {
  string user = 1;
  string password = 2;
}

message DeleteUserAuthRequest {
  string user = 1;
}

message VersionInfo {
  string version = 1;
}

message TailscaleStatus {
  string authUrl = 1;
  bool authenticated = 2;
  optional string ownIp = 3;
}

enum KubeDistro {
  K3s = 0;
  Rke2 = 1;
  Unknown = -1;
}

message KubeVersion {
  string version = 1;
  KubeDistro distro = 2;
}

message NodeInfo {
  string ip = 1;
}

message EntropyRequest {
  string input = 1;
}

message EntropyResponse {
  string entropy = 1;
}

message AddUserNsRequest {
  string user = 1;
}

message DeleteUserNsRequest {
  string user = 1;
}

service Middlewares {
  rpc AddUserAuth(AddUserAuthRequest) returns (Empty) {}
  rpc RemoveUserAuth(DeleteUserAuthRequest) returns (Empty) {}
}

service Upgrades {
  rpc GetKubeVersion(Empty) returns (KubeVersion) {}
  // These will only work if K3s is installed
  rpc GetLatestKubeVersion(Empty) returns (VersionInfo) {}
  rpc UpdateKube(VersionInfo) returns (Empty) {}
}

service Node {
  rpc GetInfo(Empty) returns (NodeInfo) {}
}

service Entropy {
  rpc DeriveEntropy(EntropyRequest) returns (EntropyResponse) {}
}

service Tailscale {
  rpc GetStatus(Empty) returns (TailscaleStatus) {}
  rpc StartLogin(Empty) returns (Empty) {}
  rpc Start(Empty) returns (Empty) {}
  rpc AddIpToMetallb(Empty) returns (Empty) {}
}

service Users {
  rpc CreateUserNamespace(AddUserNsRequest) returns (Empty) {}
  rpc DeleteUserNamespace(DeleteUserNsRequest) returns (Empty) {}
}

message AddServiceRequest {
  oneof remote {
    string dns_name = 1;
    string ip = 2;
  }
  string namespace = 3;
  string name = 4;
  repeated uint32 tcp_ports = 5;
  repeated uint32 udp_ports = 6;
}

message RemoveServiceRequest {
  string namespace = 1;
  string name = 2;
}

service Services {
  rpc AddService(AddServiceRequest) returns (Empty) {}
  rpc RemoveService(RemoveServiceRequest) returns (Empty) {}
}

enum Protocol {
  HTTP = 0;
  HTTPS = 1;
}

message HttpsOptions {
  bool insecure_skip_verify = 1;
  optional string sni = 2;
}

message Route {
  string target_svc = 1;
  string remote_prefix = 2;
  string local_prefix = 3;
  bool enable_compression = 4;
  Protocol protocol = 5;
  // Technically a u16, but Kube-rs will take any int32
  int32 port = 6;
  optional HttpsOptions https = 7;
}

message AddProxyRequest {
  string namespace = 1;
  string domain = 2;
  repeated Route routes = 3;
  ACMEProvider acme_provider = 4;
  string cert_user = 5;
  optional string parent = 6;
}

message AddTcpProxyRequest {
  string namespace = 1;
  string domain = 2;
  string target_svc = 3;
  // Technically a u16, but Kube-rs will take any int32
  int32 port = 4;
  bool enable_redirect = 5;
}

message RemoveProxyRequest {
  string namespace = 1;
  string domain = 2;
}

service Proxies {
  rpc AddProxy(AddProxyRequest) returns (Empty) {}
  rpc AddTcpProxy(AddTcpProxyRequest) returns (Empty) {}
  rpc RemoveProxy(RemoveProxyRequest) returns (Empty) {}
}

message SassProvisionClusterRequest {}
