// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

// The minimal agent version we're backwards-compatible with
pub const MINIMUM_COMPATIBLE_APP_MANAGER: &str = "0.0.1";
pub const RESERVED_NAMES: [&str; 1] = ["builtin"];
pub const FEATURES: [&str; 0] = [];
