// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use base64::engine::general_purpose::STANDARD;
use base64::Engine;
use nirvati::kubernetes::multidoc_serialize;
use nirvati_agent::apps::files::write_stores_yml;
use nirvati_agent::apps::generator::helm::chart_yml;
use nirvati_agent::apps::generator::internal::{load_permissions, ResolverCtx};
use nirvati_agent::apps::repos::types::AppStore;
use nirvati_agent::plugins::api::UserState;
use nirvati_apps::internal::InternalAppRepresentation;
use std::collections::HashMap;
use tempfile::TempDir;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let apps_root = TempDir::new().unwrap();
    let user_apps_root = apps_root.path().join("admin");
    // Create user apps root
    std::fs::create_dir_all(&user_apps_root).unwrap();

    std::env::set_var("JWT_PUBKEY_BASE64", STANDARD.encode("test".as_bytes()));
    std::env::set_var("CITADEL_BASE_DIR", "/home/citadel/citadel");
    // Create an empty app-source.yml
    let app_sources_yml = apps_root.path().join("app-sources.yml");
    std::fs::write(app_sources_yml, "").expect("Failed to write app-sources.yml");

    let snapshots_dir = std::env::args().nth(1).unwrap();
    let snapshots_dir = std::path::Path::new(&snapshots_dir);

    let user_state = UserState {
        user_id: "admin".to_string(),
        installed_apps: vec![
            "nirvati".to_string(),
            "tor".to_string(),
            "bitcoin-core".to_string(),
            "lnd".to_string(),
        ],
        app_settings: Default::default(),
        user_display_name: "Admin".to_string(),
    };

    let initial_stores = [
        AppStore {
            id: uuid::Uuid::new_v4(),
            src: HashMap::from([
                (
                    "repo_url".to_string(),
                    "https://gitlab.com/nirvati/apps/foss.git".to_string(),
                ),
                ("branch".to_string(), "main".to_string()),
            ]),
            ..Default::default()
        },
        AppStore {
            id: uuid::Uuid::new_v4(),
            src: HashMap::from([
                (
                    "repo_url".to_string(),
                    "https://gitlab.com/nirvati/apps/essentials.git".to_string(),
                ),
                ("branch".to_string(), "main".to_string()),
            ]),
            ..Default::default()
        },
        AppStore {
            id: uuid::Uuid::new_v4(),
            src: HashMap::from([
                (
                    "repo_url".to_string(),
                    "https://gitlab.com/nirvati/citadel/apps.git".to_string(),
                ),
                ("branch".to_string(), "main".to_string()),
            ]),
            ..Default::default()
        },
        AppStore {
            id: uuid::Uuid::new_v4(),
            src: HashMap::from([
                (
                    "repo_url".to_string(),
                    "https://gitlab.com/nirvati/citadel/lts/apps.git".to_string(),
                ),
                ("branch".to_string(), "hybrid".to_string()),
            ]),
            ..Default::default()
        },
        AppStore {
            id: uuid::Uuid::new_v4(),
            src: HashMap::from([
                (
                    "repo_url".to_string(),
                    "https://github.com/nirvati/runtipi-appstore".to_string(),
                ),
                ("branch".to_string(), "master".to_string()),
            ]),
            ..Default::default()
        },
    ];
    write_stores_yml(&user_apps_root, &initial_stores).expect("Failed to write stores.yml");

    nirvati_agent::apps::repos::download_apps(apps_root.path(), false, user_state.clone())
        .await
        .expect("Failed to download apps");

    let mut resolver_ctx = ResolverCtx::new(
        apps_root.path().to_path_buf(),
        user_state.clone(),
        "seed".to_string(),
    );
    resolver_ctx
        .add_alias("bitcoin".to_string(), "bitcoin-core".to_string())
        .await;

    let citadel_user_state = UserState {
        user_id: "admin".to_string(),
        installed_apps: vec![
            "nirvati".to_string(),
            "tor".to_string(),
            "bitcoin-citadel".to_string(),
            "lnd".to_string(),
        ],
        app_settings: Default::default(),
        user_display_name: "Admin".to_string(),
    };
    let mut citadel_resolver_ctx = ResolverCtx::new(
        apps_root.path().to_path_buf(),
        citadel_user_state.clone(),
        "seed".to_string(),
    );
    citadel_resolver_ctx
        .add_alias("bitcoin".to_string(), "bitcoin-citadel".to_string())
        .await;
    // Loop through all dirs in temp_dir.path() and load the app
    for entry in std::fs::read_dir(&user_apps_root).unwrap() {
        let entry = entry.unwrap();
        let path = entry.path();
        if !path.is_dir() {
            continue;
        }
        let app_id = path.file_name().unwrap().to_str().unwrap();
        let mut is_citadel = false;
        if (path.join("app.yml").exists() || path.join("app.yml.jinja").exists())
            && !(path.join("metadata.yml").exists() || path.join("metadata.yml.jinja").exists())
        {
            println!(
                "App {} appears to be a citadel app, simulating a Citadel environment",
                app_id
            );
            is_citadel = true;
        }
        let known_incompatible_apps = ["sphinx-relay", "public-pool", "bitcartcc"];
        if known_incompatible_apps.contains(&app_id) {
            continue;
        };
        println!("Processing {}...", app_id);

        let mut result = match nirvati_agent::apps::parser::load_app_inner(
            &app_id,
            &path,
            if is_citadel {
                citadel_user_state.clone()
            } else {
                user_state.clone()
            },
            "seed",
            Some("test.nirvati.org".to_string()),
        )
        .await
        {
            Ok(result) => result,
            Err(err) => {
                tracing::error!("Failed to load app {}: {:#}", app_id, err);
                continue;
            }
        };
        load_permissions(
            &mut result,
            if is_citadel {
                &mut citadel_resolver_ctx
            } else {
                &mut resolver_ctx
            },
        )
        .await
        .expect("Failed to load permissions");

        let app_snapshots_dir = snapshots_dir.join(path.file_name().unwrap());
        std::fs::create_dir_all(&app_snapshots_dir).unwrap();

        let internal_config_snapshot = app_snapshots_dir.join("internal.yml");
        if !internal_config_snapshot.exists() {
            std::fs::write(
                internal_config_snapshot,
                serde_yaml::to_string(&result).expect("Failed to serialize"),
            )
            .expect("Failed to write");
        } else {
            let expected = std::fs::read_to_string(internal_config_snapshot).unwrap();
            let mut expected: InternalAppRepresentation = serde_yaml::from_str(&expected).unwrap();
            expected.metadata.scope = result.metadata.scope;
            pretty_assertions::assert_eq!(result, expected);
        }

        let chart_yml = chart_yml::generate_chart_yml(result.metadata.clone());
        let mut k8s_config =
            match nirvati_agent::apps::generator::kubernetes::generate_kubernetes_config(
                result,
                if is_citadel {
                    &citadel_user_state
                } else {
                    &user_state
                },
                if is_citadel {
                    &mut citadel_resolver_ctx
                } else {
                    &mut resolver_ctx
                },
            )
            .await
            {
                Ok(k8s_config) => k8s_config,
                Err(err) => {
                    tracing::error!("Failed to generate k8s config: {:#}", err);
                    continue;
                }
            };

        // Sort pvcs
        k8s_config
            .pvcs
            .sort_by(|a, b| a.metadata.name.cmp(&b.metadata.name));

        let k8s_config = serde_yaml::to_value(k8s_config.clone()).expect("Failed to serialize");
        std::fs::create_dir_all(app_snapshots_dir.join("templates"))
            .expect("Failed to create templates dir");
        let chart_yml = serde_yaml::to_string(&chart_yml).expect("Failed to serialize");
        let chart_file = app_snapshots_dir.join("Chart.yaml");
        if !chart_file.exists() {
            std::fs::write(chart_file, chart_yml).expect("Failed to write");
        } else {
            let expected = std::fs::read_to_string(chart_file).unwrap();
            pretty_assertions::assert_eq!(chart_yml, expected);
        }
        for (field, vals) in k8s_config.as_mapping().unwrap() {
            let field_str = field.as_str().unwrap();
            let vals_list = vals.as_sequence().unwrap();
            if !vals_list.is_empty() {
                let serialized =
                    multidoc_serialize(vals_list.to_vec()).expect("Failed to serialize");
                let snapshot_file = app_snapshots_dir
                    .join("templates")
                    .join(field_str.to_owned() + ".yaml");
                if !snapshot_file.exists() {
                    std::fs::write(snapshot_file, serialized).expect("Failed to write");
                } else {
                    let expected = std::fs::read_to_string(snapshot_file).unwrap();
                    pretty_assertions::assert_eq!(serialized, expected);
                }
            }
        }
        // Loop through all files in the "templates" dir and ensure the field still exists
        for entry in std::fs::read_dir(app_snapshots_dir.join("templates")).unwrap() {
            let entry = entry.unwrap();
            let path = entry.path();
            if !path.is_file() {
                continue;
            }
            let field = path.file_stem().unwrap().to_str().unwrap();
            if !k8s_config
                .as_mapping()
                .unwrap()
                .contains_key(serde_yaml::Value::String(field.to_string()))
            {
                panic!("Field {} not found in k8s config", field);
            }
        }
    }
}
