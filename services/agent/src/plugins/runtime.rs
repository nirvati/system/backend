// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::plugins::api::{InstallAppRequest, UninstallAppRequest, UserState};
use crate::plugins::get_plugins;
use anyhow::{anyhow, bail, Result};
use kube::Client;
use nirvati_apps::internal::types::PluginType;
use std::path::Path;
use tonic::Request;

pub async fn apply_hook(
    kube_client: Client,
    global_apps_root: &Path,
    app_id: &str,
    plugin: &str,
    user_state: UserState,
) -> Result<()> {
    let apps_root = global_apps_root.join(&user_state.user_id);
    let plugins = get_plugins(&user_state.user_id, kube_client).await?;
    let plugin = plugins
        .get(plugin)
        .ok_or_else(|| anyhow!("Plugin not found"))?;
    if plugin.r#type != PluginType::Runtime {
        bail!("Plugin is not a runtime plugin");
    };
    let mut client = crate::plugins::api::runtime_plugin_client::RuntimePluginClient::connect(
        plugin.endpoint.clone(),
    )
    .await?;
    let app_directory = crate::utils::tar::compress_dir(&apps_root.join(app_id)).await?;
    client
        .install_app(Request::new(InstallAppRequest {
            app_id: app_id.to_string(),
            app_directory,
            user_state: Some(user_state),
        }))
        .await?;
    Ok(())
}

pub async fn uninstall_hook(
    kube_client: Client,
    apps_root: &Path,
    app_id: &str,
    plugin: &str,
    user_id: &str,
) -> Result<()> {
    let plugins = get_plugins(user_id, kube_client).await?;
    let plugin = plugins
        .get(plugin)
        .ok_or_else(|| anyhow!("Plugin not found"))?;
    if plugin.r#type != PluginType::Runtime {
        bail!("Plugin is not a runtime plugin");
    };
    let mut client = crate::plugins::api::runtime_plugin_client::RuntimePluginClient::connect(
        plugin.endpoint.clone(),
    )
    .await?;
    let app_directory = crate::utils::tar::compress_dir(&apps_root.join(app_id)).await?;
    client
        .uninstall_app(Request::new(UninstallAppRequest {
            app_id: app_id.to_string(),
            app_directory,
        }))
        .await?;
    Ok(())
}
