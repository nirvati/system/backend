// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::constants::RESERVED_NAMES;
use crate::plugins::api::{RegisterAppRequest, UnregisterAppRequest, UserState};
use crate::plugins::get_plugins;
use anyhow::Result;
use kube::Client;
use nirvati_apps::internal::PluginType;
use std::collections::{BTreeMap, HashMap};
use tonic::Request;

pub async fn apply_hook(
    kube_client: Client,
    app_id: &str,
    perms: &[String],
    mut additional_metadata: BTreeMap<String, BTreeMap<String, serde_json::Value>>,
    user_state: &UserState,
) -> Result<()> {
    // Perms are in the format <app-id>/<perm> or just <app-id>
    let mut perms_by_app = HashMap::new();
    for perm in perms {
        let mut split = perm.split('/');
        let app_id = split.next().unwrap();
        let perm = split.next().unwrap_or("");
        if RESERVED_NAMES.contains(&app_id) {
            continue;
        }
        let perms = perms_by_app
            .entry(app_id.to_owned())
            .or_insert_with(Vec::new);
        perms.push(perm.to_owned());
    }
    let plugins = get_plugins(&user_state.user_id, kube_client).await?;
    for (plugin_name, plugin) in plugins
        .into_iter()
        .filter(|(_, plugin)| plugin.r#type == PluginType::Context)
    {
        let Some((plugin_app, plugin_id)) = plugin_name.split_once("-plugin-") else {
            tracing::warn!("Invalid plugin name: {}", plugin_name);
            continue;
        };
        let metadata = additional_metadata.remove(plugin_app).unwrap_or_default();
        let metadata = metadata.get(plugin_id);
        let serialized_metadata = if let Some(metadata) = metadata {
            serde_json::to_string(metadata)?
        } else {
            "{}".to_owned()
        };
        let mut client =
            match crate::plugins::api::context_plugin_client::ContextPluginClient::connect(
                plugin.endpoint.clone(),
            )
            .await
            {
                Ok(client) => client,
                Err(err) => {
                    tracing::error!("Failed to connect to plugin {}: {:#}", plugin_app, err);
                    continue;
                }
            };
        if let Err(err) = client
            .register_app(Request::new(RegisterAppRequest {
                app_id: app_id.to_owned(),
                permissions: perms_by_app.get(plugin_app).cloned().unwrap_or_default(),
                additional_data: serialized_metadata,
                user_state: Some(user_state.clone()),
            }))
            .await
        {
            tracing::error!("Failed to call plugin {}: {:#}", plugin_app, err);
            continue;
        };
    }
    Ok(())
}

pub async fn uninstall_hook(
    kube_client: Client,
    app_id: &str,
    perms: &[String],
    user_state: &UserState,
) -> Result<()> {
    // Perms are in the format <app-id>/<perm> or just <app-id>
    let mut used_plugins = Vec::new();
    for perm in perms {
        let mut split = perm.split('/');
        let app_id = split.next().unwrap();
        if RESERVED_NAMES.contains(&app_id) {
            continue;
        }
        used_plugins.push(app_id.to_owned());
    }

    let plugins = get_plugins(&user_state.user_id, kube_client).await?;
    for (plugin_name, plugin) in plugins
        .into_iter()
        .filter(|(_, plugin)| plugin.r#type == PluginType::Context)
    {
        let Some((plugin_app, _plugin_id)) = plugin_name.split_once("-plugin-") else {
            tracing::warn!("Invalid plugin name: {}", plugin_name);
            continue;
        };
        let mut client =
            match crate::plugins::api::context_plugin_client::ContextPluginClient::connect(
                plugin.endpoint.clone(),
            )
            .await
            {
                Ok(client) => client,
                Err(err) => {
                    tracing::error!("Failed to connect to plugin {}: {:#}", plugin_app, err);
                    continue;
                }
            };
        if let Err(err) = client
            .unregister_app(Request::new(UnregisterAppRequest {
                app_id: app_id.to_owned(),
                user_state: Some(user_state.clone()),
            }))
            .await
        {
            tracing::error!("Failed to call plugin {}: {:#}", plugin_app, err);
            continue;
        };
    }
    Ok(())
}
