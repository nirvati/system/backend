use kube::CustomResource;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(CustomResource, Deserialize, Serialize, Clone, Debug, JsonSchema, PartialEq)]
#[kube(
    kind = "Plugin",
    group = "nirvati.org",
    version = "v1alpha1",
    namespaced,
    derive = "PartialEq"
)]
pub struct PluginSpec {
    pub r#type: PluginType,
    pub endpoint: String,
}

#[derive(Serialize, Deserialize, Clone, Debug, JsonSchema, PartialEq)]
pub enum PluginType {
    #[serde(rename = "context")]
    Context,
    #[serde(rename = "cr")]
    Cr,
    #[serde(rename = "runtime")]
    Runtime,
    #[serde(rename = "repository")]
    Repository,
}

#[derive(CustomResource, Deserialize, Serialize, Clone, Debug, JsonSchema, PartialEq)]
#[kube(
    kind = "ClusterPlugin",
    group = "nirvati.org",
    version = "v1alpha1",
    derive = "PartialEq"
)]
pub struct ClusterPluginSpec {
    pub r#type: PluginType,
    pub endpoint: String,
}

impl From<PluginType> for nirvati_apps::internal::PluginType {
    fn from(plugin_type: PluginType) -> Self {
        match plugin_type {
            PluginType::Context => nirvati_apps::internal::PluginType::Context,
            PluginType::Cr => nirvati_apps::internal::PluginType::CustomResource,
            PluginType::Runtime => nirvati_apps::internal::PluginType::Runtime,
            PluginType::Repository => nirvati_apps::internal::PluginType::Source,
        }
    }
}

impl From<nirvati_apps::internal::PluginType> for PluginType {
    fn from(plugin_type: nirvati_apps::internal::PluginType) -> Self {
        match plugin_type {
            nirvati_apps::internal::PluginType::Context => PluginType::Context,
            nirvati_apps::internal::PluginType::CustomResource => PluginType::Cr,
            nirvati_apps::internal::PluginType::Runtime => PluginType::Runtime,
            nirvati_apps::internal::PluginType::Source => PluginType::Repository,
        }
    }
}
