// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use super::api::ConvertCrRequest;
use crate::plugins::api::UserState;
use crate::plugins::get_plugins;
use anyhow::Result;
use kube::Client;
use nirvati_apps::internal::types::CustomResource;
#[cfg(feature = "test-parser")]
use nirvati_plugin_common::custom_resource_plugin_server::CustomResourcePlugin;
use serde_yaml::Value::Sequence;
use tonic::Request;

pub async fn convert_crs(
    #[cfg(not(feature = "test-parser"))] kube_client: Client,
    app_id: &str,
    crs: &[CustomResource],
    user_state: UserState,
    is_system_library: bool,
) -> Result<(Vec<serde_yaml::Value>, Vec<String>)> {
    #[cfg(not(feature = "test-parser"))]
    let plugins = get_plugins(&user_state.user_id, kube_client).await?;
    let mut result: Vec<serde_yaml::Value> = vec![];
    let mut additional_permissions: Vec<String> = vec![];
    for cr in crs {
        #[cfg(not(feature = "test-parser"))]
        {
            let Some(plugin) = plugins.get(format!("{}-plugin-{}", cr.app, cr.plugin_id).as_str())
            else {
                tracing::warn!("Plugin {}-plugin-{} not found", cr.app, cr.plugin_id);
                continue;
            };
            tracing::info!("Connecting to plugin at {}", plugin.endpoint);
            let mut client = crate::plugins::api::custom_resource_plugin_client::CustomResourcePluginClient::connect(plugin.endpoint.clone()).await?;
            let response = client
                .generate_c_rs(Request::new(ConvertCrRequest {
                    app_id: app_id.to_string(),
                    resource: serde_yaml::to_string(&cr.definition)?,
                    user_state: Some(user_state.clone()),
                    is_system_library,
                }))
                .await?;
            let mut response = response.into_inner();
            additional_permissions.append(&mut response.added_permissions);
            let parsed: serde_yaml::Value = serde_yaml::from_str(&response.resources)?;
            if let Sequence(parsed) = parsed {
                result.extend(parsed);
            } else {
                result.push(parsed);
            }
        }
        #[cfg(feature = "test-parser")]
        {
            if cr.app == "tor" && cr.plugin_id == "onion-service" {
                let mut client = nirvati_plugin_tor::plugin::PluginState {};
                let response = client
                    .generate_c_rs(Request::new(ConvertCrRequest {
                        app_id: app_id.to_string(),
                        resource: serde_yaml::to_string(&cr.definition)?,
                        user_state: Some(user_state.clone()),
                        is_system_library,
                    }))
                    .await?;
                let mut response = response.into_inner();
                additional_permissions.append(&mut response.added_permissions);
                let parsed: serde_yaml::Value = serde_yaml::from_str(&response.resources)?;
                if let Sequence(parsed) = parsed {
                    result.extend(parsed);
                } else {
                    result.push(parsed);
                }
            } else {
                anyhow::bail!(
                    "Unsupported CR {}/{} (No mock provided!)",
                    cr.app,
                    cr.plugin_id
                );
            }
        }
    }
    Ok((result, additional_permissions))
}
