// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

mod context;
pub mod crd;
mod custom_resource;
mod runtime;

use crate::plugins::api::UserState;
use anyhow::Result;
use kube::{Api, Client};
use nirvati_apps::internal::types::InternalAppRepresentation;
use nirvati_apps::internal::PluginType;
use nirvati_apps::metadata::Runtime;
use std::collections::{BTreeMap, HashMap};
use std::path::Path;

pub use nirvati_plugin_common as api;

pub async fn apply_hook(
    kube_client: Client,
    global_apps_root: &Path,
    app_id: &str,
    app: &InternalAppRepresentation,
    perms: &[String],
    additional_metadata: BTreeMap<String, BTreeMap<String, serde_json::Value>>,
    user_state: UserState,
) -> Result<()> {
    context::apply_hook(
        kube_client.clone(),
        app_id,
        perms,
        additional_metadata,
        &user_state,
    )
    .await?;
    let runtime = &app.get_metadata().runtime;
    if let Runtime::Plugin(plugin) = runtime {
        runtime::apply_hook(kube_client, global_apps_root, app_id, plugin, user_state).await?;
    }
    Ok(())
}

pub async fn uninstall_hook(
    kube_client: Client,
    apps_root: &Path,
    app_id: &str,
    perms: &[String],
    app: &InternalAppRepresentation,
    user_state: &UserState,
) -> Result<()> {
    context::uninstall_hook(kube_client.clone(), app_id, perms, user_state).await?;
    let runtime = &app.get_metadata().runtime;
    if let Runtime::Plugin(plugin) = runtime {
        runtime::uninstall_hook(kube_client, apps_root, app_id, plugin, &user_state.user_id)
            .await?;
    }
    Ok(())
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct BasicPluginInfo {
    pub r#type: PluginType,
    pub endpoint: String,
}

pub async fn get_plugins(
    user_id: &str,
    kube_client: Client,
) -> Result<HashMap<String, BasicPluginInfo>> {
    let cluster_plugin_api: Api<ClusterPlugin> = Api::all(kube_client);
    let plugins = cluster_plugin_api.list(&Default::default()).await?.items;
    let mut result = HashMap::new();
    for plugin in plugins {
        let Some(name) = plugin.metadata.name else {
            tracing::error!("Plugin without name found");
            continue;
        };
        result.insert(
            name,
            BasicPluginInfo {
                endpoint: plugin.spec.endpoint.clone(),
                r#type: plugin.spec.r#type.into(),
            },
        );
    }
    let user_plugin_api: Api<Plugin> = Api::namespaced(cluster_plugin_api.into_client(), user_id);
    let plugins = user_plugin_api.list(&Default::default()).await?.items;
    for plugin in plugins {
        let Some(name) = plugin.metadata.name else {
            tracing::error!("Plugin without name found");
            continue;
        };
        if let std::collections::hash_map::Entry::Vacant(e) = result.entry(name.clone()) {
            e.insert(BasicPluginInfo {
                endpoint: plugin.spec.endpoint.clone(),
                r#type: plugin.spec.r#type.into(),
            });
        } else {
            // TODO: Prevent this scenario via apply hook
            tracing::warn!("Plugin {} already exists in the cluster", name);
        }
    }
    Ok(result)
}

use crate::plugins::crd::{ClusterPlugin, Plugin};
pub use custom_resource::convert_crs;
