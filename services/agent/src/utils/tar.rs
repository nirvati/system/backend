// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::io;
use std::os::unix::fs::{MetadataExt, PermissionsExt};
use std::path::Path;

use anyhow::{anyhow, Result};
use flate2::write::GzEncoder;
use tar::Header;

pub fn list_dir_recursive(dir: &Path) -> Result<Vec<String>> {
    let mut entries = Vec::new();
    for entry in dir.read_dir()? {
        let entry = entry?;
        let path = entry.path();
        if path.is_dir() {
            entries.push(path.to_string_lossy().to_string());
            entries.extend(list_dir_recursive(&path)?);
        } else {
            entries.push(path.to_string_lossy().to_string());
        }
    }
    Ok(entries)
}

pub async fn compress_dir(dir: &Path) -> Result<Vec<u8>> {
    let output = Vec::new();
    let gz_encoder = GzEncoder::new(output, flate2::Compression::default());
    let mut tar = tar::Builder::new(gz_encoder);
    for entry in list_dir_recursive(dir)? {
        let path = Path::new(&entry);
        let mut header = Header::new_gnu();
        header.set_size(path.metadata()?.len());
        header.set_mode(path.metadata()?.permissions().mode());
        header.set_uid(path.metadata()?.uid() as u64);
        header.set_gid(path.metadata()?.gid() as u64);
        header.set_mtime(
            path.metadata()?
                .modified()?
                .duration_since(std::time::SystemTime::UNIX_EPOCH)?
                .as_secs(),
        );
        // Set path, but strip the dir prefix
        header.set_path(path.strip_prefix(dir)?)?;
        header.set_cksum();
        if path.is_dir() {
            header.set_entry_type(tar::EntryType::Directory);
            header.set_size(0);
            header.set_cksum();
            tar.append(&header, &mut io::empty())?;
        } else if path.is_symlink() {
            // Ensure symlink doesn't point outside the directory
            let link_path = path.read_link()?;
            if !link_path.starts_with(dir) {
                return Err(anyhow!("Path traversal attack detected"));
            }
            header.set_entry_type(tar::EntryType::Symlink);
            header.set_link_name(path.read_link()?)?;
            header.set_cksum();
            tar.append(&header, &mut io::empty())?;
        } else {
            let file = std::fs::File::open(path)?;
            header.set_cksum();
            tar.append(&header, file)?;
        }
    }
    tar.finish()?;
    Ok(tar.into_inner()?.finish()?)
}

// Decompresses a gz-encoded tarball into a directory, preventing any path traversal attacks
pub async fn safe_decompress_dir(archive: Vec<u8>, target: &Path) -> Result<()> {
    let mut tar = tar::Archive::new(flate2::read::GzDecoder::new(&archive[..]));
    for entry in tar.entries()? {
        let mut entry = entry?;
        let path = entry.path()?;
        let path = target.join(path);
        if !path.starts_with(target) {
            return Err(anyhow!("Path traversal attack detected"));
        }
        // Also check entry.link_name()
        if let Some(link_name) = entry.link_name()? {
            let link_path = target.join(link_name);
            if !link_path.starts_with(target) {
                return Err(anyhow!("Path traversal attack detected"));
            }
        }
        entry.unpack(path)?;
    }
    Ok(())
}
