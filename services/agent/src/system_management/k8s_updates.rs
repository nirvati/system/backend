// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::HashMap;
use std::fmt::Display;

use anyhow::{anyhow, bail, Result};
use k8s_crds_system_upgrade_controller::plans::*;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use kube::api::PostParams;
use kube::Client;
use semver::{BuildMetadata, Version};
use serde::{Deserialize, Serialize};
use slugify::slugify;
use tokio::try_join;

const K3S_UPDATES_URL: &str = "https://update.k3s.io/v1-release/channels";
const RKE2_UPDATES_URL: &str = "https://update.rke2.io/v1-release/channels";

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone)]
pub struct KubeVersion {
    pub k8s: Version,
    pub distro_version: u8,
    pub distro: Distro,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone)]
pub enum Distro {
    K3s,
    Rke2,
}

impl std::str::FromStr for KubeVersion {
    type Err = anyhow::Error;

    // Format is something like v1.28.3+k3s2
    // or v1.28.3+rke2r1
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut k8s_version = Version::parse(s.trim_start_matches('v'))?;
        let build_meta = k8s_version.build.as_str().to_string();
        k8s_version.build = BuildMetadata::EMPTY;
        let distro_version = build_meta
            .trim_start_matches("k3s")
            .trim_start_matches("rke2r")
            .parse()?;
        let distro = if build_meta.starts_with("k3s") {
            Distro::K3s
        } else if build_meta.starts_with("rke2r") {
            Distro::Rke2
        } else {
            bail!("Unknown distro!");
        };
        Ok(KubeVersion {
            k8s: k8s_version,
            distro_version,
            distro,
        })
    }
}

impl Display for KubeVersion {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let distro_str = match self.distro {
            Distro::K3s => "k3s",
            Distro::Rke2 => "rke2r",
        };
        let str = format!(
            "v{}.{}.{}+{}{}",
            self.k8s.major, self.k8s.minor, self.k8s.patch, distro_str, self.distro_version
        );
        write!(f, "{}", str)
    }
}

pub async fn get_installed_version(client: &Client) -> Result<KubeVersion> {
    let version = client.apiserver_version().await?;
    version.git_version.parse()
}

pub async fn get_installed_version_raw(client: &Client) -> Result<String> {
    let version = client.apiserver_version().await?;
    Ok(version.git_version)
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct KubeChannelData {
    pub id: String,
    pub r#type: String,
    pub links: HashMap<String, String>,
    pub name: String,
    pub latest: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct KubeChannels {
    pub r#type: String,
    pub links: HashMap<String, String>,
    pub actions: serde_json::Value,
    #[serde(rename = "resourceType")]
    pub resource_type: String,
    pub data: Vec<KubeChannelData>,
}

pub async fn get_latest_release_up_to_channel(
    installed_version: &KubeVersion,
    target_channel: &str,
) -> Result<KubeVersion> {
    let updates_url = match installed_version.distro {
        Distro::K3s => K3S_UPDATES_URL,
        Distro::Rke2 => RKE2_UPDATES_URL,
    };
    let channels: KubeChannels = reqwest::get(updates_url).await?.json().await?;
    let version_in_channel = channels
        .data
        .iter()
        .find(|channel| channel.name == target_channel)
        .ok_or(anyhow!("Failed to find channel!"))?;
    let Some(ref latest_version) = version_in_channel.latest else {
        bail!("No latest version found in channel!");
    };
    let parsed_version: KubeVersion = latest_version.parse()?;
    let diff: i32 = (parsed_version.k8s.minor as i32) - (installed_version.k8s.minor as i32);
    if (diff == 0 || diff == 1) && parsed_version.k8s.major == installed_version.k8s.major {
        Ok(parsed_version)
    } else if parsed_version.k8s.major == installed_version.k8s.major {
        let target_channel = if parsed_version.k8s.minor > installed_version.k8s.minor {
            format!(
                "v{}.{}",
                installed_version.k8s.major,
                installed_version.k8s.minor + 1
            )
        } else {
            // To avoid both downgrades and further upgrades beyond the channel, but still provide
            // security updates, check for the latest version of the current minor
            format!(
                "v{}.{}",
                installed_version.k8s.major, installed_version.k8s.minor
            )
        };
        let version_in_channel = channels
            .data
            .iter()
            .find(|channel| channel.name == target_channel)
            .ok_or(anyhow!("Failed to find channel!"))?;
        let Some(ref latest_version) = version_in_channel.latest else {
            bail!("No latest version found in channel!");
        };
        let parsed_version: KubeVersion = latest_version.parse()?;
        Ok(parsed_version)
    } else {
        bail!("We can't handle major version upgrades yet!");
    }
}

pub async fn get_upgrade_plan(version: &KubeVersion) -> [Plan; 2] {
    let upgrade_image = Some(match version.distro {
        Distro::K3s => "rancher/k3s-upgrade".to_string(),
        Distro::Rke2 => "rancher/rke2-upgrade".to_string(),
    });
    [
        Plan {
            metadata: k8s_meta::ObjectMeta {
                name: Some(format!(
                    "kube-server-upgrade-{}",
                    slugify!(&version.to_string())
                )),
                ..Default::default()
            },
            spec: PlanSpec {
                channel: None,
                concurrency: Some(1),
                cordon: Some(true),
                drain: None,
                node_selector: Some(PlanNodeSelector {
                    match_expressions: Some(vec![PlanNodeSelectorMatchExpressions {
                        key: Some("node-role.kubernetes.io/control-plane".to_string()),
                        operator: Some("In".to_string()),
                        values: Some(vec!["true".to_string()]),
                    }]),
                    ..Default::default()
                }),
                prepare: None,
                secrets: None,
                service_account_name: Some("system-upgrade".to_string()),
                tolerations: None,
                upgrade: PlanUpgrade {
                    image: upgrade_image.clone(),
                    ..Default::default()
                },
                version: Some(version.to_string()),
                exclusive: None,
                image_pull_secrets: None,
                job_active_deadline_secs: None,
            },
            status: None,
        },
        Plan {
            metadata: k8s_meta::ObjectMeta {
                name: Some(format!(
                    "kube-agent-upgrade-{}",
                    slugify!(&version.to_string())
                )),
                ..Default::default()
            },
            spec: PlanSpec {
                channel: None,
                concurrency: Some(1),
                cordon: Some(true),
                drain: None,
                node_selector: Some(PlanNodeSelector {
                    match_expressions: Some(vec![PlanNodeSelectorMatchExpressions {
                        key: Some("node-role.kubernetes.io/control-plane".to_string()),
                        operator: Some("DoesNotExist".to_string()),
                        ..Default::default()
                    }]),
                    ..Default::default()
                }),
                prepare: None,
                secrets: None,
                service_account_name: Some("system-upgrade".to_string()),
                tolerations: None,
                upgrade: PlanUpgrade {
                    image: upgrade_image,
                    ..Default::default()
                },
                version: Some(version.to_string()),
                exclusive: None,
                image_pull_secrets: None,
                job_active_deadline_secs: None,
            },
            status: None,
        },
    ]
}

pub async fn upgrade(client: &Client, version: &KubeVersion) -> Result<()> {
    let plan = get_upgrade_plan(version).await;
    let api: kube::Api<Plan> = kube::Api::namespaced(client.clone(), "system-upgrade");
    let pp = PostParams::default();
    try_join!(api.create(&pp, &plan[0]), api.create(&pp, &plan[1]))?;
    Ok(())
}

pub async fn get_distro(client: &Client) -> Result<Option<Distro>> {
    let version = client.apiserver_version().await?;
    let version = version.git_version;
    if version.contains("k3s") {
        Ok(Some(Distro::K3s))
    } else if version.contains("rke2") {
        Ok(Some(Distro::Rke2))
    } else {
        Ok(None)
    }
}
