// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

pub mod services;

use k8s_crds_traefik::ingressroutes::*;
use k8s_crds_traefik::ingressroutetcps::*;
use k8s_crds_traefik::middlewares::*;
use k8s_crds_traefik::serverstransports::*;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use k8s_openapi::apimachinery::pkg::util::intstr::IntOrString;
use slugify::slugify;

use crate::grpc::api::Protocol;

pub fn get_protocol(proto: i32) -> Protocol {
    match proto {
        0 => Protocol::Http,
        1 => Protocol::Https,
        _ => unreachable!("Invalid protocol"),
    }
}

pub fn generate_ingress_for_proxy(
    domain: &str,
    targets: Vec<crate::grpc::api::Route>,
) -> ([IngressRoute; 2], Vec<Middleware>, Vec<ServersTransport>) {
    let mut generated_middlewares = vec![];
    let mut generated_server_transports = vec![];
    let routes = targets
        .into_iter()
        .map(|target| {
            let rule = format!(
                "Host(`{}`) && PathPrefix(`{}`)",
                domain, target.local_prefix
            );
            let mut middlewares = vec![];
            if target.remote_prefix != target.local_prefix {
                if target.local_prefix != "/" {
                    middlewares.push(IngressRouteRoutesMiddlewares {
                        name: format!("strip-prefix-{}", slugify!(&target.local_prefix)),
                        namespace: None,
                    });
                    generated_middlewares.push(Middleware {
                        metadata: k8s_meta::ObjectMeta {
                            name: Some(format!("strip-prefix-{}", slugify!(&target.local_prefix))),
                            ..Default::default()
                        },
                        spec: k8s_crds_traefik::MiddlewareSpec {
                            strip_prefix: Some(MiddlewareStripPrefix {
                                prefixes: Some(vec![target.local_prefix.clone()]),
                                force_slash: None,
                            }),
                            ..Default::default()
                        },
                    });
                }
                if target.remote_prefix != "/" {
                    middlewares.push(IngressRouteRoutesMiddlewares {
                        name: format!("add-prefix-{}", slugify!(&target.remote_prefix)),
                        namespace: None,
                    });
                    generated_middlewares.push(Middleware {
                        metadata: k8s_meta::ObjectMeta {
                            name: Some(format!("add-prefix-{}", slugify!(&target.remote_prefix))),
                            ..Default::default()
                        },
                        spec: MiddlewareSpec {
                            add_prefix: Some(MiddlewareAddPrefix {
                                prefix: Some(target.remote_prefix.clone()),
                            }),
                            ..Default::default()
                        },
                    });
                }
            };
            let proto = get_protocol(target.protocol);
            let has_server_transport = proto == Protocol::Https && target.https.is_some();
            if proto == Protocol::Https {
                if let Some(https_options) = target.https {
                    generated_server_transports.push(ServersTransport {
                        metadata: k8s_meta::ObjectMeta {
                            name: Some(format!(
                                "tls-{}-{}-{}",
                                slugify!(&domain),
                                slugify!(&target.target_svc),
                                slugify!(&target.local_prefix)
                            )),
                            ..Default::default()
                        },
                        spec: ServersTransportSpec {
                            insecure_skip_verify: Some(https_options.insecure_skip_verify),
                            server_name: https_options.sni,
                            ..Default::default()
                        },
                    });
                }
            }
            IngressRouteRoutes {
                kind: IngressRouteRoutesKind::Rule,
                r#match: rule,
                middlewares: if middlewares.is_empty() {
                    None
                } else {
                    Some(middlewares.clone())
                },
                priority: None,
                services: Some(vec![IngressRouteRoutesServices {
                    kind: Some(IngressRouteRoutesServicesKind::Service),
                    servers_transport: if proto == Protocol::Https && has_server_transport {
                        Some(format!(
                            "tls-{}-{}-{}",
                            slugify!(&domain),
                            slugify!(&target.target_svc),
                            slugify!(&target.local_prefix)
                        ))
                    } else {
                        None
                    },
                    name: target.target_svc,
                    port: Some(IntOrString::Int(target.port)),
                    ..Default::default()
                }]),
                syntax: None,
            }
        })
        .collect();
    (
        [
            IngressRoute {
                metadata: k8s_meta::ObjectMeta {
                    name: Some(slugify!(domain)),
                    ..Default::default()
                },
                spec: IngressRouteSpec {
                    entry_points: Some(vec!["websecure".to_string()]),
                    routes,
                    tls: Some(IngressRouteTls {
                        secret_name: Some(format!("{}-tls", slugify!(&domain))),
                        ..Default::default()
                    }),
                },
            },
            IngressRoute {
                metadata: k8s_meta::ObjectMeta {
                    name: Some(format!("{}-http-to-https-redirect", slugify!(domain))),
                    ..Default::default()
                },
                spec: IngressRouteSpec {
                    entry_points: Some(vec!["web".to_string()]),
                    routes: vec![IngressRouteRoutes {
                        kind: IngressRouteRoutesKind::Rule,
                        r#match: format!("Host(`{}`)", domain),
                        priority: None,
                        services: None,
                        middlewares: Some(vec![IngressRouteRoutesMiddlewares {
                            name: "https-redirect".to_string(),
                            namespace: Some("nirvati".to_string()),
                        }]),
                        syntax: None,
                    }],
                    tls: None,
                },
            },
        ],
        generated_middlewares,
        generated_server_transports,
    )
}

pub fn generate_ingress_for_tcp(
    domain: &str,
    target: String,
    port: i32,
) -> (IngressRouteTCP, IngressRoute) {
    let rule = format!("HostSNI(`{}`)", &domain);
    (
        IngressRouteTCP {
            metadata: k8s_meta::ObjectMeta {
                name: Some(slugify!(&domain)),
                ..Default::default()
            },
            spec: IngressRouteTCPSpec {
                entry_points: Some(vec!["websecure".to_string()]),
                routes: vec![IngressRouteTCPRoutes {
                    r#match: rule,
                    priority: None,
                    services: Some(vec![IngressRouteTCPRoutesServices {
                        name: target,
                        port: IntOrString::Int(port),
                        ..Default::default()
                    }]),
                    middlewares: None,
                    syntax: None,
                }],
                tls: None,
            },
        },
        IngressRoute {
            metadata: k8s_meta::ObjectMeta {
                name: Some(format!("{}-http-to-https-redirect", slugify!(&domain))),
                ..Default::default()
            },
            spec: IngressRouteSpec {
                entry_points: Some(vec!["web".to_string()]),
                routes: vec![IngressRouteRoutes {
                    kind: IngressRouteRoutesKind::Rule,
                    r#match: format!("Host(`{}`)", domain),
                    priority: None,
                    services: None,
                    middlewares: Some(vec![IngressRouteRoutesMiddlewares {
                        name: "https-redirect".to_string(),
                        namespace: Some("nirvati".to_string()),
                    }]),
                    syntax: None,
                }],
                tls: None,
            },
        },
    )
}
