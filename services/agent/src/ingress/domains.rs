// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

pub mod https;
pub mod onion;

use k8s_crds_traefik::{IngressRoute, IngressRouteTCP};
use kube::api::DeleteParams;
use kube::{Api, Client};
use slugify::slugify;
use std::cmp::PartialEq;

use nirvati::kubernetes::apply_with_ns;
use nirvati_apps::internal::InternalAppRepresentation;
use nirvati_apps::metadata::AppScope;

use crate::apps::generator::kubernetes::ingress::{get_app_ingress, get_ingress_routes};
use crate::apps::generator::kubernetes::tcpingress::get_app_tcp_ingress;
use crate::grpc::api::DomainType;

#[derive(Debug, PartialEq)]
pub enum IngressType {
    Http,
    Https,
    Onion,
    Tcp,
}

pub async fn add_app_domain(
    client: &Client,
    app: InternalAppRepresentation,
    domain: &str,
    user: &str,
    protect: bool,
    custom_prefix: Option<String>,
    ingress_type: IngressType,
    component_id: Option<String>,
) -> anyhow::Result<()> {
    let mw = custom_prefix.as_ref().map(|prefix| {
        crate::apps::generator::kubernetes::middleware::strip_prefix_mw_with_name(
            prefix,
            format!("strip-custom-prefix-{}", slugify!(&prefix)),
        )
    });
    let ns = if app.get_metadata().scope == AppScope::System {
        app.get_metadata().id.clone()
    } else {
        format!("{}-{}", user, app.get_metadata().id)
    };
    match ingress_type {
        IngressType::Http | IngressType::Onion => {
            let main_ingress = get_app_ingress(
                Some(domain.to_string()),
                app.get_ingress(),
                vec!["web".to_string()],
                false,
                user,
                protect,
                custom_prefix,
                component_id,
            );
            apply_with_ns(client.clone(), &[main_ingress], &ns).await?;
        }
        IngressType::Https => {
            let ingress = get_ingress_routes(
                domain.to_string(),
                app.get_ingress(),
                user,
                protect,
                custom_prefix,
                component_id,
            );
            apply_with_ns(client.clone(), &ingress, &ns).await?;
        }
        IngressType::Tcp => {
            let main_ingress = get_app_tcp_ingress(
                domain,
                app.get_ingress(),
                vec!["websecure".to_string()],
                user,
            );
            apply_with_ns(client.clone(), &[main_ingress], &ns).await?;
            if app
                .get_ingress()
                .iter()
                .any(|source| source.r#type == nirvati_apps::internal::IngressType::HttpFallback)
            {
                let http_ingress = get_app_ingress(
                    Some(domain.to_string()),
                    app.get_ingress(),
                    vec!["web".to_string()],
                    false,
                    user,
                    protect,
                    custom_prefix,
                    component_id,
                );
                apply_with_ns(client.clone(), &[http_ingress], &ns).await?;
            }
        }
    }
    if let Some(mw) = mw {
        apply_with_ns(client.clone(), &[mw], &ns).await?;
    }
    Ok(())
}

pub async fn delete_app_domain(
    client: &Client,
    app: InternalAppRepresentation,
    domain: String,
    user: String,
    domain_type: DomainType,
) -> anyhow::Result<()> {
    let dp = DeleteParams::default();
    let ns = if app.get_metadata().scope == AppScope::System {
        app.get_metadata().id.clone()
    } else {
        format!("{}-{}", user, app.get_metadata().id)
    };
    if domain_type == DomainType::DomainHttp
        || domain_type == DomainType::DomainHttps
        || domain_type == DomainType::DomainOnion
    {
        let api: Api<IngressRoute> = Api::namespaced(client.clone(), &ns);
        api.delete(&slugify!(&domain).to_string(), &dp).await?;
        if domain_type == DomainType::DomainHttps {
            api.delete(&format!("{}-http", slugify!(&domain)), &dp)
                .await?;
        }
    } else if domain_type == DomainType::DomainTcp {
        let api: Api<IngressRouteTCP> = Api::namespaced(client.clone(), &ns);
        api.delete(&slugify!(&domain).to_string(), &dp).await?;
        let api: Api<IngressRoute> = Api::namespaced(api.into_client(), &ns);
        api.delete(&format!("{}-http", slugify!(&domain)), &dp)
            .await?;
    }
    Ok(())
}
