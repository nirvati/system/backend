use k8s_crds_arti_controller::{
    OnionService, OnionServiceKeySecret, OnionServiceRoutes, OnionServiceRoutesTarget,
    OnionServiceSpec,
};
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use kube::{Api, Client};
use nirvati::kubernetes::apply_with_ns;
use nirvati_apps::internal::InternalAppRepresentation;
use nirvati_apps::metadata::AppScope;

pub async fn get_new_onion_domain(
    client: &Client,
    app: &InternalAppRepresentation,
    user: &str,
) -> anyhow::Result<String> {
    let ns = if app.get_metadata().scope == AppScope::System {
        app.get_metadata().id.clone()
    } else {
        format!("{}-{}", user, app.get_metadata().id)
    };
    let onion_svc = OnionService {
        metadata: ObjectMeta {
            name: Some(ns.clone()),
            namespace: Some("kube-system".to_string()),
            ..Default::default()
        },
        spec: OnionServiceSpec {
            key_secret: OnionServiceKeySecret {
                name: "onion-svc-keys".to_string(),
                namespace: Some(ns.clone()),
            },
            routes: vec![OnionServiceRoutes {
                source_port: 80,
                target: OnionServiceRoutesTarget {
                    port: 80,
                    svc: Some("traefik".to_string()),
                    dns_name: None,
                    ip: None,
                    ns: None,
                },
            }],
            store_name_in_secret: None,
        },
        status: None,
    };
    apply_with_ns(client.clone(), &[onion_svc], "kube-system").await?;
    // Wait for up to 30s for the onion service to get a domain into its status
    let mut count = 0;
    let api: Api<OnionService> = Api::namespaced(client.clone(), "kube-system");
    let mut domain = None;
    while count < 30 {
        let svc = api.get(&ns).await?;
        if let Some(status) = svc.status {
            domain = Some(status.onion_name);
        }
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        count += 1;
    }
    domain.ok_or_else(|| anyhow::anyhow!("Failed to get onion domain"))
}

pub async fn delete_onion_domain(
    client: &Client,
    app: &InternalAppRepresentation,
    user: &str,
) -> anyhow::Result<()> {
    let ns = if app.get_metadata().scope == AppScope::System {
        app.get_metadata().id.clone()
    } else {
        format!("{}-{}", user, app.get_metadata().id)
    };
    let api: Api<OnionService> = Api::namespaced(client.clone(), "kube-system");
    api.delete(&ns, &Default::default()).await?;
    Ok(())
}
