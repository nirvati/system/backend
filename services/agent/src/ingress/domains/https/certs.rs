// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use super::providers::AcmeProvider;
use crate::grpc::api::CertificateStatus;
use anyhow::Result;
use k8s_crds_cert_manager::certificates::*;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use kube::api::DeleteParams;
use kube::{Api, Client};
use nirvati::kubernetes::apply_with_ns;
use slugify::slugify;

fn generate_cert_internal(
    user: String,
    domain: String,
    parent: Option<String>,
    provider: AcmeProvider,
) -> Certificate {
    Certificate {
        metadata: k8s_meta::ObjectMeta {
            name: Some(slugify!(&domain)),
            ..Default::default()
        },
        spec: CertificateSpec {
            secret_name: format!("{}-tls", slugify!(&domain)),
            issuer_ref: if let Some(parent) = &parent {
                CertificateIssuerRef {
                    name: format!("{}-{}-{}", provider.get_slug(), user, slugify!(parent)),
                    kind: Some("ClusterIssuer".to_owned()),
                    ..Default::default()
                }
            } else {
                CertificateIssuerRef {
                    name: format!("{}-{}", provider.get_slug(), user),
                    kind: Some("ClusterIssuer".to_owned()),
                    ..Default::default()
                }
            },
            dns_names: Some(vec![domain]),
            ..Default::default()
        },
        status: None,
    }
}

pub async fn delete_cert(client: &Client, domain: String, ns: &str) -> Result<()> {
    let dp = DeleteParams::default();
    let api: Api<Certificate> = Api::namespaced(client.clone(), ns);
    api.delete(&slugify!(&domain), &dp).await?;
    Ok(())
}

pub async fn generate_cert(
    client: &Client,
    user: String,
    ns: &str,
    domain: String,
    parent: Option<String>,
    acme_provider: AcmeProvider,
) -> Result<()> {
    let cert = generate_cert_internal(user, domain, parent, acme_provider);
    apply_with_ns(client.clone(), &[cert], ns).await?;
    Ok(())
}

pub async fn get_cert_status(
    client: &Client,
    ns: &str,
    domain: String,
) -> Result<CertificateStatus> {
    let api: Api<Certificate> = Api::namespaced(client.clone(), ns);
    let cert = api.get(&slugify!(&domain)).await?;
    let conditions = cert
        .status
        .unwrap_or_default()
        .conditions
        .unwrap_or_default();
    Ok(CertificateStatus {
        ready: conditions
            .into_iter()
            .any(|condition| condition.type_ == "Ready" && condition.status == "True"),
    })
}
