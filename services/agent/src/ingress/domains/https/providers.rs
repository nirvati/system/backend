// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use k8s_crds_cert_manager::ClusterIssuer;
use k8s_openapi::api::core::v1 as k8s;
use slugify::slugify;

pub enum AcmeProvider {
    LetsEncrypt,
    BuyPass,
    Custom(String),
}

impl AcmeProvider {
    pub fn get_url(&self) -> &str {
        match self {
            AcmeProvider::LetsEncrypt => "https://acme-v02.api.letsencrypt.org/directory",
            AcmeProvider::BuyPass => "https://api.buypass.com/acme/directory",
            AcmeProvider::Custom(url) => url,
        }
    }

    pub fn get_slug(&self) -> String {
        match self {
            AcmeProvider::LetsEncrypt => "letsencrypt".to_string(),
            AcmeProvider::BuyPass => "buypass".to_string(),
            AcmeProvider::Custom(url) => slugify!(url),
        }
    }
}

#[derive(Clone, PartialEq, Eq)]
pub enum DNSProviderWithAuth {
    Cloudflare(String),
    NirvatiMe(String),
}

impl DNSProviderWithAuth {
    pub fn get_slug(&self) -> &'static str {
        match self {
            DNSProviderWithAuth::Cloudflare(_) => "cloudflare",
            DNSProviderWithAuth::NirvatiMe(_) => "nirvati-me",
        }
    }
}

pub struct DnsIssuerK8s {
    pub issuer: ClusterIssuer,
    pub secret: Option<k8s::Secret>,
}
