use super::providers::{AcmeProvider, DNSProviderWithAuth as DNSProvider};
use crate::ingress::domains::https::providers::DnsIssuerK8s;
use anyhow::Result;
use k8s_crds_cert_manager::*;
use k8s_openapi::api::core::v1::Secret;
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use kube::api::{DeleteParams, PostParams};
use kube::{Api, Client};
use slugify::slugify;
use std::collections::BTreeMap;

pub fn dns_issuer_config(
    user: String,
    domain: String,
    domain_provider: Option<DNSProvider>,
    owner_email: Option<String>,
    acme_provider: AcmeProvider,
) -> Result<DnsIssuerK8s> {
    let issuer_name = format!(
        "{}-{}-{}",
        acme_provider.get_slug(),
        user,
        slugify!(&domain)
    );
    let credentials_secret_name = format!(
        "{}-{}-{}-credentials",
        acme_provider.get_slug(),
        user,
        slugify!(&domain)
    );
    let acme_account_key_secret_name = format!(
        "{}-{}-{}-acme-key",
        acme_provider.get_slug(),
        user,
        slugify!(&domain)
    );
    let dns_solver = domain_provider
        .as_ref()
        .map(|domain_provider| match &domain_provider {
            DNSProvider::Cloudflare(_) => ClusterIssuerAcmeSolversDns01 {
                cloudflare: Some(ClusterIssuerAcmeSolversDns01Cloudflare {
                    api_token_secret_ref: Some(
                        ClusterIssuerAcmeSolversDns01CloudflareApiTokenSecretRef {
                            name: credentials_secret_name.clone(),
                            key: Some("api-token".to_owned()),
                        },
                    ),
                    ..Default::default()
                }),
                ..Default::default()
            },
            DNSProvider::NirvatiMe(_) => ClusterIssuerAcmeSolversDns01 {
                webhook: Some(ClusterIssuerAcmeSolversDns01Webhook {
                    group_name: "acme.nirvati.me".to_owned(),
                    solver_name: "nirvati-me".to_owned(),
                    config: Some(serde_json::json!( {
                    "domain": domain,
                    "apiUsernameSecretRef": {
                            "name": credentials_secret_name.clone(),
                            "key": "username",
                        },
                    "apiPasswordSecretRef": {
                            "name": credentials_secret_name.clone(),
                            "key": "password",
                        }
                    })),
                }),
                ..Default::default()
            },
        });
    let http_solver = if dns_solver.is_none() {
        Some(ClusterIssuerAcmeSolversHttp01 {
            ingress: Some(ClusterIssuerAcmeSolversHttp01Ingress {
                class: Some("traefik".to_string()),
                ..Default::default()
            }),
            ..Default::default()
        })
    } else {
        None
    };
    let issuer = ClusterIssuer {
        metadata: ObjectMeta {
            name: Some(issuer_name),
            ..Default::default()
        },
        spec: ClusterIssuerSpec {
            acme: Some(ClusterIssuerAcme {
                email: owner_email,
                server: acme_provider.get_url().to_string(),
                private_key_secret_ref: ClusterIssuerAcmePrivateKeySecretRef {
                    name: acme_account_key_secret_name,
                    key: None,
                },
                solvers: Some(vec![ClusterIssuerAcmeSolvers {
                    selector: None,
                    http01: http_solver,
                    dns01: dns_solver,
                }]),
                ..Default::default()
            }),
            ..Default::default()
        },
        status: None,
    };
    let secret = domain_provider.map(|domain_provider| Secret {
        metadata: ObjectMeta {
            name: Some(credentials_secret_name),
            // For a cluster issuer, the namespace must be cert-manager
            namespace: Some("cert-manager".to_owned()),
            ..Default::default()
        },
        string_data: Some(match domain_provider {
            DNSProvider::Cloudflare(auth) => BTreeMap::from([("api-token".to_owned(), auth)]),
            DNSProvider::NirvatiMe(auth) => {
                let mut split = auth.split(':');
                BTreeMap::from([
                    ("username".to_owned(), split.next().unwrap().to_string()),
                    ("password".to_owned(), split.next().unwrap().to_string()),
                ])
            }
        }),
        type_: Some("Opaque".to_owned()),
        ..Default::default()
    });
    Ok(DnsIssuerK8s { issuer, secret })
}

pub fn http_issuer(user: &str, email: Option<String>, provider: &AcmeProvider) -> ClusterIssuer {
    ClusterIssuer {
        metadata: ObjectMeta {
            name: Some(format!("{}-{}", provider.get_slug(), user)),
            ..Default::default()
        },
        spec: ClusterIssuerSpec {
            acme: Some(ClusterIssuerAcme {
                server: provider.get_url().to_string(),
                email,
                private_key_secret_ref: ClusterIssuerAcmePrivateKeySecretRef {
                    name: format!("{}-{}-acme-key", provider.get_slug(), user),
                    key: Some("tls.key".to_string()),
                },
                solvers: Some(vec![ClusterIssuerAcmeSolvers {
                    http01: Some(ClusterIssuerAcmeSolversHttp01 {
                        ingress: Some(ClusterIssuerAcmeSolversHttp01Ingress {
                            class: Some("traefik".to_string()),
                            ..Default::default()
                        }),
                        ..Default::default()
                    }),
                    ..Default::default()
                }]),
                ..Default::default()
            }),
            ..Default::default()
        },
        status: None,
    }
}

pub async fn create_domain_issuer(
    client: &Client,
    user: String,
    domain: String,
    dns_provider: Option<DNSProvider>,
    email: Option<String>,
    acme_provider: AcmeProvider,
) -> Result<()> {
    let issuer = dns_issuer_config(user, domain, dns_provider, email, acme_provider)?;
    let pp = PostParams::default();
    let api: Api<ClusterIssuer> = Api::all(client.clone());
    api.create(&pp, &issuer.issuer).await?;
    if let Some(secret) = issuer.secret {
        let api: Api<Secret> = Api::namespaced(client.clone(), "cert-manager");
        api.create(&pp, &secret).await?;
    }
    Ok(())
}

pub async fn create_cluster_issuer(
    client: &Client,
    user: String,
    email: Option<String>,
    acme_provider: AcmeProvider,
) -> Result<()> {
    let issuer = http_issuer(&user, email, &acme_provider);
    let pp = PostParams::default();
    let api: Api<ClusterIssuer> = Api::all(client.clone());
    api.create(&pp, &issuer).await?;
    Ok(())
}

pub async fn delete_domain_issuer(
    client: &Client,
    user: String,
    domain: String,
    acme_provider: AcmeProvider,
) -> Result<()> {
    let issuer_name = format!(
        "{}-{}-{}",
        acme_provider.get_slug(),
        user,
        slugify!(&domain)
    );
    let credentials_secret_name = format!(
        "{}-{}-{}-credentials",
        acme_provider.get_slug(),
        user,
        slugify!(&domain)
    );
    let acme_account_key_secret_name = format!(
        "{}-{}-{}-acme-key",
        acme_provider.get_slug(),
        user,
        slugify!(&domain)
    );
    let dp = DeleteParams::default();
    let api: Api<Issuer> = Api::all(client.clone());
    api.delete(&issuer_name, &dp).await?;
    let api: Api<Secret> = Api::all(client.clone());
    api.delete(&credentials_secret_name, &dp).await?;
    // This is not really import, but try to delete it anyway
    let _ = api.delete(&acme_account_key_secret_name, &dp).await;
    Ok(())
}

pub async fn delete_cluster_issuer(
    client: &Client,
    user: String,
    acme_provider: AcmeProvider,
) -> Result<()> {
    let issuer_name = format!("{}-{}", acme_provider.get_slug(), user);
    let acme_account_key_secret_name = format!("{}-{}-acme-key", acme_provider.get_slug(), user);
    let dp = DeleteParams::default();
    let api: Api<Issuer> = Api::all(client.clone());
    api.delete(&issuer_name, &dp).await?;
    let api: Api<Secret> = Api::all(client.clone());
    // This is not really import, but try to delete it anyway
    let _ = api.delete(&acme_account_key_secret_name, &dp).await;
    Ok(())
}
