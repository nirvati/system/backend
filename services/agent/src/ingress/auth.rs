// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::Result;
use bcrypt::BcryptError;
use k8s_crds_traefik::{middlewares as mw, Middleware};
use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::api::core::v1::Secret;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use k8s_openapi::ByteString;
use kube::api::DeleteParams;
use kube::{Api, Client};
use nirvati::kubernetes::apply_with_ns;

fn generate_auth_middleware(
    user_id: &str,
    password: &str,
) -> Result<(mw::Middleware, k8s::Secret), BcryptError> {
    let middleware = mw::Middleware {
        metadata: k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta {
            name: Some(format!("auth-{}", user_id)),
            namespace: Some("nirvati".to_string()),
            ..Default::default()
        },
        spec: mw::MiddlewareSpec {
            basic_auth: Some(mw::MiddlewareBasicAuth {
                secret: Some(format!("auth-{}", user_id)),
                ..Default::default()
            }),
            ..Default::default()
        },
    };
    let secret = k8s::Secret {
        data: Some(
            vec![(
                "users".to_string(),
                ByteString(format!("{}:{}", user_id, bcrypt::hash(password, 14)?).into_bytes()),
            )]
            .into_iter()
            .collect(),
        ),
        metadata: k8s_meta::ObjectMeta {
            name: Some(format!("auth-{}", user_id)),
            namespace: Some("nirvati".to_string()),
            ..Default::default()
        },
        ..Default::default()
    };
    Ok((middleware, secret))
}

pub async fn add_auth_middleware(client: &Client, user_id: &str, password: &str) -> Result<()> {
    let (middleware, secret) = generate_auth_middleware(user_id, password)?;
    let mw = vec![middleware];
    apply_with_ns(client.clone(), &mw, "nirvati").await?;
    let secret = vec![secret];
    apply_with_ns(client.clone(), &secret, "nirvati").await?;
    Ok(())
}

pub async fn delete_auth_middleware(client: &Client, user: &str) -> Result<()> {
    let dp = DeleteParams::default();
    let api: Api<Middleware> = Api::namespaced(client.clone(), "nirvati");
    api.delete(&format!("auth-{}", user), &dp).await?;
    let api: Api<Secret> = Api::namespaced(client.clone(), "nirvati");
    api.delete(&format!("auth-{}", user), &dp).await?;
    Ok(())
}
