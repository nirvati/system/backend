// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

pub mod apps;
pub mod constants;
pub mod ingress;
pub mod plugins;
pub mod system_management;
pub mod utils;

pub mod grpc;

pub fn log(event: nirvati_logs::events::Event, user: &str) {
    nirvati_logs::log::log_event(event, user, "agent");
}
