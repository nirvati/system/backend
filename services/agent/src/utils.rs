// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

pub mod tar;

use anyhow::{anyhow, Result};
use std::{fs, path::Path};

// Check recursively if a dir contains any symlinks
pub fn has_symlinks(dir: &Path) -> bool {
    if !dir.is_dir() {
        return false;
    }
    for entry in fs::read_dir(dir).unwrap() {
        let entry = entry.unwrap();
        let path = entry.path();
        if path.is_dir() {
            has_symlinks(&path);
        } else if path.is_symlink() {
            return true;
        }
    }
    false
}

pub fn serde_yaml_to_json(yaml: serde_yaml::Value) -> Result<serde_json::Value> {
    match yaml {
        serde_yaml::Value::Null => Ok(serde_json::Value::Null),
        serde_yaml::Value::Bool(b) => Ok(serde_json::Value::Bool(b)),
        serde_yaml::Value::Number(n) => {
            if let Some(i) = n.as_i64() {
                Ok(serde_json::Value::Number(serde_json::Number::from(i)))
            } else if let Some(u) = n.as_u64() {
                Ok(serde_json::Value::Number(serde_json::Number::from(u)))
            } else if let Some(f) = n.as_f64() {
                Ok(serde_json::Value::Number(
                    serde_json::Number::from_f64(f).unwrap(),
                ))
            } else {
                Err(anyhow!("Failed to convert number"))
            }
        }
        serde_yaml::Value::String(s) => Ok(serde_json::Value::String(s)),
        serde_yaml::Value::Sequence(s) => {
            let mut out = Vec::new();
            for item in s {
                out.push(serde_yaml_to_json(item)?);
            }
            Ok(serde_json::Value::Array(out))
        }
        serde_yaml::Value::Mapping(m) => {
            let mut out = serde_json::Map::new();
            for (key, value) in m {
                let key = match key {
                    serde_yaml::Value::String(s) => s,
                    serde_yaml::Value::Number(num) => num.to_string(),
                    serde_yaml::Value::Bool(b) => b.to_string(),
                    _ => return Err(anyhow!("Non-string keys not supported")),
                };
                out.insert(key, serde_yaml_to_json(value)?);
            }
            Ok(serde_json::Value::Object(out))
        }
        serde_yaml::Value::Tagged { .. } => Err(anyhow!("Tagged values not supported")),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_si_to_bytes() {
        assert_eq!(si_to_bytes("1").unwrap(), 1);
        assert_eq!(si_to_bytes("1K").unwrap(), 1000);
        assert_eq!(si_to_bytes("1Ki").unwrap(), 1024);
        assert_eq!(si_to_bytes("1M").unwrap(), 1000000);
        assert_eq!(si_to_bytes("1Mi").unwrap(), 1048576);
        assert_eq!(si_to_bytes("1G").unwrap(), 1000000000);
        assert_eq!(si_to_bytes("1Gi").unwrap(), 1073741824);
        assert_eq!(si_to_bytes("1T").unwrap(), 1000000000000);
        assert_eq!(si_to_bytes("1Ti").unwrap(), 1099511627776);
        assert_eq!(si_to_bytes("1P").unwrap(), 1000000000000000);
        assert_eq!(si_to_bytes("1Pi").unwrap(), 1125899906842624);
        assert_eq!(si_to_bytes("1E").unwrap(), 1000000000000000000);
        assert_eq!(si_to_bytes("1Ei").unwrap(), 1152921504606846976);
    }

    #[test]
    fn test_si_to_bytes_invalid() {
        assert!(si_to_bytes("1Z").is_err());
        assert!(si_to_bytes("1Zi").is_err());
        assert!(si_to_bytes("1K2i").is_err());
    }
}

pub fn si_to_bytes(input: &str) -> Result<u64, std::io::Error> {
    let mut num = String::new();
    let mut suffix = String::new();
    for c in input.chars() {
        if c.is_numeric() {
            if !suffix.is_empty() {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::InvalidInput,
                    "Invalid size input",
                ));
            }
            num.push(c);
        } else {
            suffix.push(c);
        }
    }
    let num = num.parse::<u64>().unwrap();
    Ok(match suffix.as_str() {
        "" => num,
        "Ki" => num * 1024,
        "Mi" => num * 1024 * 1024,
        "Gi" => num * 1024 * 1024 * 1024,
        "Ti" => num * 1024 * 1024 * 1024 * 1024,
        "Pi" => num * 1024 * 1024 * 1024 * 1024 * 1024,
        "Ei" => num * 1024 * 1024 * 1024 * 1024 * 1024 * 1024,
        "K" => num * 1000,
        "M" => num * 1000 * 1000,
        "G" => num * 1000 * 1000 * 1000,
        "T" => num * 1000 * 1000 * 1000 * 1000,
        "P" => num * 1000 * 1000 * 1000 * 1000 * 1000,
        "E" => num * 1000 * 1000 * 1000 * 1000 * 1000 * 1000,
        _ => {
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidInput,
                "Invalid size input",
            ))
        }
    })
}
