// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use k8s_crds_traefik::{Middleware, MiddlewareSpec, MiddlewareStripPrefix};
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use slugify::slugify;
use std::collections::BTreeMap;

pub fn strip_prefix_mw(prefix: &str) -> Middleware {
    strip_prefix_mw_with_name(prefix, format!("strip-{}-prefix", slugify!(prefix)))
}

pub fn strip_prefix_mw_with_name(prefix: &str, name: String) -> Middleware {
    Middleware {
        metadata: k8s_meta::ObjectMeta {
            name: Some(name),
            ..Default::default()
        },
        spec: MiddlewareSpec {
            strip_prefix: Some(MiddlewareStripPrefix {
                prefixes: Some(vec![prefix.to_string()]),
                force_slash: None,
            }),
            ..Default::default()
        },
    }
}

pub fn generate_middlewares(
    deployment_name: &str,
    middlewares: BTreeMap<String, MiddlewareSpec>,
) -> Vec<Middleware> {
    middlewares
        .into_iter()
        .map(|(mw_id, mw)| Middleware {
            metadata: k8s_meta::ObjectMeta {
                name: Some(format!("{}-{}", deployment_name, mw_id)),
                ..Default::default()
            },
            spec: mw,
        })
        .collect()
}
