use crate::apps::generator::internal::ResolverCtx;
use anyhow::Result;
use k8s_crds_cilium::*;
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use nirvati_apps::internal::{DnsConfig, NetworkPolicy};
use std::collections::BTreeMap;

pub async fn get_network_policy(
    policy_id: String,
    component_config: &NetworkPolicy,
    selector: CiliumNetworkPolicyEndpointSelector,
    resolver_ctx: &ResolverCtx,
) -> Result<CiliumNetworkPolicy> {
    let mut ingress = vec![
        CiliumNetworkPolicyIngress {
            from_endpoints: Some(vec![
                CiliumNetworkPolicyIngressFromEndpoints {
                    match_labels: Some(BTreeMap::from([(
                        "io.kubernetes.pod.namespace".to_string(),
                        "kube-system".to_string(),
                    )])),
                    ..Default::default()
                },
                CiliumNetworkPolicyIngressFromEndpoints {
                    match_labels: Some(BTreeMap::from([(
                        "io.kubernetes.pod.namespace".to_string(),
                        "nirvati".to_string(),
                    )])),
                    ..Default::default()
                },
            ]),
            ..Default::default()
        },
        CiliumNetworkPolicyIngress {
            from_entities: Some(vec!["kube-apiserver".to_string(), "health".to_string()]),
            ..Default::default()
        },
    ];

    if component_config.ingress.allow_same_ns.allow {
        ingress.push(CiliumNetworkPolicyIngress {
            from_endpoints: Some(vec![CiliumNetworkPolicyIngressFromEndpoints {
                ..Default::default()
            }]),
            to_ports: component_config
                .ingress
                .allow_same_ns
                .allowed_ports
                .as_ref()
                .map(|ports| {
                    vec![CiliumNetworkPolicyIngressToPorts {
                        ports: Some(ports.iter().map(|port| port.into()).collect()),
                        ..Default::default()
                    }]
                }),
            ..Default::default()
        });
    }
    if component_config.ingress.allow_cluster.allow {
        ingress.push(CiliumNetworkPolicyIngress {
            from_entities: Some(vec!["cluster".to_string()]),
            to_ports: component_config
                .ingress
                .allow_cluster
                .allowed_ports
                .as_ref()
                .map(|ports| {
                    vec![CiliumNetworkPolicyIngressToPorts {
                        ports: Some(ports.iter().map(|port| port.into()).collect()),
                        ..Default::default()
                    }]
                }),
            ..Default::default()
        });
    }
    if component_config.ingress.allow_world.allow {
        ingress.push(CiliumNetworkPolicyIngress {
            from_entities: Some(vec!["world".to_string()]),
            to_ports: component_config
                .ingress
                .allow_world
                .allowed_ports
                .as_ref()
                .map(|ports| {
                    vec![CiliumNetworkPolicyIngressToPorts {
                        ports: Some(ports.iter().map(|port| port.into()).collect()),
                        ..Default::default()
                    }]
                }),
            ..Default::default()
        });
    }

    for source_pod in component_config.ingress.pods.iter() {
        let mut match_labels = source_pod.match_labels.clone().unwrap_or_default();
        let mut match_labels = BTreeMap::from_iter(match_labels.drain());

        if !source_pod.all_namespaces {
            match_labels.insert(
                "io.kubernetes.pod.namespace".to_string(),
                if let Some(ns) = &source_pod.namespace {
                    ns.clone()
                } else if let Some(app) = &source_pod.app {
                    resolver_ctx.get_app_ns(app).await?
                } else {
                    "{{ .Release.Namespace }}".to_string()
                },
            );
        }

        ingress.push(CiliumNetworkPolicyIngress {
            from_endpoints: Some(vec![CiliumNetworkPolicyIngressFromEndpoints {
                match_labels: Some(match_labels),
                ..Default::default()
            }]),
            to_ports: source_pod.allowed_ports.as_ref().map(|ports| {
                vec![CiliumNetworkPolicyIngressToPorts {
                    ports: Some(ports.iter().map(|port| port.into()).collect()),
                    ..Default::default()
                }]
            }),
            ..Default::default()
        });
    }

    for cidr in component_config.ingress.cidrs.iter() {
        ingress.push(CiliumNetworkPolicyIngress {
            from_cidr_set: Some(vec![CiliumNetworkPolicyIngressFromCidrSet {
                cidr: Some(cidr.cidr.clone()),
                except: cidr.except.clone(),
                ..Default::default()
            }]),
            ..Default::default()
        });
    }

    let mut egress = vec![];

    if component_config.egress.allow_same_ns {
        egress.push(CiliumNetworkPolicyEgress {
            to_endpoints: Some(vec![CiliumNetworkPolicyEgressToEndpoints {
                ..Default::default()
            }]),
            ..Default::default()
        });
    };

    if component_config.egress.allow_cluster {
        egress.push(CiliumNetworkPolicyEgress {
            to_entities: Some(vec!["cluster".to_string()]),
            ..Default::default()
        });
    }

    if component_config.egress.allow_kube_api {
        egress.push(CiliumNetworkPolicyEgress {
            to_entities: Some(vec!["kube-apiserver".to_string()]),
            ..Default::default()
        });
    }

    let dns = component_config.egress.dns.clone().unwrap_or(DnsConfig {
        allow: true,
        allowed_lookup_names: None,
        allowed_lookup_pattern: None,
    });

    if dns.allow {
        let mut rules = vec![];
        if let Some(ref allowed_names) = dns.allowed_lookup_names {
            for name in allowed_names {
                rules.push(CiliumNetworkPolicyEgressToPortsRulesDns {
                    match_name: Some(name.clone()),
                    ..Default::default()
                });
            }
        }
        if let Some(ref allowed_match) = dns.allowed_lookup_pattern {
            for name in allowed_match {
                rules.push(CiliumNetworkPolicyEgressToPortsRulesDns {
                    match_pattern: Some(name.clone()),
                    ..Default::default()
                });
            }
        }
        if dns.allowed_lookup_names.is_none() && dns.allowed_lookup_pattern.is_none() {
            rules.push(CiliumNetworkPolicyEgressToPortsRulesDns {
                match_pattern: Some("*".to_string()),
                ..Default::default()
            });
        }
        egress.push(CiliumNetworkPolicyEgress {
            to_endpoints: Some(vec![CiliumNetworkPolicyEgressToEndpoints {
                match_labels: Some(BTreeMap::from([
                    (
                        "io.kubernetes.pod.namespace".to_string(),
                        "kube-system".to_string(),
                    ),
                    ("k8s-app".to_string(), "kube-dns".to_string()),
                ])),
                ..Default::default()
            }]),
            to_ports: Some(vec![CiliumNetworkPolicyEgressToPorts {
                ports: Some(vec![CiliumNetworkPolicyEgressToPortsPorts {
                    port: "53".to_string(),
                    protocol: Some(CiliumNetworkPolicyEgressToPortsPortsProtocol::Udp),
                    end_port: None,
                }]),
                rules: if std::env::var("INSECURE_FORCE_DISABLE_DNS_CHECKS").is_err() {
                    Some(CiliumNetworkPolicyEgressToPortsRules {
                        dns: Some(rules),
                        ..Default::default()
                    })
                } else {
                    None
                },
                ..Default::default()
            }]),
            ..Default::default()
        });
    }

    if component_config.egress.allow_world {
        egress.push(CiliumNetworkPolicyEgress {
            to_cidr_set: Some(vec![
                CiliumNetworkPolicyEgressToCidrSet {
                    cidr: Some("0.0.0.0/0".to_string()),
                    except: Some(vec![
                        "10.0.0.0/8".to_string(),     // Private range
                        "172.16.0.0/12".to_string(),  // Private range
                        "192.168.0.0/16".to_string(), // Private range
                        "100.64.0.0/10".to_string(),  // Carrier-grade NAT
                        //- "127.0.0.0/8"   // Loopback
                        "169.254.0.0/16".to_string(),     // Link-local
                        "255.255.255.255/32".to_string(), // Broadcast
                    ]),
                    ..Default::default()
                },
                CiliumNetworkPolicyEgressToCidrSet {
                    cidr: Some("2000::/3".to_string()),
                    ..Default::default()
                },
            ]),
            ..Default::default()
        });
    }

    if component_config.egress.allow_local_net {
        egress.push(CiliumNetworkPolicyEgress {
            to_cidr_set: Some(vec![
                CiliumNetworkPolicyEgressToCidrSet {
                    cidr: Some("10.0.0.0/8".to_string()),
                    ..Default::default()
                },
                CiliumNetworkPolicyEgressToCidrSet {
                    cidr: Some("192.168.0.0/16".to_string()),
                    ..Default::default()
                },
                CiliumNetworkPolicyEgressToCidrSet {
                    cidr: Some("100.64.0.0/10".to_string()),
                    ..Default::default()
                },
                CiliumNetworkPolicyEgressToCidrSet {
                    cidr: Some("fc00::/7".to_string()),
                    ..Default::default()
                },
                CiliumNetworkPolicyEgressToCidrSet {
                    cidr: Some("fe80::/10".to_string()),
                    ..Default::default()
                },
            ]),
            ..Default::default()
        });
    }

    for cidr in &component_config.egress.cidrs {
        egress.push(CiliumNetworkPolicyEgress {
            to_cidr_set: Some(vec![CiliumNetworkPolicyEgressToCidrSet {
                cidr: Some(cidr.cidr.clone()),
                except: cidr.except.clone(),
                ..Default::default()
            }]),
            to_ports: cidr.allowed_ports.as_ref().map(|ports| {
                vec![CiliumNetworkPolicyEgressToPorts {
                    listener: None,
                    originating_tls: None,
                    ports: Some(ports.iter().map(|port| port.into()).collect::<Vec<_>>()),
                    rules: None,
                    server_names: None,
                    terminating_tls: None,
                }]
            }),
            ..Default::default()
        });
    }

    for svc in &component_config.egress.services {
        egress.push(CiliumNetworkPolicyEgress {
            to_services: Some(vec![CiliumNetworkPolicyEgressToServices {
                k8s_service: Some(CiliumNetworkPolicyEgressToServicesK8sService {
                    namespace: svc.namespace.clone(),
                    service_name: Some(svc.service_name.clone()),
                }),
                k8s_service_selector: None,
            }]),
            to_ports: svc.allowed_ports.as_ref().map(|ports| {
                vec![CiliumNetworkPolicyEgressToPorts {
                    listener: None,
                    originating_tls: None,
                    ports: Some(ports.iter().map(|port| port.into()).collect::<Vec<_>>()),
                    rules: None,
                    server_names: None,
                    terminating_tls: None,
                }]
            }),
            ..Default::default()
        });
    }

    for fqdn in component_config.egress.fqdns.iter() {
        egress.push(CiliumNetworkPolicyEgress {
            to_fqd_ns: Some(vec![CiliumNetworkPolicyEgressToFqdNs {
                match_pattern: fqdn.match_pattern.clone(),
                match_name: fqdn.match_name.clone(),
            }]),
            to_ports: Some(vec![CiliumNetworkPolicyEgressToPorts {
                ports: fqdn
                    .allowed_ports
                    .as_ref()
                    .map(|ports| ports.iter().map(|port| port.into()).collect()),
                ..Default::default()
            }]),
            ..Default::default()
        })
    }

    for target_pod in component_config.egress.pods.iter() {
        let mut match_labels = target_pod.match_labels.clone().unwrap_or_default();
        let mut match_labels = BTreeMap::from_iter(match_labels.drain());

        if !target_pod.all_namespaces {
            match_labels.insert(
                "io.kubernetes.pod.namespace".to_string(),
                if let Some(ns) = &target_pod.namespace {
                    ns.clone()
                } else if let Some(app) = &target_pod.app {
                    resolver_ctx.get_app_ns(app).await?
                } else {
                    "{{ .Release.Namespace }}".to_string()
                },
            );
        }

        egress.push(CiliumNetworkPolicyEgress {
            to_endpoints: Some(vec![CiliumNetworkPolicyEgressToEndpoints {
                match_labels: Some(match_labels),
                ..Default::default()
            }]),
            to_ports: target_pod.allowed_ports.as_ref().map(|ports| {
                vec![CiliumNetworkPolicyEgressToPorts {
                    ports: Some(ports.iter().map(|port| port.into()).collect()),
                    ..Default::default()
                }]
            }),
            ..Default::default()
        });
    }

    Ok(CiliumNetworkPolicy {
        metadata: ObjectMeta {
            name: Some(policy_id),
            ..Default::default()
        },
        spec: CiliumNetworkPolicySpec {
            endpoint_selector: Some(selector),
            egress: Some(egress),
            ingress: Some(ingress),
            ..Default::default()
        },
        status: None,
    })
}

pub fn generate_network_policy_allow_set(
    from_ns: &str,
    target_ns: &str,
    target_svc: &str,
    target_container: &str,
    allowed_ports: Vec<CiliumNetworkPolicyIngressToPortsPorts>,
) -> CiliumNetworkPolicy {
    CiliumNetworkPolicy {
        metadata: ObjectMeta {
            name: Some(format!("{}-{}", from_ns, target_svc)),
            namespace: Some(target_ns.to_owned()),
            ..Default::default()
        },
        spec: CiliumNetworkPolicySpec {
            endpoint_selector: Some(CiliumNetworkPolicyEndpointSelector {
                match_labels: Some(BTreeMap::from([(
                    "container".to_string(),
                    target_container.to_string(),
                )])),
                match_expressions: None,
            }),
            ingress: Some(vec![CiliumNetworkPolicyIngress {
                from_endpoints: Some(vec![CiliumNetworkPolicyIngressFromEndpoints {
                    match_labels: Some(BTreeMap::from([(
                        "io.kubernetes.pod.namespace".to_string(),
                        from_ns.to_string(),
                    )])),
                    ..Default::default()
                }]),
                to_ports: Some(vec![CiliumNetworkPolicyIngressToPorts {
                    ports: Some(allowed_ports),
                    ..Default::default()
                }]),
                ..Default::default()
            }]),
            ..Default::default()
        },
        status: None,
    }
}
