// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use k8s_openapi::ByteString;
use nirvati_apps::internal::Secret;

pub fn generate_secret(secret: &Secret) -> k8s::Secret {
    k8s::Secret {
        metadata: k8s_meta::ObjectMeta {
            name: Some(secret.name.clone()),
            ..Default::default()
        },
        data: Some(
            secret
                .data
                .iter()
                .map(|(k, v)| (k.clone(), ByteString(v.clone().into())))
                .collect(),
        ),
        ..Default::default()
    }
}

pub fn generate_secrets(input: &[Secret]) -> Vec<k8s::Secret> {
    input.iter().map(generate_secret).collect()
}
