// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::apps::generator::internal::ResolverCtx;
use crate::apps::generator::kubernetes::pod::generate_pod;
use k8s_openapi::api::batch::v1 as k8s_batch;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use nirvati_apps::internal::{Container, Job, Runnable};
use slugify::slugify;

pub async fn generate_job_onetime(
    service_name: String,
    service: Container,
    resolver: &ResolverCtx,
) -> anyhow::Result<k8s_batch::Job> {
    Ok(k8s_batch::Job {
        spec: Some(k8s_batch::JobSpec {
            template: generate_pod(
                service_name.clone(),
                Runnable::Once(Box::new(Job { container: service })),
                resolver,
            )
            .await?,
            ..Default::default()
        }),
        metadata: k8s_meta::ObjectMeta {
            name: Some(service_name),
            ..Default::default()
        },
        ..Default::default()
    })
}

pub async fn generate_job_on_update(
    app_version: &str,
    service_name: String,
    service: Container,
    resolver: &ResolverCtx,
) -> anyhow::Result<k8s_batch::Job> {
    Ok(k8s_batch::Job {
        spec: Some(k8s_batch::JobSpec {
            template: generate_pod(
                service_name.clone(),
                Runnable::OnAppUpdate(Box::new(Job { container: service })),
                resolver,
            )
            .await?,
            ..Default::default()
        }),
        metadata: k8s_meta::ObjectMeta {
            name: Some(format!("{}-{}", service_name, slugify!(app_version))),
            ..Default::default()
        },
        ..Default::default()
    })
}
