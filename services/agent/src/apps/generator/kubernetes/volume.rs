// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::apps::generator::internal::ResolverCtx;
use anyhow::Result;
use nirvati_apps::internal::{LonghornVolume, Volume};
use slugify::slugify;

async fn get_claim(vol: &LonghornVolume, resolver: &ResolverCtx) -> Result<String> {
    let mut prefix = None;
    if let Some(ref app_name) = vol.from_app {
        prefix = Some(resolver.get_app_ns(app_name).await?);
    } else if let Some(ref ns) = vol.from_ns {
        prefix = Some(ns.clone());
    }
    Ok(if let Some(prefix) = prefix {
        format!("mirrored-{}-{}", prefix, slugify!(&vol.name))
    } else {
        slugify!(&vol.name)
    })
}
pub async fn generate_volume(
    vol: Volume,
    resolver: &ResolverCtx,
) -> Result<k8s_openapi::api::core::v1::Volume> {
    Ok(k8s_openapi::api::core::v1::Volume {
        name: vol.get_slug(),
        persistent_volume_claim: if let Volume::Longhorn(ref vol) = vol {
            Some(
                k8s_openapi::api::core::v1::PersistentVolumeClaimVolumeSource {
                    claim_name: get_claim(vol, resolver).await?,
                    read_only: Some(vol.is_readonly),
                },
            )
        } else {
            None
        },
        host_path: if let Volume::Host(ref vol) = vol {
            Some(k8s_openapi::api::core::v1::HostPathVolumeSource {
                path: vol.host_path.clone(),
                ..Default::default()
            })
        } else {
            None
        },
        secret: if let Volume::Secret(ref vol) = vol {
            Some(k8s_openapi::api::core::v1::SecretVolumeSource {
                secret_name: Some(vol.name.clone()),
                ..Default::default()
            })
        } else {
            None
        },
        ..Default::default()
    })
}

pub async fn generate_volumes(
    mut volumes: Vec<Volume>,
    resolver_ctx: &ResolverCtx,
) -> Result<Vec<k8s_openapi::api::core::v1::Volume>> {
    volumes.sort_by_key(|a| a.get_slug());
    volumes.dedup_by(|a, b| a.get_slug() == b.get_slug());
    let futures = volumes
        .into_iter()
        .map(|vol| generate_volume(vol, resolver_ctx));
    futures::future::join_all(futures)
        .await
        .into_iter()
        .collect()
}
