// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::BTreeMap;

use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::apimachinery::pkg::api::resource::Quantity;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;

use nirvati_apps::metadata::Volume;

pub fn generate_pvc(
    volume_name: &str,
    volume: &Volume,
    has_longhorn: bool,
) -> k8s::PersistentVolumeClaim {
    k8s::PersistentVolumeClaim {
        metadata: k8s_meta::ObjectMeta {
            name: Some(volume_name.to_string()),
            labels: Some(BTreeMap::from([(
                "generator".to_string(),
                "nirvati".to_string(),
            )])),
            ..Default::default()
        },
        spec: Some(k8s::PersistentVolumeClaimSpec {
            access_modes: Some(vec!["ReadWriteMany".to_string()]),
            storage_class_name: Some(std::env::var("STORAGE_CLASS").unwrap_or_else(|_| {
                if has_longhorn {
                    "longhorn".to_string()
                } else {
                    "local-path".to_string()
                }
            })),
            resources: Some(k8s::VolumeResourceRequirements {
                requests: Some(BTreeMap::from([(
                    "storage".to_string(),
                    Quantity(volume.recommended_size.clone().to_string()),
                )])),
                ..Default::default()
            }),
            ..Default::default()
        }),
        status: None,
    }
}
