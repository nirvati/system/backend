// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use itertools::Itertools;
use std::collections::BTreeMap;

use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use k8s_openapi::apimachinery::pkg::util::intstr::IntOrString;
use nirvati_apps::internal::{Service, ServiceType};

pub fn internal_svc_to_k8s_service(svc_id: &String, service: &Service) -> Vec<k8s::Service> {
    let mut services = vec![];
    let spec = Some(k8s::ServiceSpec {
        selector: Some(BTreeMap::from([
            ("container".to_owned(), service.target_container.clone()),
            ("is-container".to_owned(), "true".to_owned()),
        ])),
        type_: Some(match service.r#type {
            ServiceType::LoadBalancer => "LoadBalancer".to_string(),
            ServiceType::ClusterIp => "ClusterIP".to_string(),
        }),
        cluster_ip: match service.r#type {
            ServiceType::ClusterIp => service.cluster_ip.clone(),
            _ => None,
        },
        ports: Some(
            service
                .ports
                .tcp
                .iter()
                .sorted()
                .map(|(public_port, internal_port)| k8s::ServicePort {
                    port: *public_port as i32,
                    target_port: Some(IntOrString::Int(*internal_port as i32)),
                    name: Some(format!("{}-{}-tcp", svc_id, public_port)),
                    ..Default::default()
                })
                .chain(
                    service
                        .ports
                        .udp
                        .iter()
                        .sorted()
                        .map(|(public_port, internal_port)| k8s::ServicePort {
                            port: *public_port as i32,
                            target_port: Some(IntOrString::Int(*internal_port as i32)),
                            name: Some(format!("{}-{}-udp", svc_id, public_port)),
                            protocol: Some("UDP".to_string()),
                            ..Default::default()
                        }),
                )
                .collect(),
        ),
        ..Default::default()
    });
    // We always generate a Tailscale service to ensure it can be turned on later
    if service.r#type == ServiceType::LoadBalancer {
        services.push(k8s::Service {
            metadata: k8s_meta::ObjectMeta {
                name: Some(format!("nirvati-internal-{}-tailscale", svc_id)),
                annotations: Some(BTreeMap::from([
                    (
                        "metallb.universe.tf/allow-shared-ip".to_owned(),
                        "ip_set_tailscale".to_owned(),
                    ),
                    (
                        "metallb.universe.tf/address-pool".to_owned(),
                        "tailscale".to_owned(),
                    ),
                ])),
                ..Default::default()
            },
            spec: spec.clone(),
            status: None,
        })
    }

    services.push(k8s::Service {
        metadata: k8s_meta::ObjectMeta {
            name: Some(svc_id.clone()),
            annotations: if service.r#type == ServiceType::LoadBalancer {
                Some(BTreeMap::from([
                    (
                        "metallb.universe.tf/allow-shared-ip".to_owned(),
                        "ip_set_main".to_owned(),
                    ),
                    (
                        "metallb.universe.tf/address-pool".to_owned(),
                        "default".to_owned(),
                    ),
                ]))
            } else {
                None
            },
            labels: Some(BTreeMap::from([(
                "container".to_owned(),
                service.target_container.clone(),
            )])),
            ..Default::default()
        },
        spec,
        status: None,
    });
    services
}
