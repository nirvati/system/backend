// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::BTreeMap;

use crate::apps::generator::internal::ResolverCtx;
use crate::apps::generator::kubernetes::pod::generate_pod;
use k8s_openapi::api::apps::v1 as k8s_apps;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use nirvati_apps::internal::{Deployment, Runnable};

pub async fn generate_deployment(
    service_name: String,
    service: Deployment,
    resolver: &ResolverCtx,
) -> anyhow::Result<k8s_apps::Deployment> {
    Ok(k8s_apps::Deployment {
        spec: Some(k8s_apps::DeploymentSpec {
            selector: k8s_meta::LabelSelector {
                match_labels: Some(BTreeMap::from([(
                    "container".to_owned(),
                    service_name.clone(),
                )])),
                ..Default::default()
            },
            template: generate_pod(
                service_name.clone(),
                Runnable::Deployment(Box::from(service)),
                resolver,
            )
            .await?,
            ..Default::default()
        }),
        metadata: k8s_meta::ObjectMeta {
            name: Some(service_name),
            ..Default::default()
        },
        ..Default::default()
    })
}
