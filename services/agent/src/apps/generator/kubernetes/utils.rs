// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::{bail, Result};

pub fn compose_restart_policy_to_kubernetes(policy: &str) -> Result<String> {
    match policy.to_lowercase().as_str() {
        "no" | "never" => Ok("Never".to_string()),
        "always" => Ok("Always".to_string()),
        "on-failure" | "onfailure" => Ok("OnFailure".to_string()),
        _ => bail!("Unknown restart policy: {}", policy),
    }
}
