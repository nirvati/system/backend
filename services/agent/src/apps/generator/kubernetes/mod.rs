// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::apps::generator::internal::{get_svc_access_perms, ResolverCtx};
use crate::apps::generator::kubernetes::deployment::generate_deployment;
use crate::apps::generator::kubernetes::middleware::generate_middlewares;
use crate::apps::generator::kubernetes::networkpolicy::get_network_policy;
use crate::apps::generator::kubernetes::pvc::generate_pvc;
use crate::apps::generator::kubernetes::secret::generate_secrets;
use crate::apps::generator::kubernetes::services::internal_svc_to_k8s_service;
use crate::apps::kubernetes::pvcs::create_pvc_clone;
use crate::plugins::api::UserState;
use crate::plugins::crd::{ClusterPlugin, ClusterPluginSpec, Plugin, PluginSpec};
use anyhow::{bail, Result};
use k8s_crds_cilium::{
    CiliumNetworkPolicy, CiliumNetworkPolicyEndpointSelector,
    CiliumNetworkPolicyIngressToPortsPorts, CiliumNetworkPolicyIngressToPortsPortsProtocol,
};
use k8s_openapi::api::apps::v1 as k8s_apps;
use k8s_openapi::api::batch::v1 as k8s_batch;
use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::api::core::v1::{PersistentVolume, PersistentVolumeClaim};
use k8s_openapi::api::rbac::v1 as k8s_rbac;
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use kube::{Api, Client};
use nirvati_apps::internal::{
    port_config_for_service_egress_only, DnsConfig, InternalAppRepresentation, NetworkPolicy,
    NetworkPolicyEgress, PodConfig, Runnable, ServiceConfig, Volume,
};
use nirvati_apps::metadata::AppScope;
use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap, HashMap};
use itertools::Itertools;

mod cronjob;
mod deployment;
pub mod ingress;
pub(crate) mod job;
pub(crate) mod middleware;
pub(crate) mod networkpolicy;
mod pod;
mod pvc;
mod secret;
mod service_account;
mod services;
pub mod tcpingress;
mod utils;
mod volume;

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
pub struct KubernetesConfig {
    pub deployments: Vec<k8s_apps::Deployment>,
    pub services: Vec<k8s::Service>,
    pub cronjobs: Vec<k8s_batch::CronJob>,
    pub jobs: Vec<k8s_batch::Job>,
    pub pvcs: Vec<k8s::PersistentVolumeClaim>,
    pub pvs: Vec<k8s::PersistentVolume>,
    pub roles: Vec<k8s_rbac::Role>,
    pub cluster_roles: Vec<k8s_rbac::ClusterRole>,
    pub cluster_role_bindings: Vec<k8s_rbac::ClusterRoleBinding>,
    pub role_bindings: Vec<k8s_rbac::RoleBinding>,
    pub service_accounts: Vec<k8s::ServiceAccount>,
    pub middlewares: Vec<k8s_crds_traefik::middlewares::Middleware>,
    pub network_policies: Vec<CiliumNetworkPolicy>,
    pub secrets: Vec<k8s::Secret>,
    pub plugins: Vec<Plugin>,
    pub cluster_plugins: Vec<ClusterPlugin>,
    pub others: Vec<serde_yaml::Value>,
}

pub async fn generate_kubernetes_config(
    #[cfg(not(feature = "test-parser"))] kube_client: Client,
    mut app: InternalAppRepresentation,
    user_state: &UserState,
    permission_resolver: &ResolverCtx,
) -> Result<KubernetesConfig> {
    #[cfg(feature = "test-parser")]
    let has_longhorn = true;
    #[cfg(not(feature = "test-parser"))]
    let has_longhorn = {
        let api: Api<k8s::Namespace> = Api::all(kube_client.clone());
        let longhorn_ns = api.get_opt("longhorn-system").await?;
        longhorn_ns.is_some()
    };

    let mut deployments = Vec::new();
    let mut jobs = Vec::new();
    let mut cronjobs = Vec::new();
    let mut middlewares = Vec::new();
    let mut plugins = Vec::new();
    let mut cluster_plugins = Vec::new();
    let mut pvs: HashMap<String, PersistentVolume> = HashMap::new();
    let mut cloned_pvcs: HashMap<String, PersistentVolumeClaim> = HashMap::new();
    let mut pvcs: Vec<PersistentVolumeClaim> = app
        .metadata
        .volumes
        .iter()
        .map(|(vol_name, vol)| {
            if vol_name.starts_with("mirrored-") {
                bail!("Volume names starting with 'mirrored-' are reserved for internal use");
            }
            Ok(generate_pvc(vol_name, vol, has_longhorn))
        })
        .collect::<Result<_>>()?;
    for (plugin_id, plugin) in app.plugins.iter() {
        if app.metadata.scope == AppScope::System {
            cluster_plugins.push(ClusterPlugin {
                metadata: ObjectMeta {
                    name: Some(format!(
                        "{}-plugin-{}",
                        app.metadata
                            .implements
                            .clone()
                            .unwrap_or_else(|| app.metadata.id.clone()),
                        plugin_id
                    )),
                    ..Default::default()
                },
                spec: ClusterPluginSpec {
                    r#type: plugin.r#type.clone().into(),
                    endpoint: plugin.endpoint.clone(),
                },
            });
        } else {
            plugins.push(Plugin {
                metadata: ObjectMeta {
                    name: Some(format!(
                        "{}-plugin-{}",
                        app.metadata
                            .implements
                            .clone()
                            .unwrap_or_else(|| app.metadata.id.clone()),
                        plugin_id
                    )),
                    namespace: Some(user_state.user_id.clone()),
                    ..Default::default()
                },
                spec: PluginSpec {
                    r#type: plugin.r#type.clone().into(),
                    endpoint: plugin.endpoint.clone(),
                },
            });
        }
    }

    let mut network_policies = vec![];
    let services = app
        .services
        .iter()
        .flat_map(|(svc_id, svc)| internal_svc_to_k8s_service(svc_id, svc))
        .collect();
    for (service_name, service) in app.services.iter() {
        if let Some(ref network_config) = service.network_policy {
            network_policies.push(
                get_network_policy(
                    format!("service-{}", service_name),
                    &NetworkPolicy {
                        ingress: network_config.clone(),
                        // Network traffic is denied by default, so this just means not to create an allow rule
                        // Existing rules won't be invalidated
                        egress: NetworkPolicyEgress {
                            allow_cluster: false,
                            allow_same_ns: false,
                            allow_kube_api: false,
                            allow_world: false,
                            allow_local_net: false,
                            dns: Some(DnsConfig {
                                allow: false,
                                allowed_lookup_names: None,
                                allowed_lookup_pattern: None,
                            }),
                            fqdns: vec![],
                            cidrs: vec![],
                            services: vec![],
                            pods: vec![],
                        },
                    },
                    CiliumNetworkPolicyEndpointSelector {
                        match_expressions: None,
                        match_labels: Some(BTreeMap::from([(
                            "container".to_owned(),
                            service.target_container.clone(),
                        )])),
                    },
                    permission_resolver,
                )
                .await?,
            );
        }
    }
    let mut service_accounts = Vec::new();
    let mut roles = Vec::new();
    let mut cluster_roles = Vec::new();
    let mut cluster_role_bindings = Vec::new();
    let mut role_bindings = Vec::new();
    let app_id = app.metadata.id.clone();
    let (mut others, _) = crate::plugins::convert_crs(
        #[cfg(not(feature = "test-parser"))]
        kube_client.clone(),
        &app_id,
        &app.custom_resources,
        user_state.clone(),
        app.metadata.scope == AppScope::System,
    )
    .await?;
    others.append(&mut app.other);
    for (name, runnable) in app.containers.clone() {
        if let Some(mut service_account) = service_account::generate_service_account(
            &app_id,
            if app.metadata.scope == AppScope::System {
                None
            } else {
                Some(&user_state.user_id)
            },
            name.clone(),
            runnable.get_container(),
        ) {
            service_accounts.push(service_account.account);
            roles.append(&mut service_account.roles);
            if let Some(cluster_role) = service_account.cluster_role {
                cluster_roles.push(cluster_role);
            }
            role_bindings.append(&mut service_account.role_bindings);
            cluster_role_bindings.append(&mut service_account.cluster_role_bindings);
        }

        if let Some(ref network_config) = runnable.get_container().network_policy {
            let svc_access_perms = get_svc_access_perms(
                &runnable.get_container().has_permissions,
                permission_resolver,
            )
            .await?;
            let mut policy = network_config.clone();
            for (app, perms) in svc_access_perms.iter() {
                let ns = Some(permission_resolver.get_app_ns(app).await?);
                for (svc, (target_pod, ports)) in perms.ports_by_svc.iter() {
                    policy.egress.services.push(ServiceConfig {
                        namespace: ns.clone(),
                        service_name: svc.clone(),
                        allowed_ports: Some(port_config_for_service_egress_only(ports.clone())),
                    });
                    policy.egress.pods.push(PodConfig {
                        namespace: ns.clone(),
                        app: None,
                        allowed_ports: Some(port_config_for_service_egress_only(ports.clone())),
                        match_labels: Some(HashMap::from([(
                            "container".to_string(),
                            target_pod.clone(),
                        )])),
                        all_namespaces: false,
                    });
                }
            }
            network_policies.push(
                get_network_policy(
                    format!("container-{}", name),
                    &policy,
                    CiliumNetworkPolicyEndpointSelector {
                        match_expressions: None,
                        match_labels: Some(BTreeMap::from([(
                            "container".to_string(),
                            name.clone(),
                        )])),
                    },
                    permission_resolver,
                )
                .await?,
            );
        }

        for vol in runnable.get_container().volumes.iter() {
            match vol {
                Volume::Longhorn(longhorn_vol) => {
                    #[cfg(not(feature = "test-parser"))]
                    {
                        if let Some(source_ns) = longhorn_vol
                            .from_app
                            .clone()
                            .map(|app| app)
                            .or_else(|| longhorn_vol.from_ns.clone())
                        {
                            let self_ns = if app.metadata.scope == AppScope::System {
                                app_id.clone()
                            } else {
                                format!("{}-{}", user_state.user_id, app_id.clone())
                            };
                            let slug = vol.get_slug();
                            if cloned_pvcs.contains_key(&slug) {
                                continue;
                            }
                            if let Ok((pvc, pv)) = create_pvc_clone(
                                kube_client.clone(),
                                &source_ns,
                                &vol.get_target_slug(),
                                &self_ns,
                                &slug,
                            )
                            .await
                            {
                                cloned_pvcs.insert(slug.clone(), pvc);
                                pvs.insert(slug, pv);
                            }
                        }
                    }
                }
                Volume::Secret(_) | Volume::Host(_) => {}
            }
        }

        match runnable {
            Runnable::Deployment(deployment) => {
                middlewares.append(&mut generate_middlewares(
                    &name,
                    deployment.middlewares.clone(),
                ));
                deployments.push(
                    generate_deployment(name.clone(), *deployment, permission_resolver).await?,
                );
            }
            Runnable::Once(job) => {
                jobs.push(
                    job::generate_job_onetime(
                        name.clone(),
                        job.container.clone(),
                        permission_resolver,
                    )
                    .await?,
                );
            }
            Runnable::OnAppUpdate(job) => {
                jobs.push(
                    job::generate_job_on_update(
                        &app.metadata.version.to_string(),
                        name.clone(),
                        job.container.clone(),
                        permission_resolver,
                    )
                    .await?,
                );
            }
            Runnable::Cron(cronjob) => {
                cronjobs.push(
                    cronjob::generate_cronjob(name.clone(), *cronjob.clone(), permission_resolver)
                        .await?,
                );
            }
        }
    }
    let svc_access_perms =
        get_svc_access_perms(&app.metadata.has_permissions, permission_resolver).await?;
    if let Some(ref network_config) = app.app_network_policy {
        let mut policy = network_config.clone();
        for (app, perms) in svc_access_perms.iter() {
            let ns = Some(permission_resolver.get_app_ns(app).await?);
            for (svc, (target_pod, ports)) in perms.ports_by_svc.iter() {
                policy.egress.services.push(ServiceConfig {
                    namespace: ns.clone(),
                    service_name: svc.clone(),
                    allowed_ports: Some(port_config_for_service_egress_only(ports.clone())),
                });
                policy.egress.pods.push(PodConfig {
                    namespace: ns.clone(),
                    app: None,
                    allowed_ports: Some(port_config_for_service_egress_only(ports.clone())),
                    match_labels: Some(HashMap::from([(
                        "container".to_string(),
                        target_pod.clone(),
                    )])),
                    all_namespaces: false,
                });
            }
        }
        network_policies.push(
            get_network_policy(
                "app".to_string(),
                &policy,
                CiliumNetworkPolicyEndpointSelector {
                    match_expressions: None,
                    match_labels: Some(BTreeMap::from([(
                        "use-fallback-network-policy".to_string(),
                        "true".to_string(),
                    )])),
                },
                permission_resolver,
            )
            .await?,
        );
    }
    for (target_app, app_port_perms) in svc_access_perms {
        let target_app_ns = if app_port_perms.is_system_lib {
            target_app
        } else {
            format!("{}-{}", user_state.user_id, target_app.clone())
        };
        let current_app_ns = if app.metadata.scope == AppScope::System {
            app_id.clone()
        } else {
            format!("{}-{}", user_state.user_id, app_id.clone())
        };
        for (svc, (container, ports)) in app_port_perms.ports_by_svc {
            network_policies.push(networkpolicy::generate_network_policy_allow_set(
                &current_app_ns,
                &target_app_ns,
                &svc,
                &container,
                ports
                    .tcp
                    .iter()
                    .map(
                        |(_public_port, target_port)| CiliumNetworkPolicyIngressToPortsPorts {
                            port: target_port.to_string(),
                            protocol: Some(CiliumNetworkPolicyIngressToPortsPortsProtocol::Tcp),
                            end_port: None,
                        },
                    )
                    .chain(ports.udp.iter().map(|(_public_port, target_port)| {
                        CiliumNetworkPolicyIngressToPortsPorts {
                            port: target_port.to_string(),
                            protocol: Some(CiliumNetworkPolicyIngressToPortsPortsProtocol::Udp),
                            end_port: None,
                        }
                    }))
                    .collect::<Vec<CiliumNetworkPolicyIngressToPortsPorts>>(),
            ));
        }
    }

    // Insert all the cloned pvcs
    pvcs.extend(cloned_pvcs.into_values());

    Ok(KubernetesConfig {
        deployments,
        services,
        cronjobs,
        jobs,
        pvcs,
        pvs: pvs.into_values().collect(),
        roles,
        cluster_roles,
        cluster_role_bindings,
        role_bindings,
        service_accounts,
        middlewares: app
            .ingress
            .iter()
            .filter_map(|ingress| {
                if ingress.strip_prefix
                    && ingress
                        .path_prefix
                        .as_ref()
                        .is_some_and(|prefix| prefix != "/")
                {
                    Some(middleware::strip_prefix_mw(
                        ingress.path_prefix.as_ref().unwrap(),
                    ))
                } else {
                    None
                }
            })
            .chain(middlewares.into_iter())
            .collect(),
        secrets: generate_secrets(&app.secrets),
        network_policies,
        plugins,
        cluster_plugins,
        others,
    })
}

impl KubernetesConfig {
    pub fn get_pvs_to_mirror(&self) -> Vec<String> {
        self.pvs
            .iter()
            .map(|pv| {
                pv.metadata
                    .name
                    .clone()
                    .unwrap()
                    .split_once("-mirror-")
                    .unwrap()
                    .0
                    .to_string()
            })
            .sorted()
            .dedup()
            .collect()
    }
}
