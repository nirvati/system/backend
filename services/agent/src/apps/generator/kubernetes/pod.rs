// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use itertools::Itertools;

use super::utils;
use super::volume::generate_volumes;
use crate::apps::generator::internal::ResolverCtx;
use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use nirvati_apps::internal::{EnvVar, Runnable};

pub async fn generate_pod(
    service_name: String,
    service: Runnable,
    resolver: &ResolverCtx,
) -> anyhow::Result<k8s::PodTemplateSpec> {
    let container = service.into_container();
    let mut labels = container.additional_labels.clone();
    labels.insert("container".to_owned(), service_name.clone());
    labels.insert("is-container".to_owned(), "true".to_owned());
    if container.network_policy.is_none() {
        labels.insert("use-fallback-network-policy".to_owned(), "true".to_owned());
    }
    Ok(k8s::PodTemplateSpec {
        metadata: Some(k8s_meta::ObjectMeta {
            labels: Some(labels),
            ..Default::default()
        }),
        spec: Some(k8s::PodSpec {
            service_account_name: container.service_account.map(|_| service_name.clone()),
            security_context: if container.uid.is_some()
                || container.gid.is_some()
                || container.fs_group.is_some()
            {
                Some(k8s::PodSecurityContext {
                    run_as_user: container.uid.map(|uid| uid as i64),
                    run_as_group: container.gid.map(|gid| gid as i64),
                    fs_group: container
                        .fs_group
                        .map(|gid| gid as i64)
                        .or(container.gid.map(|gid| gid as i64)),
                    ..Default::default()
                })
            } else {
                None
            },
            termination_grace_period_seconds: container.stop_grace_period.and_then(|s| {
                // String can be a time duration, we need to parse it as such
                match go_parse_duration::parse_duration(&s) {
                    Ok(duration_ns) => {
                        let duration = std::time::Duration::from_nanos(duration_ns as u64);
                        Some(duration.as_secs() as i64)
                    }
                    Err(err) => {
                        tracing::error!("Failed to parse stop_grace_period: {:?}", err);
                        None
                    }
                }
            }),
            hostname: container.hostname,
            host_network: container.host_network.then_some(true),
            volumes: {
                let vols = generate_volumes(container.volumes.clone(), resolver).await?;
                if !vols.is_empty() {
                    Some(vols)
                } else {
                    None
                }
            },
            restart_policy: container
                .restart
                .and_then(|policy| utils::compose_restart_policy_to_kubernetes(&policy).ok()),
            containers: vec![k8s::Container {
                name: service_name,
                image: Some(container.image),
                // This is correct, kubernetes names things better
                args: container.command,
                command: container.entrypoint,
                env: if !container.environment.is_empty() {
                    Some(
                        container
                            .environment
                            .into_iter()
                            .map(|(k, v)| k8s::EnvVar {
                                name: k,
                                value: if let EnvVar::StringLike(v) = &v {
                                    Some(v.clone().into())
                                } else {
                                    None
                                },
                                value_from: if let EnvVar::SecretRef(v) = v {
                                    Some(k8s::EnvVarSource {
                                        secret_key_ref: Some(k8s::SecretKeySelector {
                                            name: v.secret,
                                            key: v.key,
                                            ..Default::default()
                                        }),
                                        ..Default::default()
                                    })
                                } else {
                                    None
                                },
                            })
                            .collect(),
                    )
                } else {
                    None
                },
                volume_mounts: Some(
                    container
                        .volumes
                        .into_iter()
                        .map(|vol| k8s::VolumeMount {
                            name: vol.get_slug(),
                            mount_path: vol.get_mount_path().to_owned(),
                            sub_path: vol.get_sub_path().cloned(),
                            ..Default::default()
                        })
                        .collect(),
                ),
                ports: if !container.exposes.tcp.is_empty() || !container.exposes.udp.is_empty() {
                    Some(
                        container
                            .exposes
                            .tcp
                            .iter()
                            .sorted()
                            .map(|container_port| k8s::ContainerPort {
                                container_port: *container_port as i32,
                                ..Default::default()
                            })
                            .chain(container.exposes.udp.iter().sorted().map(|container_port| {
                                k8s::ContainerPort {
                                    container_port: *container_port as i32,
                                    protocol: Some("UDP".to_string()),
                                    ..Default::default()
                                }
                            }))
                            .collect(),
                    )
                } else {
                    None
                },
                security_context: if !container.cap_add.is_empty()
                    || !container.cap_drop.is_empty()
                    || container.privileged
                {
                    Some(k8s::SecurityContext {
                        privileged: if container.privileged {
                            Some(container.privileged)
                        } else {
                            None
                        },
                        capabilities: if !container.cap_add.is_empty()
                            || !container.cap_drop.is_empty()
                        {
                            Some(k8s::Capabilities {
                                add: if !container.cap_add.is_empty() {
                                    Some(container.cap_add)
                                } else {
                                    None
                                },
                                drop: if !container.cap_drop.is_empty() {
                                    Some(container.cap_drop)
                                } else {
                                    None
                                },
                                ..Default::default()
                            })
                        } else {
                            None
                        },
                        ..Default::default()
                    })
                } else {
                    None
                },
                ..Default::default()
            }],
            ..Default::default()
        }),
    })
}
