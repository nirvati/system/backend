// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::apps::generator::internal::ResolverCtx;
use crate::apps::generator::kubernetes::pod::generate_pod;
use k8s_openapi::api::batch::v1 as k8s_batch;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use nirvati_apps::internal::{CronJob, Runnable};

pub async fn generate_cronjob(
    service_name: String,
    service: CronJob,
    resolver: &ResolverCtx,
) -> anyhow::Result<k8s_batch::CronJob> {
    Ok(k8s_batch::CronJob {
        spec: Some(k8s_batch::CronJobSpec {
            schedule: service.schedule.clone(),
            job_template: k8s_batch::JobTemplateSpec {
                spec: Some(k8s_batch::JobSpec {
                    template: generate_pod(
                        service_name.clone(),
                        Runnable::Cron(Box::from(service)),
                        resolver,
                    )
                    .await?,
                    ..Default::default()
                }),
                ..Default::default()
            },
            ..Default::default()
        }),
        metadata: k8s_meta::ObjectMeta {
            name: Some(service_name),
            ..Default::default()
        },
        ..Default::default()
    })
}
