// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use k8s_crds_traefik::*;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use k8s_openapi::apimachinery::pkg::util::intstr::IntOrString;
use nirvati_apps::internal::{Ingress, IngressType};
use slugify::slugify;

pub fn get_app_tcp_ingress(
    domain: &str,
    sources: &[Ingress],
    entrypoints: Vec<String>,
    user: &str,
) -> IngressRouteTCP {
    IngressRouteTCP {
        metadata: k8s_meta::ObjectMeta {
            name: Some(slugify!(domain)),
            ..Default::default()
        },
        spec: IngressRouteTCPSpec {
            entry_points: Some(entrypoints),
            routes: sources
                .iter()
                .filter_map(|source| {
                    if source.r#type != IngressType::TlsTcp {
                        return None;
                    };
                    Some(IngressRouteTCPRoutes {
                        r#match: format!("HostSNI(`{}`)", domain),
                        middlewares: None,
                        priority: None,
                        services: Some(
                            if let (Some(svc), Some(port)) =
                                (source.target_service.as_ref(), source.target_port)
                            {
                                vec![IngressRouteTCPRoutesServices {
                                    name: svc.clone(),
                                    port: IntOrString::Int(port as i32),
                                    namespace: source.target_ns.clone().or_else(|| {
                                        source
                                            .target_app
                                            .as_ref()
                                            .map(|app| format!("{}-{}", user, app))
                                    }),
                                    ..Default::default()
                                }]
                            } else {
                                vec![]
                            },
                        ),
                        syntax: None,
                    })
                })
                .collect(),
            tls: Some(IngressRouteTCPTls {
                passthrough: Some(true),
                cert_resolver: None,
                domains: None,
                options: None,
                secret_name: None,
                store: None,
            }),
        },
    }
}
