// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::api::rbac::v1 as k8s_rbac;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use nirvati_apps::internal::Container;
use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
pub struct ServiceAccount {
    pub roles: Vec<k8s_rbac::Role>,
    pub cluster_role: Option<k8s_rbac::ClusterRole>,
    pub cluster_role_bindings: Vec<k8s_rbac::ClusterRoleBinding>,
    pub role_bindings: Vec<k8s_rbac::RoleBinding>,
    pub account: k8s::ServiceAccount,
}

pub fn generate_service_account(
    app_id: &str,
    user: Option<&String>,
    service_name: String,
    service: &Container,
) -> Option<ServiceAccount> {
    service.service_account.clone().map(|service_account| {
        let subjects = Some(vec![k8s_rbac::Subject {
            kind: "ServiceAccount".to_string(),
            name: service_name.clone(),
            namespace: Some(
                user.map(|user| format!("{}-{}", user, app_id))
                    .unwrap_or(app_id.to_string()),
            ),
            ..Default::default()
        }]);

        let mut cluster_role_bindings = Vec::new();
        let mut role_bindings = Vec::new();
        let mut roles = Vec::new();

        if !service_account.cluster_rules.is_empty() {
            cluster_role_bindings.push(k8s_rbac::ClusterRoleBinding {
                metadata: k8s_meta::ObjectMeta {
                    name: Some(format!(
                        "nirvati:{}:{}:{}:custom",
                        user.map(|user| user.as_str()).unwrap_or("system"),
                        app_id,
                        service_name
                    )),
                    ..Default::default()
                },
                role_ref: k8s_rbac::RoleRef {
                    api_group: "rbac.authorization.k8s.io".to_string(),
                    kind: "ClusterRole".to_string(),
                    name: format!(
                        "nirvati:{}:{}:{}",
                        user.map(|user| user.as_str()).unwrap_or("system"),
                        app_id,
                        service_name
                    ),
                },
                subjects: subjects.clone(),
            });
        }

        for role in &service_account.builtin_cluster_roles {
            cluster_role_bindings.push(k8s_rbac::ClusterRoleBinding {
                metadata: k8s_meta::ObjectMeta {
                    name: Some(format!(
                        "nirvati:{}:{}:{}:{}",
                        user.map(|user| user.as_str()).unwrap_or("system"),
                        app_id,
                        service_name,
                        role
                    )),
                    ..Default::default()
                },
                role_ref: k8s_rbac::RoleRef {
                    api_group: "rbac.authorization.k8s.io".to_string(),
                    kind: "ClusterRole".to_string(),
                    name: role.clone(),
                },
                subjects: subjects.clone(),
            });
        }

        if !service_account.inner.rules.is_empty() {
            role_bindings.push(k8s_rbac::RoleBinding {
                metadata: k8s_meta::ObjectMeta {
                    name: Some(format!(
                        "nirvati:{}:{}:{}:custom",
                        user.map(|user| user.as_str()).unwrap_or("system"),
                        app_id,
                        service_name
                    )),
                    ..Default::default()
                },
                role_ref: k8s_rbac::RoleRef {
                    api_group: "rbac.authorization.k8s.io".to_string(),
                    kind: "Role".to_string(),
                    name: format!(
                        "nirvati:{}:{}:{}",
                        user.map(|user| user.as_str()).unwrap_or("system"),
                        app_id,
                        service_name
                    ),
                },
                subjects: subjects.clone(),
            });
            roles.push(k8s_rbac::Role {
                metadata: k8s_meta::ObjectMeta {
                    name: Some(format!(
                        "nirvati:{}:{}:{}",
                        user.map(|user| user.as_str()).unwrap_or("system"),
                        app_id,
                        service_name
                    )),
                    ..Default::default()
                },
                rules: Some(service_account.inner.rules.clone()),
            });
        }

        for role in &service_account.inner.builtin_roles {
            role_bindings.push(k8s_rbac::RoleBinding {
                metadata: k8s_meta::ObjectMeta {
                    name: Some(format!(
                        "nirvati:{}:{}:{}:{}",
                        user.map(|user| user.as_str()).unwrap_or("system"),
                        app_id,
                        service_name,
                        role
                    )),
                    ..Default::default()
                },
                role_ref: k8s_rbac::RoleRef {
                    api_group: "rbac.authorization.k8s.io".to_string(),
                    kind: "Role".to_string(),
                    name: role.clone(),
                },
                subjects: subjects.clone(),
            });
        }

        for (other_ns, account) in &service_account.other_ns {
            if !account.rules.is_empty() {
                role_bindings.push(k8s_rbac::RoleBinding {
                    metadata: k8s_meta::ObjectMeta {
                        name: Some(format!(
                            "nirvati:{}:{}:{}",
                            user.map(|user| user.as_str()).unwrap_or("system"),
                            app_id,
                            service_name
                        )),
                        namespace: Some(other_ns.clone()),
                        ..Default::default()
                    },
                    role_ref: k8s_rbac::RoleRef {
                        api_group: "rbac.authorization.k8s.io".to_string(),
                        kind: "Role".to_string(),
                        name: format!(
                            "nirvati:{}:{}:{}",
                            user.map(|user| user.as_str()).unwrap_or("system"),
                            app_id,
                            service_name
                        ),
                    },
                    subjects: subjects.clone(),
                });
                roles.push(k8s_rbac::Role {
                    metadata: k8s_meta::ObjectMeta {
                        name: Some(format!(
                            "nirvati:{}:{}:{}",
                            user.map(|user| user.as_str()).unwrap_or("system"),
                            app_id,
                            service_name
                        )),
                        namespace: Some(other_ns.clone()),
                        ..Default::default()
                    },
                    rules: Some(account.rules.clone()),
                });
            }

            for role in &account.builtin_roles {
                role_bindings.push(k8s_rbac::RoleBinding {
                    metadata: k8s_meta::ObjectMeta {
                        name: Some(format!(
                            "nirvati:{}:{}:{}:{}",
                            user.map(|user| user.as_str()).unwrap_or("system"),
                            app_id,
                            service_name,
                            role
                        )),
                        namespace: Some(other_ns.clone()),
                        ..Default::default()
                    },
                    role_ref: k8s_rbac::RoleRef {
                        api_group: "rbac.authorization.k8s.io".to_string(),
                        kind: "Role".to_string(),
                        name: role.clone(),
                    },
                    subjects: subjects.clone(),
                });
            }
        }

        ServiceAccount {
            cluster_role: if !service_account.cluster_rules.is_empty() {
                Some(k8s_rbac::ClusterRole {
                    metadata: k8s_meta::ObjectMeta {
                        name: Some(format!(
                            "nirvati:{}:{}:{}",
                            user.map(|user| user.as_str()).unwrap_or("system"),
                            app_id,
                            service_name
                        )),
                        ..Default::default()
                    },
                    rules: Some(service_account.cluster_rules),
                    ..Default::default()
                })
            } else {
                None
            },
            roles,
            cluster_role_bindings,
            role_bindings,
            account: k8s::ServiceAccount {
                metadata: k8s_meta::ObjectMeta {
                    name: Some(service_name.clone()),
                    ..Default::default()
                },
                ..Default::default()
            },
        }
    })
}
