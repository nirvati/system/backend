// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::io::Write;
use std::str::FromStr;

use crate::apps::generator::kubernetes::KubernetesConfig;
use anyhow::Result;
use flate2::write::GzEncoder;
use nirvati::kubernetes::multidoc_serialize;
use nirvati_apps::metadata::Metadata;
use semver::BuildMetadata;
use tar::Header;

pub mod chart_yml;
pub mod types;

fn write_file<T: Write>(archive: &mut tar::Builder<T>, path: &str, data: &[u8]) -> Result<()> {
    let mut header = Header::new_gnu();
    header.set_size(data.len() as u64);
    header.set_mode(0o755);
    header.set_cksum();
    archive.append_data(&mut header, path, data)?;
    Ok(())
}

pub fn generate_chart(
    app: KubernetesConfig,
    mut metadata: Metadata,
    user: &str,
    settings_hash: &str,
) -> Result<(Vec<u8>, String)> {
    let output = Vec::new();
    let gz_encoder = GzEncoder::new(output, flate2::Compression::default());
    let mut tar = tar::Builder::new(gz_encoder);
    let app_id = format!("{}-{}", user, metadata.id.clone());
    metadata.version.build =
        BuildMetadata::from_str(&format!("{}-{}", metadata.version.build, settings_hash)).unwrap();
    let version_str = metadata.version.to_string();
    let chart = chart_yml::generate_chart_yml(metadata);
    let data = serde_yaml::to_string(&chart)?;
    write_file(
        &mut tar,
        format!("{}/Chart.yaml", &app_id).as_str(),
        data.as_bytes(),
    )?;
    let kubernetes_config = serde_yaml::to_value(app.clone())?;
    for (field, vals) in kubernetes_config.as_mapping().unwrap() {
        let field_str = field.as_str().unwrap();
        let vals_list = vals.as_sequence().unwrap();
        if !vals_list.is_empty() {
            let serialized = multidoc_serialize(vals_list.to_vec())?;
            write_file(
                &mut tar,
                &format!("{}/templates/{}.yaml", &app_id, field_str),
                serialized.as_bytes(),
            )?;
        }
    }
    // into_inner calls finish() internally
    let gz_encoder = tar.into_inner()?;
    let output = gz_encoder.finish()?;
    Ok((output, version_str))
}
