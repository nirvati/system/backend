// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

mod permissions;

use crate::apps::generator::internal::permissions::flatten::flatten_perms;
use crate::apps::generator::internal::permissions::{get_app_perms, ResolvedApp};
use nirvati_apps::internal::InternalAppRepresentation;
use nirvati_apps::metadata::AppScope;
pub use permissions::{get_svc_access_perms, AppPortPerms, ResolverCtx};

pub async fn load_permissions(
    internal_app: &mut InternalAppRepresentation,
    ctx: &ResolverCtx,
) -> anyhow::Result<()> {
    ctx.insert(
        internal_app.metadata.id.clone(),
        ResolvedApp {
            real_id: internal_app.metadata.id.clone(),
            is_system_lib: internal_app.metadata.scope == AppScope::System,
            permissions: internal_app.metadata.exposes_permissions.clone(),
            services: internal_app.services.clone(),
        },
    )
    .await;
    let mut additional_perms = get_app_perms(internal_app, ctx).await?;
    internal_app.metadata.base_permissions = additional_perms.global_perms.clone();
    internal_app.metadata.base_permissions.sort();
    internal_app.metadata.base_permissions.dedup();
    flatten_perms(&mut internal_app.metadata.base_permissions, ctx).await;
    internal_app.metadata.base_permissions.sort();
    internal_app.metadata.has_permissions = internal_app.metadata.base_permissions.clone();
    internal_app
        .metadata
        .has_permissions
        .append(&mut additional_perms.scoped_perms);
    internal_app.metadata.has_permissions.sort();
    internal_app.metadata.has_permissions.dedup();
    flatten_perms(&mut internal_app.metadata.has_permissions, ctx).await;
    internal_app.metadata.has_permissions.sort();
    Ok(())
}
