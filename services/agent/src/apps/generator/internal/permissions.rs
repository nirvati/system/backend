// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

pub mod flatten;
mod networkpolicy;
mod resolver;

pub use resolver::ResolvedApp;
pub use resolver::ResolverCtx;
use std::collections::BTreeMap;

use nirvati_apps::internal::{InternalAppRepresentation, PluginType, Ports, ServiceType, Volume};
use nirvati_apps::metadata::AppScope;

pub struct PermissionResult {
    pub scoped_perms: Vec<String>,
    pub global_perms: Vec<String>,
}
#[async_recursion::async_recursion]
pub async fn get_app_perms(
    app: &InternalAppRepresentation,
    ctx: &ResolverCtx,
) -> Result<PermissionResult, anyhow::Error> {
    let mut app_perms = PermissionResult {
        global_perms: app.metadata.has_permissions.clone(),
        scoped_perms: Vec::new(),
    };

    if let Some(ref netpol) = app.app_network_policy {
        app_perms
            .global_perms
            .extend(networkpolicy::network_policy_to_perms(netpol));
    }

    for service in app.services.values() {
        if service.r#type == ServiceType::LoadBalancer {
            app_perms
                .global_perms
                .push("builtin/network/public-port".to_string());
        }
        if service.cluster_ip.as_ref().is_some_and(|ip| ip != "None") {
            app_perms.global_perms.push("builtin/root".to_string());
        }
    }

    for ingress in &app.ingress {
        if let Some(target_app) = ingress.target_app.as_ref() {
            let (Some(target_svc), Some(target_port)) =
                (ingress.target_service.as_ref(), ingress.target_port)
            else {
                // Invalid app
                tracing::warn!(
                    "Ingress {} in app {} has an invalid target service",
                    ingress.path_prefix.clone().unwrap_or("/".to_string()),
                    app.metadata.id
                );
                continue;
            };
            let other_app = ctx.get_or_load(target_app).await?;
            let ingress_perm = other_app.find_ideal_perm(&app_perms.global_perms, |perm| {
                perm
                    .services
                    .get(target_svc)
                    .is_some_and(|svc| svc.tcp.contains(&target_port))
            });
            if let Some((ingress_perm_id, already_granted)) = ingress_perm {
                if !already_granted {
                    app_perms.global_perms.push(ingress_perm_id);
                }
            } else {
                tracing::warn!(
                    "Ingress {} in app {} has a target service that is not explicitly exposed!",
                    ingress.path_prefix.clone().unwrap_or("/".to_string()),
                    app.metadata.id
                );
                // This does not automatically grant the right to access the target svc, policies on the side of the target app still apply
                app_perms
                    .global_perms
                    .push("builtin/network/cluster-egress".to_string());
            }
        } else if ingress.target_ns.is_some() {
            app_perms
                .global_perms
                .push("builtin/network/cluster-egress".to_string());
        }
    }

    for runnable in app.containers.values() {
        let container = runnable.get_container();
        for perm in container.has_permissions.iter() {
            app_perms.scoped_perms.push(perm.clone());
        }
        for vol in &container.volumes {
            match vol {
                Volume::Host(_) => {
                    app_perms.global_perms.push("builtin/root".to_string());
                }
                Volume::Longhorn(longhorn_vol) => {
                    if longhorn_vol.from_ns.is_some() {
                        app_perms.global_perms.push("builtin/root".to_string());
                    } else if let Some(ref from_app) = longhorn_vol.from_app {
                        let other_app = ctx.get_or_load(from_app).await?;
                        let perm = other_app.find_ideal_perm(&app_perms.global_perms, |perm| {
                            perm.volumes.iter().any(|vol_perm| {
                                if vol_perm.volume != longhorn_vol.name {
                                    false
                                } else if let Some(ref subpath) = longhorn_vol.sub_path {
                                    vol_perm
                                        .sub_paths
                                        .as_ref()
                                        .is_none_or(|paths| paths.contains(subpath))
                                } else {
                                    vol_perm.sub_paths.is_none()
                                }
                            })
                        });
                        if let Some((perm_id, already_granted)) = perm {
                            if !already_granted {
                                app_perms.global_perms.push(perm_id);
                            }
                        } else {
                            tracing::warn!(
                                "Volume {} in app {} is not explicitly exposed!",
                                longhorn_vol.name,
                                from_app
                            );
                            app_perms.global_perms.push("builtin/root".to_string());
                        }
                    }
                }
                Volume::Secret(_) => {}
            };
        }

        if container.host_network {
            app_perms
                .global_perms
                .push("builtin/network/host".to_string());
        }

        for capability in &container.cap_add {
            match capability.as_str() {
                "CAP_NET_RAW" => {
                    app_perms
                        .global_perms
                        .push("builtin/network/raw".to_string());
                }
                _ => {
                    app_perms.global_perms.push("builtin/root".to_string());
                }
            }
        }

        if container.privileged {
            app_perms.global_perms.push("builtin/root".to_string());
        }

        if let Some(ref netpol) = container.network_policy {
            app_perms
                .global_perms
                .extend(networkpolicy::network_policy_to_perms(netpol));
        }

        // TODO: Make this more fine-grained
        if let Some(ref service_account) = container.service_account {
            if !service_account.builtin_cluster_roles.is_empty()
                || !service_account.cluster_rules.is_empty()
                || !service_account.other_ns.is_empty()
            {
                app_perms
                    .global_perms
                    .push("builtin/kube-api/full".to_string());
                app_perms.global_perms.push("builtin/root".to_string());
            } else {
                // Check if the access is read-only or full
                if !service_account.inner.builtin_roles.is_empty()
                    || service_account.inner.rules.iter().any(|rule| {
                        rule.non_resource_urls.is_some()
                            || rule
                                .verbs
                                .iter()
                                .any(|verb| verb != "get" && verb != "list" && verb != "watch")
                    })
                {
                    app_perms
                        .global_perms
                        .push("builtin/kube-api/full".to_string());
                    app_perms.global_perms.push("builtin/root".to_string());
                } else {
                    app_perms
                        .global_perms
                        .push("builtin/kube-api/read-only".to_string());
                }
            }
        }
    }

    if app
        .plugins
        .values()
        .any(|plugin| plugin.r#type == PluginType::Source)
    {
        app_perms
            .global_perms
            .push("builtin/plugins/source".to_string());
    }
    if app
        .plugins
        .iter()
        .any(|(_plugin_id, plugin)| plugin.r#type == PluginType::Context)
    {
        app_perms
            .global_perms
            .push("builtin/plugins/context".to_string());
    }
    if app
        .plugins
        .values()
        .any(|plugin| plugin.r#type == PluginType::Runtime)
    {
        app_perms
            .global_perms
            .push("builtin/plugins/runtime".to_string());
    }
    if app
        .plugins
        .values()
        .any(|plugin| plugin.r#type == PluginType::CustomResource)
    {
        app_perms
            .global_perms
            .push("builtin/plugins/custom-resource".to_string());
    }
    if !app.other.is_empty() {
        // Declearing custom stuff to run inside Kubernetes is basically equal to root access right now
        app_perms.global_perms.push("builtin/root".to_string());
    }

    app_perms.global_perms.sort();
    app_perms.global_perms.dedup();
    for perm in app_perms.global_perms.clone().iter() {
        let (other_app_id, other_perm) = perm.split_once('/').ok_or(anyhow::anyhow!(
            "Permission {} can not be requested, as it is not in the format app/perm",
            perm
        ))?;
        if other_app_id == "builtin" {
            continue;
        }
        let other_app = ctx.get_or_load(other_app_id).await?;
        let Some(other_perm) = other_app.permissions.iter().find(|p| p.id == other_perm) else {
            return Err(anyhow::anyhow!(
                "Permission {} does not exist in app {}",
                other_perm,
                other_app_id,
            ));
        };

        if app.metadata.scope != AppScope::System
            && !other_perm.open_to_all_users
            && other_app.is_system_lib
        {
            return Err(anyhow::anyhow!(
                "Regular app {} is trying to access a protected permission on system library {}",
                app.metadata.id,
                other_app_id,
            ));
        }
    }

    Ok(app_perms)
}

#[derive(Clone, Debug)]
pub struct AppPortPerms {
    pub is_system_lib: bool,
    pub ports_by_svc: BTreeMap<String, (String, Ports)>,
}

pub async fn get_svc_access_perms(
    permissions: &[String],
    ctx: &ResolverCtx,
) -> Result<BTreeMap<String, AppPortPerms>, anyhow::Error> {
    let mut output = BTreeMap::new();
    let mut perms_by_app = BTreeMap::new();
    for perm in permissions {
        let (app_id, perm) = perm.split_once('/').ok_or(anyhow::anyhow!(
            "Permission {} can not be requested, as it is not in the format app/perm",
            perm
        ))?;
        let perms = perms_by_app.entry(app_id).or_insert_with(Vec::new);
        perms.push(perm);
    }

    for (app, perms) in perms_by_app {
        if app == "builtin" {
            continue;
        }
        let mut app_output = BTreeMap::new();

        let app_perms = ctx.get_or_load(app).await?;

        for perm in perms {
            let perm = app_perms.permissions.iter().find(|p| p.id == perm).unwrap();
            for (svc_id, ports) in &perm.services {
                let Some(target_svc) = app_perms.services.get(svc_id) else {
                    return Err(anyhow::anyhow!(
                        "Service {} does not exist in app {}",
                        svc_id,
                        app
                    ));
                };
                let internal_ports = Ports {
                    tcp: ports
                        .tcp
                        .iter()
                        .filter_map(|public_port| {
                            target_svc
                                .ports
                                .tcp
                                .get(public_port)
                                .copied()
                                .map(|internal_port| (*public_port, internal_port))
                        })
                        .collect(),
                    udp: ports
                        .udp
                        .iter()
                        .filter_map(|public_port| {
                            target_svc
                                .ports
                                .tcp
                                .get(public_port)
                                .copied()
                                .map(|internal_port| (*public_port, internal_port))
                        })
                        .collect(),
                };
                if internal_ports.tcp.len() != ports.tcp.len()
                    || internal_ports.udp.len() != ports.udp.len()
                {
                    return Err(anyhow::anyhow!(
                        "Service {} in app {} does not expose all requested ports",
                        svc_id,
                        app
                    ));
                }
                app_output
                    .entry(svc_id.clone())
                    .or_insert_with(|| {
                        (target_svc.target_container.clone(), internal_ports.clone())
                    })
                    .1
                    .extend(internal_ports);
            }
        }

        output.insert(
            app_perms.real_id.clone(),
            AppPortPerms {
                is_system_lib: app_perms.is_system_lib,
                ports_by_svc: app_output,
            },
        );
    }
    Ok(output)
}
