use nirvati_apps::internal::networkpolicy::NetworkPolicyEgress;

use ipnet::{Ipv4Net, Ipv6Net};
use iprange::IpRange;
use nirvati_apps::internal::NetworkPolicy;

fn includes_private_egress_target(netpol: &NetworkPolicyEgress) -> bool {
    let mut private_ipv4_cidrs: IpRange<Ipv4Net> = IpRange::new();
    private_ipv4_cidrs.add("10.0.0.0/8".parse().unwrap());
    private_ipv4_cidrs.add("172.16.0.0/12".parse().unwrap());
    private_ipv4_cidrs.add("192.168.0.0/16".parse().unwrap());
    // Not really used, but let's treat it as private, because it could be used for example in Tailscale
    private_ipv4_cidrs.add("100.64.0.0/10".parse().unwrap());
    private_ipv4_cidrs.add("169.254.0.0/16".parse().unwrap());
    let mut private_ipv6_cidrs: IpRange<Ipv6Net> = IpRange::new();
    private_ipv6_cidrs.add("fec0::/10".parse().unwrap()); // deprecated
    private_ipv6_cidrs.add("fc00::/7".parse().unwrap()); // unique local unicast
    private_ipv6_cidrs.add("fe80::/10".parse().unwrap()); // link-local unicast

    netpol.cidrs.iter().any(|cidr| {
        if let Ok(range) = cidr.cidr.parse::<Ipv4Net>() {
            let mut tmp = IpRange::new();
            tmp.add(range);
            !private_ipv4_cidrs.intersect(&tmp).is_empty()
        } else if let Ok(range) = cidr.cidr.parse::<Ipv6Net>() {
            let mut tmp = IpRange::new();
            tmp.add(range);
            !private_ipv6_cidrs.intersect(&tmp).is_empty()
        } else {
            true
        }
    })
}

pub fn network_policy_to_perms(netpol: &NetworkPolicy) -> Vec<String> {
    let mut perms = vec![];
    if netpol.egress.allow_local_net || includes_private_egress_target(&netpol.egress) {
        perms.push("builtin/network/local-net".to_string());
    }
    if netpol.egress.allow_cluster {
        perms.push("builtin/network/cluster-egress".to_string());
    }
    if netpol
        .egress
        .pods
        .iter()
        .any(|pod| pod.app.is_some() || pod.all_namespaces || pod.app.is_some())
        || netpol
            .egress
            .services
            .iter()
            .any(|svc| svc.namespace.is_some())
    {
        perms.push("builtin/network/cluster-egress".to_string());
    }
    perms
}
