// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::apps::files::get_system_sources_yml;
use crate::apps::parser::{load_app, unsafe_load_app_basics};
use crate::plugins::api::UserState;
use anyhow::Result;
use kube::Client;
use nirvati_apps::internal::Service;
use nirvati_apps::metadata::{AppScope, Permission};
use std::collections::{BTreeMap, HashMap};
use std::path::PathBuf;
use tokio::sync::RwLock;

#[derive(Clone)]
pub struct ResolvedApp {
    pub real_id: String,
    pub is_system_lib: bool,
    pub permissions: Vec<Permission>,
    pub services: BTreeMap<String, Service>,
}

impl ResolvedApp {
    pub fn find_ideal_perm<T: Fn(&Permission) -> bool>(
        &self,
        existing_perms: &[String],
        f: T,
    ) -> Option<(String, bool)> {
        let possible_perms = self.permissions.iter().filter(|perm| f(perm));
        let mut result = None;
        for perm in possible_perms {
            let id = format!("{}/{}", self.real_id, perm.id);
            if existing_perms.contains(&id) {
                return Some((id, true));
            } else if result.is_none() {
                result = Some((id, false));
            }
        }
        result
    }
}
pub struct ResolverCtx {
    processed_apps: RwLock<BTreeMap<String, ResolvedApp>>,
    global_apps_root: PathBuf,
    user_state: UserState,
    nirvati_seed: String,
    alias: RwLock<HashMap<String, String>>,
    #[cfg(not(feature = "test-parser"))]
    kube_client: Client,
    is_initialized: RwLock<bool>,
}

impl ResolverCtx {
    pub async fn get_app_ns(&self, app: &str) -> Result<String> {
        let username = self.user_state.user_id.clone();
        let app = self.get_or_load(app).await?;
        if app.is_system_lib {
            Ok(app.real_id.clone())
        } else {
            Ok(format!("{}-{}", username, app.real_id))
        }
    }

    pub async fn get_or_load(&self, app: &str) -> Result<ResolvedApp> {
        tracing::debug!("Getting or loading app in ResolverCtx: {}", app);
        let real_app = {
            let alias = self.alias.read().await;
            alias.get(app).cloned()
        };
        if let Some(real_app) = real_app {
            tracing::debug!("Found alias for app: {} -> {}", app, real_app);
            self._get_or_load(&real_app).await
        } else {
            tracing::debug!("No alias found for app: {}", app);
            self._get_or_load(app).await
        }
    }

    async fn _get_or_load(&self, app: &str) -> Result<ResolvedApp> {
        let app_exists = {
            let processed_apps = self.processed_apps.read().await;
            processed_apps.contains_key(app)
        };
        if !app_exists {
            let system_apps_sources = get_system_sources_yml(&self.global_apps_root).await?;
            let apps_root = if system_apps_sources.contains_key(app) {
                self.global_apps_root.join("system")
            } else {
                self.global_apps_root.join(&self.user_state.user_id)
            };
            let nirvati_seed = self.nirvati_seed.clone();
            let parsed = load_app(
                #[cfg(not(feature = "test-parser"))]
                self.kube_client.clone(),
                &self.global_apps_root.clone(),
                app,
                &apps_root.join(app),
                self.user_state.clone(),
                &nirvati_seed,
                None,
                self,
            )
            .await?;
            {
                let mut processed_apps = self.processed_apps.write().await;
                processed_apps.insert(
                    app.to_string(),
                    ResolvedApp {
                        real_id: app.to_string(),
                        is_system_lib: parsed.metadata.scope == AppScope::System,
                        permissions: parsed.metadata.exposes_permissions.clone(),
                        services: parsed.services,
                    },
                );
            }
            if let Some(implements) = parsed.metadata.implements {
                self.add_alias(implements, app.to_string()).await;
            }
        }
        Ok(self.processed_apps.read().await.get(app).unwrap().clone())
    }

    pub async fn insert(&self, app: String, permissions: ResolvedApp) {
        let mut processed_apps = self.processed_apps.write().await;
        processed_apps.insert(app, permissions);
    }

    pub async fn add_alias(&self, alias: String, app: String) {
        let mut alias_list = self.alias.write().await;
        alias_list.insert(alias, app);
    }

    pub fn new(
        global_apps_root: PathBuf,
        user_state: UserState,
        nirvati_seed: String,
        #[cfg(not(feature = "test-parser"))] kube_client: Client,
    ) -> Self {
        Self {
            processed_apps: RwLock::new(BTreeMap::new()),
            global_apps_root,
            user_state,
            nirvati_seed,
            alias: RwLock::new(HashMap::new()),
            #[cfg(not(feature = "test-parser"))]
            kube_client,
            is_initialized: RwLock::new(false),
        }
    }

    pub async fn load_installed_apps(&self) -> Result<()> {
        let mut init_lock = self.is_initialized.write().await;
        if *init_lock {
            return Ok(());
        }
        let system_apps_sources = get_system_sources_yml(&self.global_apps_root).await?;
        for app in self.user_state.installed_apps.clone() {
            tracing::debug!("Loading installed app: {}", app);
            let user = if system_apps_sources.contains_key(&app) {
                "system"
            } else {
                &self.user_state.user_id
            };
            let metadata = unsafe_load_app_basics(
                #[cfg(not(feature = "test-parser"))]
                self.kube_client.clone(),
                &app,
                &self.global_apps_root.join(user).join(&app),
                self.user_state.clone(),
                &self.nirvati_seed,
                None,
            )
            .await?;
            if let Some(implements) = metadata.metadata.implements {
                tracing::debug!("Adding alias for implements: {}", implements);
                self.add_alias(implements, app.to_string()).await;
            }
        }
        *init_lock = true;
        Ok(())
    }
}
