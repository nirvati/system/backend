use crate::apps::generator::internal::ResolverCtx;

#[async_recursion::async_recursion]
pub async fn flatten_perms(perms: &mut Vec<String>, resolver: &ResolverCtx) {
    let mut pending = perms.clone();
    loop {
        let mut new_perms = Vec::new();

        for perm in pending.iter() {
            if perm.starts_with("builtin/") {
                continue;
            }
            let Some((app_id, perm_id)) = perm.split_once('/') else {
                tracing::warn!("Invalid permission: {}", perm);
                continue;
            };
            let Ok(app) = resolver.get_or_load(app_id).await else {
                tracing::warn!("App not found: {}", app_id);
                continue;
            };
            let Some(resolved_perm) = app.permissions.iter().find(|p| p.id == perm_id) else {
                tracing::warn!("Permission not found: {}", perm);
                continue;
            };
            for perm in resolved_perm.includes.clone() {
                if !perms.contains(&perm) {
                    new_perms.push(perm);
                }
            }
        }

        if new_perms.is_empty() {
            break;
        }
        pending = new_perms.clone();
        perms.append(&mut new_perms);
    }
}
