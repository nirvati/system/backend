use k8s_crds_snapshots::volumegroupsnapshots::{
    VolumeGroupSnapshot, VolumeGroupSnapshotSource, VolumeGroupSnapshotSourceSelector,
    VolumeGroupSnapshotSpec,
};
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use kube::Api;
use std::collections::BTreeMap;

pub async fn take_app_snapshot(client: kube::Client, app_ns: &str, snapshot_id: String) {
    let api: Api<VolumeGroupSnapshot> = Api::namespaced(client, app_ns);
    let snapshot = VolumeGroupSnapshot {
        metadata: ObjectMeta {
            name: Some(snapshot_id),
            ..Default::default()
        },
        spec: VolumeGroupSnapshotSpec {
            source: VolumeGroupSnapshotSource {
                selector: Some(VolumeGroupSnapshotSourceSelector {
                    match_expressions: None,
                    match_labels: Some(BTreeMap::from([(
                        "generator".to_string(),
                        "nirvati".to_string(),
                    )])),
                }),
                volume_group_snapshot_content_name: None,
            },
            volume_group_snapshot_class_name: None,
        },
        status: Default::default(),
    };
    api.create(&kube::api::PostParams::default(), &snapshot)
        .await
        .expect("Failed to create snapshot");
}
