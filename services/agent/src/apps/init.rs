// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::HashMap;
use std::path::Path;

use crate::apps::files::{write_app_registry, write_stores_yml};
use crate::apps::generator::internal::ResolverCtx;
use crate::apps::parser::process_apps;
use crate::apps::registry::translate_all_categories;
use crate::apps::repos::types::AppStore;
use crate::plugins::api::UserState;
use anyhow::Result;
use kube::Client;
use nirvati_apps::metadata::Metadata;

async fn init(
    kube_client: Client,
    global_apps_root: &Path,
    user_id: &str,
    user_display_name: &str,
    nirvati_seed: &str,
    ctx: &ResolverCtx,
) {
    let apps_root = global_apps_root.join(user_id);
    let mut registry: Vec<Metadata> = process_apps(
        kube_client,
        global_apps_root,
        UserState {
            user_id: user_id.to_string(),
            installed_apps: vec![],
            app_settings: Default::default(),
            user_display_name: user_display_name.to_string(),
        },
        nirvati_seed,
        ctx,
    )
    .await
    .expect("Failed to gather app metadata!")
    .into_iter()
    .map(|app| app.into_metadata())
    .collect();
    translate_all_categories(&mut registry);
    write_app_registry(&apps_root, &registry).expect("Failed to write app registry!");
}

pub async fn preload_apps(
    kube_client: Client,
    global_apps_root: &Path,
    nirvati_seed: &str,
    user_id: &str,
    user_display_name: &str,
    branch: &str,
) -> Result<()> {
    let user_apps_root = global_apps_root.join(user_id);
    let user_state = UserState {
        user_id: user_id.to_string(),
        installed_apps: vec![],
        app_settings: Default::default(),
        user_display_name: user_display_name.to_string(),
    };
    std::fs::create_dir_all(&user_apps_root)?;
    let initial_stores = if std::env::var("CITADEL_BASE_DIR").is_err() {
        vec![
            AppStore {
                id: uuid::Uuid::new_v4(),
                src: HashMap::from([
                    (
                        "repo_url".to_string(),
                        "https://gitlab.com/nirvati/apps/foss.git".to_string(),
                    ),
                    ("branch".to_string(), branch.to_string()),
                ]),
                ..Default::default()
            },
            AppStore {
                id: uuid::Uuid::new_v4(),
                src: HashMap::from([
                    (
                        "repo_url".to_string(),
                        "https://gitlab.com/nirvati/apps/essentials.git".to_string(),
                    ),
                    ("branch".to_string(), branch.to_string()),
                ]),
                ..Default::default()
            },
        ]
    } else {
        vec![AppStore {
            id: uuid::Uuid::new_v4(),
            src: HashMap::from([
                (
                    "repo_url".to_string(),
                    "https://gitlab.com/nirvati/citadel/lts/apps.git".to_string(),
                ),
                ("branch".to_string(), "hybrid".to_string()),
            ]),
            ..Default::default()
        }]
    };
    write_stores_yml(&user_apps_root, &initial_stores)?;
    crate::apps::repos::download_apps(
        #[cfg(not(feature = "test-parser"))]
        kube_client.clone(),
        global_apps_root,
        false,
        user_state.clone(),
    )
    .await?;

    let resolver_ctx = ResolverCtx::new(
        global_apps_root.to_path_buf(),
        user_state.clone(),
        nirvati_seed.to_string(),
        #[cfg(not(feature = "test-parser"))]
        kube_client.clone(),
    );
    init(
        kube_client,
        global_apps_root,
        user_id,
        user_display_name,
        nirvati_seed,
        &resolver_ctx,
    )
    .await;
    Ok(())
}
