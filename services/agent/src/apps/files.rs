// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::HashMap;
use std::path::Path;

use anyhow::Result;

use nirvati_apps::metadata::Metadata;

use crate::apps::repos::types::{AppStore, StoresYml};

pub fn read_raw_app_registry(apps_root: &Path) -> Result<String> {
    let app_registry_path = apps_root.join("registry.json");
    Ok(std::fs::read_to_string(app_registry_path)?)
}

pub fn load_app_registry(apps_root: &Path) -> Result<Vec<Metadata>> {
    let app_registry = read_raw_app_registry(apps_root)?;
    let app_registry: Vec<Metadata> = serde_json::from_str(&app_registry)?;
    Ok(app_registry)
}

pub fn write_app_registry(apps_root: &Path, app_registry: &[Metadata]) -> Result<()> {
    let app_registry_path = apps_root.join("registry.json");
    let app_registry = serde_json::to_string_pretty(app_registry)?;
    std::fs::write(app_registry_path, app_registry)?;
    Ok(())
}

pub fn append_installed_list(apps_root: &Path, app_name: &str) -> Result<()> {
    let installed_path = apps_root.join("installed.json");
    let mut installed_apps = if installed_path.exists() {
        let installed_apps = std::fs::read_to_string(&installed_path)?;
        serde_json::from_str(&installed_apps)?
    } else {
        Vec::new()
    };
    installed_apps.push(app_name.to_string());
    let installed_apps = serde_json::to_string(&installed_apps)?;
    std::fs::write(installed_path, installed_apps)?;
    Ok(())
}

pub fn remove_installed_list(apps_root: &Path, app_name: &str) -> Result<()> {
    let installed_path = apps_root.join("installed.json");
    if !installed_path.exists() {
        return Ok(());
    }
    let installed_apps = std::fs::read_to_string(&installed_path)?;
    let mut installed_apps: Vec<String> = serde_json::from_str(&installed_apps)?;
    installed_apps.retain(|app| app != app_name);
    let installed_apps = serde_json::to_string(&installed_apps)?;
    std::fs::write(installed_path, installed_apps)?;
    Ok(())
}

pub fn app_exists(apps_root: &Path, app_name: &str) -> bool {
    let app_dir = apps_root.join(app_name);
    app_dir.exists()
}

pub fn read_raw_stores_yml(apps_root: &Path) -> Result<String> {
    let stores_yml_path = apps_root.join("stores.yml");
    Ok(std::fs::read_to_string(stores_yml_path)?)
}

pub fn read_stores_yml(apps_root: &Path) -> Result<StoresYml> {
    let stores_yml: StoresYml = serde_yaml::from_str(&read_raw_stores_yml(apps_root)?)?;
    Ok(stores_yml)
}

pub fn write_stores_yml(apps_root: &Path, data: &[AppStore]) -> Result<()> {
    let stores_yml_path = apps_root.join("stores.yml");
    let stores_yml = serde_yaml::to_string(&data)?;
    std::fs::write(stores_yml_path, stores_yml)?;
    Ok(())
}

pub async fn get_system_sources_yml(apps_root: &Path) -> Result<HashMap<String, AppStore>> {
    let stores_yml_path = apps_root.join("app-sources.yml");
    let stores_yml = tokio::fs::read_to_string(stores_yml_path).await?;
    let stores = serde_yaml::from_str(&stores_yml)?;
    Ok(stores)
}

pub async fn add_system_source(apps_root: &Path, app: &str, source: AppStore) -> Result<()> {
    let stores_yml_path = apps_root.join("app-sources.yml");
    let stores_yml = tokio::fs::read_to_string(&stores_yml_path)
        .await
        .unwrap_or_else(|_| "".to_string());
    let mut stores: HashMap<String, AppStore> = serde_yaml::from_str(&stores_yml)?;
    stores.insert(app.to_string(), source);
    let stores_yml = serde_yaml::to_string(&stores)?;
    tokio::fs::write(&stores_yml_path, stores_yml).await?;
    Ok(())
}

pub async fn remove_system_source(apps_root: &Path, app: &str) -> Result<()> {
    let stores_yml_path = apps_root.join("app-sources.yml");
    let stores_yml = tokio::fs::read_to_string(&stores_yml_path).await?;
    let mut stores: HashMap<String, AppStore> = serde_yaml::from_str(&stores_yml)?;
    stores.remove(app);
    let stores_yml = serde_yaml::to_string(&stores)?;
    tokio::fs::write(&stores_yml_path, stores_yml).await?;
    Ok(())
}
