// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::BTreeMap;

use nirvati::utils::MultiLanguageItem;
use nirvati_apps::metadata::Metadata;

pub fn translate_all_categories(apps: &mut Vec<Metadata>) {
    // app.category is a map of language to category
    // We need to ensure all apps have an english/default category name (If they don't have one, try to find any that starts with en or use the first one)
    // Also, if a category name has no translations / missing translations, we can use these of other apps
    // If an english name has different translations in multiple apps, we don't automatically do that
    let mut category_map: BTreeMap<String, MultiLanguageItem> = BTreeMap::new();
    for app in apps.iter() {
        let english_category = app.category.0.get("en").unwrap_or_else(|| {
            app.category
                .0
                .iter()
                .find(|(key, _)| key.starts_with("en"))
                .map(|(_, value)| value)
                .unwrap_or_else(|| {
                    app.category
                        .0
                        .iter()
                        .next()
                        .map(|(_, value)| value)
                        .unwrap()
                })
        });
        if let Some(category) = category_map.get_mut(english_category) {
            for (key, value) in app.category.0.iter() {
                if !category.0.contains_key(key) {
                    category.0.insert(key.to_owned(), value.to_owned());
                }
            }
        } else {
            category_map.insert(english_category.to_owned(), app.category.clone());
        }
    }
    for app in apps {
        let english_category = app.category.0.get("en").unwrap_or_else(|| {
            app.category
                .0
                .iter()
                .find(|(key, _)| key.starts_with("en"))
                .map(|(_, value)| value)
                .unwrap_or_else(|| {
                    app.category
                        .0
                        .iter()
                        .next()
                        .map(|(_, value)| value)
                        .unwrap()
                })
        });
        if let Some(category) = category_map.get(english_category) {
            for (key, value) in category.0.iter() {
                if !app.category.0.contains_key(key) {
                    app.category.0.insert(key.to_owned(), value.to_owned());
                }
            }
        }
    }
}
