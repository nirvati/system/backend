// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

pub mod plugins;

use crate::plugins::api::UserState;
#[cfg(feature = "test-parser")]
use base64::Engine;
use nirvati::kubernetes::get_node_ip;
use nirvati::utils::is_ip_private;
use std::collections::BTreeMap;
use tera::Context;

pub async fn get_processing_context(
    #[cfg(not(feature = "test-parser"))] kube_client: kube::Client,
    app_id: &str,
    perms: &[String],
    additional_metadata: BTreeMap<String, BTreeMap<String, serde_json::Value>>,
    user_state: &UserState,
) -> anyhow::Result<Context> {
    let mut tera_ctx = Context::new();
    tera_ctx.insert("installed_apps", &user_state.installed_apps);
    tera_ctx.insert("user", &user_state.user_id);
    tera_ctx.insert("user_display_name", &user_state.user_display_name);
    tera_ctx.insert(
        "jwt_pubkey_base64",
        &std::env::var("JWT_PUBKEY_BASE64")?.to_string(),
    );

    if let Some(settings) = user_state.app_settings.get(app_id) {
        let parsed_settings: serde_json::Value = serde_json::from_str(settings)?;
        tera_ctx.insert("settings", &parsed_settings);
    }

    #[cfg(not(feature = "test-parser"))]
    let node_ip = get_node_ip(&kube_client).await?;

    #[cfg(feature = "test-parser")]
    let node_ip = "127.0.0.1".to_string();

    tera_ctx.insert(
        "server_is_public",
        &node_ip
            .parse()
            .map(|ip| !is_ip_private(&ip))
            .ok()
            .unwrap_or_default(),
    );
    tera_ctx.insert("server_ip", &node_ip);

    let plugins = plugins::call_plugins(
        #[cfg(not(feature = "test-parser"))]
        kube_client,
        app_id,
        perms,
        additional_metadata,
        user_state,
    )
    .await?;
    tera_ctx.insert("plugins", &plugins);
    Ok(tera_ctx)
}
