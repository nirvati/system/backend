// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::{bail, Result};
use base64::prelude::*;
use regex::Regex;
use slugify::slugify;
use std::collections::BTreeMap;
use std::sync::LazyLock;

use crate::apps::parser::app_yml::v1::types::app::{Container, Job, Runnable};
use nirvati_apps::internal::{
    port_config_for_pod_or_ingress, types as output, CommonComponentConfig, CustomResource,
    DnsConfig, IngressType, NetworkPolicy, NetworkPolicyEgress, NetworkPolicyIngress,
};
use nirvati_apps::internal::{
    types::{InternalAppRepresentation, Volume},
    Container as OutContainer, HostVolume, Ingress, LonghornVolume, PluginInfo,
    Runnable as OutRunnable, Secret, SecretMount, Service as OutService,
    ServiceAccount as OutServiceAccount, ServiceAccountInner as OutServiceAccountInner,
    ServiceType,
};
use nirvati_apps::metadata::Metadata;

use super::types::AppYml;

pub static PATH_PREFIX_VALIDATION: LazyLock<Regex> =
    LazyLock::new(|| Regex::new(r"^[A-z/\-_0-9]+$").unwrap());

fn validate_path_prefix(path_prefix: &str) -> Result<()> {
    if !PATH_PREFIX_VALIDATION.is_match(path_prefix) {
        bail!("Path prefix {} is invalid!", path_prefix);
    }
    Ok(())
}

pub fn convert_mounts(
    app_id: &str,
    result: &mut OutContainer,
    input_service: &Container,
    metadata: &Metadata,
) -> Result<()> {
    for (volume_path, container_dir) in &input_service.mounts.data {
        let name = volume_path.split('/').next().unwrap().to_owned();
        if name.is_empty() || name.len() > 63 {
            bail!("Volume name {} is invalid!", name);
        }
        if !metadata.volumes.contains_key(&name) {
            bail!(
                "Volume {} does not exist, mounted by {}",
                volume_path,
                app_id
            );
        };
        result.volumes.push(Volume::Longhorn(LonghornVolume {
            name: name.clone(),
            mount_path: container_dir.to_owned(),
            sub_path: if volume_path.contains('/') {
                Some(
                    volume_path
                        .split('/')
                        .skip(1)
                        .collect::<Vec<_>>()
                        .to_owned()
                        .join("/"),
                )
            } else {
                None
            },
            is_readonly: false,
            from_app: None,
            from_ns: None,
        }))
    }
    for (secret_name, container_dir) in &input_service.mounts.secrets {
        result.volumes.push(Volume::Secret(SecretMount {
            name: secret_name.clone(),
            mount_path: container_dir.to_owned(),
        }))
    }
    for (host_dir, container_dir) in &input_service.mounts.host_paths {
        let name = slugify!(host_dir);
        result.volumes.push(Volume::Host(HostVolume {
            name: name.clone(),
            mount_path: container_dir.to_owned(),
            host_path: host_dir.to_owned(),
            is_readonly: false,
        }))
    }
    Ok(())
}

pub async fn convert_app_yml(
    app_id: &str,
    user_id: &str,
    app_yml: &AppYml,
    metadata: &Metadata,
    others: Vec<serde_yaml::Value>,
    module_id: Option<String>,
) -> Result<InternalAppRepresentation> {
    let mut services = BTreeMap::new();
    let mut containers = BTreeMap::new();
    let mut ingress = Vec::new();
    let mut plugins = BTreeMap::new();
    let mut secrets = Vec::new();
    for (secret_name, secret) in &app_yml.secrets {
        let mut data: BTreeMap<String, Box<[u8]>> = secret
            .data
            .iter()
            .filter_map(|(k, v)| match BASE64_STANDARD.decode(v) {
                Ok(bytes) => Some((k.to_owned(), bytes.into_boxed_slice())),
                Err(decoding_error) => {
                    tracing::warn!(
                        "Failed to decode secret {} of app {}: {}. If you are passing a non-base64 string, please put it into string_data instead of data.",
                        secret_name,
                        app_id,
                        decoding_error
                    );
                    None
                }
            })
            .collect();
        for (key, val) in secret.string_data.iter() {
            data.insert(key.to_owned(), val.as_bytes().to_owned().into_boxed_slice());
        }
        secrets.push(Secret {
            name: secret_name.to_owned(),
            data,
        });
    }
    let primary_ingresses = app_yml
        .ingress
        .iter()
        .filter(|ingress| ingress.path_prefix.is_none())
        .collect::<Vec<_>>();
    let primary_http_ingresses = primary_ingresses
        .iter()
        .filter(|ingress| ingress.is_http_fallback)
        .collect::<Vec<_>>();
    let primary_https_ingresses = primary_ingresses
        .iter()
        .filter(|ingress| !ingress.is_http_fallback)
        .collect::<Vec<_>>();
    if primary_https_ingresses.len() > 1 || primary_http_ingresses.len() > 1 {
        bail!("Multiple primary ingresses are not allowed!");
    }
    let has_primary_http_ingress = !primary_http_ingresses.is_empty();
    let has_primary_https_ingress = !primary_https_ingresses.is_empty();
    let app_network_policy = app_yml
        .network_policy
        .clone()
        .map(|rule| rule.into())
        .unwrap_or_else(|| NetworkPolicy {
            ingress: NetworkPolicyIngress {
                allow_world: CommonComponentConfig {
                    allow: false,
                    allowed_ports: None,
                },
                allow_cluster: CommonComponentConfig {
                    allow: false,
                    allowed_ports: None,
                },
                allow_same_ns: CommonComponentConfig {
                    allow: true,
                    allowed_ports: None,
                },
                cidrs: vec![],
                pods: vec![],
            },
            egress: NetworkPolicyEgress {
                allow_cluster: false,
                allow_same_ns: true,
                allow_kube_api: false,
                allow_world: true,
                allow_local_net: false,
                dns: Some(DnsConfig {
                    allow: true,
                    allowed_lookup_names: None,
                    allowed_lookup_pattern: None,
                }),
                fqdns: vec![],
                cidrs: vec![],
                services: vec![],
                pods: vec![],
            },
        });
    for (container_id, runnable) in &app_yml.containers {
        let container = runnable.get_container();
        if let Runnable::Deployment(deployment) = runnable {
            let mut internal_ports: output::Ports = deployment.internal_ports.clone().into();
            if let Some(main_port) = deployment.ingress_port {
                internal_ports.tcp.entry(main_port).or_insert(main_port);
                if let Some(ref path_prefix) = deployment.path_prefix {
                    if !has_primary_https_ingress && container_id.as_str() == "main" {
                        bail!("Main service is not allowed to have a path prefix!");
                    }
                    validate_path_prefix(path_prefix)?;
                }
                if deployment.path_prefix.is_some()
                    || (!has_primary_https_ingress && container_id.as_str() == "main")
                {
                    ingress.push(Ingress {
                        target_service: Some(container_id.to_owned()),
                        path_prefix: deployment.path_prefix.clone(),
                        target_port: Some(main_port),
                        enable_compression: container.enable_http_compression,
                        strip_prefix: container.strip_prefix,
                        target_app: None,
                        target_ns: None,
                        auth_exclude: deployment.auth_exclude,
                        r#type: if metadata.raw_ingress {
                            IngressType::TlsTcp
                        } else {
                            IngressType::Https
                        },
                        component: module_id.clone(),
                    });
                };
            };
            if let Some(http_port) = deployment.http_ingress_port {
                internal_ports.tcp.entry(http_port).or_insert(http_port);
                if let Some(ref path_prefix) = deployment.path_prefix {
                    if !has_primary_http_ingress && container_id.as_str() == "main" {
                        bail!("Main service is not allowed to have a path prefix!");
                    }
                    validate_path_prefix(path_prefix)?;
                }
                if deployment.path_prefix.is_some()
                    || (!has_primary_http_ingress && container_id.as_str() == "main")
                {
                    ingress.push(Ingress {
                        target_service: Some(container_id.to_owned()),
                        path_prefix: deployment.path_prefix.clone(),
                        target_port: Some(http_port),
                        enable_compression: container.enable_http_compression,
                        strip_prefix: container.strip_prefix,
                        target_app: None,
                        target_ns: None,
                        auth_exclude: deployment.auth_exclude,
                        r#type: IngressType::HttpFallback,
                        component: module_id.clone(),
                    });
                };
            };
            for (plugin_app, plugin) in &deployment.plugins {
                plugins.insert(
                    plugin_app.clone(),
                    PluginInfo {
                        endpoint: format!(
                            "http://{}.{}.{}.nirvati:{}{}",
                            container_id,
                            app_id,
                            user_id,
                            plugin.port,
                            plugin.path.clone().unwrap_or("/".to_owned())
                        ),
                        r#type: plugin.r#type.into(),
                        display_name: plugin.display_name.clone(),
                    },
                );
                internal_ports.tcp.entry(plugin.port).or_insert(plugin.port);
            }

            if !internal_ports.is_empty() {
                services.insert(
                    container_id.clone(),
                    OutService {
                        target_container: container_id.clone(),
                        r#type: ServiceType::ClusterIp,
                        ports: internal_ports,
                        cluster_ip: None,
                        network_policy: None,
                    },
                );
            }
            if !deployment.public_ports.is_empty() {
                let svc_name = container_id.clone() + "-public";
                if services.contains_key(&svc_name) {
                    bail!("Implicitly created services for public ports use the -public suffix, but a service named {} already exists!", svc_name);
                }
                let ports: output::Ports = deployment.public_ports.clone().into();
                services.insert(
                    svc_name.clone(),
                    OutService {
                        target_container: container_id.clone(),
                        r#type: ServiceType::LoadBalancer,
                        ports: ports.clone(),
                        cluster_ip: None,
                        network_policy: Some(NetworkPolicyIngress {
                            allow_world: CommonComponentConfig {
                                allow: true,
                                allowed_ports: Some(port_config_for_pod_or_ingress(ports.clone())),
                            },
                            allow_cluster: CommonComponentConfig {
                                allow: true,
                                allowed_ports: Some(port_config_for_pod_or_ingress(ports.clone())),
                            },
                            allow_same_ns: CommonComponentConfig {
                                allow: true,
                                allowed_ports: Some(port_config_for_pod_or_ingress(ports)),
                            },
                            cidrs: vec![],
                            pods: vec![],
                        }),
                    },
                );
            }
        };
        let is_job = match runnable {
            Runnable::CronJob(_) | Runnable::Job(_) => true,
            Runnable::Deployment(_) => false,
        };
        let mut network_policy = container.network_policy.clone().map(|rule| {
            let mut rule: NetworkPolicy = rule.into();
            if container.service_account.is_some() {
                rule.egress.allow_kube_api = true;
            };
            rule
        });
        if container.service_account.is_some()
            && container.network_policy.is_none()
            && !app_network_policy.egress.allow_kube_api
        {
            let mut new_policy = app_network_policy.clone();
            new_policy.egress.allow_kube_api = true;
            network_policy = Some(new_policy);
        };
        let mut result_service = OutContainer {
            image: container.image.clone(),
            restart: if is_job {
                Some(
                    container
                        .restart
                        .clone()
                        .unwrap_or_else(|| "on-failure".to_string()),
                )
            } else {
                container.restart.clone()
            },
            stop_grace_period: container.stop_grace_period.clone(),
            uid: container
                .user
                .clone()
                .map(|p| p.split(':').next().unwrap().parse().unwrap_or(0)),
            gid: if let Some(user) = container.user.clone() {
                user.split(':').nth(1).map(|p| p.parse().unwrap())
            } else {
                None
            },
            working_dir: container.working_dir.clone(),
            shm_size: container.shm_size.clone(),
            volumes: Vec::new(),
            cap_add: container.cap_add.clone(),
            cap_drop: container.cap_drop.clone(),
            command: container.command.clone().map(|cmd| cmd.into()),
            entrypoint: container.entrypoint.clone().map(|cmd| cmd.into()),
            host_network: container.host_network,
            environment: container
                .environment
                .clone()
                .into_iter()
                .map(|(k, v)| (k, v.into()))
                .collect(),
            ulimits: container.ulimits.clone(),
            privileged: container.privileged,
            fs_group: container.fs_group,
            has_permissions: container.has_permissions.clone(),
            network_policy,
            service_account: container
                .service_account
                .clone()
                .map(|sa| OutServiceAccount {
                    cluster_rules: sa.cluster_rules,
                    inner: OutServiceAccountInner {
                        rules: sa.inner.rules,
                        builtin_roles: sa.inner.builtin_roles,
                    },
                    builtin_cluster_roles: sa.builtin_cluster_roles,
                    other_ns: sa
                        .other_ns
                        .into_iter()
                        .map(|(k, v)| {
                            (
                                k,
                                OutServiceAccountInner {
                                    rules: v.rules,
                                    builtin_roles: v.builtin_roles,
                                },
                            )
                        })
                        .collect(),
                }),
            ..Default::default()
        };
        if let Runnable::Deployment(deployment) = runnable {
            for (_public_port, internal_port) in deployment.internal_ports.tcp.iter() {
                result_service.exposes.tcp.push(*internal_port);
            }
            for (_public_port, internal_port) in deployment.internal_ports.udp.iter() {
                result_service.exposes.udp.push(*internal_port);
            }
        }

        convert_mounts(app_id, &mut result_service, container, metadata)?;
        containers.insert(
            container_id.to_owned(),
            match runnable {
                Runnable::Deployment(depl) => {
                    OutRunnable::Deployment(Box::new(output::Deployment {
                        container: result_service,
                        middlewares: depl
                            .middlewares
                            .iter()
                            .map(|(mw_id, middleware)| (mw_id.clone(), middleware.clone().into()))
                            .collect(),
                    }))
                }
                Runnable::Job(job) => match job {
                    Job::OneTime(_) => OutRunnable::Once(Box::new(output::Job {
                        container: result_service,
                    })),
                    Job::OnUpdate(_) => OutRunnable::OnAppUpdate(Box::new(output::Job {
                        container: result_service,
                    })),
                },
                Runnable::CronJob(cron_job) => OutRunnable::Cron(Box::new(output::CronJob {
                    schedule: cron_job.schedule.clone(),
                    container: result_service,
                })),
            },
        );
    }

    for (service_id, svc) in &app_yml.services {
        if services.contains_key(service_id) {
            bail!(
                "Service {} conflicts with implicitly created service by a container of the same name.",
                service_id
            );
        }
        let OutRunnable::Deployment(target_container) =
            containers.get_mut(&svc.target_container).ok_or_else(|| {
                anyhow::anyhow!(
                    "Service {} targets non-existent container {}",
                    service_id,
                    svc.target_container
                )
            })?
        else {
            bail!(
                "Service {} targets non-deployment container {}",
                service_id,
                svc.target_container
            );
        };
        let ports: output::Ports = svc.ports.clone().into();
        for port in ports.tcp.values() {
            target_container.container.exposes.tcp.push(*port);
        }
        for port in ports.udp.values() {
            target_container.container.exposes.udp.push(*port);
        }
        services.insert(
            service_id.clone(),
            OutService {
                target_container: svc.target_container.clone(),
                r#type: svc.r#type.into(),
                ports: ports.clone(),
                cluster_ip: svc.cluster_ip.clone(),
                network_policy: Some(svc.network_policy.clone().map(|pol| pol.into()).unwrap_or(
                    NetworkPolicyIngress {
                        allow_world: CommonComponentConfig {
                            allow: svc.r#type == super::types::app::ServiceType::LoadBalancer,
                            allowed_ports: Some(port_config_for_pod_or_ingress(ports.clone())),
                        },
                        allow_cluster: CommonComponentConfig {
                            allow: svc.r#type == super::types::app::ServiceType::LoadBalancer,
                            allowed_ports: Some(port_config_for_pod_or_ingress(ports.clone())),
                        },
                        allow_same_ns: CommonComponentConfig {
                            allow: true,
                            allowed_ports: Some(port_config_for_pod_or_ingress(ports)),
                        },
                        cidrs: vec![],
                        pods: vec![],
                    },
                )),
            },
        );
        if svc.ingress_port.is_some()
            || (!has_primary_https_ingress && service_id.as_str() == "main")
        {
            let Some(ingress_port) = svc.ingress_port else {
                bail!("The ingress target service must have an ingress port!");
            };
            if let Some(ref path_prefix) = svc.path_prefix {
                if !has_primary_https_ingress && service_id.as_str() == "main" {
                    bail!("Main service is not allowed to have a path prefix!");
                }
                validate_path_prefix(path_prefix)?;
            }
            ingress.push(Ingress {
                target_service: Some(service_id.to_owned()),
                path_prefix: svc.path_prefix.clone(),
                target_port: Some(ingress_port),
                enable_compression: svc.enable_http_compression,
                strip_prefix: svc.strip_prefix,
                target_app: None,
                target_ns: None,
                auth_exclude: svc.auth_exclude,
                r#type: if metadata.raw_ingress {
                    IngressType::TlsTcp
                } else {
                    IngressType::Https
                },
                component: module_id.clone(),
            });
        }
        if let Some(http_port) = svc.http_ingress_port {
            if let Some(ref path_prefix) = svc.path_prefix {
                if !has_primary_http_ingress && service_id.as_str() == "main" {
                    bail!("Main service is not allowed to have a path prefix!");
                }
                validate_path_prefix(path_prefix)?;
            }
            ingress.push(Ingress {
                target_service: Some(service_id.to_owned()),
                path_prefix: svc.path_prefix.clone(),
                target_port: Some(http_port),
                enable_compression: svc.enable_http_compression,
                strip_prefix: svc.strip_prefix,
                target_app: None,
                target_ns: None,
                auth_exclude: svc.auth_exclude,
                r#type: if metadata.raw_ingress {
                    IngressType::TlsTcp
                } else {
                    IngressType::Https
                },
                component: module_id.clone(),
            });
        }
    }
    for ingress_route in &app_yml.ingress {
        ingress.push(Ingress {
            target_service: ingress_route.target_svc.clone(),
            target_app: ingress_route.target_app.clone(),
            target_ns: ingress_route.target_ns.clone(),
            path_prefix: ingress_route.path_prefix.clone(),
            target_port: ingress_route.port,
            enable_compression: ingress_route.enable_http_compression,
            strip_prefix: ingress_route.strip_prefix,
            auth_exclude: ingress_route.auth_exclude,
            r#type: if metadata.raw_ingress {
                IngressType::TlsTcp
            } else if ingress_route.is_http_fallback {
                IngressType::HttpFallback
            } else {
                IngressType::Https
            },
            component: module_id.clone().or(ingress_route.component.clone()),
        })
    }
    let mut encountered_prefixes: Vec<&str> = Vec::new();
    for ingress in &ingress {
        if let Some(ref prefix) = ingress.path_prefix {
            let normalized_prefix = prefix.trim_end_matches('/');
            if encountered_prefixes.contains(&normalized_prefix) {
                bail!("Path prefix {} is used by multiple services!", prefix);
            }
            encountered_prefixes.push(normalized_prefix);
        }
    }
    Ok(InternalAppRepresentation::new(
        metadata.clone(),
        containers,
        services,
        ingress,
        secrets,
        plugins,
        app_yml
            .other
            .iter()
            .flat_map(|(app_id, cr_map)| {
                cr_map.iter().map(|(plugin_id, cr)| CustomResource {
                    app: app_id.clone(),
                    plugin_id: plugin_id.clone(),
                    definition: cr.clone(),
                })
            })
            .collect(),
        Some(app_network_policy),
        others,
    ))
}
