// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

mod permissions;

use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};

use crate::apps::parser::app_yml::v1::types::metadata::permissions::Permission;
use crate::utils::si_to_bytes;
use nirvati_apps::utils::true_default;

use super::shared::MultiLanguageItem;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, Hash)]
#[serde(untagged)]
pub enum Dependency {
    OneDependency(String),
    AlternativeDependency(Vec<String>),
}

impl From<Dependency> for nirvati_apps::metadata::Dependency {
    fn from(dependency: Dependency) -> Self {
        match dependency {
            Dependency::OneDependency(dep) => {
                nirvati_apps::metadata::Dependency::OneDependency(dep)
            }
            Dependency::AlternativeDependency(deps) => {
                nirvati_apps::metadata::Dependency::AlternativeDependency(deps)
            }
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[serde(tag = "type")]
pub enum Setting {
    Enum {
        name: MultiLanguageItem,
        description: MultiLanguageItem,
        #[serde(default = "Vec::default")]
        #[serde(skip_serializing_if = "Vec::<String>::is_empty")]
        values: Vec<String>,
        default: Option<String>,
    },
    String {
        name: MultiLanguageItem,
        description: MultiLanguageItem,
        default: Option<String>,
        max_len: Option<usize>,
        string_type: Option<StringType>,
        #[serde(default = "true_default")]
        required: bool,
        placeholder: Option<MultiLanguageItem>,
    },
    Bool {
        name: MultiLanguageItem,
        description: MultiLanguageItem,
        default: bool,
    },
    Int {
        name: MultiLanguageItem,
        description: MultiLanguageItem,
        default: Option<i64>,
        min: Option<i64>,
        max: Option<i64>,
        step_size: Option<i64>,
    },
    Float {
        name: MultiLanguageItem,
        description: MultiLanguageItem,
        default: Option<f64>,
        min: Option<f64>,
        max: Option<f64>,
        step_size: Option<f64>,
    },
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum StringType {
    #[serde(rename = "password")]
    Password,
    #[serde(rename = "email")]
    Email,
    #[serde(rename = "url")]
    Url,
    #[serde(rename = "ip")]
    Ip,
}

impl From<Setting> for nirvati_apps::metadata::Setting {
    fn from(value: Setting) -> Self {
        match value {
            Setting::Enum {
                name,
                description,
                values,
                default,
            } => nirvati_apps::metadata::Setting::Enum {
                name: name.into(),
                description: description.into(),
                values,
                default,
            },
            Setting::String {
                name,
                description,
                default,
                max_len,
                string_type,
                required,
                placeholder,
            } => nirvati_apps::metadata::Setting::String {
                name: name.into(),
                description: description.into(),
                default,
                max_len,
                string_type: string_type.map(|s| match s {
                    StringType::Password => nirvati_apps::metadata::StringType::Password,
                    StringType::Email => nirvati_apps::metadata::StringType::Email,
                    StringType::Url => nirvati_apps::metadata::StringType::Url,
                    StringType::Ip => nirvati_apps::metadata::StringType::Ip,
                }),
                required,
                placeholder: placeholder.map(|p| p.into()),
            },
            Setting::Bool {
                name,
                description,
                default,
            } => nirvati_apps::metadata::Setting::Bool {
                name: name.into(),
                description: description.into(),
                default,
            },
            Setting::Int {
                name,
                description,
                default,
                min,
                max,
                step_size,
            } => nirvati_apps::metadata::Setting::Int {
                name: name.into(),
                description: description.into(),
                default,
                min,
                max,
                step_size,
            },
            Setting::Float {
                name,
                description,
                default,
                min,
                max,
                step_size,
            } => nirvati_apps::metadata::Setting::Float {
                name: name.into(),
                description: description.into(),
                default,
                min,
                max,
                step_size,
            },
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, Default)]
pub enum AppType {
    #[default]
    App,
    Library,
    SystemLibrary,
}

#[derive(Serialize, Deserialize, Copy, Clone, Debug, PartialEq, Eq, Default)]
pub enum AppScope {
    #[default]
    User,
    System,
}

impl From<AppScope> for nirvati_apps::metadata::AppScope {
    fn from(val: AppScope) -> Self {
        match val {
            AppScope::User => nirvati_apps::metadata::AppScope::User,
            AppScope::System => nirvati_apps::metadata::AppScope::System,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct InputDeclaration {
    pub label: MultiLanguageItem,
    pub description: MultiLanguageItem,
}

impl From<InputDeclaration> for nirvati_apps::metadata::InputDeclaration {
    fn from(val: InputDeclaration) -> Self {
        Self {
            label: val.label.into(),
            description: val.description.into(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct StorePlugin {
    pub name: String,
    pub icon: String,
    pub description: String,
    pub id: String,
    pub inputs: BTreeMap<String, InputDeclaration>,
}

impl From<StorePlugin> for nirvati_apps::metadata::StorePlugin {
    fn from(val: StorePlugin) -> Self {
        Self {
            name: val.name,
            icon: val.icon,
            description: val.description,
            id: val.id,
            inputs: val.inputs.into_iter().map(|(k, v)| (k, v.into())).collect(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct UiMenuEntry {
    pub name: MultiLanguageItem,
    // Icon name, must be either a raw SVG or a name of a heroicons icon
    pub icon: String,
    pub path: String,
}

impl From<UiMenuEntry> for nirvati_apps::metadata::UiMenuEntry {
    fn from(val: UiMenuEntry) -> Self {
        Self {
            name: val.name.into(),
            icon: val.icon,
            path: val.path,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct UiModule {
    pub menu_entries: Vec<UiMenuEntry>,
}

impl From<UiModule> for nirvati_apps::metadata::UiModule {
    fn from(val: UiModule) -> Self {
        Self {
            menu_entries: val
                .menu_entries
                .into_iter()
                .map(|entry| entry.into())
                .collect(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct AppComponent {
    /// The component id
    pub id: String,
    /// The name of the component
    pub name: MultiLanguageItem,
    /// A description of the component
    pub description: MultiLanguageItem,
    /// Whether the component is required
    pub required: bool,
}

impl From<AppComponent> for nirvati_apps::metadata::AppComponent {
    fn from(val: AppComponent) -> Self {
        Self {
            id: val.id,
            name: val.name.into(),
            description: val.description.into(),
            required: val.required,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct InputMetadata {
    /// The name of the app
    pub name: MultiLanguageItem,
    /// The version of the app
    #[serde(deserialize_with = "deserialize_version")]
    pub version: semver::Version,
    /// The version of the app to display
    /// This is useful if the app is not using semver
    pub display_version: Option<String>,
    /// The category for the app
    pub category: MultiLanguageItem,
    /// A short tagline for the app
    pub tagline: MultiLanguageItem,
    // Developer name -> their website
    pub developers: BTreeMap<String, String>,
    /// A description of the app
    pub description: MultiLanguageItem,
    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    /// Other apps this app depends on
    pub dependencies: Vec<Dependency>,
    /// App repository name -> repo URL
    pub repos: BTreeMap<String, String>,
    /// A support link for the app
    pub support: String,
    /// A list of promo images for the apps
    #[serde(default)]
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub gallery: Vec<String>,
    /// The URL to the app icon
    pub icon: Option<String>,
    /// The path the "Open" link on the dashboard should lead to
    #[serde(skip_serializing_if = "Option::is_none")]
    pub path: Option<String>,
    /// The app's default username
    #[serde(skip_serializing_if = "Option::is_none")]
    pub default_username: Option<String>,
    /// The app's default password.
    pub default_password: Option<String>,
    /// A list of containers to update automatically (still validated by the Citadel team)
    #[serde(skip_serializing_if = "Option::is_none")]
    pub update_containers: Option<Vec<String>>,
    /// For "virtual" apps, the service the app implements
    #[serde(skip_serializing_if = "Option::is_none")]
    pub implements: Option<String>,
    #[serde(
        default,
        skip_serializing_if = "BTreeMap::<String, MultiLanguageItem>::is_empty"
    )]
    pub release_notes: BTreeMap<String, MultiLanguageItem>,
    /// Permissions this app requests
    /// This will be used as a "guide" for the app manager if an app requests access to a resource
    /// That is provided by multiple permissions
    #[serde(
        default = "Vec::default",
        skip_serializing_if = "Vec::<String>::is_empty"
    )]
    pub has_permissions: Vec<String>,
    #[serde(default = "Vec::default", skip_serializing_if = "Vec::is_empty")]
    pub exposes_permissions: Vec<Permission>,
    /// The SPDX identifier of the app license
    #[serde(default = "unknown_license")]
    pub license: String,
    /// Config files to automatically overwrite when updating
    #[serde(skip_serializing_if = "Vec::is_empty", default)]
    pub update_config_files: Vec<String>,
    /// Exported data shared with plugins (Map plugin -> data)
    #[serde(skip_serializing_if = "Option::is_none")]
    pub exported_data: Option<BTreeMap<String, BTreeMap<String, serde_json::Value>>>,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub settings: BTreeMap<String, Setting>,
    /// Volumes this app uses (Can be mounted in mounts.data)
    /// Volumes need to be declared explicitly, and have a default size limit of 10Gi
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub volumes: BTreeMap<String, Volume>,
    // Backwards compat, replaced by "scope"
    #[serde(default)]
    pub r#type: AppType,
    pub scope: Option<AppScope>,
    #[serde(default)]
    pub store_plugins: Vec<StorePlugin>,
    #[serde(default)]
    pub ui_module: Option<UiModule>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub allowed_scopes: Option<Vec<AppScope>>,
    pub raw_ingress: Option<bool>,
    pub post_install_redirect: Option<String>,
    #[serde(default)]
    pub components: Vec<AppComponent>,
}

pub fn deserialize_version<'de, D>(deserializer: D) -> Result<semver::Version, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    let s = s.trim_start_matches('v');
    semver::Version::parse(s).map_err(serde::de::Error::custom)
}

pub fn unknown_license() -> String {
    "Unknown".to_string()
}

#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq, Eq)]
pub struct Volume {
    pub minimum_size: String,
    pub recommended_size: String,
    pub name: MultiLanguageItem,
    pub description: MultiLanguageItem,
}

impl TryFrom<Volume> for nirvati_apps::metadata::Volume {
    type Error = std::io::Error;

    fn try_from(val: Volume) -> Result<Self, Self::Error> {
        Ok(nirvati_apps::metadata::Volume {
            minimum_size: si_to_bytes(&val.minimum_size)?,
            recommended_size: si_to_bytes(&val.recommended_size)?,
            name: nirvati::utils::MultiLanguageItem(val.name.into()),
            description: nirvati::utils::MultiLanguageItem(val.description.into()),
        })
    }
}
