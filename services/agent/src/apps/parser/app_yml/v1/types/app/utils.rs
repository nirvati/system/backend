// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use serde::{Deserialize, Serialize};

use nirvati_apps::internal;
use nirvati_apps::utils::StringLike;

#[derive(Serialize, Deserialize, Clone, Copy, Debug, PartialEq)]
pub enum PluginType {
    #[serde(rename = "context")]
    Context,
    #[serde(rename = "source")]
    Source,
    #[serde(rename = "runtime")]
    Runtime,
    #[serde(rename = "custom-resource")]
    CustomResource,
}

impl From<PluginType> for internal::types::PluginType {
    fn from(value: PluginType) -> Self {
        match value {
            PluginType::Context => Self::Context,
            PluginType::Source => Self::Source,
            PluginType::Runtime => Self::Runtime,
            PluginType::CustomResource => Self::CustomResource,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct Plugin {
    pub port: u16,
    pub path: Option<String>,
    pub r#type: PluginType,
    pub display_name: String,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct SecretRef {
    pub secret: String,
    pub key: String,
}

impl From<SecretRef> for internal::types::SecretRef {
    fn from(value: SecretRef) -> Self {
        Self {
            secret: value.secret,
            key: value.key,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[serde(untagged)]
pub enum EnvVar {
    StringLike(StringLike),
    SecretRef(SecretRef),
}

impl From<EnvVar> for internal::types::EnvVar {
    fn from(value: EnvVar) -> Self {
        match value {
            EnvVar::StringLike(v) => Self::StringLike(v),
            EnvVar::SecretRef(v) => Self::SecretRef(v.into()),
        }
    }
}
