use serde::{Deserialize, Serialize};
use std::collections::HashMap;

mod convert;

fn default_true() -> bool {
    true
}

fn default_false_component() -> CommonComponentConfig {
    CommonComponentConfig::Allow(false)
}

fn default_true_component() -> CommonComponentConfig {
    CommonComponentConfig::Allow(true)
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct NetworkPolicy {
    pub ingress: NetworkPolicyIngress,
    pub egress: NetworkPolicyEgress,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct NetworkPolicyIngress {
    #[serde(default = "default_false_component")]
    pub allow_world: CommonComponentConfig,
    #[serde(default = "default_false_component")]
    pub allow_cluster: CommonComponentConfig,
    #[serde(default = "default_true_component")]
    pub allow_same_ns: CommonComponentConfig,
    #[serde(default)]
    pub cidrs: Vec<Cidr>,
    #[serde(default)]
    pub pods: Vec<PodConfig>,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum CommonComponentConfig {
    Full(CommonComponentFull),
    Allow(bool),
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct CommonComponentFull {
    pub allow: bool,
    pub allowed_ports: Option<Vec<PortConfig>>,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum PortConfig {
    JustPort(u16),
    PortAndProtocol(PortConfigFull),
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct PortConfigFull {
    pub port: u16,
    pub protocol: Protocol,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct NetworkPolicyEgress {
    #[serde(default)]
    pub allow_cluster: bool,
    #[serde(default = "default_true")]
    pub allow_same_ns: bool,
    #[serde(default)]
    pub allow_kube_api: bool,
    #[serde(default = "default_true")]
    pub allow_world: bool,
    #[serde(default)]
    pub allow_local_net: bool,
    #[serde(default)]
    pub dns: Option<DnsConfig>,
    #[serde(default)]
    pub fqdns: Vec<FqdnConfig>,
    #[serde(default)]
    pub cidrs: Vec<Cidr>,
    #[serde(default)]
    pub services: Vec<ServiceConfig>,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct DnsConfig {
    pub allow: bool,
    /// The lookups to allow, if not specified, all lookups are allowed
    pub allowed_lookup_names: Option<Vec<String>>,
    pub allowed_lookup_pattern: Option<Vec<String>>,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct FqdnConfig {
    pub match_name: Option<String>,
    pub match_pattern: Option<String>,
    pub allowed_ports: Option<Vec<PortConfig>>,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize, Default)]
pub enum Protocol {
    #[serde(alias = "tcp", alias = "TCP")]
    Tcp,
    #[serde(alias = "udp", alias = "UDP")]
    Udp,
    #[serde(alias = "sctp", alias = "SCTP")]
    Sctp,
    #[serde(alias = "any", alias = "ANY")]
    #[default]
    Any,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct Cidr {
    pub cidr: String,
    pub except: Option<Vec<String>>,
    pub allowed_ports: Option<Vec<PortConfig>>,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct PodConfig {
    pub namespace: Option<String>,
    pub app: Option<String>,
    pub container_name: Option<String>,
    pub match_labels: Option<HashMap<String, String>>,
    pub allowed_ports: Option<Vec<PortConfig>>,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct ServiceConfig {
    pub namespace: Option<String>,
    pub service_name: String,
    pub allowed_ports: Option<Vec<PortConfig>>,
}
