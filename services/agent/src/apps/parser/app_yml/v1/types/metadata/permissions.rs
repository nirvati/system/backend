// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};

use nirvati_apps::metadata::Permission as OutputPermission;
use nirvati_apps::metadata::SvcPorts as OutputSvcPorts;

use crate::apps::parser::app_yml::types::MultiLanguageItem;

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Default)]
pub struct SvcPortsDeclaration {
    pub udp: Vec<u16>,
    pub tcp: Vec<u16>,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug)]
#[serde(untagged)]
pub enum SvcPorts {
    WithProtocol(SvcPortsDeclaration),
    Tcp(Vec<u16>),
}

impl From<SvcPorts> for OutputSvcPorts {
    fn from(value: SvcPorts) -> Self {
        match value {
            SvcPorts::WithProtocol(ports) => OutputSvcPorts {
                udp: ports.udp,
                tcp: ports.tcp,
            },
            SvcPorts::Tcp(ports) => OutputSvcPorts {
                udp: vec![],
                tcp: ports,
            },
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq, Eq)]
pub struct Permission {
    pub id: String,
    pub name: MultiLanguageItem,
    pub description: MultiLanguageItem,
    /// Other permissions this permission implies
    /// May also contain permissions of other apps
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub includes: Vec<String>,
    /// Secrets (+ keys) accessible with this permission
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub secrets: BTreeMap<String, Vec<String>>,
    /// Makes this permission "invisible" (Hidden from the UI) if requested by the apps listed in this field
    /// The * wildcard can be used to hide from all apps
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub hidden: Vec<String>,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub services: BTreeMap<String, SvcPorts>,
    /// For system apps, set this to true to allow all users to access this permission
    /// If false (default), only other system apps can access this permission
    #[serde(default = "bool::default")]
    pub open_to_all_users: bool,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub volumes: Vec<VolumePermission>,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
#[serde(untagged)]
pub enum VolumePermission {
    VolumeName(String),
    VolumeSubPaths(VolumeWithSubPaths),
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct VolumeWithSubPaths {
    pub volume: String,
    #[serde(default)]
    pub sub_paths: Option<Vec<String>>,
    #[serde(default)]
    pub is_readonly: bool,
}

impl From<Permission> for OutputPermission {
    fn from(value: Permission) -> Self {
        OutputPermission {
            id: value.id,
            name: value.name.into(),
            description: value.description.into(),
            includes: value.includes,
            secrets: value.secrets,
            hidden: value.hidden,
            services: value
                .services
                .into_iter()
                .map(|(k, v)| (k, v.into()))
                .collect(),
            open_to_all_users: value.open_to_all_users,
            volumes: value
                .volumes
                .into_iter()
                .map(|v| match v {
                    VolumePermission::VolumeName(name) => {
                        nirvati_apps::metadata::VolumePermission {
                            volume: name,
                            sub_paths: None,
                        }
                    }
                    VolumePermission::VolumeSubPaths(v) => {
                        nirvati_apps::metadata::VolumePermission {
                            volume: v.volume,
                            sub_paths: v.sub_paths,
                        }
                    }
                })
                .collect(),
        }
    }
}
