use nirvati_apps::internal::types::networkpolicy as internal;

use super::*;

impl From<NetworkPolicy> for internal::NetworkPolicy {
    fn from(network_policy: NetworkPolicy) -> Self {
        internal::NetworkPolicy {
            ingress: network_policy.ingress.into(),
            egress: network_policy.egress.into(),
        }
    }
}

impl From<NetworkPolicyIngress> for internal::NetworkPolicyIngress {
    fn from(network_policy_ingress: NetworkPolicyIngress) -> Self {
        internal::NetworkPolicyIngress {
            allow_world: network_policy_ingress.allow_world.into(),
            allow_cluster: network_policy_ingress.allow_cluster.into(),
            allow_same_ns: network_policy_ingress.allow_same_ns.into(),
            cidrs: network_policy_ingress
                .cidrs
                .into_iter()
                .map(|cidr| cidr.into())
                .collect(),
            pods: network_policy_ingress
                .pods
                .into_iter()
                .map(|pod| pod.into())
                .collect(),
        }
    }
}

impl From<CommonComponentConfig> for internal::CommonComponentConfig {
    fn from(common_component_config: CommonComponentConfig) -> Self {
        match common_component_config {
            CommonComponentConfig::Full(common_component_full) => internal::CommonComponentConfig {
                allow: common_component_full.allow,
                allowed_ports: common_component_full.allowed_ports.map(|allowed_ports| {
                    allowed_ports.into_iter().map(|port| port.into()).collect()
                }),
            },
            CommonComponentConfig::Allow(allow) => internal::CommonComponentConfig {
                allow,
                allowed_ports: None,
            },
        }
    }
}

impl From<NetworkPolicyEgress> for internal::NetworkPolicyEgress {
    fn from(network_policy_egress: NetworkPolicyEgress) -> Self {
        internal::NetworkPolicyEgress {
            allow_cluster: network_policy_egress.allow_cluster,
            allow_same_ns: network_policy_egress.allow_same_ns,
            allow_kube_api: network_policy_egress.allow_kube_api,
            allow_world: network_policy_egress.allow_world,
            allow_local_net: network_policy_egress.allow_local_net,
            dns: network_policy_egress.dns.map(|dns| dns.into()),
            fqdns: network_policy_egress
                .fqdns
                .into_iter()
                .map(|fqdn| fqdn.into())
                .collect(),
            cidrs: network_policy_egress
                .cidrs
                .into_iter()
                .map(|cidr| cidr.into())
                .collect(),
            services: network_policy_egress
                .services
                .into_iter()
                .map(|service| service.into())
                .collect(),
            pods: vec![],
        }
    }
}

impl From<DnsConfig> for internal::DnsConfig {
    fn from(dns_config: DnsConfig) -> Self {
        internal::DnsConfig {
            allow: dns_config.allow,
            allowed_lookup_names: dns_config.allowed_lookup_names,
            allowed_lookup_pattern: dns_config.allowed_lookup_pattern,
        }
    }
}

impl From<FqdnConfig> for internal::FqdnConfig {
    fn from(fqdn_config: FqdnConfig) -> Self {
        internal::FqdnConfig {
            match_name: fqdn_config.match_name,
            match_pattern: fqdn_config.match_pattern,
            allowed_ports: fqdn_config
                .allowed_ports
                .map(|allowed_ports| allowed_ports.into_iter().map(|port| port.into()).collect()),
        }
    }
}

impl From<Cidr> for internal::Cidr {
    fn from(cidr: Cidr) -> Self {
        internal::Cidr {
            cidr: cidr.cidr,
            except: cidr.except,
            allowed_ports: cidr
                .allowed_ports
                .map(|allowed_ports| allowed_ports.into_iter().map(|port| port.into()).collect()),
        }
    }
}

impl From<PodConfig> for internal::PodConfig {
    fn from(mut pod_config: PodConfig) -> Self {
        if let Some(container_name) = pod_config.container_name {
            pod_config
                .match_labels
                .get_or_insert_with(HashMap::new)
                .insert("container".to_string(), container_name);
        }
        internal::PodConfig {
            namespace: pod_config.namespace,
            app: pod_config.app,
            match_labels: pod_config.match_labels,
            allowed_ports: pod_config
                .allowed_ports
                .map(|allowed_ports| allowed_ports.into_iter().map(|port| port.into()).collect()),
            all_namespaces: false,
        }
    }
}

impl From<ServiceConfig> for internal::ServiceConfig {
    fn from(service_config: ServiceConfig) -> Self {
        internal::ServiceConfig {
            namespace: service_config.namespace,
            service_name: service_config.service_name,
            allowed_ports: service_config
                .allowed_ports
                .map(|allowed_ports| allowed_ports.into_iter().map(|port| port.into()).collect()),
        }
    }
}

impl From<PortConfig> for internal::PortConfig {
    fn from(port_config: PortConfig) -> Self {
        match port_config {
            PortConfig::JustPort(port) => internal::PortConfig {
                port,
                protocol: internal::Protocol::Any,
            },
            PortConfig::PortAndProtocol(port) => internal::PortConfig {
                port: port.port,
                protocol: port.protocol.into(),
            },
        }
    }
}

impl From<Protocol> for internal::Protocol {
    fn from(protocol: Protocol) -> Self {
        match protocol {
            Protocol::Tcp => internal::Protocol::Tcp,
            Protocol::Udp => internal::Protocol::Udp,
            Protocol::Sctp => internal::Protocol::Sctp,
            Protocol::Any => internal::Protocol::Any,
        }
    }
}
