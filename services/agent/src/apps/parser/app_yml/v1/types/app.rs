// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::BTreeMap;

use k8s_openapi::api::rbac::v1::PolicyRule;
use serde::{Deserialize, Serialize};

use nirvati_apps::internal::types as internal;
use nirvati_apps::utils::{is_false, true_default, StringOrNumber};

use crate::apps::parser::app_yml::v1::types::app::ports::Ports;
use crate::apps::parser::app_yml::v1::types::app::utils::{EnvVar, Plugin};
use crate::apps::parser::app_yml::v1::types::command::Command;

pub mod ports;
pub mod utils;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Default)]
pub struct Mounts {
    #[serde(skip_serializing_if = "BTreeMap::is_empty", default)]
    pub data: BTreeMap<String, String>,
    #[serde(skip_serializing_if = "BTreeMap::is_empty", default)]
    pub secrets: BTreeMap<String, String>,
    #[serde(skip_serializing_if = "BTreeMap::is_empty", default)]
    pub host_paths: BTreeMap<String, String>,
}

impl Mounts {
    pub fn is_empty(&self) -> bool {
        self.data.is_empty() && self.secrets.is_empty() && self.host_paths.is_empty()
    }
}

#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq)]
pub struct Container {
    // These can be copied directly without validation
    pub image: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub stop_grace_period: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub restart: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub working_dir: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shm_size: Option<StringOrNumber>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ulimits: Option<serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub entrypoint: Option<Command>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub command: Option<Command>,
    #[serde(skip_serializing_if = "BTreeMap::is_empty", default)]
    pub environment: BTreeMap<String, EnvVar>,
    // These need security checks
    #[serde(default = "bool::default")]
    #[serde(skip_serializing_if = "is_false")]
    pub privileged: bool,
    #[serde(skip_serializing_if = "Vec::is_empty", default)]
    pub cap_add: Vec<String>,
    #[serde(skip_serializing_if = "Vec::is_empty", default)]
    pub cap_drop: Vec<String>,
    // These properties have no direct equivalent in Deployment, so we need to convert them
    #[serde(skip_serializing_if = "Mounts::is_empty", default)]
    pub mounts: Mounts,
    #[serde(skip_serializing_if = "Option::is_none")]
    /// The cron schedule at which to execute this service (only if type is CronJob)
    pub schedule: Option<String>,
    /// The permission this container's files have
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fs_group: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub service_account: Option<ServiceAccount>,
    /// Whether Traefik should handle HTTP compression for this service (default: true)
    #[serde(default = "true_default")]
    pub enable_http_compression: bool,
    /// Whether to strip the prefix from the request before forwarding to the target service
    /// Defaults to true
    #[serde(default = "true_default")]
    pub strip_prefix: bool,
    /// Whether to use host networking
    #[serde(default = "bool::default")]
    pub host_network: bool,
    pub network_policy: Option<super::networkpolicy::NetworkPolicy>,
    #[serde(default)]
    pub has_permissions: Vec<String>,
}

#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq)]
pub struct ServiceAccountInner {
    #[serde(default)]
    pub rules: Vec<PolicyRule>,
    #[serde(default)]
    pub builtin_roles: Vec<String>,
}

#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq)]
pub struct ServiceAccount {
    #[serde(default)]
    pub cluster_rules: Vec<PolicyRule>,
    #[serde(default)]
    pub builtin_cluster_roles: Vec<String>,
    #[serde(flatten)]
    pub inner: ServiceAccountInner,
    #[serde(flatten)]
    pub other_ns: BTreeMap<String, ServiceAccountInner>,
}

#[derive(Serialize, Deserialize, Clone, Copy, Debug, PartialEq)]
pub enum ServiceType {
    LoadBalancer,
    ClusterIp,
}

impl From<ServiceType> for internal::ServiceType {
    fn from(s: ServiceType) -> internal::ServiceType {
        match s {
            ServiceType::LoadBalancer => internal::ServiceType::LoadBalancer,
            ServiceType::ClusterIp => internal::ServiceType::ClusterIp,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct Service {
    #[serde(default, skip_serializing_if = "Ports::is_empty")]
    pub ports: Ports,
    pub r#type: ServiceType,
    pub target_container: String,
    /// The port ingress for this deployment should be redirected to
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ingress_port: Option<u16>,
    /// The port HTTP ingress for this deployment should be redirected to
    #[serde(skip_serializing_if = "Option::is_none")]
    pub http_ingress_port: Option<u16>,
    /// The path prefix this service should be exposed at if it should be exposed and is not the main target
    #[serde(skip_serializing_if = "Option::is_none")]
    pub path_prefix: Option<String>,
    /// Whether Traefik should handle HTTP compression for this service (default: true, ignored if path_prefix is not set and the service is not the main target)
    #[serde(default = "true_default")]
    pub enable_http_compression: bool,
    /// Whether to strip the prefix from the request before forwarding to the target service
    /// Defaults to true
    #[serde(default = "true_default")]
    pub strip_prefix: bool,
    pub cluster_ip: Option<String>,
    /// Exclude this service from authentication if the app is marked as "protected"
    #[serde(default)]
    pub auth_exclude: bool,
    pub network_policy: Option<super::networkpolicy::NetworkPolicyIngress>,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[serde(tag = "type")]
pub enum Middleware {
    Headers(k8s_crds_traefik::MiddlewareHeaders),
}

impl From<Middleware> for k8s_crds_traefik::MiddlewareSpec {
    //noinspection ALL
    #[allow(irrefutable_let_patterns)]
    fn from(value: Middleware) -> Self {
        k8s_crds_traefik::MiddlewareSpec {
            headers: if let Middleware::Headers(h) = value {
                Some(h)
            } else {
                None
            },
            ..Default::default()
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct Deployment {
    #[serde(flatten)]
    pub container: Container,
    #[serde(default, skip_serializing_if = "Ports::is_empty")]
    pub public_ports: Ports,
    #[serde(default, skip_serializing_if = "Ports::is_empty")]
    pub internal_ports: Ports,
    /// The port ingress for this deployment should be redirected to
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ingress_port: Option<u16>,
    /// The port HTTP ingress for this deployment should be redirected to
    #[serde(skip_serializing_if = "Option::is_none")]
    pub http_ingress_port: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    /// The path prefix this container should be exposed at, if the container is not the main container and exposes a port
    pub path_prefix: Option<String>,
    #[serde(skip_serializing_if = "BTreeMap::is_empty", default)]
    pub plugins: BTreeMap<String, Plugin>,
    #[serde(skip_serializing_if = "BTreeMap::is_empty", default)]
    pub middlewares: BTreeMap<String, Middleware>,
    /// Exclude this service from authentication if the app is marked as "protected"
    #[serde(default)]
    pub auth_exclude: bool,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct CronJob {
    pub schedule: String,
    #[serde(flatten)]
    pub container: Container,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct JobInner {
    #[serde(flatten)]
    pub container: Container,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[serde(tag = "job_type")]
pub enum Job {
    OneTime(JobInner),
    OnUpdate(JobInner),
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[serde(untagged)]
pub enum Runnable {
    CronJob(CronJob),
    Job(Job),
    Deployment(Deployment),
}

impl Runnable {
    pub fn get_container(&self) -> &Container {
        match self {
            Runnable::CronJob(c) => &c.container,
            Runnable::Job(Job::OneTime(j) | Job::OnUpdate(j)) => &j.container,
            Runnable::Deployment(d) => &d.container,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct Secret {
    #[serde(default, skip_serializing_if = "BTreeMap::<String, String>::is_empty")]
    pub data: BTreeMap<String, String>,
    #[serde(default, skip_serializing_if = "BTreeMap::<String, String>::is_empty")]
    pub string_data: BTreeMap<String, String>,
}
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct IngressRoute {
    pub target_svc: Option<String>,
    pub target_app: Option<String>,
    pub target_ns: Option<String>,
    pub port: Option<u16>,
    #[serde(default = "true_default")]
    pub enable_http_compression: bool,
    #[serde(default = "true_default")]
    pub strip_prefix: bool,
    /// Exclude this service from authentication if the app is marked as "protected"
    #[serde(default)]
    pub auth_exclude: bool,
    pub path_prefix: Option<String>,
    #[serde(default)]
    pub is_http_fallback: bool,
    pub component: Option<String>,
}
