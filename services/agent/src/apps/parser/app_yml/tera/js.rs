// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::cell::RefCell;
use std::collections::{BTreeMap, HashMap};
use std::rc::Rc;

use anyhow::{bail, Result};
use boa_engine::module::{ModuleLoader, Referrer};
use boa_engine::property::Attribute;
use boa_engine::{
    js_string, JsError, JsNativeError, JsObject, JsResult, JsString, JsValue, Module,
    NativeFunction, Source,
};
use serde::de::StdError;
use serde_json::Value;
use tera::{Context, Tera};

enum Message {
    Eval(String),
    Invoke(String, Vec<Value>),
    Postprocess(String, String),
    Value(Value),
    Error((String, Box<dyn StdError + Send + Sync>)),
    RegisterHandlerInTera(String),
    RequestNextResult(std::sync::mpsc::Sender<Message>),
    EndThread,
}

#[derive(Clone)]
struct JSState {
    handlers: BTreeMap<String, JsObject>,
    postprocessor: Option<JsObject>,
}

pub struct TeraWithJs {
    tera: Tera,
    js_request_channel: std::sync::mpsc::Sender<Message>,
    syncer_request_channel: std::sync::mpsc::Sender<Message>,
}

struct DummyLoader;

impl ModuleLoader for DummyLoader {
    fn load_imported_module(
        &self,
        _referrer: Referrer,
        _specifier: JsString,
        finish_load: Box<dyn FnOnce(JsResult<Module>, &mut boa_engine::Context)>,
        context: &mut boa_engine::Context,
    ) {
        finish_load(
            Err(JsError::from(
                JsNativeError::typ().with_message("Module loading is not supported"),
            )),
            context,
        );
    }

    fn register_module(&self, _specifier: JsString, _module: Module) {
        // Do nothing
    }

    fn get_module(&self, _specifier: JsString) -> Option<Module> {
        None
    }

    fn init_import_meta(
        &self,
        _import_meta: &JsObject,
        _module: &Module,
        _context: &mut boa_engine::Context,
    ) {
        // Do nothing
    }
}

impl TeraWithJs {
    pub fn new(tera: Tera) -> Result<Self> {
        let (main_tx, thread_rx) = std::sync::mpsc::channel::<Message>();
        let (thread_tx, syncer_rx) = std::sync::mpsc::channel::<Message>();
        let syncer_add_tx = thread_tx.clone();
        std::thread::spawn(move || {
            let mut tx = None;
            loop {
                match syncer_rx.recv() {
                    Ok(Message::RequestNextResult(new_tx)) => {
                        if tx.is_some() {
                            panic!("RequestNextResult without previous result");
                        }
                        tx = Some(new_tx);
                    }
                    Ok(Message::EndThread) => {
                        break;
                    }
                    Ok(Message::Value(val)) => {
                        if let Some(tx) = tx.take() {
                            tx.send(Message::Value(val)).unwrap();
                        }
                    }
                    Ok(Message::Error(val)) => {
                        if let Some(tx) = tx.take() {
                            tx.send(Message::Error(val)).unwrap();
                        }
                    }
                    Ok(msg) => {
                        if let Some(ref tx) = tx {
                            tx.send(msg).unwrap();
                        }
                    }
                    _ => {
                        break;
                    }
                }
            }
        });
        std::thread::spawn(move || {
            let tokio_rt = tokio::runtime::Runtime::new().unwrap();
            tokio_rt.block_on(async {
                let state = JSState {
                    handlers: BTreeMap::new(),
                    postprocessor: None,
                };
                let mut boa_ctx = boa_engine::Context::builder().module_loader(Rc::new(DummyLoader)).build().unwrap();
                let console = boa_runtime::Console::init(&mut boa_ctx);
                boa_ctx
                    .register_global_property(js_string!(boa_runtime::Console::NAME), console, Attribute::all())
                    .expect("the console object shouldn't exist yet");
                let state_rc = Rc::new(RefCell::new(state));
                let rc_clone = state_rc.clone();
                let rc_clone_2 = state_rc.clone();
                let tx_clone = thread_tx.clone();
                unsafe {
                    boa_ctx.register_global_callable(js_string!("registerTeraHandler"), 1, NativeFunction::from_closure(move |_this, args, _ctx| {
                        let name = args
                            .first()
                            .and_then(|v| v.as_string())
                            .ok_or_else(|| JsNativeError::typ().with_message("Expected string as first argument to registerTeraHandler"))?;
                        let handler = args
                            .get(1)
                            .and_then(|v| v.as_callable())
                            .ok_or_else(|| JsNativeError::typ().with_message("Expected callable as second argument to registerTeraHandler"))?;
                        let name = name.to_std_string().map_err(|_| JsNativeError::typ().with_message("Failed to convert string to Rust string"))?;
                        rc_clone.borrow_mut().handlers.insert(name.clone(), handler.clone());
                        tx_clone.send(Message::RegisterHandlerInTera(name)).unwrap();
                        Ok(JsValue::Undefined)
                    })).unwrap();
                    boa_ctx.register_global_callable(js_string!("registerPostprocessor"), 1, NativeFunction::from_closure(move |_this, args, _ctx| {
                        let postprocessor = args
                            .first()
                            .and_then(|v| v.as_callable())
                            .ok_or_else(|| JsNativeError::typ().with_message("Expected callable as first argument to registerPostprocessor"))?;
                        rc_clone_2.borrow_mut().postprocessor = Some(postprocessor.clone());
                        Ok(JsValue::Undefined)
                    })).unwrap();
                }
                async fn handle_result(result: JsResult<JsValue>, boa: &mut boa_engine::Context, tx: &std::sync::mpsc::Sender<Message>) {
                    let mut result = match result {
                        Ok(result) => result,
                        Err(err) => {
                            tx.send(Message::Error((err.to_string(), Box::new(err.into_erased(boa))))).unwrap();
                            return;
                        }
                    };
                    if let Some(promise) = result.as_promise() {
                        let future = promise.into_js_future(boa);
                        let future1 = async move { tokio::time::timeout(std::time::Duration::from_secs(2), future).await };
                        let future2 = async { boa.run_jobs_async().await };
                        let (new_result, _) = futures_lite::future::block_on(futures_lite::future::zip(future1, future2));
                        let new_result = match new_result {
                            Ok(Ok(result)) => result,
                            Ok(Err(err)) => {
                                tx.send(Message::Error((err.to_string(), Box::new(err.into_erased(boa))))).unwrap();
                                return;
                            }
                            Err(_) => {
                                tx.send(Message::Error(("Timeout".to_string(), Box::new(std::io::Error::new(std::io::ErrorKind::TimedOut, "Timeout"))))).unwrap();
                                return;
                            }
                        };
                        result = new_result;
                    }
                    if result.is_undefined() {
                        tx.send(Message::Value(Value::Null)).unwrap();
                        return;
                    }
                    tx.send(Message::Value(result.to_json(boa).unwrap_or(Value::Null))).unwrap();
                }
                loop {
                    match thread_rx.recv() {
                        Ok(Message::Eval(code)) => {
                            handle_result(boa_ctx.eval(Source::from_bytes(&code)), &mut boa_ctx, &thread_tx).await;
                        }
                        Ok(Message::Invoke(name, args)) => {
                            let handler_result = {
                                let state = state_rc.borrow();
                                let Some(handler) = state.handlers.get(&name) else {
                                    thread_tx.send(Message::Value(Value::Null)).unwrap();
                                    continue;
                                };
                                let args = args
                                    .iter()
                                    .map(|v| JsValue::from_json(v, &mut boa_ctx).unwrap())
                                    .collect::<Vec<_>>();
                                handler.call(&JsValue::Undefined, &args, &mut boa_ctx)
                            };
                            handle_result(handler_result, &mut boa_ctx, &thread_tx).await;
                        }
                        Ok(Message::Postprocess(filename, result)) => {
                            if let Some(postprocessor_result) = {
                                let state = state_rc.borrow();
                                state.postprocessor.as_ref().map(|postprocessor| {
                                    let filename = JsValue::String(JsString::from(filename));
                                    let result = JsValue::String(JsString::from(result.clone()));
                                    let args = vec![filename, result];
                                    postprocessor.call(&JsValue::Undefined, &args, &mut boa_ctx)
                                })
                            } {
                                handle_result(postprocessor_result, &mut boa_ctx, &thread_tx).await;
                            } else {
                                thread_tx.send(Message::Value(Value::String(result))).unwrap();
                            }
                        }
                        Ok(Message::EndThread) => {
                            break;
                        }
                        Err(_) => {
                            break;
                        }
                        _ => {}
                    }
                }
            });
        });
        Ok(Self {
            tera,
            js_request_channel: main_tx,
            syncer_request_channel: syncer_add_tx,
        })
    }

    pub fn eval(&mut self, code: String) -> Result<JsValue> {
        let (tx, rx) = std::sync::mpsc::channel::<Message>();
        self.syncer_request_channel
            .send(Message::RequestNextResult(tx))
            .unwrap();
        self.js_request_channel.send(Message::Eval(code)).unwrap();
        loop {
            match rx.recv() {
                Ok(Message::Value(value)) => {
                    return Ok(
                        JsValue::from_json(&value, &mut boa_engine::Context::default()).unwrap(),
                    );
                }
                Ok(Message::RegisterHandlerInTera(name)) => {
                    self.register_tera_handler(name);
                }
                Ok(_) => {
                    panic!("Unexpected message!");
                }
                _ => {}
            }
        }
    }

    pub fn register_tera_handler(&mut self, name: String) {
        let js_channel = self.js_request_channel.clone();
        let syncer_channel = self.syncer_request_channel.clone();
        self.tera
            .register_function(&name.clone(), move |args: &HashMap<String, Value>| {
                let args = Value::Object(args.clone().into_iter().collect());
                let (tx, rx) = std::sync::mpsc::channel::<Message>();
                syncer_channel.send(Message::RequestNextResult(tx)).unwrap();
                js_channel
                    .send(Message::Invoke(name.to_string(), vec![args]))
                    .unwrap();
                loop {
                    match rx.recv() {
                        Ok(Message::Value(value)) => {
                            return Ok(value);
                        }
                        Ok(Message::Error((value, source))) => {
                            return Err(tera::Error::chain(value, source));
                        }
                        _ => {}
                    }
                }
            });
    }

    pub fn render_str(
        &mut self,
        filename: String,
        input: &str,
        context: &Context,
    ) -> Result<String> {
        let result = self.tera.render_str(input, context)?;
        let (tx, rx) = std::sync::mpsc::channel::<Message>();
        self.syncer_request_channel
            .send(Message::RequestNextResult(tx))
            .unwrap();
        self.js_request_channel
            .send(Message::Postprocess(filename, result.clone()))
            .unwrap();
        loop {
            match rx.recv() {
                Ok(Message::Value(value)) => match value {
                    Value::String(s) => {
                        return Ok(s);
                    }
                    _ => {
                        bail!("Unexpected value type");
                    }
                },
                Ok(Message::Error((msg, source))) => {
                    return Err(anyhow::anyhow!(msg).context(source));
                }
                Ok(_) => {
                    bail!("Unexpected message!");
                }
                _ => {}
            }
        }
    }
}

impl Drop for TeraWithJs {
    fn drop(&mut self) {
        self.js_request_channel.send(Message::EndThread).unwrap();
        self.syncer_request_channel
            .send(Message::EndThread)
            .unwrap();
    }
}

#[cfg(test)]
mod tests {
    use std::error::Error;
    use tera::Tera;

    use super::TeraWithJs;

    #[test]
    fn js_execution() {
        let code = r#"
            function math(args) {
                return (args.num1 + 1) * args.num2;
            }

            registerTeraHandler("math", math);
            "#;
        let mut tera = TeraWithJs::new(Tera::default()).unwrap();
        tera.eval(code.to_string()).unwrap();
        let result = tera
            .tera
            .render_str("{{ math(num1=5, num2=2) }}", &tera::Context::new())
            .unwrap();
        assert_eq!(result, "12");
    }

    #[test]
    fn async_js_execution() {
        let code = r#"
            async function async_math(args) {
                return new Promise((resolve) => {
                    resolve((args.num1 + 1) * args.num2);
                });
            }

            registerTeraHandler("async_math", async_math);
            "#;
        let mut tera = TeraWithJs::new(Tera::default()).unwrap();
        tera.eval(code.to_string()).unwrap();
        let result = tera
            .tera
            .render_str("{{ async_math(num1=5, num2=2) }}", &tera::Context::new())
            .unwrap();
        assert_eq!(result, "12");
    }

    #[test]
    fn async_js_exception() {
        let code = r#"
            async function async_math(args) {
                return new Promise((resolve, reject) => {
                    reject("JS error message");
                });
            }

            registerTeraHandler("async_math", async_math);
            "#;
        let mut tera = TeraWithJs::new(Tera::default()).unwrap();
        tera.eval(code.to_string()).unwrap();
        let result = tera
            .tera
            .render_str("{{ async_math(num1=5, num2=2) }}", &tera::Context::new())
            .unwrap_err();

        assert_eq!(
            result.source().unwrap().source().unwrap().to_string(),
            "\"JS error message\""
        );
    }

    #[test]
    fn async_js_timeout() {
        let code = r#"
            async function idle_forever(args) {
                return new Promise((resolve) => {});
            }

            registerTeraHandler("idle_forever", idle_forever);
            "#;
        let mut tera = TeraWithJs::new(Tera::default()).unwrap();
        tera.eval(code.to_string()).unwrap();
        let result = tera
            .tera
            .render_str("{{ idle_forever() }}", &tera::Context::new())
            .unwrap_err();
        assert_eq!(
            result.source().unwrap().source().unwrap().to_string(),
            "Timeout"
        );
    }
}
