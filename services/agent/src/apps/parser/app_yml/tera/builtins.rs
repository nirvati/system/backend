// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::HashMap;

use anyhow::Result;
use base64::engine::general_purpose::STANDARD;
use base64::prelude::*;
use serde_json::Value;
use sha2::Digest;
use tera::Tera;

use crate::constants::FEATURES;
use nirvati::utils::derive_entropy;

pub fn register_builtins(
    tera: &mut Tera,
    nirvati_seed: String,
    user_id: &str,
    app_id: &str,
) -> Result<()> {
    let app_id = app_id.to_string();
    let user_id = user_id.to_string();
    tera.register_function(
        "derive_entropy",
        move |args: &HashMap<String, Value>| -> tera::Result<Value> {
            let identifier = args
                .get("identifier")
                .ok_or_else(|| tera::Error::msg("identifier not provided"))?
                .as_str()
                .ok_or_else(|| tera::Error::msg("identifier is not a string"))?;
            Ok(Value::String(derive_entropy(
                &nirvati_seed,
                format!("{}:{}:{}", user_id, app_id, identifier).as_str(),
            )))
        },
    );
    tera.register_function(
        "nirvati_has_feature",
        move |args: &HashMap<String, Value>| -> tera::Result<Value> {
            let identifier = args
                .get("feature")
                .ok_or_else(|| tera::Error::msg("feature not provided"))?
                .as_str()
                .ok_or_else(|| tera::Error::msg("feature is not a string"))?;
            Ok(Value::Bool(FEATURES.contains(&identifier)))
        },
    );
    tera.register_filter(
        "hex_decode",
        |value: &Value, _args: &HashMap<String, Value>| {
            let value = value
                .as_str()
                .ok_or_else(|| tera::Error::msg("value is not a string"))?;
            let value = hex::decode(value).map_err(|e| tera::Error::msg(e.to_string()))?;
            Ok(Value::String(
                String::from_utf8(value).map_err(|e| tera::Error::msg(e.to_string()))?,
            ))
        },
    );
    tera.register_filter(
        "hex_encode",
        |value: &Value, _args: &HashMap<String, Value>| {
            let value = value
                .as_str()
                .ok_or_else(|| tera::Error::msg("value is not a string"))?;
            Ok(Value::String(hex::encode(value)))
        },
    );
    tera.register_filter(
        "base64_encode",
        |value: &Value, _args: &HashMap<String, Value>| {
            let value = value
                .as_str()
                .ok_or_else(|| tera::Error::msg("value is not a string"))?;
            Ok(Value::String(STANDARD.encode(value.as_bytes())))
        },
    );
    tera.register_filter(
        "base64_decode",
        |value: &Value, _args: &HashMap<String, Value>| {
            let value = value
                .as_str()
                .ok_or_else(|| tera::Error::msg("value is not a string"))?;
            let value = STANDARD
                .decode(value)
                .map_err(|e| tera::Error::msg(e.to_string()))?;
            Ok(Value::String(
                String::from_utf8(value).map_err(|e| tera::Error::msg(e.to_string()))?,
            ))
        },
    );
    tera.register_filter(
        "hex_to_base64",
        |value: &Value, _args: &HashMap<String, Value>| {
            let value = value
                .as_str()
                .ok_or_else(|| tera::Error::msg("value is not a string"))?;
            let value = hex::decode(value).map_err(|e| tera::Error::msg(e.to_string()))?;
            Ok(Value::String(STANDARD.encode(&value)))
        },
    );
    tera.register_filter(
        "base64_to_hex",
        |value: &Value, _args: &HashMap<String, Value>| {
            let value = value
                .as_str()
                .ok_or_else(|| tera::Error::msg("value is not a string"))?;
            let value = STANDARD
                .decode(value)
                .map_err(|e| tera::Error::msg(e.to_string()))?;
            Ok(Value::String(hex::encode(&value)))
        },
    );
    tera.register_filter("bcrypt", |value: &Value, args: &HashMap<String, Value>| {
        let cost = args
            .get("cost")
            .and_then(|v| v.as_u64())
            .and_then(|v| v.try_into().ok())
            .unwrap_or(12);
        if !(4..=15).contains(&cost) {
            return Err(tera::Error::msg("cost must be between 4 and 15"));
        }
        let value = value
            .as_str()
            .ok_or_else(|| tera::Error::msg("value is not a string"))?;
        let value = bcrypt::hash(value, cost).map_err(|e| tera::Error::msg(e.to_string()))?;
        Ok(Value::String(value))
    });
    tera.register_filter("sha256", |value: &Value, _args: &HashMap<String, Value>| {
        let value = value
            .as_str()
            .ok_or_else(|| tera::Error::msg("value is not a string"))?;
        let value = sha2::Sha256::digest(value.as_bytes());
        Ok(Value::String(hex::encode(value)))
    });
    Ok(())
}
