// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::{anyhow, Result};
use kube::Client;
use rand::RngCore;
use sha1::Digest;
use std::collections::BTreeMap;
use std::{collections::HashMap, path::Path};
use tera::{Context, Tera};

use crate::apps::parser::context::get_processing_context;
use crate::plugins::api::UserState;

mod builtins;
pub mod js;

// Creates a S2K hash like used by Tor
fn tor_hash(input: &str, salt: [u8; 8]) -> String {
    let mut bytes = Vec::new();
    bytes.extend_from_slice(&salt);
    bytes.extend_from_slice(input.as_bytes());
    let mut hash = sha1::Sha1::new();
    while bytes.len() < 0x10000 {
        bytes.extend_from_slice(&salt);
        bytes.extend_from_slice(input.as_bytes());
    }
    bytes.truncate(0x10000);
    hash.update(&bytes);
    let hash = hash.finalize();
    let mut hash_str = String::new();
    for byte in hash {
        hash_str.push_str(&format!("{byte:02X}"));
    }
    format!(
        "16:{}60{}",
        hex::encode(salt).to_uppercase(),
        hash_str.to_uppercase()
    )
}

pub fn load_tera_helpers(dir: &Path, eval_cb: &mut dyn FnMut(String) -> Result<()>) -> Result<()> {
    for entry in std::fs::read_dir(dir)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_file() {
            let ext = path
                .extension()
                .ok_or_else(|| anyhow!("Failed to get extension of file"))?;
            if ext == "js" {
                // This should be safe, but run it in a separate thread without any FS access to be sure it can't be doing anything malicious
                let code = std::fs::read_to_string(&path)?;
                eval_cb(code)?;
            }
        }
    }
    Ok(())
}

pub fn process_metadata_yml_jinja(
    file: &Path,
    app_id: &str,
    nirvati_seed: &str,
    user_state: &UserState,
) -> Result<String> {
    let contents = std::fs::read_to_string(file)?;
    let dir = file
        .parent()
        .ok_or_else(|| anyhow!("Failed to get parent dir"))?;

    let mut tera_ctx = Context::new();
    tera_ctx.insert("user", &user_state.user_id);
    tera_ctx.insert("user_display_name", &user_state.user_display_name);
    tera_ctx.insert("installed_apps", &user_state.installed_apps);
    let mut tera = Tera::default();
    tera.functions
        .remove("get_env")
        .expect("get_env was not available in Tera, this is a bug in Nirvati!");
    builtins::register_builtins(
        &mut tera,
        nirvati_seed.to_string(),
        &user_state.user_id,
        app_id,
    )?;
    let mut tera = js::TeraWithJs::new(tera)?;

    let tera_dir = dir.join("_tera");
    if tera_dir.is_dir() {
        load_tera_helpers(&tera_dir, &mut |code| {
            tera.eval(code)?;
            Ok(())
        })?;
    }
    let file_name = file.file_name().unwrap().to_str().unwrap().to_string();
    tera.render_str(file_name, &contents, &tera_ctx)
}

pub async fn process_later_jinja(
    #[cfg(not(feature = "test-parser"))] kube_client: Client,
    app_id: &str,
    file: &Path,
    nirvati_seed: &str,
    perms: &[String],
    additional_metadata: BTreeMap<String, BTreeMap<String, serde_json::Value>>,
    user_state: UserState,
    init_domain: Option<String>,
    citadel_compat_vars: Option<&HashMap<String, String>>,
) -> Result<String> {
    let contents = std::fs::read_to_string(file)?;
    let dir = file
        .parent()
        .ok_or_else(|| anyhow!("Failed to get parent dir"))?;

    let mut tera = Tera::default();
    tera.functions
        .remove("get_env")
        .expect("get_env was not available in Tera, this is a bug in Nirvati!");
    builtins::register_builtins(
        &mut tera,
        nirvati_seed.to_string(),
        &user_state.user_id,
        app_id,
    )?;
    let file_name = file.file_name().unwrap().to_str().unwrap().to_string();
    let mut tera_ctx = get_processing_context(
        #[cfg(not(feature = "test-parser"))]
        kube_client,
        app_id,
        perms,
        additional_metadata,
        &user_state,
    )
    .await?;
    if let Some(citadel_compat_vars) = citadel_compat_vars {
        for (key, value) in citadel_compat_vars {
            tera_ctx.insert(key, value);
        }
        // Backwards compatibility with Citadel
        tera.register_filter(
            "tor_hash",
            |val: &tera::Value,
             _args: &HashMap<String, tera::Value>|
             -> Result<tera::Value, tera::Error> {
                let Some(input) = val.as_str() else {
                    return Err(tera::Error::msg("Identifier must be a string"));
                };
                let mut salt = [0u8; 8];
                rand::thread_rng().fill_bytes(&mut salt);
                Ok(tera::to_value(tor_hash(input, salt)).expect("Failed to serialize value"))
            },
        );
    }

    let mut tera = js::TeraWithJs::new(tera)?;

    let tera_dir = dir.join("_tera");
    if tera_dir.is_dir() {
        load_tera_helpers(&tera_dir, &mut |code| {
            tera.eval(code)?;
            Ok(())
        })?;
    }

    let init_domain = init_domain.unwrap_or_else(|| "placeholder.nirvati.local".to_string());
    tera_ctx.insert("init_domain", &init_domain);
    tera.render_str(file_name, &contents, &tera_ctx)
}
