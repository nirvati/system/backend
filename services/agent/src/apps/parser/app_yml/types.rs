// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use serde::{Deserialize, Serialize};

use nirvati_apps::metadata::{AppScope, Metadata, PreloadType, Runtime, SaasCompatibility, Settings};

use super::v1::types as v1;

#[derive(Clone, Debug, PartialEq)]
pub enum AppYml {
    V1(v1::AppYml),
}

impl AppYml {
    pub fn get_ports(&self) -> Vec<u16> {
        match self {
            AppYml::V1(app) => app.get_ports(),
        }
    }
}
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum MetadataYml {
    V1(v1::MetadataYml),
}

impl MetadataYml {
    pub fn get_implements(&self) -> &Option<String> {
        match self {
            MetadataYml::V1(metadata) => &metadata.metadata.implements,
        }
    }

    pub fn into_implements(self) -> Option<String> {
        match self {
            MetadataYml::V1(metadata) => metadata.metadata.implements,
        }
    }

    pub fn try_into_output_metadata(
        self,
        app_id: String,
        ports: Vec<u16>,
    ) -> Result<Metadata, std::io::Error> {
        Ok(match self {
            MetadataYml::V1(metadata) => {
                let default_scope = metadata
                    .metadata
                    .scope
                    .map(|scope| scope.into())
                    .unwrap_or_else(|| match metadata.metadata.r#type {
                        v1::metadata::AppType::App | v1::metadata::AppType::Library => {
                            AppScope::User
                        }
                        v1::metadata::AppType::SystemLibrary => AppScope::System,
                    });
                let allowed_scopes = metadata
                    .metadata
                    .allowed_scopes
                    .map(|scopes| scopes.iter().map(|scope| (*scope).into()).collect())
                    .unwrap_or_else(|| vec![default_scope]);

                Metadata {
                    id: app_id,
                    name: metadata.metadata.name.into(),
                    version: metadata.metadata.version.clone(),
                    display_version: metadata
                        .metadata
                        .display_version
                        .unwrap_or_else(|| metadata.metadata.version.to_string()),
                    category: metadata.metadata.category.into(),
                    tagline: metadata.metadata.tagline.into(),
                    developers: metadata.metadata.developers,
                    description: metadata.metadata.description.into(),
                    dependencies: metadata
                        .metadata
                        .dependencies
                        .into_iter()
                        .map(|dep| dep.into())
                        .collect(),
                    has_permissions: metadata.metadata.has_permissions.clone(),
                    base_permissions: metadata.metadata.has_permissions,
                    repos: metadata.metadata.repos,
                    support: metadata.metadata.support,
                    gallery: metadata.metadata.gallery,
                    icon: metadata.metadata.icon,
                    path: metadata.metadata.path,
                    default_username: metadata.metadata.default_username,
                    default_password: metadata.metadata.default_password,
                    implements: metadata.metadata.implements,
                    release_notes: metadata
                        .metadata
                        .release_notes
                        .into_iter()
                        .map(|(key, val)| (key, val.into()))
                        .collect(),
                    license: metadata.metadata.license,
                    settings: Settings(
                        metadata
                            .metadata
                            .settings
                            .into_iter()
                            .map(|(k, v)| (k, v.into()))
                            .collect(),
                    ),
                    volumes: metadata
                        .metadata
                        .volumes
                        .into_iter()
                        .map(|(id, vol)| Ok((id, vol.try_into()?)))
                        .collect::<Result<_, std::io::Error>>()?,
                    exported_data: metadata.metadata.exported_data,
                    ports,
                    runtime: Runtime::Kubernetes,
                    exposes_permissions: metadata
                        .metadata
                        .exposes_permissions
                        .into_iter()
                        .map(|p| p.into())
                        .collect(),
                    default_scope,
                    allowed_scopes,
                    scope: default_scope,
                    store_plugins: metadata
                        .metadata
                        .store_plugins
                        .into_iter()
                        .map(|plugin| plugin.into())
                        .collect(),
                    ui_module: metadata.metadata.ui_module.map(|ui| ui.into()),
                    supports_ingress: true,
                    can_be_protected: true,
                    saas_compatibility: SaasCompatibility::Unknown,
                    raw_ingress: metadata.metadata.raw_ingress.unwrap_or(false),
                    post_install_redirect: metadata.metadata.post_install_redirect,
                    components: metadata
                        .metadata
                        .components
                        .into_iter()
                        .map(|comp| comp.into())
                        .collect(),
                    is_citadel: false,
                    preload_type: Some(PreloadType::Nirvati),
                }
            }
        })
    }
}

pub use v1::MultiLanguageItem;
