// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::{BTreeMap, HashMap};
use std::path::Path;

use anyhow::{anyhow, Result};
use k8s_openapi::api::apps::v1::Deployment;
use kube::Client;
use nirvati::kubernetes::multidoc_deserialize;
use nirvati_apps::internal::InternalAppRepresentation;

use crate::apps::parser::app_yml::types::AppYml;
use crate::apps::parser::app_yml::types::MetadataYml;
use crate::apps::parser::app_yml::v1;
use crate::apps::parser::app_yml::v1::types::app::{CronJob, Job};
use crate::apps::parser::unsafe_load_app_basics;
use crate::plugins::api::UserState;
use nirvati_apps::metadata::Metadata;

pub fn load_metadata_yml(metadata_yml: serde_yaml::Value) -> Result<MetadataYml> {
    let metadata_version = metadata_yml
        .get("version")
        .ok_or_else(|| anyhow!("metadata.yml does not contain a version"))?
        .as_i64()
        .ok_or_else(|| anyhow!("metadata.yml version is not an integer"))?;
    match metadata_version {
        1 => {
            let metadata_yml = MetadataYml::V1(serde_yaml::from_value(metadata_yml)?);
            Ok(metadata_yml)
        }
        _ => Err(anyhow!("metadata.yml version is not supported")),
    }
}

pub fn load_app_yml(app_yml: serde_yaml::Value) -> Result<AppYml> {
    let app_version = app_yml
        .get("version")
        .ok_or_else(|| anyhow!("app.yml does not contain a version"))?
        .as_i64()
        .ok_or_else(|| anyhow!("app.yml version is not an integer"))?;
    match app_version {
        1 => {
            let result = serde_yaml::from_value::<v1::types::AppYml>(app_yml.clone());
            if let Ok(parsed) = result {
                return Ok(AppYml::V1(parsed));
            }
            let err = result.err().unwrap();
            let basic_structure =
                serde_yaml::from_value::<v1::types::AppYmlBasicStructure>(app_yml)?;
            for (ctr_name, ctr) in basic_structure.containers {
                if ctr.get("job_type").is_some() {
                    let _: Job = serde_yaml::from_value(serde_yaml::Value::Mapping(ctr))
                        .map_err(|err| anyhow!("Failed to parse Job {}: {:#?}", ctr_name, err))?;
                } else if ctr.get("schedule").is_some() {
                    let _: CronJob = serde_yaml::from_value(serde_yaml::Value::Mapping(ctr))
                        .map_err(|err| {
                            anyhow!("Failed to parse Cron Job {}: {:#?}", ctr_name, err)
                        })?;
                } else {
                    let _: Deployment = serde_yaml::from_value(serde_yaml::Value::Mapping(ctr))
                        .map_err(|err| {
                            anyhow!("Failed to parse Deployment {}: {:#?}", ctr_name, err)
                        })?;
                }
            }
            Err(err.into())
        }
        _ => Err(anyhow!("app.yml version is not supported")),
    }
}

fn load_metadata(
    app_root: &Path,
    user_state: UserState,
    nirvati_seed: &str,
    ports: Vec<u16>,
) -> Result<Metadata> {
    let app_id = app_root
        .file_name()
        .ok_or_else(|| anyhow!("Failed to get app id"))?
        .to_str()
        .ok_or_else(|| anyhow!("Failed to get app id"))?
        .to_owned();
    let metadata_yml_jinja = app_root.join("metadata.yml.jinja");
    Ok(if metadata_yml_jinja.exists() {
        let metadata_str = super::tera::process_metadata_yml_jinja(
            &metadata_yml_jinja,
            &app_id,
            nirvati_seed,
            &user_state,
        )?;
        let metadata = serde_yaml::from_str::<serde_yaml::Value>(&metadata_str)?;
        load_metadata_yml(metadata)
    } else {
        let metadata_yml = app_root.join("metadata.yml");
        let metadata =
            serde_yaml::from_str::<serde_yaml::Value>(&std::fs::read_to_string(metadata_yml)?)?;
        load_metadata_yml(metadata)
    }?
    .try_into_output_metadata(app_id, ports)?)
}

pub async fn load_app(
    #[cfg(not(feature = "test-parser"))] kube_client: kube::Client,
    app_root: &Path,
    mut user_state: UserState,
    nirvati_seed: &str,
    init_domain: Option<String>,
) -> Result<InternalAppRepresentation> {
    let app_id = app_root
        .file_name()
        .ok_or_else(|| anyhow!("Failed to get app id"))?
        .to_str()
        .ok_or_else(|| anyhow!("Failed to get app id"))?
        .to_owned();
    let user_id = user_state.user_id.clone();
    let mut metadata = load_metadata(app_root, user_state.clone(), nirvati_seed, Vec::new())?;
    let user_state_app_settings = user_state.app_settings.entry(app_id.clone()).or_default();
    let mut parsed_settings: BTreeMap<String, serde_json::Value> =
        serde_yaml::from_str(user_state_app_settings).unwrap_or_default();
    for (setting, config) in metadata.settings.0.iter() {
        parsed_settings
            .entry(setting.clone())
            .or_insert(config.default_value());
    }
    *user_state_app_settings = serde_json::to_string(&parsed_settings)?;
    let app_yml_jinja = app_root.join("app.yml.jinja");
    let app_yml = if app_yml_jinja.exists() {
        let citadel_compat_vars =
            if app_id == "nirvati" || app_id == "bitcoin-citadel" || app_id == "lnd" {
                std::env::var("CITADEL_BASE_DIR")
                    .ok()
                    .map(|host_path| HashMap::from([("CITADEL_BASE_DIR".to_owned(), host_path)]))
            } else {
                None
            };
        let app_str = super::tera::process_later_jinja(
            #[cfg(not(feature = "test-parser"))]
            kube_client.clone(),
            &app_id,
            &app_yml_jinja,
            nirvati_seed,
            &metadata.has_permissions,
            metadata.exported_data.clone().unwrap_or_default(),
            user_state.clone(),
            init_domain.clone(),
            citadel_compat_vars.as_ref(),
        )
        .await?;
        // Save processed app.yml for debugging
        #[cfg(debug_assertions)]
        std::fs::write(app_root.join("app.yml"), &app_str)?;
        let app = serde_yaml::from_str::<serde_yaml::Value>(&app_str)?;
        load_app_yml(app)?
    } else {
        let app_yml = app_root.join("app.yml");
        let app = serde_yaml::from_str::<serde_yaml::Value>(&std::fs::read_to_string(app_yml)?)?;
        load_app_yml(app)?
    };
    metadata.ports = app_yml.get_ports();

    let custom_yml_jinja = app_root.join("custom.yml.jinja");
    let custom_yml = app_root.join("custom.yml");
    let others = if custom_yml_jinja.exists() {
        let custom_yml = super::tera::process_later_jinja(
            #[cfg(not(feature = "test-parser"))]
            kube_client.clone(),
            &app_id,
            &custom_yml_jinja,
            nirvati_seed,
            &metadata.has_permissions,
            metadata.exported_data.clone().unwrap_or_default(),
            user_state,
            init_domain,
            None,
        )
        .await?;
        multidoc_deserialize(&custom_yml)?
    } else if custom_yml.exists() {
        let custom_yml = std::fs::read_to_string(custom_yml)?;
        multidoc_deserialize(&custom_yml)?
    } else {
        Vec::new()
    };
    Ok(match app_yml {
        AppYml::V1(app_yml) => {
            v1::convert::convert_app_yml(&app_id, &user_id, &app_yml, &metadata, others, None)
                .await?
        }
    })
}

fn get_all_dir_files(dir: &Path) -> Result<Vec<String>> {
    let mut files = Vec::new();
    for entry in std::fs::read_dir(dir)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_dir() {
            files.append(&mut get_all_dir_files(&path)?);
        } else if path.is_file() {
            files.push(
                path.to_str()
                    .ok_or_else(|| anyhow!("Failed to get path"))?
                    .to_owned(),
            );
        }
    }
    Ok(files)
}

pub async fn do_postprocessing(
    kube_client: Client,
    app_root: &Path,
    mut user_state: UserState,
    nirvati_seed: &str,
    init_domain: Option<String>,
) -> Result<()> {
    let app_id = app_root
        .file_name()
        .ok_or_else(|| anyhow!("Failed to get app id"))?
        .to_str()
        .ok_or_else(|| anyhow!("Failed to get app id"))?
        .to_owned();
    let metadata = unsafe_load_app_basics(
        #[cfg(not(feature = "test-parser"))]
        kube_client.clone(),
        &app_id,
        app_root,
        user_state.clone(),
        nirvati_seed,
        init_domain.clone(),
    )
    .await?.metadata;
    let user_state_app_settings = user_state.app_settings.entry(app_id.clone()).or_default();
    let mut parsed_settings: BTreeMap<String, serde_json::Value> =
        serde_yaml::from_str(user_state_app_settings).unwrap_or_default();
    for (setting, config) in metadata.settings.0.iter() {
        parsed_settings
            .entry(setting.clone())
            .or_insert(config.default_value());
    }
    *user_state_app_settings = serde_json::to_string(&parsed_settings)?;

    let data_dir = app_root.join("data");
    if !data_dir.is_dir() {
        return Ok(());
    }

    let data_files = get_all_dir_files(&data_dir)?;
    for data_file in data_files
        .into_iter()
        .filter(|f| f.ends_with(".nirvati_jinja") || f.ends_with(".nirvati_jinja_hex"))
    {
        let is_hex = data_file.ends_with(".nirvati_jinja_hex");
        let data_file = Path::new(&data_file);
        let processed = super::tera::process_later_jinja(
            #[cfg(not(feature = "test-parser"))]
            kube_client.clone(),
            &app_id,
            data_file,
            nirvati_seed,
            &metadata.has_permissions,
            metadata.exported_data.clone().unwrap_or_default(),
            user_state.clone(),
            init_domain.clone(),
            None,
        )
        .await?;
        let processed_file = data_file.with_extension("");
        if is_hex {
            let processed = hex::decode(processed)?;
            std::fs::write(processed_file, processed)?;
        } else {
            std::fs::write(processed_file, processed)?;
        }
    }
    Ok(())
}
