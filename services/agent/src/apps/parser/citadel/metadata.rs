use citadel_apps::composegenerator::types::Permissions;
use nirvati::utils::MultiLanguageItem;
use nirvati_apps::metadata::{Dependency, Metadata, Runtime};
use slugify::slugify;
use std::collections::BTreeMap;
use std::str::FromStr;

pub fn convert_metadata(
    metadata: citadel_apps::composegenerator::types::OutputMetadata,
    exported_data: Option<BTreeMap<String, BTreeMap<String, serde_json::Value>>>,
) -> Metadata {
    Metadata {
        id: metadata.id.clone(),
        name: MultiLanguageItem::from_string(metadata.name),
        version: semver::Version::from_str(&format!("0.1.0+{}", slugify!(&metadata.id)))
            .expect("Failed to parse version"),
        display_version: metadata.version,
        category: MultiLanguageItem::from_string(metadata.category),
        tagline: MultiLanguageItem::from_string(metadata.tagline),
        developers: metadata.developers,
        description: MultiLanguageItem::from_string(metadata.description),
        dependencies: metadata
            .permissions
            .iter()
            .map(|perm| match perm {
                Permissions::OneDependency(dep) => Dependency::OneDependency(dep.clone()),
                Permissions::AlternativeDependency(deps) => {
                    Dependency::AlternativeDependency(deps.clone())
                }
            })
            .chain(vec![Dependency::OneDependency("tor".to_string())])
            .collect(),
        base_permissions: vec![
            "tor/proxy-requests".to_string(),
            "bitcoin/access-rpc".to_string(),
            "bitcoin/access-zmq".to_string(),
            "lnd/access-grpc".to_string(),
            "lnd/access-rest".to_string(),
        ],
        has_permissions: vec![
            "tor/proxy-requests".to_string(),
            "bitcoin/access-rpc".to_string(),
            "bitcoin/access-zmq".to_string(),
            "lnd/access-grpc".to_string(),
            "lnd/access-rest".to_string(),
        ],
        exposes_permissions: vec![],
        repos: metadata.repo,
        support: metadata.support,
        gallery: metadata.gallery.unwrap_or_default(),
        icon: Some(format!(
            "https://runcitadel.github.io/old-apps-gallery/{}/icon.svg",
            metadata.id
        )),
        path: metadata.path,
        default_username: metadata.default_username,
        default_password: metadata.default_password,
        implements: metadata.implements,
        runtime: Runtime::Kubernetes,
        supports_ingress: true,
        ports: Vec::new(),
        release_notes: Default::default(),
        license: "".to_string(),
        settings: Default::default(),
        volumes: Default::default(),
        exported_data,
        scope: Default::default(),
        allowed_scopes: vec![],
        default_scope: Default::default(),
        store_plugins: vec![],
        ui_module: None,
        can_be_protected: false,
        saas_compatibility: Default::default(),
        raw_ingress: false,
        post_install_redirect: None,
        components: vec![],
        is_citadel: true,
        preload_type: None,
    }
}
