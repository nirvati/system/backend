mod metadata;

use anyhow::bail;
use citadel_apps::composegenerator::convert_config;
use citadel_apps::composegenerator::output::types::{Command, StringOrInt, StringOrIntOrBool};
use citadel_apps::composegenerator::v4::utils::derive_entropy;
use kube::Client;
use nirvati::utils::split_with_quotes;
use nirvati_apps::internal::{
    CommonComponentConfig, Container, CustomResource, Deployment, HostVolume, Ingress, IngressType,
    InternalAppRepresentation, NetworkPolicy, NetworkPolicyEgress, NetworkPolicyIngress, PodConfig,
    Runnable, SecretRef, Service, ServiceType, Volume,
};
use nirvati_apps::utils::{StringLike, StringOrNumber};
use nirvati_plugin_common::UserState;
use nirvati_plugin_tor::types::{OnionServiceStoreNameInSecret, Port, PortWithSvc};
use serde_json::json;
use slugify::slugify;
use std::borrow::Cow;
use std::collections::{BTreeMap, HashMap};
use std::path::Path;

pub async fn load_app(
    #[cfg(not(feature = "test-parser"))] kube_client: &Client,
    app_root: &Path,
    user_state: UserState,
    nirvati_seed: &str,
    init_domain: Option<String>,
) -> anyhow::Result<InternalAppRepresentation> {
    let known_incompatible_apps = ["sphinx-relay", "public-pool", "bitcartcc"];
    let app_id = app_root.file_name().unwrap().to_str().unwrap();
    if known_incompatible_apps.contains(&app_id) {
        bail!("App {} is not compatible with Nirvati", app_id);
    }
    if std::env::var("CITADEL_BASE_DIR").is_err() {
        bail!("This does not seem to be a Citadel instance");
    }

    let exported_data = BTreeMap::from([(
        "bitcoin".to_string(),
        BTreeMap::from([(
            "info".to_string(),
            json!({ "rpc_accounts": [
                { "name": "main" }
            ] }),
        )]),
    )]);

    let plugins = crate::apps::parser::context::plugins::call_plugins(
        #[cfg(not(feature = "test-parser"))]
        kube_client.clone(),
        app_id,
        &[
            "tor/proxy-requests".to_string(),
            "bitcoin/access-rpc".to_string(),
            "bitcoin/access-zmq".to_string(),
        ],
        exported_data.clone(),
        &user_state,
    )
    .await?;

    let bitcoin_network =
        std::env::var("CITADEL_NETWORK").unwrap_or_else(|_| "mainnet".to_string());
    let citadel_base_dir = std::env::var("CITADEL_BASE_DIR")?;
    let citadel_app_data_dir = citadel_base_dir.clone() + "/app-data";
    let mock_env = HashMap::from([
        (
            "BITCOIN_IP".to_string(),
            format!("gateway.bitcoin.{}.nirvati", &user_state.user_id),
        ),
        ("BITCOIN_NETWORK".to_string(), bitcoin_network),
        ("BITCOIN_P2P_PORT".to_string(), "8333".to_string()),
        ("BITCOIN_RPC_PORT".to_string(), "8332".to_string()),
        (
            "BITCOIN_RPC_USER".to_string(),
            format!("{}-{}-main", &app_id, &user_state.user_id),
        ),
        (
            "BITCOIN_RPC_PASS".to_string(),
            plugins
                .get("bitcoin")
                .unwrap()
                .get("info")
                .unwrap()
                .as_object()
                .unwrap()
                .get("account_passwords")
                .unwrap()
                .as_object()
                .unwrap()
                .get("main")
                .unwrap()
                .as_str()
                .unwrap()
                .to_string(),
        ),
        // Citadel also has BITCOIN_RPC_AUTH, but that's unused
        ("BITCOIN_ZMQ_RAWBLOCK_PORT".to_string(), "28332".to_string()),
        ("BITCOIN_ZMQ_RAWTX_PORT".to_string(), "28333".to_string()),
        (
            "BITCOIN_ZMQ_HASHBLOCK_PORT".to_string(),
            "28334".to_string(),
        ),
        ("BITCOIN_ZMQ_SEQUENCE_PORT".to_string(), "28335".to_string()),
        ("TOR_PROXY_IP".to_string(), "proxy.tor.nirvati".to_string()),
        ("TOR_PROXY_PORT".to_string(), "9050".to_string()),
        (
            "BITCOIN_DATA_DIR".to_string(),
            format!("{}/bitcoin", citadel_base_dir),
        ),
        (
            "APP_DATA_DIR".to_string(),
            format!("{}/{}", citadel_app_data_dir, &app_id),
        ),
        ("CITADEL_APP_DATA".to_string(), citadel_app_data_dir),
        // Hardcode this even if it was dynamic on original Citadel
        ("APP_LND_SHARED_SUBDIR".to_string(), "lnd".to_string()),
        (
            "APP_ELECTRUM_IP".to_string(),
            format!("service.electrum.{}.nirvati", &user_state.user_id),
        ),
        (
            "APP_LND_SERVICE_IP".to_string(),
            format!("service.lnd.{}.nirvati", &user_state.user_id),
        ),
        (
            "APP_DOMAIN".to_string(),
            init_domain.clone().unwrap_or_default(),
        ),
        (
            "DEVICE_HOSTS".to_string(),
            init_domain.clone().unwrap_or_default(),
        ),
        (
            "APP_SEED".to_string(),
            derive_entropy(nirvati_seed, &format!("app-{}-seed", &app_id)),
        ),
        (
            "APP_SEED_1".to_string(),
            derive_entropy(nirvati_seed, &format!("app-{}-seed1", &app_id)),
        ),
        (
            "APP_SEED_2".to_string(),
            derive_entropy(nirvati_seed, &format!("app-{}-seed2", &app_id)),
        ),
        (
            "APP_SEED_3".to_string(),
            derive_entropy(nirvati_seed, &format!("app-{}-seed3", &app_id)),
        ),
        (
            "APP_SEED_4".to_string(),
            derive_entropy(nirvati_seed, &format!("app-{}-seed4", &app_id)),
        ),
        (
            "APP_SEED_5".to_string(),
            derive_entropy(nirvati_seed, &format!("app-{}-seed5", &app_id)),
        ),
    ]);
    let mut services = user_state.installed_apps.clone();
    services.push("bitcoind".to_string());
    citadel_apps::tera::convert_app_yml(
        app_root,
        &services,
        &mock_env,
        &Some(nirvati_seed.to_string()),
    )?;
    let app_yml = std::fs::File::open(app_root.join("app.yml"))?;
    let result = convert_config(
        app_root
            .file_name()
            .ok_or(anyhow::anyhow!("Failed to get app id"))?
            .to_str()
            .ok_or(anyhow::anyhow!("Failed to get app id"))?,
        app_yml,
        &None,
        &Some(services),
        &None,
    )?;

    let main_container = result.metadata.main_container.clone();
    let metadata = metadata::convert_metadata(result.metadata, Some(exported_data));

    let mut containers = BTreeMap::new();
    let mut services = BTreeMap::new();
    let mut ingress = Vec::new();

    let primary_route = result
        .caddy_entries
        .into_iter()
        .find(|entry| entry.is_primary);
    if let Some(primary_route) = primary_route {
        ingress.push(Ingress {
            target_service: Some(primary_route.container_name),
            target_ns: None,
            target_app: None,
            path_prefix: None,
            target_port: Some(primary_route.internal_port),
            r#type: IngressType::Https,
            enable_compression: true,
            strip_prefix: false,
            auth_exclude: false,
            component: None,
        });
    } else {
        ingress.push(Ingress {
            target_service: Some(main_container.clone()),
            target_ns: None,
            target_app: None,
            path_prefix: None,
            target_port: Some(3000),
            r#type: IngressType::Https,
            enable_compression: true,
            strip_prefix: false,
            auth_exclude: false,
            component: None,
        });
    }

    let Some(in_containers) = result.spec.services else {
        return Err(anyhow::anyhow!("No services found in app.yml"));
    };

    let first_ingress = ingress.first().unwrap();

    let mut crs = vec![];
    let mut onion_services = BTreeMap::new();
    for svc in result.new_tor_entries {
        let hs = nirvati_plugin_tor::types::OnionService {
            target_svc: first_ingress.target_service.clone().unwrap(),
            target_ns: None,
            ports: svc
                .targets
                .into_iter()
                .map(|(exposed_port, (target_ip, target_port))| {
                    (
                        exposed_port,
                        Port::WithSvc(PortWithSvc {
                            target_svc: slugify!(&target_ip),
                            target_ns: None,
                            target_port,
                        }),
                    )
                })
                .collect(),
            insecure_single_hop: None,
            store_name_in_secret: Some(OnionServiceStoreNameInSecret {
                key: "name".to_string(),
                name: format!("onion-svc-{}-name", svc.hs_id),
            }),
        };
        onion_services.insert(svc.hs_id, hs);
    }

    crs.push(CustomResource {
        definition: serde_yaml::to_value(&onion_services).unwrap(),
        app: "tor".to_string(),
        plugin_id: "onion-service".to_string(),
    });

    for (container_id, container) in in_containers {
        containers.insert(
            container_id.clone(),
            Runnable::Deployment(Box::new(Deployment {
                middlewares: Default::default(),
                container: Container {
                    cap_add: container.cap_add.unwrap_or_default(),
                    cap_drop: vec![], // Does not exist in Citadel
                    command: container.command.map(|cmd| match cmd {
                        Command::SimpleCommand(cmd) => split_with_quotes(&cmd),
                        Command::ArrayCommand(vec) => vec,
                    }),
                    entrypoint: container.entrypoint.map(|cmd| match cmd {
                        Command::SimpleCommand(cmd) => split_with_quotes(&cmd),
                        Command::ArrayCommand(vec) => vec,
                    }),
                    environment: container
                        .environment
                        .unwrap_or_default()
                        .into_iter()
                        .map(|(k, v)| {
                            (
                                k,
                                match v {
                                    StringOrIntOrBool::String(str) => {
                                        if str == "$APP_HIDDEN_SERVICE"
                                            || str == "${APP_HIDDEN_SERVICE}"
                                        {
                                            nirvati_apps::internal::EnvVar::SecretRef(SecretRef {
                                                key: "name".to_string(),
                                                secret: format!("onion-svc-app-{}-name", app_id),
                                            })
                                        } else if str.starts_with("$APP_HIDDEN_SERVICE_")
                                            || str.starts_with("${APP_HIDDEN_SERVICE_")
                                        {
                                            let hs_id = str
                                                .trim_start_matches("$APP_HIDDEN_SERVICE_")
                                                .trim_start_matches("${APP_HIDDEN_SERVICE_")
                                                .trim_end_matches('}');
                                            // Ensure hs_id only contains uppercase letters
                                            if hs_id.chars().all(|c| {
                                                c.is_ascii_uppercase()
                                                    || c.is_ascii_digit()
                                                    || c == '_'
                                            }) {
                                                nirvati_apps::internal::EnvVar::SecretRef(
                                                    SecretRef {
                                                        key: "name".to_string(),
                                                        secret: format!(
                                                            "onion-svc-app-{}-{}-name",
                                                            app_id,
                                                            hs_id.to_lowercase().replace("_", "-")
                                                        ),
                                                    },
                                                )
                                            } else {
                                                tracing::warn!(
                                                    "Failed to parse HS env var: {}",
                                                    str
                                                );
                                                nirvati_apps::internal::EnvVar::StringLike(
                                                    StringLike::String(str),
                                                )
                                            }
                                        } else {
                                            let new_val = shellexpand::env_with_context(
                                                &str,
                                                |arg| -> Result<_, ()> {
                                                    if arg.starts_with("APP_")
                                                        && arg.ends_with("_IP")
                                                        && arg != "APP_ELECTRUM_IP"
                                                    {
                                                        let components =
                                                            arg.rsplitn(3, "_").collect::<Vec<_>>();
                                                        let container_id = components[1]
                                                            .to_lowercase()
                                                            .replace("_", "-");
                                                        let Some(app_id) = components[2]
                                                            .split_once("_")
                                                            .map(|(_, s)| {
                                                                s.to_lowercase().replace("_", "-")
                                                            })
                                                        else {
                                                            tracing::warn!(
                                                                "Invalid env var format: {}",
                                                                arg
                                                            );
                                                            return Ok(None);
                                                        };
                                                        return Ok(Some(Cow::Owned(format!(
                                                            "{}.{}.{}.nirvati",
                                                            container_id,
                                                            app_id,
                                                            &user_state.user_id
                                                        ))));
                                                    };
                                                    if arg
                                                        == format!(
                                                            "APP_{}_{}_PORT",
                                                            app_id.to_uppercase().replace("-", "_"),
                                                            main_container
                                                                .to_uppercase()
                                                                .replace("-", "_")
                                                        )
                                                    {
                                                        return Ok(Some(Cow::Owned(
                                                            "3000".to_string(),
                                                        )));
                                                    }

                                                    let data = mock_env
                                                        .get(arg)
                                                        .map(|val| Cow::Owned(val.clone()));
                                                    if data.is_none() {
                                                        tracing::warn!(
                                                            "Env var not found: {}",
                                                            arg
                                                        );
                                                    }
                                                    Ok(data)
                                                },
                                            )
                                            .unwrap();
                                            nirvati_apps::internal::EnvVar::StringLike(
                                                StringLike::String(new_val.to_string()),
                                            )
                                        }
                                    }
                                    StringOrIntOrBool::Int(int) => {
                                        nirvati_apps::internal::EnvVar::StringLike(StringLike::Int(
                                            int,
                                        ))
                                    }
                                    StringOrIntOrBool::Bool(bool) => {
                                        nirvati_apps::internal::EnvVar::StringLike(
                                            StringLike::Bool(bool),
                                        )
                                    }
                                },
                            )
                        })
                        .collect(),
                    hostname: container.hostname,
                    image: container.image,
                    additional_labels: BTreeMap::from([(
                        "uses-compat-layer".to_string(),
                        "citadel".to_string(),
                    )]),
                    privileged: false,
                    restart: None,
                    stop_grace_period: container.stop_grace_period,
                    uid: container
                        .user
                        .as_ref()
                        .and_then(|user| user.split(":").next().unwrap().parse().ok()),
                    gid: container
                        .user
                        .as_ref()
                        .and_then(|user| user.split(":").nth(1).unwrap().parse().ok()),
                    volumes: container
                        .volumes
                        .into_iter()
                        .filter_map(|vol| {
                            let mut split = vol.split(':');
                            let host = split.next().expect("No host path defined!");
                            if host == "jwt-public-key" {
                                return None;
                            }

                            let new_host =
                                shellexpand::env_with_context(host, |arg| -> Result<_, ()> {
                                    let data = mock_env.get(arg).map(|val| Cow::Owned(val.clone()));
                                    if data.is_none() {
                                        tracing::warn!("Env var not found: {}", arg);
                                    }
                                    Ok(data)
                                })
                                .unwrap();

                            let container = split.next().expect("No container path defined!");
                            // Will not happen in practice with current Citadel, but just keep it here anyway
                            let is_readonly = split.next().map(|ro| ro == "ro").unwrap_or(false);
                            Some(Volume::Host(HostVolume {
                                name: slugify!(&new_host),
                                host_path: new_host.to_string(),
                                mount_path: container.to_string(),
                                is_readonly,
                            }))
                        })
                        .collect(),
                    working_dir: container.working_dir,
                    shm_size: container.shm_size.map(|shm_size| match shm_size {
                        StringOrInt::String(str) => StringOrNumber::String(str),
                        StringOrInt::Int(int) => StringOrNumber::Int(int as i64),
                    }),
                    ulimits: None,
                    host_network: container.network_mode == Some("host".to_string()),
                    exposes: Default::default(),
                    fs_group: None,
                    service_account: None,
                    network_policy: None,
                    has_permissions: vec![],
                },
            })),
        );
        services.insert(
            container_id.clone(),
            Service {
                r#type: ServiceType::ClusterIp,
                target_container: container_id.clone(),
                cluster_ip: Some("None".to_string()),
                ports: Default::default(),
                network_policy: None,
            },
        );
        if !container.public_ports.is_empty() {
            services.insert(
                format!("{}-public-ports", container_id.clone()),
                Service {
                    r#type: ServiceType::LoadBalancer,
                    target_container: container_id.clone(),
                    cluster_ip: None,
                    ports: container.public_ports,
                    network_policy: None,
                },
            );
        }
    }

    // Postprocessing

    for file in std::fs::read_dir(app_root)? {
        let file = file?;
        let path = file.path();
        if !path.is_file() {
            continue;
        }
        if file
            .path()
            .extension()
            .is_some_and(|ext| ext == "nirvati_jinja")
        {
            let processed = super::app_yml::tera::process_later_jinja(
                #[cfg(not(feature = "test-parser"))]
                kube_client.clone(),
                app_id,
                &file.path(),
                nirvati_seed,
                &metadata.has_permissions,
                metadata.exported_data.clone().unwrap_or_default(),
                user_state.clone(),
                init_domain.clone(),
                Some(&mock_env),
            )
            .await?;
            let out_path = file.path().with_extension("");
            std::fs::write(&out_path, processed)?;
        }
    }

    Ok(InternalAppRepresentation::new(
        metadata,
        containers,
        services,
        ingress,
        vec![],
        Default::default(),
        crs,
        Some(NetworkPolicy {
            ingress: NetworkPolicyIngress {
                allow_world: CommonComponentConfig {
                    allow: true,
                    allowed_ports: None,
                },
                allow_cluster: CommonComponentConfig {
                    allow: false,
                    allowed_ports: None,
                },
                allow_same_ns: CommonComponentConfig {
                    allow: true,
                    allowed_ports: None,
                },
                cidrs: vec![],
                pods: vec![PodConfig {
                    namespace: None,
                    app: None,
                    match_labels: Some(HashMap::from([(
                        "uses-compat-layer".to_string(),
                        "citadel".to_string(),
                    )])),
                    allowed_ports: None,
                    all_namespaces: true,
                }],
            },
            egress: NetworkPolicyEgress {
                allow_cluster: false,
                allow_same_ns: true,
                allow_kube_api: false,
                allow_world: true,
                allow_local_net: true,
                dns: None,
                fqdns: vec![],
                cidrs: vec![],
                services: vec![],
                pods: vec![PodConfig {
                    namespace: None,
                    app: None,
                    match_labels: Some(HashMap::from([(
                        "uses-compat-layer".to_string(),
                        "citadel".to_string(),
                    )])),
                    allowed_ports: None,
                    all_namespaces: true,
                }],
            },
        }),
        vec![],
    ))
}
