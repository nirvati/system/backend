// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::path::Path;

use crate::apps::generator::internal::{load_permissions, ResolverCtx};
use crate::apps::repos::is_official_store;
use crate::plugins::api::UserState;
use anyhow::Result;
use futures::stream::FuturesUnordered;
use futures::FutureExt;
use futures::StreamExt;
use kube::Client;
use nirvati_apps::internal::InternalAppRepresentation;
use nirvati_apps::metadata::SaasCompatibility;

pub mod app_yml;
mod citadel;
pub mod context;
mod plugin;

use crate::apps::files::read_stores_yml;
pub use app_yml::do_postprocessing;

pub async fn process_apps(
    kube_client: Client,
    global_apps_root: &Path,
    user_state: UserState,
    nirvati_seed: &str,
    ctx: &ResolverCtx,
) -> Result<Vec<InternalAppRepresentation>> {
    let apps_root = global_apps_root.join(&user_state.user_id);
    let mut out_apps = Vec::new();
    let apps = std::fs::read_dir(&apps_root)?
        .filter_map(|entry| {
            let entry = match entry {
                Ok(entry) => entry,
                Err(err) => {
                    tracing::error!("Failed to read app directory: {:#?}", err);
                    return None;
                }
            };
            let path = entry.path();
            #[cfg(not(feature = "test-parser"))]
            let client = kube_client.clone();
            let user_state = user_state.clone();
            if path.is_dir() {
                let future = async move {
                    load_app(
                        #[cfg(not(feature = "test-parser"))]
                        client,
                        global_apps_root,
                        path.file_name().unwrap().to_str().unwrap(),
                        &path,
                        user_state,
                        nirvati_seed,
                        None,
                        ctx,
                    )
                    .await
                }
                .map(move |app| (app, entry.path()));

                Some(future)
            } else {
                None
            }
        })
        .collect::<FuturesUnordered<_>>()
        .collect::<Vec<_>>()
        .await;
    for (app, path) in apps {
        match app {
            Ok(app) => out_apps.push(app),
            Err(err) => {
                tracing::warn!("Failed to load app details: {:#}", err);
                let basic_app = unsafe_load_app_basics(
                    #[cfg(not(feature = "test-parser"))]
                    kube_client.clone(),
                    path.file_name().unwrap().to_str().unwrap(),
                    &path,
                    user_state.clone(),
                    nirvati_seed,
                    None,
                )
                .await;
                match basic_app {
                    Ok(app) => out_apps.push(app),
                    Err(err) => {
                        tracing::error!("Failed to load app: {:#}", err);
                    }
                }
            }
        }
    }
    Ok(out_apps)
}

pub async fn load_app(
    #[cfg(not(feature = "test-parser"))] kube_client: Client,
    global_apps_root: &Path,
    app: &str,
    app_path: &Path,
    user_state: UserState,
    nirvati_seed: &str,
    init_domain: Option<String>,
    resolver_ctx: &ResolverCtx,
) -> Result<InternalAppRepresentation> {
    let apps_root = &global_apps_root.join(&user_state.user_id);
    resolver_ctx.load_installed_apps().await?;
    let mut app = load_app_inner(
        #[cfg(not(feature = "test-parser"))]
        kube_client,
        app,
        app_path,
        user_state,
        nirvati_seed,
        init_domain,
    )
    .await?;
    load_permissions(&mut app, resolver_ctx).await?;
    app.metadata.saas_compatibility = nirvati_saas::saas_compatibility_state(&app);
    if app.metadata.id == "docker" {
        let Ok(stores_yml) = read_stores_yml(apps_root) else {
            // This can happen during repo sync sometimes, so we just ignore it
            return Ok(app);
        };
        if stores_yml
            .into_iter()
            .find(|store| store.apps.contains(&app.metadata.id))
            .is_some_and(|store| is_official_store(&store))
        {
            app.metadata.saas_compatibility = SaasCompatibility::Compatible;
            // Remove builtin/root from permissions
            app.metadata
                .has_permissions
                .retain(|perm| perm != "builtin/root" && !perm.starts_with("builtin/"));
        }
    };
    Ok(app)
}
pub async fn load_app_inner(
    #[cfg(not(feature = "test-parser"))] kube_client: Client,
    app: &str,
    app_path: &Path,
    user_state: UserState,
    nirvati_seed: &str,
    init_domain: Option<String>,
) -> Result<InternalAppRepresentation> {
    if app_path.is_dir() {
        let has_metadata =
            app_path.join("metadata.yml").exists() || app_path.join("metadata.yml.jinja").exists();
        let has_app_yml =
            app_path.join("app.yml").exists() || app_path.join("app.yml.jinja").exists();
        if has_metadata && has_app_yml {
            match app_yml::load_app(
                #[cfg(not(feature = "test-parser"))]
                kube_client.clone(),
                app_path,
                user_state.clone(),
                nirvati_seed,
                init_domain.clone(),
            )
            .await
            {
                Ok(result) => return Ok(result),
                Err(err) => {
                    tracing::warn!(
                        "Failed to load app {} with builtin app.yml parser, trying Citadel: {:#?}",
                        app,
                        err
                    );
                }
            }
        };
        if has_app_yml {
            match citadel::load_app(
                #[cfg(not(feature = "test-parser"))]
                &kube_client,
                app_path,
                user_state.clone(),
                nirvati_seed,
                init_domain.clone(),
            )
            .await
            {
                Ok(result) => return Ok(result),
                Err(err) => {
                    tracing::warn!(
                        "Failed to load app {} with legacy Citadel parser, trying plugins: {:#?}",
                        app,
                        err
                    );
                }
            }
        }
        plugin::load_app(
            #[cfg(not(feature = "test-parser"))]
            kube_client,
            app_path,
            app,
            user_state,
            &init_domain,
            nirvati_seed,
        )
        .await
    } else {
        tracing::error!("App path is not a directory: {:#?}", app_path);
        Err(anyhow::anyhow!("App {} does not exist", app))
    }
}

// This does not give correct output regarding permissions. Only to be used when we can't have a
// ResolverCtx for any reason
pub async fn unsafe_load_app_basics(
    #[cfg(not(feature = "test-parser"))] kube_client: Client,
    app: &str,
    app_path: &Path,
    user_state: UserState,
    nirvati_seed: &str,
    init_domain: Option<String>,
) -> Result<InternalAppRepresentation> {
    load_app_inner(
        #[cfg(not(feature = "test-parser"))]
        kube_client,
        app,
        app_path,
        user_state,
        nirvati_seed,
        init_domain,
    )
    .await
}
