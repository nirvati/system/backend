// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::Result;
use kube::Client;
use std::collections::{BTreeMap, HashMap};
use tonic::Request;

use crate::constants::RESERVED_NAMES;
use crate::plugins::api::{GetContextRequest, UserState};
use crate::plugins::get_plugins;
use nirvati_apps::internal::types::PluginType;
#[cfg(feature = "test-parser")]
use nirvati_plugin_common::context_plugin_server::ContextPlugin;

#[cfg(not(feature = "test-parser"))]
pub async fn call_plugins(
    kube_client: Client,
    app_id: &str,
    perms: &[String],
    additional_metadata: BTreeMap<String, BTreeMap<String, serde_json::Value>>,
    user_state: &UserState,
) -> Result<HashMap<String, HashMap<String, serde_json::Value>>> {
    let mut plugins_ctx = HashMap::new();
    // Perms are in the format <app-id>/<perm> or just <app-id>
    let mut perms_by_app = HashMap::new();
    for perm in perms {
        let mut split = perm.split('/');
        let app_id = split.next().unwrap();
        let perm = split.next().unwrap_or("");
        if RESERVED_NAMES.contains(&app_id) {
            continue;
        }
        let perms = perms_by_app
            .entry(app_id.to_owned())
            .or_insert_with(Vec::new);
        perms.push(perm.to_owned());
    }
    let plugins = get_plugins(&user_state.user_id, kube_client).await?;
    let plugins = plugins.into_iter().filter_map(|(plugin_name, plugin)| {
        let Some((plugin_app, plugin_id)) = plugin_name.split_once("-plugin-") else {
            tracing::warn!("Invalid plugin name: {}", plugin_name);
            return None;
        };
        if !perms_by_app.contains_key(plugin_app) {
            None
        } else if plugin.r#type == PluginType::Context {
            Some((plugin_app.to_string(), plugin_id.to_string(), plugin))
        } else {
            None
        }
    });
    for (plugin_app_id, plugin_id, plugin_endpoint) in plugins
        .into_iter()
        .filter(|(_, _, plugin)| plugin.r#type == PluginType::Context)
    {
        let metadata = additional_metadata
            .get(&plugin_app_id.to_string())
            .and_then(|metadata| metadata.get(&plugin_id.to_string()));
        let serialized_metadata = if let Some(metadata) = metadata {
            serde_json::to_string(metadata)?
        } else {
            "{}".to_owned()
        };
        let mut client =
            match crate::plugins::api::context_plugin_client::ContextPluginClient::connect(
                plugin_endpoint.endpoint.clone(),
            )
            .await
            {
                Ok(client) => client,
                Err(err) => {
                    tracing::error!(
                        "Failed to connect to plugin {}/{}: {:#}",
                        plugin_app_id,
                        plugin_id,
                        err
                    );
                    continue;
                }
            };
        let additions = match client
            .get_context(Request::new(GetContextRequest {
                app_id: app_id.to_owned(),
                permissions: perms_by_app[&plugin_app_id.to_string()].clone(),
                additional_data: serialized_metadata,
                user_state: Some(user_state.clone()),
            }))
            .await
        {
            Ok(additions) => additions.into_inner(),
            Err(err) => {
                tracing::error!(
                    "Failed to get jinja context for app {} from plugin {}/{}: {:#}",
                    app_id,
                    plugin_app_id,
                    plugin_id,
                    err
                );
                continue;
            }
        };
        let deserialized: serde_json::Value = match serde_json::from_str(&additions.context) {
            Ok(deserialized) => deserialized,
            Err(err) => {
                tracing::error!(
                    "Failed to deserialize jinja context from plugin {}/{}: {:#}",
                    plugin_app_id,
                    plugin_id,
                    err
                );
                continue;
            }
        };
        plugins_ctx
            .entry(plugin_app_id.to_string())
            .or_insert(HashMap::new())
            .insert(plugin_id.to_string(), deserialized);
    }
    tracing::debug!("Plugins context: {:#?}", plugins_ctx);
    Ok(plugins_ctx)
}

#[cfg(feature = "test-parser")]
pub async fn call_plugins(
    app_id: &str,
    perms: &[String],
    additional_metadata: BTreeMap<String, BTreeMap<String, serde_json::Value>>,
    user_state: &UserState,
) -> Result<HashMap<String, HashMap<String, serde_json::Value>>> {
    let mut plugins_ctx = HashMap::new();
    // Perms are in the format <app-id>/<perm> or just <app-id>
    let mut perms_by_app = HashMap::new();
    for perm in perms {
        let mut split = perm.split('/');
        let app_id = split.next().unwrap();
        let perm = split.next().unwrap_or("");
        if RESERVED_NAMES.contains(&app_id) {
            continue;
        }
        let perms = perms_by_app
            .entry(app_id.to_owned())
            .or_insert_with(Vec::new);
        perms.push(perm.to_owned());
    }
    for (plugin_app_id, plugin_id) in [("bitcoin", "info"), ("lnd", "auth")] {
        let metadata = additional_metadata
            .get(&plugin_app_id.to_string())
            .and_then(|metadata| metadata.get(&plugin_id.to_string()));
        let serialized_metadata = if let Some(metadata) = metadata {
            serde_json::to_string(metadata)?
        } else {
            "{}".to_owned()
        };
        let mut client: Box<dyn ContextPlugin> = match plugin_app_id {
            "lnd" => Box::new(nirvati_plugin_lnd::plugin::PluginState {
                btc_network: "mainnet".to_string(),
                client: None,
                tls_cert: "placeholder_cert".to_string(),
            }),
            "bitcoin" => Box::new(nirvati_plugin_bitcoin::plugin::PluginState {
                pw_seed: "placeholder".to_string(),
                btc_network: "mainnet".to_string(),
                is_mock: true,
            }),
            _ => unreachable!(),
        };
        if !perms_by_app.contains_key(plugin_app_id) {
            continue;
        }
        let additions = match client
            .get_context(Request::new(GetContextRequest {
                app_id: app_id.to_owned(),
                permissions: perms_by_app[&plugin_app_id.to_string()].clone(),
                additional_data: serialized_metadata,
                user_state: Some(user_state.clone()),
            }))
            .await
        {
            Ok(additions) => additions.into_inner(),
            Err(err) => {
                tracing::error!(
                    "Failed to get jinja context for app {} from plugin {}/{}: {:#}",
                    app_id,
                    plugin_app_id,
                    plugin_id,
                    err
                );
                continue;
            }
        };
        let deserialized: serde_json::Value = match serde_json::from_str(&additions.context) {
            Ok(deserialized) => deserialized,
            Err(err) => {
                tracing::error!(
                    "Failed to deserialize jinja context from plugin {}/{}: {:#}",
                    plugin_app_id,
                    plugin_id,
                    err
                );
                continue;
            }
        };
        plugins_ctx
            .entry(plugin_app_id.to_string())
            .or_insert(HashMap::new())
            .insert(plugin_id.to_string(), deserialized);
    }
    tracing::debug!("Plugins context: {:#?}", plugins_ctx);
    Ok(plugins_ctx)
}
