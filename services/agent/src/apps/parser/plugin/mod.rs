// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::path::Path;

use crate::plugins::api::UserState;
use crate::plugins::get_plugins;
use anyhow::anyhow;
use futures::stream::FuturesUnordered;
use kube::Client;
use nirvati_apps::internal::types::InternalAppRepresentation;
use nirvati_apps::internal::PluginType;
use nirvati_apps::metadata::Runtime;
use tokio_stream::StreamExt;
use tonic::Request;
use nirvati::utils::derive_entropy;

pub async fn load_app(
    #[cfg(not(feature = "test-parser"))] kube_client: Client,
    app_root: &Path,
    app_id: &str,
    user_state: UserState,
    initial_domain: &Option<String>,
    nirvati_seed: &str,
) -> anyhow::Result<InternalAppRepresentation> {
    let app_seed_base = derive_entropy(
        nirvati_seed,
        format!("{}:{}", user_state.user_id, app_id).as_str(),
    );
    #[cfg(not(feature = "test-parser"))]
    {
        let app_dir = crate::utils::tar::compress_dir(app_root).await?;
        let plugins = get_plugins(&user_state.user_id, kube_client).await?;
        let threads = plugins.into_iter().filter_map(|(plugin_name, plugin)| {
            if plugin.r#type == PluginType::Runtime {
                Some(async {
                    let mut plugin_endpoint =
                        crate::plugins::api::runtime_plugin_client::RuntimePluginClient::connect(
                            plugin.endpoint,
                        )
                        .await?;
                    let resp = plugin_endpoint
                        .is_supported_app(crate::plugins::api::IsSupportedAppRequest {
                            app_id: app_id.to_string(),
                            app_directory: app_dir.clone(),
                            user_state: Some(user_state.clone()),
                        })
                        .await?
                        .into_inner();
                    if resp.supported {
                        let parsed = plugin_endpoint
                            .parse_app(crate::plugins::api::ParseAppRequest {
                                app_id: app_id.to_string(),
                                app_directory: app_dir.clone(),
                                user_state: Some(user_state.clone()),
                                initial_domain: initial_domain.clone(),
                                app_seed_base: app_seed_base.clone(),
                            })
                            .await?
                            .into_inner();
                        let mut app: InternalAppRepresentation =
                            serde_json::from_str(&parsed.parsed_app)?;
                        app.set_runtime(Runtime::Plugin(plugin_name));
                        app.finalize();
                        Ok(Some(app))
                    } else {
                        Ok(None)
                    }
                })
            } else {
                None
            }
        });
        let mut futures_unordered = FuturesUnordered::from_iter(threads);
        let mut return_err = None;
        while let Some(result) = futures_unordered.next().await {
            match result {
                Ok(Some(mut app)) => {
                    // Ignore this flag if coming from a plugin, it is for internal use only
                    app.metadata.is_citadel = false;
                    return Ok(app);
                }
                Ok(None) => continue,
                Err(err) => return_err = Some(err),
            }
        }
        if let Some(err) = return_err {
            Err(err)
        } else {
            Err(anyhow!("No plugins found for app"))
        }
    }
    #[cfg(feature = "test-parser")]
    {
        let app_dir = crate::utils::tar::compress_dir(app_root).await?;
        use nirvati_plugin_common::runtime_plugin_server::RuntimePlugin;
        let mut plugin_endpoint = nirvati_plugin_tipi::plugin::TipiPlugin {};
        let resp = plugin_endpoint
            .is_supported_app(Request::new(nirvati_plugin_common::IsSupportedAppRequest {
                app_id: app_id.to_string(),
                app_directory: app_dir.clone(),
                user_state: Some(user_state.clone()),
            }))
            .await?
            .into_inner();
        if resp.supported {
            let parsed = plugin_endpoint
                .parse_app(Request::new(nirvati_plugin_common::ParseAppRequest {
                    app_id: app_id.to_string(),
                    app_directory: app_dir.clone(),
                    user_state: Some(user_state.clone()),
                    initial_domain: initial_domain.clone(),
                    app_seed_base: app_seed_base.clone(),
                }))
                .await?
                .into_inner();
            let mut app: InternalAppRepresentation = serde_json::from_str(&parsed.parsed_app)?;
            app.set_runtime(Runtime::Plugin("tipi".to_string()));
            app.finalize();
            Ok(app)
        } else {
            Err(anyhow!("No plugins found for app"))
        }
    }
}
