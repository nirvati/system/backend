// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::BTreeMap;
use std::path::Path;

use anyhow::{anyhow, bail};
use kube::Client;
use serde::{Deserialize, Serialize};
use tempfile::TempDir;
use tonic::Request;

use nirvati::utils::MultiLanguageItem;
use nirvati_apps::internal::types::PluginType;
use nirvati_apps::stores::StoreMetadata;

use crate::apps::repos::types::{AppStore, AppUpdateInfo, StoreType};
use crate::plugins::api::{DownloadAppRequest, DownloadAppsRequest, UserState};
use crate::plugins::get_plugins;

#[derive(Serialize, Deserialize)]
struct ProviderData {
    pub plugin: String,
    pub state: Option<serde_json::Value>,
}

async fn get_plugin_endpoint(
    kube_client: Client,
    plugin: &str,
    user: &str,
) -> anyhow::Result<String> {
    let plugins = get_plugins(user, kube_client).await?;
    let endpoint = plugins
        .get(plugin)
        .ok_or_else(|| anyhow!("Plugin not found"))?;
    if endpoint.r#type != PluginType::Source {
        return Err(anyhow!("Plugin is not a source plugin"));
    }
    Ok(endpoint.endpoint.clone())
}

pub async fn download_app(
    kube_client: Client,
    global_apps_root: &Path,
    app: &str,
    store: &mut AppStore,
    user_state: UserState,
) -> anyhow::Result<()> {
    let apps_root = global_apps_root.join(&user_state.user_id);
    let provider_data: ProviderData = serde_yaml::from_value(store.provider_data.clone())?;
    let state = provider_data.state;
    let encoded_state = serde_json::to_string(&state.unwrap_or(serde_json::Value::Null))?;
    let endpoint =
        get_plugin_endpoint(kube_client, &provider_data.plugin, &user_state.user_id).await?;
    let mut client =
        crate::plugins::api::source_plugin_client::SourcePluginClient::connect(endpoint).await?;
    let app_data = client
        .download_app(Request::new(DownloadAppRequest {
            source: store.src.clone(),
            app_id: app.to_string(),
            state: encoded_state,
            user_state: user_state.clone().into(),
        }))
        .await?
        .into_inner();
    // Now, extract the app data
    let app_dir = apps_root.join(app);
    crate::utils::tar::safe_decompress_dir(app_data.app_directory, &app_dir).await?;
    let parsed_state = serde_json::from_str(&app_data.state)?;
    store.provider_data = serde_yaml::to_value(ProviderData {
        plugin: provider_data.plugin.clone(),
        state: Some(parsed_state),
    })?;
    Ok(())
}

#[cfg(feature = "test-parser")]
pub async fn download_apps_for_store(
    global_apps_root: &Path,
    store: &mut AppStore,
    skip_apps: &[String],
    user_state: UserState,
) -> anyhow::Result<Vec<String>> {
    bail!("No store plugins mocked!");
}
#[cfg(not(feature = "test-parser"))]
pub async fn download_apps_for_store(
    kube_client: Client,
    global_apps_root: &Path,
    store: &mut AppStore,
    skip_apps: &[String],
    user_state: UserState,
) -> anyhow::Result<Vec<String>> {
    let apps_root = global_apps_root.join(&user_state.user_id);
    let provider_data: ProviderData = serde_yaml::from_value(store.provider_data.clone())?;
    let state = provider_data.state;
    let encoded_state = serde_json::to_string(&state.unwrap_or(serde_json::Value::Null))?;
    let endpoint =
        get_plugin_endpoint(kube_client, &provider_data.plugin, &user_state.user_id).await?;
    let mut client =
        crate::plugins::api::source_plugin_client::SourcePluginClient::connect(endpoint).await?;
    let app_data = client
        .download_all_apps(Request::new(DownloadAppsRequest {
            source: store.src.clone(),
            skip_apps: skip_apps.to_vec(),
            state: encoded_state,
            user_state: user_state.clone().into(),
        }))
        .await?
        .into_inner();
    // Now, extract the app data to a temporary directory
    let mut downloaded_apps = Vec::new();
    let temp_dir = TempDir::new()?;
    crate::utils::tar::safe_decompress_dir(app_data.apps_directory, temp_dir.path()).await?;
    for entry in temp_dir.path().read_dir()? {
        let entry = entry?;
        let app = entry.file_name().to_string_lossy().to_string();
        if skip_apps.contains(&app) {
            tracing::warn!(
                "Store plugin tried to download app that was skipped: {}",
                app
            );
            continue;
        }
        let app_dir = apps_root.join(&app);
        std::fs::rename(entry.path(), &app_dir)?;
        downloaded_apps.push(app);
    }
    let parsed_state = serde_json::from_str(&app_data.state)?;
    store.provider_data = serde_yaml::to_value(ProviderData {
        plugin: provider_data.plugin.clone(),
        state: Some(parsed_state),
    })?;
    Ok(downloaded_apps)
}

pub async fn check_updates_for_store(
    kube_client: Client,
    store: AppStore,
    app: Option<String>,
    user_state: UserState,
) -> anyhow::Result<Vec<AppUpdateInfo>> {
    let provider_data: ProviderData = serde_yaml::from_value(store.provider_data.clone())?;
    let state = provider_data.state;
    let encoded_state = serde_json::to_string(&state.unwrap_or(serde_json::Value::Null))?;
    let endpoint =
        get_plugin_endpoint(kube_client, &provider_data.plugin, &user_state.user_id).await?;
    let mut client =
        crate::plugins::api::source_plugin_client::SourcePluginClient::connect(endpoint).await?;
    if let Some(app) = app {
        let resp = client
            .get_update(Request::new(crate::plugins::api::GetUpdateRequest {
                source: store.src,
                app_id: app.clone(),
                state: encoded_state,
                user_state: user_state.into(),
            }))
            .await?
            .into_inner();
        let mut updates = Vec::new();
        if let Some(update) = resp.update {
            let parsed_release_notes: BTreeMap<String, BTreeMap<String, String>> =
                serde_json::from_str(&update.release_notes)?;
            updates.push(AppUpdateInfo {
                id: app,
                new_version: update.new_version,
                release_notes: parsed_release_notes,
                has_permissions: update.has_permissions,
            });
        }
        Ok(updates)
    } else {
        let resp = client
            .get_all_updates(Request::new(crate::plugins::api::GetAllUpdatesRequest {
                source: store.src,
                state: encoded_state,
                user_state: user_state.into(),
            }))
            .await?
            .into_inner();
        let mut updates = Vec::new();
        for (app, update) in resp.updates {
            let parsed_release_notes: BTreeMap<String, BTreeMap<String, String>> =
                serde_json::from_str(&update.release_notes)?;
            updates.push(AppUpdateInfo {
                id: app,
                new_version: update.new_version,
                release_notes: parsed_release_notes,
                has_permissions: update.has_permissions,
            });
        }
        Ok(updates)
    }
}

pub async fn load_store_metadata(
    kube_client: Client,
    store: &mut AppStore,
    user: &str,
) -> anyhow::Result<StoreMetadata> {
    if let StoreType::Plugin = store.r#type {
        let provider_data: ProviderData = serde_yaml::from_value(store.provider_data.clone())?;
        let endpoint = get_plugin_endpoint(kube_client, &provider_data.plugin, user).await?;
        let mut client =
            crate::plugins::api::source_plugin_client::SourcePluginClient::connect(endpoint)
                .await?;
        let resp = client
            .get_app_store_metadata(Request::new(
                crate::plugins::api::GetAppStoreMetadataRequest {
                    source: store.src.clone(),
                },
            ))
            .await?
            .into_inner();
        let metadata = StoreMetadata {
            name: MultiLanguageItem::from_hashmap(resp.name),
            tagline: MultiLanguageItem::from_hashmap(resp.tagline),
            description: MultiLanguageItem::from_hashmap(resp.description),
            icon: resp.icon,
            developers: BTreeMap::from_iter(resp.developers.into_iter()),
            license: resp.license,
        };
        Ok(metadata)
    } else {
        bail!("Store is not a plugin store")
    }
}

pub fn get_plugin(store: &AppStore) -> anyhow::Result<String> {
    let provider_data: ProviderData = serde_yaml::from_value(store.provider_data.clone())?;
    Ok(provider_data.plugin)
}
