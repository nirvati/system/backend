// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::{BTreeMap, HashMap};

use anyhow::anyhow;
use serde::{Deserialize, Serialize};

use nirvati_apps::stores::StoreMetadata;

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct AppStore {
    pub id: uuid::Uuid,
    #[serde(flatten)]
    pub metadata: StoreMetadata,
    #[serde(default)]
    pub r#type: StoreType,
    pub src: HashMap<String, String>,
    pub apps: Vec<String>,

    // Data specific to the app store type
    pub provider_data: serde_yaml::Value,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AppUpdateInfo {
    pub id: String,
    pub new_version: String,
    pub release_notes: BTreeMap<String, BTreeMap<String, String>>,
    pub has_permissions: Vec<String>,
}

pub type StoresYml = Vec<AppStore>;

#[derive(Debug, Serialize, Deserialize, Clone, Default, Eq, PartialEq)]
pub enum StoreType {
    #[default]
    // Rename for backwards compatibility
    #[serde(rename = "Nirvati")]
    Git,
    Plugin,
}

impl TryFrom<i32> for StoreType {
    type Error = anyhow::Error;

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(StoreType::Git),
            -1 => Ok(StoreType::Plugin),
            _ => Err(anyhow!("Invalid data!")),
        }
    }
}
