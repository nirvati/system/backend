// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::{anyhow, bail, Result};
use gix::bstr::{BStr, ByteSlice};
use kube::Client;
use nirvati::utils::MultiLanguageItem;
use nirvati_apps::stores::StoreMetadata;
use semver::Version;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::path::Path;
use tempfile::TempDir;

use crate::apps::generator::internal::ResolverCtx;
use crate::apps::parser::load_app;
use crate::apps::repos::git::files::load_app_store_yml;
use crate::apps::repos::git::types::{AppStoreV1, AppStoreYml};
use crate::apps::repos::types::{AppStore, AppUpdateInfo, StoreType};
use crate::constants::{MINIMUM_COMPATIBLE_APP_MANAGER, RESERVED_NAMES};
use crate::plugins::api::UserState;
use crate::utils::has_symlinks;

mod citadel_compat;
mod files;
mod types;

#[derive(Serialize, Deserialize)]
struct ProviderData {
    pub apps: HashMap<String, String>,
    pub commit: String,
}

impl From<serde_yaml::Value> for ProviderData {
    fn from(value: serde_yaml::Value) -> Self {
        serde_yaml::from_value(value).unwrap_or(Self {
            apps: HashMap::new(),
            commit: String::new(),
        })
    }
}

struct RepoSrc {
    repo_url: gix::url::Url,
    branch: String,
}

impl TryFrom<HashMap<String, String>> for RepoSrc {
    type Error = anyhow::Error;

    fn try_from(value: HashMap<String, String>) -> Result<Self> {
        let repo_url = value
            .get("repo_url")
            .ok_or_else(|| anyhow!("No repo_url found in store"))?
            .clone();
        let repo_url = gix::url::parse(BStr::new(repo_url.as_bytes()))?;
        let branch = value
            .get("branch")
            .ok_or_else(|| anyhow!("No branch found in store"))?;
        Ok(Self {
            repo_url,
            branch: branch.clone(),
        })
    }
}

fn get_subdir(app_store: &AppStoreV1) -> Option<String> {
    let mut subdir = None;
    if app_store.content.contains_key(env!("CARGO_PKG_VERSION")) {
        subdir = Some(
            app_store
                .content
                .get(env!("CARGO_PKG_VERSION"))
                .unwrap()
                .clone(),
        );
    } else if app_store
        .content
        .contains_key(&("v".to_owned() + env!("CARGO_PKG_VERSION")))
    {
        subdir = Some(
            app_store
                .content
                .get(&("v".to_owned() + env!("CARGO_PKG_VERSION")))
                .unwrap()
                .clone(),
        );
    } else {
        let current_version = Version::parse(env!("CARGO_PKG_VERSION")).unwrap();
        let minimum_nirvati_agent = Version::parse(MINIMUM_COMPATIBLE_APP_MANAGER).unwrap();
        let citadel_version = Version::new(0, 5, 0);
        let check_version = if app_store.is_citadel {
            citadel_version
        } else {
            current_version
        };
        // The semver of the latest found version so we can compare it to find a later one
        let mut found_version: Option<Version> = None;
        for (key, value) in app_store.content.iter() {
            let key = key.strip_prefix('v').unwrap_or(key);
            let key = Version::parse(key).unwrap();
            if key >= minimum_nirvati_agent
                && key <= check_version
                && (found_version.is_none() || key > found_version.as_ref().unwrap().clone())
            {
                found_version = Some(key);
                subdir = Some(value.clone());
            }
        }
    }
    subdir
}

// Returns apps downloaded from this store
pub fn download_apps_for_store(
    apps_root: &Path,
    store: &mut AppStore,
    skip_apps: &[String],
) -> Result<Vec<String>> {
    let mut store_apps = vec![];
    let tmp_dir = TempDir::new()?;
    let mut provider_data: ProviderData = store.provider_data.clone().into();
    let parsed_repo: RepoSrc = store.src.clone().try_into()?;
    nirvati_git::clone(parsed_repo.repo_url, &parsed_repo.branch, tmp_dir.path())?;
    // Read the app-store.yml, and match the store_version
    let app_store_yml = tmp_dir.path().join("app-store.yml");
    let app_store_yml = load_app_store_yml(&app_store_yml)?;
    match app_store_yml {
        AppStoreYml::AppStoreV1(app_store) => {
            let Some(subdir) = get_subdir(&app_store) else {
                bail!("No compatible version found for {:?}", &store.src);
            };
            let mut out_app_store = AppStore {
                id: store.id,
                src: store.src.clone(),
                metadata: StoreMetadata {
                    name: MultiLanguageItem(app_store.name.into()),
                    tagline: MultiLanguageItem(app_store.tagline.into()),
                    description: MultiLanguageItem(app_store.description.into()),
                    icon: app_store.icon,
                    developers: app_store.developers,
                    license: app_store.license,
                },
                r#type: StoreType::Git,
                // This will always be replaced later
                provider_data: serde_yaml::Value::Null,
                apps: store.apps.clone(),
            };
            let subdir_path = Path::new(&subdir);
            // Copy all dirs from the subdir to the apps dir
            // Overwrite any existing files
            // Skip apps that are already in installed_apps
            for entry in std::fs::read_dir(tmp_dir.path().join(subdir_path))? {
                let entry = entry?;
                if !entry.path().is_dir() {
                    continue;
                }
                let app_id = entry
                    .file_name()
                    .to_str()
                    .unwrap()
                    .to_string()
                    .to_lowercase()
                    .replace('_', "-");
                if !app_id.is_ascii() {
                    tracing::error!(
                        "App store \"{}\" tries to install an app with the non-ASCII name \"{}\",",
                        out_app_store.metadata.name,
                        app_id
                    );
                    continue;
                }
                if skip_apps.contains(&app_id) {
                    continue;
                }
                if RESERVED_NAMES.contains(&app_id.as_str()) {
                    tracing::error!(
                        "App store \"{}\" tries to install an app with the reserved name \"{}\",",
                        out_app_store.metadata.name,
                        app_id
                    );
                    continue;
                }
                if has_symlinks(&entry.path()) {
                    tracing::warn!(
                        "App store \"{}\" tries to install app \"{}\", but it contains symlinks.",
                        out_app_store.id,
                        app_id
                    );
                    continue;
                }
                fs_extra::dir::copy(
                    entry.path(),
                    apps_root,
                    &fs_extra::dir::CopyOptions {
                        overwrite: true,
                        skip_exist: true,
                        buffer_size: 64000,
                        copy_inside: true,
                        depth: 0,
                        content_only: false,
                    },
                )?;
                store_apps.push(app_id);
            }
            let new_apps =
                nirvati_git::get_latest_commit_for_apps(tmp_dir.path(), &subdir, &store_apps)?;
            for (app, commit) in new_apps {
                provider_data.apps.insert(app, commit);
            }
            out_app_store.provider_data = serde_yaml::to_value(&provider_data)?;
            out_app_store.apps = provider_data.apps.keys().cloned().collect();
            *store = out_app_store;
        }
    }
    Ok(store_apps)
}

pub fn download_app(apps_root: &Path, app: &str, store: &mut AppStore) -> Result<()> {
    let tmp_dir = TempDir::new()?;
    let parsed_repo: RepoSrc = store.src.clone().try_into()?;
    let mut provider_data: ProviderData = store.provider_data.clone().into();
    nirvati_git::clone(parsed_repo.repo_url, &parsed_repo.branch, tmp_dir.path())?;
    let app_store_yml = load_app_store_yml(&tmp_dir.path().join("app-store.yml"))?;
    match app_store_yml {
        AppStoreYml::AppStoreV1(app_store) => {
            let Some(subdir) = get_subdir(&app_store) else {
                bail!("No compatible version found for {:?}", store.src);
            };
            // Check if app exists in store
            let app_dir = tmp_dir.path().join(&subdir).join(app);
            if !app_dir.exists() {
                bail!("App {} not present in {:?} anymore", app, store.src);
            }

            // Overwrite app
            let nirvati_app_dir = apps_root.join(app);
            if nirvati_app_dir.exists() {
                std::fs::remove_dir_all(&nirvati_app_dir)?;
            }
            std::fs::create_dir_all(&nirvati_app_dir)?;

            fs_extra::dir::copy(
                &app_dir,
                apps_root,
                &fs_extra::dir::CopyOptions {
                    overwrite: true,
                    ..Default::default()
                },
            )?;

            let commit = nirvati_git::get_latest_commit_for_apps(
                tmp_dir.path(),
                &subdir,
                &[app.to_string()],
            )?
            .get(app)
            .ok_or_else(|| anyhow!("Failed to get latest commit for app"))?
            .clone();
            provider_data.apps.insert(app.to_string(), commit);
        }
    };

    store.provider_data = serde_yaml::to_value(provider_data)?;
    Ok(())
}

pub async fn check_updates_for_store(
    #[cfg(not(feature = "test-parser"))] kube_client: Client,
    global_apps_root: &Path,
    store: AppStore,
    app: Option<String>,
    nirvati_seed: &str,
    user_state: UserState,
    ctx: &ResolverCtx,
) -> Result<Vec<AppUpdateInfo>> {
    let mut updatable_apps = vec![];
    let tmp_dir = TempDir::new()?;
    let provider_data: ProviderData = store.provider_data.clone().into();
    let parsed_repo: RepoSrc = store.src.clone().try_into()?;
    nirvati_git::clone(parsed_repo.repo_url, &parsed_repo.branch, tmp_dir.path())?;
    let commit = nirvati_git::get_commit(tmp_dir.path())?;
    if commit != provider_data.commit {
        let app_store_yml = tmp_dir.path().join("app-store.yml");
        let app_store_yml = load_app_store_yml(&app_store_yml)?;
        match app_store_yml {
            AppStoreYml::AppStoreV1(app_store) => {
                let Some(subdir) = get_subdir(&app_store) else {
                    bail!("No compatible version found for {:?}", store.src);
                };
                let mut all_store_updatable_apps: Vec<String>;
                let latest_commits = nirvati_git::get_latest_commit_for_apps(
                    tmp_dir.path(),
                    &subdir,
                    &provider_data
                        .apps
                        .clone()
                        .into_keys()
                        .collect::<Vec<String>>(),
                );
                let Ok(mut latest_commits) = latest_commits else {
                    bail!("Failed to get latest commits for apps in {:?}", store.src);
                };
                latest_commits.retain(|app_id, commit| {
                    provider_data.apps.contains_key(app_id)
                        && provider_data.apps.get(app_id).unwrap() != commit
                });
                all_store_updatable_apps = latest_commits.into_keys().collect();
                let subdir_path = tmp_dir.path().join(subdir);
                all_store_updatable_apps.retain(|v| subdir_path.join(v).exists());
                for app_id in all_store_updatable_apps {
                    if app.as_ref().is_some_and(|app| app != &app_id) {
                        continue;
                    }
                    let metadata = match load_app(
                        #[cfg(not(feature = "test-parser"))]
                        kube_client.clone(),
                        global_apps_root,
                        &app_id,
                        &subdir_path.join(&app_id),
                        user_state.clone(),
                        nirvati_seed,
                        None,
                        ctx,
                    )
                    .await
                    {
                        Ok(app) => app.into_metadata(),
                        Err(error) => {
                            tracing::error!(
                                "Failed to load app metadata for app {} in store {:?}: {:#?}",
                                app_id,
                                store.src,
                                error
                            );
                            continue;
                        }
                    };
                    updatable_apps.push(AppUpdateInfo {
                        id: metadata.id,
                        new_version: metadata.display_version,
                        release_notes: metadata.release_notes,
                        has_permissions: metadata.has_permissions,
                    })
                }
            }
        }
    }
    Ok(updatable_apps)
}

pub fn is_official_repo(store: &AppStore) -> bool {
    let Ok(parsed_repo) = TryInto::<RepoSrc>::try_into(store.src.clone()) else {
        return false;
    };
    let Ok(path_str) = parsed_repo.repo_url.path.to_str() else {
        return false;
    };
    let is_safe_branch = parsed_repo.branch == "main"
        || parsed_repo.branch == "stable"
        || parsed_repo.branch == "beta";
    let is_safe_url = parsed_repo.repo_url.host() == Some("gitlab.com")
        && path_str.starts_with("/nirvati/apps")
        && !path_str.contains("..");
    is_safe_branch && is_safe_url
}

pub fn get_commit_for_app(app: &str, store: &AppStore) -> Option<String> {
    let provider_data: ProviderData = store.provider_data.clone().into();
    provider_data.apps.get(app).cloned()
}

pub fn set_latest_commits(store: &mut AppStore, latest_commits: HashMap<String, String>) {
    let mut provider_data: ProviderData = store.provider_data.clone().into();
    provider_data.apps.extend(latest_commits);
    store.provider_data = serde_yaml::to_value(provider_data).unwrap();
}

pub fn stringify_src(store: &AppStore) -> Result<String> {
    let parsed_repo: RepoSrc = store.src.clone().try_into()?;
    Ok(format!("{}#{}", parsed_repo.repo_url, parsed_repo.branch))
}
