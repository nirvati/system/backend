use crate::apps::parser::app_yml::types::MultiLanguageItem;
use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap, HashMap};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct LegacyAppStore {
    store_version: u8,

    id: String,
    name: String,
    tagline: String,
    icon: String,
    developers: String,
    license: String,

    content: HashMap<String, String>,
}

impl From<LegacyAppStore> for super::AppStoreV1 {
    fn from(store: LegacyAppStore) -> Self {
        super::AppStoreV1 {
            store_version: store.store_version,
            name: MultiLanguageItem::String(store.name),
            tagline: MultiLanguageItem::String(store.tagline),
            description: MultiLanguageItem::String("".to_string()),
            icon: store.icon,
            developers: BTreeMap::from([(
                store.developers,
                "https://runcitadel.space/".to_string(),
            )]),
            license: store.license,
            content: BTreeMap::from_iter(store.content),
            is_citadel: true,
        }
    }
}
