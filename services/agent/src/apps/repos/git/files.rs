// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::fs::File;
use std::path::Path;

use super::types::{AppStoreV1, AppStoreYml};
use crate::apps::repos::git::citadel_compat::LegacyAppStore;
use anyhow::bail;

pub fn load_app_store_yml(file: &Path) -> anyhow::Result<AppStoreYml> {
    let app_store_yml = File::open(file)?;
    let app_store = serde_yaml::from_reader::<File, serde_yaml::Value>(app_store_yml)?;
    let app_store_version = app_store.get("store_version");
    if app_store_version.is_none() || !app_store_version.unwrap().is_u64() {
        bail!("App store version not defined.");
    }
    let app_store_version = app_store_version.unwrap().as_u64().unwrap();
    match app_store_version {
        1 => Ok(
            if app_store
                .get("developers")
                .is_some_and(|devs| devs.is_string())
            {
                AppStoreYml::AppStoreV1(
                    serde_yaml::from_value::<LegacyAppStore>(app_store)
                        .map(|legacy_store| legacy_store.into())?,
                )
            } else {
                AppStoreYml::AppStoreV1(serde_yaml::from_value::<AppStoreV1>(app_store)?)
            },
        ),
        _ => bail!("App store version not supported."),
    }
}
