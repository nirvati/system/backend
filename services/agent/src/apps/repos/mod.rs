// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::apps::files::{
    add_system_source, get_system_sources_yml, read_stores_yml, write_stores_yml,
};
use crate::apps::generator::internal::ResolverCtx;
use crate::apps::repos::types::{AppStore, AppUpdateInfo, StoreType};
use crate::plugins::api::UserState;
use anyhow::{anyhow, Result};
use futures::stream::FuturesUnordered;
use futures::StreamExt;
use itertools::Itertools;
use kube::Client;
use std::collections::{BTreeMap, HashMap};
use std::path::Path;
use std::time::Duration;
use tokio::time::timeout;

mod git;
mod plugin;
pub mod types;

/// Download the latest version of the app, and possibly update its data
pub async fn download_app(
    kube_client: Client,
    global_apps_root: &Path,
    app: &str,
    user_state: UserState,
) -> Result<()> {
    let apps_root = global_apps_root.join(&user_state.user_id);
    let mut stores = read_stores_yml(&apps_root)?;
    let app_src = stores
        .iter_mut()
        .find(|store| store.apps.contains(&app.to_string()))
        .ok_or_else(|| {
            anyhow!("App not found in any store - Make sure it has been downloaded before.")
        })?;
    match app_src.r#type {
        StoreType::Git => git::download_app(&apps_root, app, app_src).map(|_| ())?,
        StoreType::Plugin => {
            plugin::download_app(kube_client, global_apps_root, app, app_src, user_state)
                .await
                .map(|_| ())?
        }
    };
    write_stores_yml(&apps_root, &stores)?;
    Ok(())
}

pub async fn download_apps_for_store(
    #[cfg(not(feature = "test-parser"))] kube_client: Client,
    global_apps_root: &Path,
    store: &mut AppStore,
    skip_apps: &[String],
    user_state: UserState,
) -> Result<Vec<String>> {
    let apps_root = global_apps_root.join(&user_state.user_id);
    match store.r#type {
        StoreType::Git => git::download_apps_for_store(&apps_root, store, skip_apps),
        StoreType::Plugin => {
            plugin::download_apps_for_store(
                #[cfg(not(feature = "test-parser"))]
                kube_client,
                global_apps_root,
                store,
                skip_apps,
                user_state,
            )
            .await
        }
    }
}

pub async fn add_new_store(
    kube_client: Client,
    global_apps_root: &Path,
    src: HashMap<String, String>,
    type_: StoreType,
    user_state: UserState,
) -> Result<Vec<String>> {
    let apps_root = global_apps_root.join(&user_state.user_id);
    let mut stores = read_stores_yml(&apps_root)?;
    let all_apps = stores
        .iter()
        .flat_map(|store| store.apps.clone())
        .collect::<Vec<_>>();
    let mut new_store = AppStore {
        id: uuid::Uuid::new_v4(),
        src,
        r#type: type_,
        ..Default::default()
    };
    if let StoreType::Plugin = new_store.r#type {
        let metadata =
            plugin::load_store_metadata(kube_client.clone(), &mut new_store, &user_state.user_id)
                .await?;
        new_store.metadata = metadata;
    }
    let result = download_apps_for_store(
        #[cfg(not(feature = "test-parser"))]
        kube_client,
        global_apps_root,
        &mut new_store,
        &all_apps,
        user_state,
    )
    .await?;
    stores.push(new_store);
    write_stores_yml(&apps_root, &stores)?;
    Ok(result)
}

pub async fn download_apps(
    #[cfg(not(feature = "test-parser"))] kube_client: Client,
    global_apps_root: &Path,
    new_and_not_installed_only: bool,
    user_state: UserState,
) -> Result<()> {
    let apps_root = global_apps_root.join(&user_state.user_id);
    let mut stores = read_stores_yml(&apps_root)?;
    let mut all_apps_from_stores: Vec<String> = vec![];
    if new_and_not_installed_only {
        all_apps_from_stores.extend(user_state.installed_apps.clone());
    }
    // For each AppSrc, clone the repo into a tempdir
    for store in stores.iter_mut() {
        let store_apps = download_apps_for_store(
            #[cfg(not(feature = "test-parser"))]
            kube_client.clone(),
            global_apps_root,
            store,
            &all_apps_from_stores,
            user_state.clone(),
        )
        .await?;
        all_apps_from_stores.extend(store_apps);
    }

    write_stores_yml(&apps_root, &stores)?;

    Ok(())
}

pub async fn check_updates_for_store(
    kube_client: Client,
    global_apps_root: &Path,
    store: AppStore,
    app: Option<String>,
    nirvati_seed: &str,
    user_state: UserState,
    ctx: &ResolverCtx,
) -> Result<Vec<AppUpdateInfo>> {
    match store.r#type {
        StoreType::Git => {
            git::check_updates_for_store(
                #[cfg(not(feature = "test-parser"))]
                kube_client,
                global_apps_root,
                store,
                app,
                nirvati_seed,
                user_state,
                ctx,
            )
            .await
        }
        StoreType::Plugin => {
            plugin::check_updates_for_store(kube_client, store, app, user_state).await
        }
    }
}

pub async fn check_updates(
    kube_client: Client,
    global_apps_root: &Path,
    nirvati_seed: &str,
    user_state: UserState,
    ctx: &ResolverCtx,
) -> Result<Vec<AppUpdateInfo>> {
    let apps_root = global_apps_root.join(&user_state.user_id);
    let mut updatable_apps = vec![];

    let stores = read_stores_yml(&apps_root)?;

    let updates = stores
        .into_iter()
        .map(move |store| {
            timeout(
                Duration::from_secs(15),
                check_updates_for_store(
                    kube_client.clone(),
                    global_apps_root,
                    store,
                    None,
                    nirvati_seed,
                    user_state.clone(),
                    ctx,
                ),
            )
        })
        .collect::<FuturesUnordered<_>>()
        .collect::<Vec<_>>()
        .await;
    for updates in updates {
        match updates {
            Ok(Ok(updates)) => updatable_apps.extend(updates),
            Ok(Err(err)) => {
                tracing::warn!("Failed to check updates for store: {:?}", err);
            }
            Err(_) => {
                tracing::warn!("Timeout while checking updates for store");
            }
        }
    }

    Ok(updatable_apps)
}

pub async fn check_sys_app_updates(
    kube_client: Client,
    global_apps_root: &Path,
    nirvati_seed: &str,
    user_state: UserState,
    ctx: &ResolverCtx,
) -> Result<Vec<AppUpdateInfo>> {
    let mut updatable_apps = vec![];

    let stores = get_system_sources_yml(global_apps_root).await?;

    let official_stores_app_commits = stores.iter().filter_map(|(app, store)| {
        if store.r#type == StoreType::Git {
            let src = git::stringify_src(store).ok()?;
            git::get_commit_for_app(app, store).map(|commit| (src, app.clone(), commit))
        } else {
            None
        }
    });
    let mut official_stores_app_commits_map = BTreeMap::new();
    for (src, app, commit) in official_stores_app_commits {
        official_stores_app_commits_map
            .entry(src)
            .or_insert_with(HashMap::new)
            .insert(app, commit);
    }

    let mut deduped_stores = stores
        .into_values()
        .dedup_by(|a, b| {
            if a.src != b.src || a.r#type != b.r#type {
                return false;
            };
            if a.r#type == StoreType::Plugin {
                plugin::get_plugin(a)
                    .is_ok_and(|plugin| plugin == plugin::get_plugin(b).unwrap_or_default())
            } else {
                true
            }
        })
        .collect::<Vec<_>>();

    for store in deduped_stores.iter_mut() {
        if store.r#type == StoreType::Git {
            match git::stringify_src(store) {
                Ok(src) => {
                    if let Some(apps) = official_stores_app_commits_map.remove(&src) {
                        git::set_latest_commits(store, apps);
                    }
                }
                Err(err) => {
                    tracing::warn!("Failed to stringify src: {:?}", err);
                }
            }
        }
    }

    let updates = deduped_stores
        .into_iter()
        .map(|store| {
            check_updates_for_store(
                kube_client.clone(),
                global_apps_root,
                store,
                None,
                nirvati_seed,
                user_state.clone(),
                ctx,
            )
        })
        .collect::<FuturesUnordered<_>>()
        .collect::<Vec<_>>()
        .await;
    for updates in updates {
        match updates {
            Ok(updates) => updatable_apps.extend(updates),
            Err(err) => {
                tracing::warn!("Failed to check updates for store: {:?}", err);
            }
        }
    }

    Ok(updatable_apps)
}

/// Download the latest version of a system app, and possibly update its data
pub async fn download_sys_app(
    kube_client: Client,
    global_apps_root: &Path,
    app: &str,
    user_state: UserState,
) -> Result<()> {
    let apps_root = global_apps_root.join(&user_state.user_id);
    let mut stores = get_system_sources_yml(global_apps_root).await?;
    let mut app_src = stores
        .remove(app)
        .ok_or(anyhow!("No source found for system app!"))?;
    match app_src.r#type {
        StoreType::Git => git::download_app(&apps_root, app, &mut app_src).map(|_| ())?,
        StoreType::Plugin => {
            plugin::download_app(kube_client, global_apps_root, app, &mut app_src, user_state)
                .await
                .map(|_| ())?
        }
    };
    add_system_source(global_apps_root, app, app_src).await?;
    Ok(())
}

pub fn is_official_store(store: &AppStore) -> bool {
    store.r#type == StoreType::Git && git::is_official_repo(store)
}
