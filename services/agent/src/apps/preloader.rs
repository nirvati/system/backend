use crate::apps::generator::internal::ResolverCtx;
use crate::apps::generator::kubernetes::job::generate_job_onetime;
use crate::apps::generator::kubernetes::networkpolicy::get_network_policy;
use k8s_crds_cilium::{CiliumNetworkPolicy, CiliumNetworkPolicyEndpointSelector};
use k8s_openapi::api::core::v1::PersistentVolumeClaim;
use kube::{Api, Client};
use nirvati::kubernetes::{apply_with_ns, create_namespace};
use nirvati_apps::internal::{
    CommonComponentConfig, Container, DnsConfig, EnvVar, InternalAppRepresentation, LonghornVolume,
    NetworkPolicy, NetworkPolicyEgress, NetworkPolicyIngress, Volume,
};
use nirvati_apps::metadata::PreloadType;
use nirvati_apps::utils::StringLike;
use std::collections::BTreeMap;
use std::path::Path;

pub async fn preload_data(
    kube: &Client,
    app: &InternalAppRepresentation,
    app_root: &Path,
    user: &str,
    mut pvcs: Vec<PersistentVolumeClaim>,
    resolver: &ResolverCtx,
) -> anyhow::Result<()> {
    let Some(ref preload_type) = app.metadata.preload_type else {
        return Ok(());
    };
    let vols: Vec<_> = match preload_type {
        PreloadType::Nirvati => {
            if !app_root.join("data").exists() {
                vec![]
            } else {
                app.metadata
                    .volumes
                    .keys()
                    .filter(|vol_id| app_root.join("data").join(vol_id).is_dir())
                    .map(|vol_id| vol_id.to_string())
                    .collect()
            }
        }
        PreloadType::MainVolumeFromSubdir(subdir) => {
            if app.metadata.volumes.len() != 1 {
                tracing::warn!("Main volume from subdir preload type requires exactly one volume");
                vec![]
            } else {
                let vol_id = app.metadata.volumes.keys().next().unwrap();
                if !app_root.join(subdir).exists() {
                    vec![]
                } else {
                    vec![vol_id.to_string()]
                }
            }
        }
    };
    if vols.is_empty() {
        return Ok(());
    }
    let app_ns = if user == "system" {
        app.metadata.id.clone()
    } else {
        format!("{}-{}", user, app.metadata.id)
    };
    let ns_api: Api<k8s_openapi::api::core::v1::Namespace> = Api::all(kube.clone());
    if ns_api.get_opt(&app_ns).await?.is_some() {
        return Ok(());
    }
    create_namespace(kube.clone(), &app_ns).await?;
    for pvc in pvcs.iter_mut() {
        pvc.metadata.labels.get_or_insert(BTreeMap::new()).insert(
            "app.kubernetes.io/managed-by".to_string(),
            "Helm".to_string(),
        );
        pvc.metadata
            .annotations
            .get_or_insert(BTreeMap::new())
            .insert(
                "meta.helm.sh/release-name".to_string(),
                app.metadata.id.clone(),
            );
        pvc.metadata
            .annotations
            .get_or_insert(BTreeMap::new())
            .insert("meta.helm.sh/release-namespace".to_string(), app_ns.clone());
    }
    apply_with_ns(kube.clone(), &pvcs, &app_ns).await?;
    let bootstrap_job = generate_job_onetime(
        "bootstrap".to_string(),
        Container {
            environment: BTreeMap::from([
                (
                    "APP_ID".to_string(),
                    EnvVar::StringLike(StringLike::String(app.metadata.id.clone())),
                ),
                (
                    "USER_ID".to_string(),
                    EnvVar::StringLike(StringLike::String(user.to_string())),
                ),
                (
                    "TARGET_ENDPOINT".to_string(),
                    EnvVar::StringLike(StringLike::String(
                        "http://agent-public.nirvati.nirvati:8081".to_string(),
                    )),
                ),
                (
                    "VOLUMES".to_string(),
                    EnvVar::StringLike(StringLike::String(
                        vols.iter()
                            .map(|v| v.to_string())
                            .collect::<Vec<String>>()
                            .join(","),
                    )),
                ),
                (
                    "PRELOAD_TYPE".to_string(),
                    EnvVar::StringLike(StringLike::String(
                        serde_json::to_string(&preload_type).unwrap(),
                    )),
                ),
                (
                    "RUST_LOG".to_string(),
                    EnvVar::StringLike(StringLike::String("debug".to_string())),
                ),
            ]),
            // TODO: use the same version as current Nirvati
            image: "harbor.nirvati.org/nirvati/app-data-loader:v0.1.24".to_string(),
            volumes: vols
                .into_iter()
                .map(|vol_id| {
                    Volume::Longhorn(LonghornVolume {
                        name: vol_id.clone(),
                        mount_path: format!("/volumes/{}", vol_id),
                        sub_path: None,
                        is_readonly: false,
                        from_app: None,
                        from_ns: None,
                    })
                })
                .collect(),
            restart: Some("OnFailure".to_string()),
            ..Default::default()
        },
        resolver,
    )
    .await?;
    let network_policy = get_network_policy(
        "temp-bootstrap".to_string(),
        &NetworkPolicy {
            egress: NetworkPolicyEgress {
                allow_cluster: true,
                allow_same_ns: false,
                allow_kube_api: false,
                allow_world: false,
                allow_local_net: false,
                dns: Some(DnsConfig {
                    allow: true,
                    allowed_lookup_names: Some(vec!["agent-public.nirvati.nirvati".to_string()]),
                    allowed_lookup_pattern: None,
                }),
                fqdns: vec![],
                cidrs: vec![],
                services: vec![],
                pods: vec![],
            },
            ingress: NetworkPolicyIngress {
                allow_world: CommonComponentConfig {
                    allow: false,
                    allowed_ports: None,
                },
                allow_cluster: CommonComponentConfig {
                    allow: false,
                    allowed_ports: None,
                },
                allow_same_ns: CommonComponentConfig {
                    allow: false,
                    allowed_ports: None,
                },
                cidrs: vec![],
                pods: vec![],
            },
        },
        CiliumNetworkPolicyEndpointSelector {
            match_expressions: None,
            match_labels: Some(BTreeMap::from([(
                "job-name".to_string(),
                "bootstrap".to_string(),
            )])),
        },
        resolver,
    )
    .await?;
    apply_with_ns(kube.clone(), &[network_policy], &app_ns).await?;
    apply_with_ns(kube.clone(), &[bootstrap_job], &app_ns).await?;
    // Wait for the job to finish
    let api: Api<k8s_openapi::api::batch::v1::Job> = Api::namespaced(kube.clone(), &app_ns);
    let mut i = 0;
    loop {
        let job = api.get("bootstrap").await?;
        if let Some(status) = job.status {
            if let Some(succeeded) = status.succeeded {
                if succeeded > 0 {
                    break;
                }
            }
        }
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        i += 1;
        if i > 600 {
            anyhow::bail!("Failed to preload data");
        }
    }
    // Delete the job
    api.delete("bootstrap", &Default::default()).await?;
    let policy_api: Api<CiliumNetworkPolicy> = Api::namespaced(api.into_client(), &app_ns);
    policy_api
        .delete("temp-bootstrap", &Default::default())
        .await?;
    Ok(())
}
