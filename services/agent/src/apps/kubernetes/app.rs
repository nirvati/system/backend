// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::Result;
use base64::Engine;
use k8s_crds_helm_controller::{HelmChart, HelmChartSpec};
use k8s_openapi::api::apps::v1::{DaemonSet, Deployment, ReplicaSet, StatefulSet};
use k8s_openapi::api::batch::v1::{CronJob, Job};
use k8s_openapi::api::core::v1::Namespace;
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use kube::{Api, Client};
use nirvati::for_each_type_parallel_result;
use nirvati::kubernetes::{apply_with_ns, scale_all_in_ns, set_job_suspense};

pub async fn install_chart(
    client: Client,
    app: &str,
    user: &str,
    is_sys_lib: bool,
    chart: &[u8],
) -> Result<()> {
    let chart = HelmChart {
        metadata: ObjectMeta {
            name: Some(app.to_string()),
            ..Default::default()
        },
        spec: HelmChartSpec {
            chart_content: Some(base64::engine::general_purpose::STANDARD.encode(chart)),
            create_namespace: Some(true),
            target_namespace: if is_sys_lib && app == "longhorn" {
                Some("longhorn-system".to_string())
            } else if is_sys_lib && app == "metallb" {
                Some("metallb-system".to_string())
            } else if is_sys_lib {
                Some(app.to_string())
            } else {
                Some(format!("{}-{}", user, app))
            },
            failure_policy: Some("keep".to_string()),
            ..Default::default()
        },
        status: None,
    };
    apply_with_ns(client.clone(), &[chart], user).await?;
    // Get the chart from the ns
    let api: Api<HelmChart> = Api::namespaced(client.clone(), user);
    let job;
    let mut i = 0;
    loop {
        let chart = api.get(app).await?;
        if let Some(status) = &chart.status {
            if let Some(j) = &status.job_name {
                job = j.clone();
                break;
            }
        }
        if i > 20 {
            return Err(anyhow::anyhow!("Failed to get job name"));
        }
        tokio::time::sleep(std::time::Duration::from_secs(1)).await;
        i += 1;
    }
    i = 0;
    let api: Api<Job> = Api::namespaced(client.clone(), user);
    loop {
        let job = api.get(&job).await?;
        if let Some(status) = &job.status {
            if status.completion_time.is_some() {
                break;
            }
        }
        if i > 20 {
            return Err(anyhow::anyhow!("Install did not complete in time"));
        }
        tokio::time::sleep(std::time::Duration::from_secs(1)).await;
        i += 1;
    }
    Ok(())
}

pub async fn uninstall_chart(client: Client, app: &str, user: &str) -> Result<()> {
    let api: kube::Api<k8s_crds_helm_controller::HelmChart> =
        kube::Api::namespaced(client.clone(), user);
    api.delete(app, &Default::default()).await?;
    let api: kube::Api<Namespace> = kube::Api::all(client);
    let ns = if user == "system" {
        app.to_string()
    } else {
        format!("{}-{}", user, app)
    };
    api.delete(&ns, &Default::default()).await?;
    Ok(())
}

pub async fn suspend_job(client: Client, app_ns: &str, job_name: &str) -> Result<()> {
    // Suspend a job by setting spec.suspend to true
    let api: kube::Api<Job> = kube::Api::namespaced(client.clone(), app_ns);
    let mut job = api.get(job_name).await?;
    let Some(spec) = job.spec.as_mut() else {
        return Ok(());
    };
    spec.suspend = Some(true);
    api.replace(job_name, &Default::default(), &job).await?;
    Ok(())
}

pub async fn pause_ns(client: Client, app_ns: &str) -> Result<()> {
    for_each_type_parallel_result!(scale_all_in_ns, Deployment, StatefulSet, DaemonSet, ReplicaSet; (client.clone(), app_ns, 0))?;
    for_each_type_parallel_result!(set_job_suspense, Job, CronJob; (client.clone(), app_ns, true))?;
    Ok(())
}

pub async fn resume_ns(client: Client, app_ns: &str) -> Result<()> {
    for_each_type_parallel_result!(scale_all_in_ns, Deployment, StatefulSet, DaemonSet, ReplicaSet; (client.clone(), app_ns, 1))?;
    for_each_type_parallel_result!(set_job_suspense, Job, CronJob; (client.clone(), app_ns, false))?;
    Ok(())
}
