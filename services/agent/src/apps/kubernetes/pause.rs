use k8s_crds_traefik::{IngressRoute, IngressRouteRoutesServices};
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use k8s_openapi::apimachinery::pkg::util::intstr::IntOrString;
use slugify::slugify;

pub fn get_paused_ingress(domain: &str) -> [IngressRoute; 2] {
    let main_svc = IngressRouteRoutesServices {
        name: "main".to_string(),
        namespace: Some("nirvati".to_string()),
        port: Some(IntOrString::Int(3000)),
        ..Default::default()
    };
    [
        IngressRoute {
            metadata: ObjectMeta {
                name: Some(slugify!(&domain)),
                ..Default::default()
            },
            spec: k8s_crds_traefik::IngressRouteSpec {
                entry_points: Some(vec!["websecure".to_owned()]),
                routes: vec![
                    k8s_crds_traefik::IngressRouteRoutes {
                        kind: k8s_crds_traefik::IngressRouteRoutesKind::Rule,
                        r#match: format!("Host(`{}`) && PathPrefix(`/_nuxt/`)", domain),
                        priority: None,
                        middlewares: Some(vec![k8s_crds_traefik::IngressRouteRoutesMiddlewares {
                            name: "compress".to_owned(),
                            namespace: Some("nirvati".to_string()),
                        }]),
                        services: Some(vec![main_svc.clone()]),
                        syntax: None,
                    },
                    k8s_crds_traefik::IngressRouteRoutes {
                        kind: k8s_crds_traefik::IngressRouteRoutesKind::Rule,
                        r#match: format!("Host(`{}`)", domain),
                        priority: None,
                        middlewares: Some(vec![k8s_crds_traefik::IngressRouteRoutesMiddlewares {
                            name: "paused-app".to_owned(),
                            namespace: Some("nirvati".to_string()),
                        }]),
                        services: Some(vec![main_svc.clone()]),
                        syntax: None,
                    },
                ],
                tls: Some(k8s_crds_traefik::IngressRouteTls {
                    secret_name: Some(format!("{}-tls", slugify!(&domain))),
                    ..Default::default()
                }),
            },
        },
        IngressRoute {
            metadata: ObjectMeta {
                name: Some(format!("{}-http", slugify!(&domain))),
                ..Default::default()
            },
            spec: k8s_crds_traefik::IngressRouteSpec {
                entry_points: Some(vec!["web".to_owned()]),
                routes: vec![k8s_crds_traefik::IngressRouteRoutes {
                    kind: k8s_crds_traefik::IngressRouteRoutesKind::Rule,
                    r#match: format!("Host(`{}`)", domain),
                    priority: None,
                    middlewares: Some(vec![
                        k8s_crds_traefik::IngressRouteRoutesMiddlewares {
                            name: "https-redirect".to_owned(),
                            namespace: Some("nirvati".to_string()),
                        },
                        k8s_crds_traefik::IngressRouteRoutesMiddlewares {
                            name: "paused-app".to_owned(),
                            namespace: Some("nirvati".to_string()),
                        },
                    ]),
                    services: Some(vec![main_svc]),
                    syntax: None,
                }],

                tls: None,
            },
        },
    ]
}
