use k8s_openapi::api::core::v1::{PersistentVolume, PersistentVolumeClaim};
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use kube::{Api, Client};
use nirvati::kubernetes::apply;

pub async fn turn_pv_cross_ns(
    client: Client,
    pv: &str,
) -> anyhow::Result<()> {
    let pv_api: Api<PersistentVolume> = Api::all(client);
    let mut pv_details = pv_api.get(pv).await?;
    let pv_spec = pv_details
        .spec
        .as_mut()
        .ok_or(anyhow::anyhow!("No spec found in pvc"))?;
    if pv_spec.persistent_volume_reclaim_policy.as_ref().is_some_and(|x| x == "Retain") {
        return Ok(());
    }
    pv_details.metadata.managed_fields = None;
    // Make the pv be retain
    {
        let pv_spec = pv_details
            .spec
            .as_mut()
            .ok_or(anyhow::anyhow!("No spec found in pvc"))?;
        pv_spec.persistent_volume_reclaim_policy = Some("Retain".to_string());
    }
    apply(pv_api.into_client(), &[pv_details]).await?;
    Ok(())
}

pub async fn create_pvc_clone(
    client: Client,
    original_ns: &str,
    original_pvc: &str,
    new_ns: &str,
    new_pvc: &str,
) -> anyhow::Result<(PersistentVolumeClaim, PersistentVolume)> {
    let api: Api<PersistentVolumeClaim> = Api::namespaced(client, original_ns);
    let pvc = api.get(original_pvc).await?;
    let mut pvc_clone = pvc.clone();
    pvc_clone.metadata = ObjectMeta {
        name: Some(new_pvc.to_string()),
        namespace: Some(new_ns.to_string()),
        ..Default::default()
    };
    let pvc_clone_spec = pvc_clone
        .spec
        .as_mut()
        .ok_or(anyhow::anyhow!("No spec found in pvc"))?;
    let pv = pvc_clone_spec
        .volume_name
        .clone()
        .ok_or(anyhow::anyhow!("No volume name found in pvc"))?;
    let pv_api: Api<PersistentVolume> = Api::all(api.into_client());
    // Create a clone of the pv
    let mut pv_details = pv_api.get(&pv).await?;
    pv_details.metadata = ObjectMeta {
        name: Some(format!("{}-mirror-{}", pv, new_ns)),
        ..Default::default()
    };
    {
        let pv_spec = pv_details
            .spec
            .as_mut()
            .ok_or(anyhow::anyhow!("No spec found in pv"))?;
        pv_spec.persistent_volume_reclaim_policy = Some("Retain".to_string());
    }
    pvc_clone_spec.volume_name = Some(pv_details.metadata.name.clone().unwrap());
    Ok((pvc_clone, pv_details))
}
