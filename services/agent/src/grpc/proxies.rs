// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use k8s_crds_traefik::{IngressRoute, IngressRouteTCP};
use kube::Api;
use slugify::slugify;
use tonic::{async_trait, Request, Response, Status};

use nirvati::kubernetes::apply_with_ns;

use crate::grpc::api::proxies_server::Proxies;
use crate::grpc::api::{AddProxyRequest, AddTcpProxyRequest, Empty, RemoveProxyRequest};
use crate::grpc::https::grpc_to_internal_acme_provider;
use crate::grpc::{expect_result, ApiServer};
use crate::ingress::domains::https::certs::generate_cert;
use crate::ingress::proxies::{generate_ingress_for_proxy, generate_ingress_for_tcp};

#[async_trait]
impl Proxies for ApiServer {
    async fn add_proxy(
        &self,
        request: Request<AddProxyRequest>,
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();
        let (ingresses, middlewares, server_transports) =
            generate_ingress_for_proxy(&request.domain, request.routes);
        expect_result!(
            apply_with_ns(self.kube_client.clone(), &ingresses, &request.namespace).await,
            "Failed to apply ingresses"
        )?;
        expect_result!(
            apply_with_ns(self.kube_client.clone(), &middlewares, &request.namespace).await,
            "Failed to apply middlewares"
        )?;
        expect_result!(
            apply_with_ns(
                self.kube_client.clone(),
                &server_transports,
                &request.namespace
            )
            .await,
            "Failed to apply server transports"
        )?;
        expect_result!(
            generate_cert(
                &self.kube_client,
                request.cert_user,
                &request.namespace,
                request.domain,
                request.parent,
                grpc_to_internal_acme_provider(request.acme_provider)?,
            )
            .await,
            "Failed to generate certificate"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn add_tcp_proxy(
        &self,
        request: Request<AddTcpProxyRequest>,
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();
        let (tcp_ingress, redirect_ingress) =
            generate_ingress_for_tcp(&request.domain, request.target_svc, request.port);
        expect_result!(
            apply_with_ns(self.kube_client.clone(), &[tcp_ingress], &request.namespace).await,
            "Failed to apply tcp ingress"
        )?;
        expect_result!(
            apply_with_ns(
                self.kube_client.clone(),
                &[redirect_ingress],
                &request.namespace
            )
            .await,
            "Failed to apply redirect ingress"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn remove_proxy(
        &self,
        request: Request<RemoveProxyRequest>,
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();
        let ingress_api: Api<IngressRoute> =
            Api::namespaced(self.kube_client.clone(), &request.namespace);
        let domain = slugify!(&request.domain);
        let redirect_ingress = format!("{}-http-to-https-redirect", &domain);
        expect_result!(
            ingress_api
                .delete(&redirect_ingress, &Default::default())
                .await,
            "Failed to delete ingress"
        )?;
        if let Err(e) = ingress_api.delete(&domain, &Default::default()).await {
            let tcp_ingress_api: Api<IngressRouteTCP> =
                Api::namespaced(self.kube_client.clone(), &request.namespace);
            if let Err(tcp_err) = tcp_ingress_api.delete(&domain, &Default::default()).await {
                tracing::error!("Failed to delete ingress: {}", e);
                tracing::error!("Failed to delete tcp ingress: {}", tcp_err);
                return Err(Status::internal("Failed to delete ingress"));
            }
        }
        Ok(Response::new(Empty {}))
    }
}
