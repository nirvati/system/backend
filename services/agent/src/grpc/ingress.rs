// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use tonic::{async_trait, Request, Response, Status};

use super::api::{
    ingress_server::Ingress, AddDomainRequest, AddDomainResponse, DomainType, Empty,
    PauseAppDomainRequest, RemoveDomainRequest, ResumeAppDomainRequest, SetDomainProtectRequest,
};
use super::{expect_result, require_opt, ApiServer};
use crate::apps::kubernetes::pause::get_paused_ingress;
use crate::grpc::https::grpc_to_internal_acme_provider;
use crate::ingress::domains::https::certs::{delete_cert, generate_cert};
use crate::ingress::domains::onion::{delete_onion_domain, get_new_onion_domain};
use crate::ingress::domains::{add_app_domain, delete_app_domain};
use nirvati::kubernetes::apply_with_ns;
use nirvati_apps::metadata::AppScope;

impl From<DomainType> for crate::ingress::domains::IngressType {
    fn from(domain_type: DomainType) -> Self {
        match domain_type {
            DomainType::DomainHttp => crate::ingress::domains::IngressType::Http,
            DomainType::DomainHttps => crate::ingress::domains::IngressType::Https,
            DomainType::DomainOnion => crate::ingress::domains::IngressType::Onion,
            DomainType::DomainTcp => crate::ingress::domains::IngressType::Tcp,
        }
    }
}

#[async_trait]
impl Ingress for ApiServer {
    async fn add_domain(
        &self,
        request: Request<AddDomainRequest>,
    ) -> Result<Response<AddDomainResponse>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let (app, _) = self.load_app(&req.id, &user_state, None).await?;
        let domain_type: DomainType = req
            .r#type
            .try_into()
            .map_err(|_| Status::invalid_argument("Invalid domain type"))?;
        let app_ns = if app.metadata.scope == AppScope::System {
            app.metadata.id.clone()
        } else {
            format!("{}-{}", user_state.user_id, app.metadata.id)
        };
        let domain = if domain_type == DomainType::DomainOnion {
            expect_result!(
                get_new_onion_domain(&self.kube_client, &app, &user_state.user_id).await
            )?
        } else {
            require_opt!(
                req.domain.clone(),
                "Domain is required for non-onion domains"
            )?
        };
        if domain_type == DomainType::DomainHttps {
            let cert_user = req
                .cert_user
                .clone()
                .unwrap_or_else(|| user_state.user_id.clone());
            expect_result!(
                generate_cert(
                    &self.kube_client,
                    cert_user,
                    &app_ns,
                    domain.clone(),
                    req.parent,
                    grpc_to_internal_acme_provider(req.acme_provider)?,
                )
                .await
            )?;
        }
        expect_result!(
            add_app_domain(
                &self.kube_client,
                app,
                &domain,
                &user_state.user_id,
                req.protect,
                req.custom_prefix,
                domain_type.into(),
                req.app_component,
            )
            .await,
            "Failed to add ingress for domain"
        )?;
        Ok(Response::new(AddDomainResponse { domain }))
    }

    async fn remove_domain(
        &self,
        request: Request<RemoveDomainRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let (app, _) = self.load_app(&req.id, &user_state, None).await?;
        let ns = if app.get_metadata().scope == AppScope::System {
            app.get_metadata().id.clone()
        } else {
            format!("{}-{}", user_state.user_id, app.get_metadata().id)
        };
        let domain_type: DomainType = req
            .r#type
            .try_into()
            .map_err(|_| Status::invalid_argument("Invalid domain type"))?;
        if domain_type == DomainType::DomainOnion {
            expect_result!(
                delete_onion_domain(&self.kube_client, &app, &user_state.user_id).await
            )?;
        } else if domain_type == DomainType::DomainHttps {
            expect_result!(delete_cert(&self.kube_client, req.domain.clone(), &ns).await)?;
        }
        expect_result!(
            delete_app_domain(
                &self.kube_client,
                app,
                req.domain,
                user_state.user_id,
                domain_type
            )
            .await
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn pause_app_domain(
        &self,
        request: Request<PauseAppDomainRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let app_ns = if user_state.user_id == "system" {
            req.app.clone()
        } else {
            format!("{}-{}", user_state.user_id, req.app)
        };
        let paused_ingress = get_paused_ingress(&req.domain);
        expect_result!(
            apply_with_ns(self.kube_client.clone(), &paused_ingress, &app_ns).await,
            "Failed to apply paused ingress"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn resume_app_domain(
        &self,
        request: Request<ResumeAppDomainRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let domain_type: DomainType = req
            .r#type
            .try_into()
            .map_err(|_| Status::invalid_argument("Invalid domain type"))?;
        let (app, _) = self.load_app(&req.app, &user_state, None).await?;
        expect_result!(
            add_app_domain(
                &self.kube_client,
                app,
                &req.domain,
                &user_state.user_id,
                req.protect,
                req.custom_prefix,
                domain_type.into(),
                req.app_component,
            )
            .await,
            "Failed to add ingress for domain"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn set_domain_protect(
        &self,
        request: Request<SetDomainProtectRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let (app, _) = self.load_app(&req.id, &user_state, None).await?;
        let domain_type: DomainType = req
            .r#type
            .try_into()
            .map_err(|_| Status::invalid_argument("Invalid domain type"))?;
        // add_app_domain uses apply, so we can reuse it here
        expect_result!(
            add_app_domain(
                &self.kube_client,
                app,
                &req.domain,
                &user_state.user_id,
                req.protect,
                req.custom_prefix,
                domain_type.into(),
                req.app_component,
            )
            .await,
            "Failed to add ingress for domain"
        )?;
        Ok(Response::new(Empty {}))
    }
}
