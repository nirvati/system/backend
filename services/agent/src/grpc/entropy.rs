// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use tonic::{async_trait, Request, Response, Status};

use crate::grpc::api::entropy_server::Entropy;
use crate::grpc::api::{EntropyRequest, EntropyResponse};
use crate::grpc::ApiServer;

#[async_trait]
impl Entropy for ApiServer {
    async fn derive_entropy(
        &self,
        request: Request<EntropyRequest>,
    ) -> Result<Response<EntropyResponse>, Status> {
        let req = request.into_inner();
        let entropy = nirvati::utils::derive_entropy(&req.input, &self.nirvati_seed);
        Ok(Response::new(EntropyResponse { entropy }))
    }
}
