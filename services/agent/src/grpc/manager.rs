// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::apps::files::{
    app_exists, get_system_sources_yml, load_app_registry, read_raw_app_registry,
    read_raw_stores_yml, read_stores_yml, write_app_registry, write_stores_yml,
};
use crate::apps::generator::internal::ResolverCtx;
use crate::apps::init::preload_apps;
use crate::apps::parser::process_apps;
use crate::apps::registry::translate_all_categories;
use crate::apps::repos::{
    add_new_store, check_sys_app_updates, check_updates, check_updates_for_store, download_app,
    download_apps, download_sys_app,
};
use crate::grpc::api::download_apps_request::Download;
use crate::grpc::api::{
    AddStoreRequest, AppUpdate, AppUpdates, DeleteUserRequest, GetAppUpdateRequest,
    InitUserRequest, LoadRegistryRequest, LoadRegistryResponse, LoadStoresRequest,
    LoadStoresResponse, Release, RemoveStoreRequest,
};
use anyhow::anyhow;
use nirvati_apps::metadata::Metadata;
use semver::Version;
use std::collections::HashMap;
use tonic::{async_trait, Request, Response, Status};

use super::api::{manager_server::Manager, DownloadAppsRequest, Empty, GenerateFilesRequest};
use super::{expect_result, require_opt, ApiServer};

#[async_trait]
impl Manager for ApiServer {
    async fn init_user(
        &self,
        request: Request<InitUserRequest>,
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();
        if request.user_id == "system" {
            // Create a dir for the user
            let apps_root = self.main_apps_root.join(&request.user_id);
            expect_result!(
                std::fs::create_dir_all(apps_root),
                "Failed to create user dir"
            )?;
        } else {
            expect_result!(
                preload_apps(
                    self.kube_client.clone(),
                    &self.main_apps_root,
                    &self.nirvati_seed,
                    &request.user_id,
                    &request.user_display_name,
                    &self.default_apps_branch
                )
                .await,
                "Failed to preload apps"
            )?;
        }

        Ok(Response::new(Empty {}))
    }

    async fn delete_user(
        &self,
        request: Request<DeleteUserRequest>,
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();
        let apps_root = self.main_apps_root.join(request.user);
        expect_result!(std::fs::remove_dir_all(apps_root), "Failed to delete user")?;
        Ok(Response::new(Empty {}))
    }

    async fn generate_files(
        &self,
        request: Request<GenerateFilesRequest>,
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();
        let user_state = require_opt!(request.user_state.clone())?;
        let apps_root = self.main_apps_root.join(&user_state.user_id);
        let resolver_ctx = ResolverCtx::new(
            self.main_apps_root.clone(),
            user_state.clone().into(),
            self.nirvati_seed.clone(),
            #[cfg(not(feature = "test-parser"))]
            self.kube_client.clone(),
        );
        {
            let mut registry: Vec<Metadata> = expect_result!(
                process_apps(
                    self.kube_client.clone(),
                    &self.main_apps_root,
                    user_state.into(),
                    &self.nirvati_seed,
                    &resolver_ctx,
                )
                .await,
                "Failed to gather app metadata!"
            )?
            .into_iter()
            .map(|app| app.into_metadata())
            .collect();
            translate_all_categories(&mut registry);
            expect_result!(
                write_app_registry(&apps_root, &registry),
                "Failed to write app registry"
            )?;
        }
        Ok(Response::new(Empty {}))
    }

    async fn download(
        &self,
        request: Request<DownloadAppsRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let apps_root = self.main_apps_root.join(&user_state.user_id);
        let Some(download) = req.download else {
            return Err(Status::invalid_argument("No download request provided"));
        };
        match download {
            Download::NewOrNotInstalledOnly(new_or_not_installed_only) => {
                if user_state.user_id == "system" {
                    return Err(Status::permission_denied(
                        "System user cannot download apps",
                    ));
                }
                expect_result!(
                    download_apps(
                        #[cfg(not(feature = "test-parser"))]
                        self.kube_client.clone(),
                        &self.main_apps_root,
                        new_or_not_installed_only,
                        user_state.into()
                    )
                    .await,
                    "Failed to download apps"
                )?;
            }
            Download::App(app) => {
                // Check if app exists
                if !app_exists(&apps_root, &app) {
                    return Err(Status::not_found("App does not exist"));
                }
                if user_state.user_id == "system" {
                    expect_result!(
                        download_sys_app(
                            self.kube_client.clone(),
                            &self.main_apps_root,
                            &app,
                            user_state.into()
                        )
                        .await,
                        "Failed to download app"
                    )?;
                } else {
                    expect_result!(
                        download_app(
                            self.kube_client.clone(),
                            &self.main_apps_root,
                            &app,
                            user_state.into()
                        )
                        .await,
                        "Failed to download app"
                    )?;
                }
            }
        }
        Ok(Response::new(Empty {}))
    }

    async fn get_updates(
        &self,
        request: Request<GetAppUpdateRequest>,
    ) -> Result<Response<AppUpdates>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let apps_root = self.main_apps_root.join(&user_state.user_id);
        let resolver_ctx = ResolverCtx::new(
            self.main_apps_root.clone(),
            user_state.clone().into(),
            self.nirvati_seed.clone(),
            #[cfg(not(feature = "test-parser"))]
            self.kube_client.clone(),
        );
        let updates = expect_result!(
            match (req.app, user_state.user_id.as_str()) {
                (None, "system") => {
                    check_sys_app_updates(
                        self.kube_client.clone(),
                        &self.main_apps_root,
                        &self.nirvati_seed,
                        user_state.clone().into(),
                        &resolver_ctx,
                    )
                    .await
                }
                (None, _) => {
                    check_updates(
                        self.kube_client.clone(),
                        &self.main_apps_root,
                        &self.nirvati_seed,
                        user_state.clone().into(),
                        &resolver_ctx,
                    )
                    .await
                }
                (Some(app), user) => {
                    // Check if app exists
                    if !app_exists(&apps_root, &app) {
                        return Err(Status::not_found("App does not exist"));
                    }
                    let store = if user == "system" {
                        let mut system_sources_yml = expect_result!(
                            get_system_sources_yml(&self.main_apps_root).await,
                            "Failed to read system sources"
                        )?;
                        expect_result!(
                            system_sources_yml
                                .remove(&app)
                                .ok_or(anyhow!("Failed to get store")),
                            "Failed to get store"
                        )?
                    } else {
                        let stores_yml = expect_result!(
                            read_stores_yml(&apps_root),
                            "Failed to read stores.yml"
                        )?;
                        expect_result!(
                            stores_yml
                                .into_iter()
                                .find(|store| store.apps.contains(&app))
                                .ok_or(anyhow!("Failed to get store")),
                            "Failed to get store"
                        )?
                    };
                    check_updates_for_store(
                        self.kube_client.clone(),
                        &self.main_apps_root,
                        store.to_owned(),
                        Some(app),
                        &self.nirvati_seed,
                        user_state.clone().into(),
                        &resolver_ctx,
                    )
                    .await
                }
            },
            "Failed to check for updates"
        )?;
        let registry =
            expect_result!(load_app_registry(&apps_root), "Failed to read app registry")?;
        let app_updates = updates
            .into_iter()
            .flat_map(|update| {
                // Should always be defined, but we want to be more fail-safe in the future and better handle apps in an invalid state
                let existing_app = registry.iter().find(|app| app.id == update.id);
                let current_version = existing_app
                    .map(|metadata| metadata.version.clone())
                    .unwrap_or_else(|| Version::new(0, 0, 0));
                let current_perms = existing_app
                    .map(|metadata| metadata.has_permissions.clone())
                    .unwrap_or_else(Vec::new);
                let latest_version = update.new_version;
                // Get all releases in the release_notes until we reach the current version
                let releases_notes = update
                    .release_notes
                    .into_iter()
                    .map_while(|(version, r)| {
                        let parsed = Version::parse(&version);
                        match parsed {
                            Ok(version) => {
                                if version == current_version {
                                    None
                                } else {
                                    Some((
                                        version.to_string(),
                                        Release {
                                            notes: HashMap::from_iter(r.into_iter()),
                                        },
                                    ))
                                }
                            }
                            Err(_) => {
                                tracing::warn!("Failed to parse version {}", version);
                                None
                            }
                        }
                    })
                    .collect::<HashMap<_, _>>();
                Some((
                    update.id,
                    AppUpdate {
                        current: current_version.to_string(),
                        latest: latest_version,
                        releases_notes,
                        new_permissions: update
                            .has_permissions
                            .iter()
                            .filter(|p| !current_perms.contains(p))
                            .cloned()
                            .collect(),
                        removed_permissions: current_perms
                            .iter()
                            .filter(|p| !update.has_permissions.contains(p))
                            .cloned()
                            .collect(),
                    },
                ))
            })
            .collect();
        Ok(Response::new(AppUpdates {
            updates: app_updates,
        }))
    }

    async fn add_store(
        &self,
        request: Request<AddStoreRequest>,
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();
        let user_state = require_opt!(request.user_state.clone())?;
        let store_type = expect_result!(
            request.r#type.try_into(),
            "Invalid store type",
            Status::invalid_argument
        )?;
        expect_result!(
            add_new_store(
                self.kube_client.clone(),
                &self.main_apps_root,
                request.src,
                store_type,
                user_state.into()
            )
            .await,
            "Failed to add new store!"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn remove_store(
        &self,
        request: Request<RemoveStoreRequest>,
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();
        let user_state = require_opt!(request.user_state.clone())?;
        let apps_root = self.main_apps_root.join(user_state.user_id);
        let mut stores = expect_result!(read_stores_yml(&apps_root), "Failed to read stores.yml")?;
        stores = stores
            .into_iter()
            .filter(|store| request.src == store.src)
            .collect::<Vec<_>>();
        expect_result!(
            write_stores_yml(&apps_root, &stores),
            "Failed to save stores.yml"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn load_registry(
        &self,
        request: Request<LoadRegistryRequest>,
    ) -> Result<Response<LoadRegistryResponse>, Status> {
        let request = request.into_inner();
        let user_state = require_opt!(request.user_state.clone())?;
        let apps_root = self.main_apps_root.join(user_state.user_id);
        let registry = expect_result!(
            read_raw_app_registry(&apps_root),
            "Failed to read app registry"
        )?;
        Ok(Response::new(LoadRegistryResponse { registry }))
    }

    async fn load_stores(
        &self,
        request: Request<LoadStoresRequest>,
    ) -> Result<Response<LoadStoresResponse>, Status> {
        let request = request.into_inner();
        let user_state = require_opt!(request.user_state.clone())?;
        let apps_root = self.main_apps_root.join(user_state.user_id);
        let stores = expect_result!(read_raw_stores_yml(&apps_root), "Failed to read stores.yml")?;
        Ok(Response::new(LoadStoresResponse { stores }))
    }
}
