// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use super::api::https_server::Https;
use super::api::{
    acme_provider::Provider as ApiInnerAcmeProvider, AcmeProvider as ApiAcmeProvider,
    AddUserIssuerRequest, BuiltInAcmeProvider as ApiBuiltInAcmeProvider, CertificateStatus,
    CreateDomainIssuerRequest, DeleteDomainIssuerRequest, DeleteUserIssuerRequest,
    DnsProvider as ApiDNSProvider, Empty, GetCertificateStatusRequest,
};
use super::{expect_result, ApiServer};
use crate::ingress::domains::https::certs::get_cert_status;
use crate::ingress::domains::https::issuers::{
    create_cluster_issuer, create_domain_issuer, delete_cluster_issuer, delete_domain_issuer,
};
use crate::ingress::domains::https::providers::{AcmeProvider, DNSProviderWithAuth};
use tonic::{async_trait, Request, Response, Status};

const CLOUDFLARE: i32 = ApiDNSProvider::Cloudflare as i32;
const NIRVATI_ME: i32 = ApiDNSProvider::NirvatiMe as i32;
const LETS_ENCRYPT: i32 = ApiBuiltInAcmeProvider::LetsEncrypt as i32;
const BUY_PASS: i32 = ApiBuiltInAcmeProvider::BuyPass as i32;

pub fn grpc_to_internal_acme_provider(
    provider: Option<ApiAcmeProvider>,
) -> Result<AcmeProvider, Status> {
    let acme_provider = provider
        .ok_or(Status::invalid_argument("No ACME provider defined"))?
        .provider
        .ok_or(Status::invalid_argument("No ACME provider defined"))?;
    match acme_provider {
        ApiInnerAcmeProvider::BuiltIn(LETS_ENCRYPT) => Ok(AcmeProvider::LetsEncrypt),
        ApiInnerAcmeProvider::BuiltIn(BUY_PASS) => Ok(AcmeProvider::BuyPass),
        ApiInnerAcmeProvider::Custom(url) => Ok(AcmeProvider::Custom(url)),
        _ => Err(Status::invalid_argument("Invalid ACME provider")),
    }
}

#[async_trait]
impl Https for ApiServer {
    async fn add_domain_issuer(
        &self,
        request: Request<CreateDomainIssuerRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let dns_provider = req
            .dns_provider
            .and_then(|provider| match provider.dns_provider {
                CLOUDFLARE => Some(DNSProviderWithAuth::Cloudflare(provider.auth)),
                NIRVATI_ME => Some(DNSProviderWithAuth::NirvatiMe(provider.auth)),
                _ => None,
            });
        if let Some(DNSProviderWithAuth::NirvatiMe(ref auth)) = dns_provider {
            if !auth.contains(':') {
                return Err(Status::invalid_argument(
                    "Nirvati.me provider requires username:password",
                ));
            }
        }
        expect_result!(
            create_domain_issuer(
                &self.kube_client,
                req.user,
                req.domain.clone(),
                dns_provider,
                req.email,
                grpc_to_internal_acme_provider(req.acme_provider)?,
            )
            .await,
            "Failed to create issuer for domain"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn remove_domain_issuer(
        &self,
        request: Request<DeleteDomainIssuerRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        expect_result!(
            delete_domain_issuer(
                &self.kube_client,
                req.user,
                req.domain.clone(),
                grpc_to_internal_acme_provider(req.acme_provider)?,
            )
            .await,
            "Failed to delete issuer for domain"
        )?;
        Ok(Response::new(Empty {}))
    }

    /*async fn add_certificate(
        &self,
        request: Request<AddCertificateRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        manage::kubernetes::generate_cert(
            &self.kube_client,
            req.user,
            &req.namespace,
            req.domain.clone(),
            req.parent,
            grpc_to_internal_acme_provider(req.acme_provider)?,
        )
        .await
        .map_err(|err| {
            tracing::error!("Failed to add domain {}: {:#?}", req.domain, err);
            Status::internal("Failed to add domain")
        })?;
        Ok(Response::new(Empty {}))
    }

    async fn remove_certificate(
        &self,
        request: Request<DeleteCertificateRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        expect_result!(
            manage::kubernetes::delete_cert(&self.kube_client, req.domain.clone(), &req.namespace)
                .await,
            "Failed to delete domain"
        )?;
        Ok(Response::new(Empty {}))
    }*/

    async fn get_certificate_status(
        &self,
        request: Request<GetCertificateStatusRequest>,
    ) -> Result<Response<CertificateStatus>, Status> {
        let req = request.into_inner();
        let status = expect_result!(
            get_cert_status(&self.kube_client, &req.namespace, req.domain.clone(),).await,
            "Failed to get domain status"
        )?;
        Ok(Response::new(status))
    }

    async fn add_issuer(
        &self,
        request: Request<AddUserIssuerRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        expect_result!(
            create_cluster_issuer(
                &self.kube_client,
                req.user,
                req.email,
                grpc_to_internal_acme_provider(req.provider)?,
            )
            .await,
            "Failed to create issuer"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn delete_issuer(
        &self,
        request: Request<DeleteUserIssuerRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        expect_result!(
            delete_cluster_issuer(
                &self.kube_client,
                req.user,
                grpc_to_internal_acme_provider(req.provider)?
            )
            .await,
            "Failed to delete issuer for domain"
        )?;
        Ok(Response::new(Empty {}))
    }
}
