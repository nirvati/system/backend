use super::api::citadel_compat_server::CitadelCompat;
use crate::grpc::api::apps_server::Apps;
use crate::grpc::api::{InstallCitadelAppsRequest, InstallCitadelAppsResponse, InstallRequest};
use crate::grpc::ApiServer;
use kube::Client;
use serde::{Deserialize, Serialize};
use tonic::{Request, Response, Status};

#[derive(Debug, Clone, Serialize, Deserialize)]
struct UserJson {
    #[serde(rename = "installedApps")]
    installed_apps: Vec<String>,
    https: Option<serde_json::Value>,
}

async fn wait_for_deployment_ready(
    kube_client: &Client,
    ns: &str,
    name: &str,
) -> anyhow::Result<()> {
    let mut timeout = 60 * 3;
    let api =
        kube::Api::<k8s_openapi::api::apps::v1::Deployment>::namespaced(kube_client.clone(), ns);
    loop {
        let deployment = api.get_opt(name).await?;
        if let Some(deployment) = deployment {
            if let Some(status) = &deployment.status {
                if let Some(available_replicas) = status.available_replicas {
                    if available_replicas >= 1 {
                        return Ok(());
                    }
                }
            }
        }
        tokio::time::sleep(std::time::Duration::from_secs(1)).await;
        timeout -= 1;
        if timeout == 0 {
            return Err(anyhow::anyhow!(
                "Timeout waiting for deployment to be ready"
            ));
        }
    }
}

#[tonic::async_trait]
impl CitadelCompat for ApiServer {
    async fn install_citadel_apps(
        &self,
        request: Request<InstallCitadelAppsRequest>,
    ) -> Result<Response<InstallCitadelAppsResponse>, Status> {
        let request = request.into_inner();
        let citadel_user_json = std::env::var("CITADEL_USER_JSON")
            .or(Err(Status::internal("CITADEL_USER_JSON not set")))?;
        let citadel_user_json = std::fs::read_to_string(citadel_user_json)
            .or(Err(Status::internal("Failed to read CITADEL_USER_JSON")))?;
        let citadel_user_json: UserJson = serde_json::from_str(&citadel_user_json)
            .or(Err(Status::internal("Failed to parse CITADEL_USER_JSON")))?;
        let mut user_state = request
            .user_state
            .ok_or(Status::invalid_argument("user_state is required"))?;
        let system_user_state = request
            .system_user_state
            .ok_or(Status::invalid_argument("system_user_state is required"))?;
        self.apply(Request::new(InstallRequest {
            id: "tor".to_string(),
            user_state: Some(system_user_state.clone()),
            initial_domain: None,
            src_user: Some(user_state.user_id.clone()),
            is_existing_on_citadel: None,
        }))
        .await?;
        user_state.installed_apps.push("tor".to_string());
        wait_for_deployment_ready(&self.kube_client, "tor", "plugin")
            .await
            .map_err(|e| {
                tracing::error!("Failed to wait for lnd deployment: {:?}", e);
                Status::internal("Failed to wait for lnd deployment")
            })?;
        self.apply(Request::new(InstallRequest {
            id: "citadel-bitcoin".to_string(),
            user_state: Some(user_state.clone()),
            initial_domain: None,
            src_user: None,
            is_existing_on_citadel: None,
        }))
        .await?;
        user_state
            .installed_apps
            .push("citadel-bitcoin".to_string());
        let citadel_btc_ns = format!("{}-citadel-bitcoin", user_state.user_id);
        wait_for_deployment_ready(&self.kube_client, &citadel_btc_ns, "plugin")
            .await
            .map_err(|e| {
                tracing::error!("Failed to wait for citadel-bitcoin deployment: {:?}", e);
                Status::internal("Failed to wait for citadel-bitcoin deployment")
            })?;
        self.apply(Request::new(InstallRequest {
            id: "lnd".to_string(),
            user_state: Some(user_state.clone()),
            initial_domain: None,
            src_user: None,
            is_existing_on_citadel: None,
        }))
        .await?;
        user_state.installed_apps.push("lnd".to_string());
        let lnd_ns = format!("{}-lnd", user_state.user_id);
        wait_for_deployment_ready(&self.kube_client, &lnd_ns, "plugin")
            .await
            .map_err(|e| {
                tracing::error!("Failed to wait for lnd deployment: {:?}", e);
                Status::internal("Failed to wait for lnd deployment")
            })?;
        self.apply(Request::new(InstallRequest {
            id: "citadel".to_string(),
            user_state: Some(user_state.clone()),
            initial_domain: None,
            src_user: None,
            is_existing_on_citadel: None,
        }))
        .await?;
        user_state.installed_apps.push("citadel".to_string());

        let mut installed_apps = vec![];
        let mut failed_apps = vec![];

        for app in citadel_user_json.installed_apps {
            if app == "tor" || app == "citadel-bitcoin" || app == "lnd" || app == "citadel" {
                continue;
            }
            match self
                .apply(Request::new(InstallRequest {
                    id: app.clone(),
                    user_state: Some(user_state.clone()),
                    initial_domain: None,
                    src_user: None,
                    is_existing_on_citadel: Some(true),
                }))
                .await
            {
                Ok(_) => {
                    installed_apps.push(app.clone());
                    user_state.installed_apps.push(app);
                }
                Err(e) => {
                    tracing::error!("Failed to install app {}: {:?}", app, e);
                    failed_apps.push(app);
                }
            }
        }

        Ok(Response::new(InstallCitadelAppsResponse {
            installed_apps,
            failed_apps,
        }))
    }
}
