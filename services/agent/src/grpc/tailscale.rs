// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use super::{expect_result, ApiServer};
use crate::grpc::api::tailscale_server::Tailscale;
use crate::grpc::api::{Empty, TailscaleStatus};
use k8s_crds_metallb::IPAddressPool;
use k8s_openapi::api::core::v1::Service;
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use nirvati::kubernetes::apply_with_ns;
use std::collections::BTreeMap;
use std::net::IpAddr;
use std::sync::OnceLock;
use tailscale_localapi::{LocalApi, UnixStreamClient};
use tonic::{async_trait, Request, Response, Status};

static TAILSCALE_SOCKET: OnceLock<LocalApi<UnixStreamClient>> = OnceLock::new();

#[async_trait]
impl Tailscale for ApiServer {
    async fn get_status(
        &self,
        _request: Request<Empty>,
    ) -> Result<Response<TailscaleStatus>, Status> {
        let client =
            TAILSCALE_SOCKET.get_or_init(|| LocalApi::new_with_socket_path(&self.tailscale_socket));
        let response = expect_result!(client.status().await, "Failed to get tailscale status")?;
        Ok(Response::new(TailscaleStatus {
            auth_url: response.auth_url,
            authenticated: response.current_tailnet.is_some(),
            own_ip: response
                .self_status
                .tailscale_ips
                .first()
                .map(|ip| ip.to_string()),
        }))
    }

    async fn start(&self, _request: Request<Empty>) -> Result<Response<Empty>, Status> {
        let client =
            TAILSCALE_SOCKET.get_or_init(|| LocalApi::new_with_socket_path(&self.tailscale_socket));
        expect_result!(client.start().await, "Failed to start tailscale")?;
        Ok(Response::new(Empty {}))
    }

    async fn start_login(&self, _request: Request<Empty>) -> Result<Response<Empty>, Status> {
        let client =
            TAILSCALE_SOCKET.get_or_init(|| LocalApi::new_with_socket_path(&self.tailscale_socket));
        expect_result!(
            client.login_interactive().await,
            "Failed to start tailscale"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn add_ip_to_metallb(&self, _request: Request<Empty>) -> Result<Response<Empty>, Status> {
        let client =
            TAILSCALE_SOCKET.get_or_init(|| LocalApi::new_with_socket_path(&self.tailscale_socket));
        let status = expect_result!(client.status().await, "Failed to get tailscale status")?;
        let ip = status
            .self_status
            .tailscale_ips
            .first()
            .ok_or_else(|| Status::internal("No IP found"))?;
        let pool = IPAddressPool {
            metadata: ObjectMeta {
                name: Some("tailscale".to_string()),
                ..Default::default()
            },
            spec: k8s_crds_metallb::IPAddressPoolSpec {
                addresses: vec![match ip {
                    IpAddr::V4(ip) => format!("{}/32", ip),
                    IpAddr::V6(ip) => format!("{}/128", ip),
                }],
                ..Default::default()
            },
        };
        expect_result!(
            apply_with_ns(self.kube_client.clone(), &[pool], "metallb-system").await,
            "Failed to create IP pool"
        )?;
        let traefik_svc = Service {
            metadata: ObjectMeta {
                name: Some("traefik-tailscale".to_string()),
                annotations: Some(BTreeMap::from([
                    (
                        "metallb.universe.tf/allow-shared-ip".to_owned(),
                        "ip_set_tailscale".to_owned(),
                    ),
                    (
                        "metallb.universe.tf/address-pool".to_owned(),
                        "tailscale".to_owned(),
                    ),
                ])),
                ..Default::default()
            },
            spec: Some(k8s_openapi::api::core::v1::ServiceSpec {
                type_: Some("LoadBalancer".to_string()),
                selector: Some(BTreeMap::from([
                    ("app.kubernetes.io/name".to_owned(), "traefik".to_owned()),
                    (
                        "app.kubernetes.io/instance".to_owned(),
                        "traefik-kube-system".to_owned(),
                    ),
                ])),
                ports: Some(vec![
                    k8s_openapi::api::core::v1::ServicePort {
                        port: 80,
                        target_port: Some(
                            k8s_openapi::apimachinery::pkg::util::intstr::IntOrString::String(
                                "web".to_string(),
                            ),
                        ),
                        name: Some("web".to_string()),
                        ..Default::default()
                    },
                    k8s_openapi::api::core::v1::ServicePort {
                        port: 443,
                        target_port: Some(
                            k8s_openapi::apimachinery::pkg::util::intstr::IntOrString::String(
                                "websecure".to_string(),
                            ),
                        ),
                        name: Some("websecure".to_string()),
                        ..Default::default()
                    },
                ]),
                ..Default::default()
            }),
            ..Default::default()
        };
        expect_result!(
            apply_with_ns(self.kube_client.clone(), &[traefik_svc], "kube-system").await,
            "Failed to create traefik service"
        )?;
        Ok(Response::new(Empty {}))
    }
}
