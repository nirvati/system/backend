// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::grpc::api::node_server::Node;
use crate::grpc::api::{Empty, NodeInfo};
use crate::grpc::{expect_result, ApiServer};
use nirvati::kubernetes::get_node_ip;
use tonic::{async_trait, Request, Response, Status};

#[async_trait]
impl Node for ApiServer {
    async fn get_info(&self, _request: Request<Empty>) -> Result<Response<NodeInfo>, Status> {
        let ip = expect_result!(get_node_ip(&self.kube_client).await, "Failed to get own IP")?;
        Ok(Response::new(NodeInfo { ip }))
    }
}
