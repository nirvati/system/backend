// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use k8s_crds_traefik::IngressRoute;
use kube::Api;
use tonic::{async_trait, Request, Response, Status};

use crate::grpc::api::setup_server::Setup;
use crate::grpc::api::Empty;
use crate::grpc::{expect_result, ApiServer};

#[async_trait]
impl Setup for ApiServer {
    async fn delete_nirvati_ingress(
        &self,
        _req: Request<Empty>,
    ) -> Result<Response<Empty>, Status> {
        let ingress_api: Api<IngressRoute> = Api::namespaced(self.kube_client.clone(), "nirvati");
        expect_result!(
            ingress_api.delete("ingress", &Default::default()).await,
            "Failed to delete ingress route"
        )?;
        Ok(Response::new(Empty {}))
    }
}
