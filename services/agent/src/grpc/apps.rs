// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::HashMap;

use super::api::{
    apps_server::Apps, ApplyResponse, Empty, InstallRequest, LoadAppMetadataRequest,
    LoadAppMetadataResponse, PauseAppRequest, RemoveRunnablesRequest, RestoreSnapshotRequest,
    ResumeAppRequest, TakeSnapshotRequest, UninstallRequest, UserState,
};
use super::{expect_result, require_opt, ApiServer};
use crate::apps::files::{
    add_system_source, append_installed_list, read_stores_yml, remove_installed_list,
    remove_system_source,
};
use crate::apps::generator::helm::generate_chart;
use crate::apps::generator::kubernetes::generate_kubernetes_config;
use crate::apps::kubernetes::app::{install_chart, pause_ns, resume_ns, uninstall_chart};
use crate::apps::parser::do_postprocessing;
use crate::apps::preloader::preload_data;
use crate::apps::snapshots::take_app_snapshot;
use crate::plugins::{apply_hook, uninstall_hook};
use k8s_openapi::api::apps::v1::{DaemonSet, Deployment, ReplicaSet, StatefulSet};
use k8s_openapi::api::batch::v1::{CronJob, Job};
use k8s_openapi::api::core::v1::Pod;
use nirvati::kubernetes::remove_all_from_ns;
use nirvati_apps::metadata::AppScope;
use nirvati_logs::events::AppEvent;
use sha2::Digest;
use tonic::{async_trait, Request, Response, Status};
use uuid::Uuid;
use crate::apps::kubernetes::pvcs::turn_pv_cross_ns;

impl UserState {
    pub fn get_app_settings(&self) -> HashMap<String, HashMap<String, serde_yaml::Value>> {
        self.app_settings
            .iter()
            .map(|(k, v)| {
                (
                    k.clone(),
                    serde_json::from_str(v).unwrap_or_else(|_| HashMap::new()),
                )
            })
            .collect()
    }

    pub fn into_app_settings(self) -> HashMap<String, HashMap<String, serde_yaml::Value>> {
        self.app_settings
            .into_iter()
            .map(|(k, v)| {
                (
                    k,
                    serde_json::from_str(&v).unwrap_or_else(|_| HashMap::new()),
                )
            })
            .collect()
    }
}

#[async_trait]
impl Apps for ApiServer {
    async fn apply(
        &self,
        request: Request<InstallRequest>,
    ) -> Result<Response<ApplyResponse>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let apps_root = self.main_apps_root.join(&user_state.user_id);
        if let Some(src_user) = req.src_user {
            if user_state.user_id != "system" {
                return Err(Status::permission_denied(
                    "Only system can copy apps from other apps",
                ));
            }
            let other_apps_root = self.main_apps_root.join(&src_user);

            let src_user_stores_yml = expect_result!(
                read_stores_yml(&other_apps_root),
                "Failed to read stores.yml"
            )?;
            let Some(mut app_src) = src_user_stores_yml
                .into_iter()
                .find(|store| store.apps.contains(&req.id))
            else {
                return Err(Status::not_found("App not found in source user"));
            };
            app_src.apps = vec![req.id.clone()];
            app_src.id = Uuid::new_v4();
            expect_result!(
                fs_extra::dir::copy(other_apps_root.join(&req.id), &apps_root, &{
                    let mut options = fs_extra::dir::CopyOptions::new();
                    options.overwrite = true;
                    options
                }),
                "Failed to copy app"
            )?;
            expect_result!(
                add_system_source(&self.main_apps_root, &req.id, app_src).await,
                "Failed to add system source"
            )?;
        }
        let (app, resolver_ctx) = self
            .load_app(&req.id, &user_state, req.initial_domain.clone())
            .await?;

        if app.metadata.is_citadel && !req.is_existing_on_citadel.is_some_and(|exists| exists) {
            if let Ok(citadel_app_data) = std::env::var("CITADEL_APP_DATA_MOUNT") {
                let citadel_app_data = std::path::Path::new(&citadel_app_data);
                // Copy the app to the citadel app data mount
                let citadel_app_data = citadel_app_data.join(&req.id);
                if citadel_app_data.exists() {
                    tracing::info!("Data dir already exists");
                } else {
                    expect_result!(
                        fs_extra::dir::copy(apps_root.join(&req.id), &citadel_app_data, &{
                            let mut options = fs_extra::dir::CopyOptions::new();
                            options.overwrite = true;
                            options
                        }),
                        "Failed to copy app to citadel app data"
                    )?;
                }
            } else {
                return Err(Status::internal(
                    "CITADEL_APP_DATA_MOUNT is not set, cannot install citadel app",
                ));
            }
        }

        crate::log(
            nirvati_logs::events::Event::App(AppEvent::Apply {
                app_id: req.id.clone(),
                version: app.metadata.version.to_string(),
                settings: serde_json::to_value(
                    user_state
                        .get_app_settings()
                        .get(&req.id)
                        .cloned()
                        .unwrap_or_else(HashMap::new),
                )
                .unwrap(),
            }),
            &user_state.user_id,
        );

        let app_metadata = app.get_metadata().clone();
        let is_sys_lib = user_state.user_id == "system";
        let app_exported_data = app_metadata.exported_data.clone().unwrap_or_default();
        let perms = app.get_metadata().has_permissions.clone();
        let kube_config = expect_result!(
            generate_kubernetes_config(
                #[cfg(not(feature = "test-parser"))]
                self.kube_client.clone(),
                app.clone(),
                &user_state.clone().into(),
                &resolver_ctx
            )
            .await,
            "Failed to generate Kubernetes config"
        )?;
        let app_settings_hash = user_state
            .app_settings
            .get(&req.id)
            .map(|settings| {
                // SHA256
                let mut sha = sha2::Sha256::new();
                sha.update(settings.as_bytes());
                let hash = sha.finalize();
                let hash = hash.iter().fold(String::new(), |mut acc, x| {
                    acc.push_str(&format!("{:02x}", x));
                    acc
                });
                hash
            })
            .unwrap_or_else(|| "default".to_string());
        let pvcs = kube_config.pvcs.clone();
        expect_result!(
            do_postprocessing(
                self.kube_client.clone(),
                &self.main_apps_root.join(&user_state.user_id).join(&req.id),
                user_state.clone().into(),
                &self.nirvati_seed,
                req.initial_domain.clone()
            )
            .await,
            "Failed to do postprocessing on nirvati_jinja files"
        )?;
        expect_result!(
            preload_data(
                &self.kube_client,
                &app,
                &self.main_apps_root.join(&user_state.user_id).join(&req.id),
                &user_state.user_id,
                pvcs,
                &resolver_ctx,
            )
            .await,
            "Failed to preload data"
        )?;
        expect_result!(
            apply_hook(
                self.kube_client.clone(),
                &self.main_apps_root,
                &req.id,
                &app,
                &perms,
                app_exported_data,
                user_state.clone().into()
            )
            .await,
            "Failed to run plugin hooks"
        )?;
        for pv in kube_config.get_pvs_to_mirror() {
            turn_pv_cross_ns(self.kube_client.clone(), &pv)
                .await
                .map_err(|e| Status::internal(format!("Failed to turn pvc cross ns: {}", e)))?;
        }
        let (chart, chart_version) = expect_result!(
            generate_chart(
                kube_config,
                app_metadata,
                &user_state.user_id,
                &app_settings_hash,
            ),
            "Failed to generate chart"
        )?;
        expect_result!(
            install_chart(
                self.kube_client.clone(),
                &req.id,
                &user_state.user_id,
                is_sys_lib,
                &chart,
            )
            .await,
            "Failed to apply kubernetes config"
        )?;
        append_installed_list(&apps_root, &req.id)
            .map_err(|e| Status::internal(format!("Failed to append installed list: {}", e)))?;
        Ok(Response::new(ApplyResponse { chart_version }))
    }

    async fn load_app_metadata(
        &self,
        request: Request<LoadAppMetadataRequest>,
    ) -> Result<Response<LoadAppMetadataResponse>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let (mut app, _) = self
            .load_app(&req.id, &user_state, req.initial_domain)
            .await?;

        let (_, other_permissions) = expect_result!(
            crate::plugins::convert_crs(
                #[cfg(not(feature = "test-parser"))]
                self.kube_client.clone(),
                &req.id,
                &app.custom_resources,
                user_state.into(),
                app.metadata.scope == AppScope::System,
            )
            .await,
            "Failed to convert CRs"
        )?;
        app.metadata.has_permissions.extend(other_permissions);
        Ok(Response::new(LoadAppMetadataResponse {
            metadata: serde_json::to_string(&app.get_metadata()).unwrap(),
        }))
    }

    async fn uninstall(
        &self,
        request: Request<UninstallRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let apps_root = self.main_apps_root.join(&user_state.user_id);
        let (app, _) = self.load_app(&req.id, &user_state, None).await?;
        expect_result!(
            uninstall_chart(self.kube_client.clone(), &req.id, &user_state.user_id).await,
            "Failed to remove chart"
        )?;
        expect_result!(
            uninstall_hook(
                self.kube_client.clone(),
                &apps_root,
                &req.id,
                &app.get_metadata().has_permissions,
                &app,
                &user_state.clone().into(),
            )
            .await,
            "Failed to run plugin hooks"
        )?;
        if user_state.user_id == "system" {
            expect_result!(
                remove_system_source(&self.main_apps_root, &req.id).await,
                "Failed to remove system source"
            )?;
        }
        remove_installed_list(&apps_root, &req.id)
            .map_err(|e| Status::internal(format!("Failed to remove installed list: {}", e)))?;
        Ok(Response::new(Empty {}))
    }

    async fn pause_app(
        &self,
        request: Request<PauseAppRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let app_ns = if user_state.user_id == "system" {
            req.app.clone()
        } else {
            format!("{}-{}", user_state.user_id, req.app)
        };
        // TODO: With multi-server support, we need to keep track of how many deployments we are running
        pause_ns(self.kube_client.clone(), &app_ns)
            .await
            .map_err(|e| Status::internal(format!("Failed to pause namespace: {}", e)))?;
        Ok(Response::new(Empty {}))
    }

    async fn resume_app(
        &self,
        request: Request<ResumeAppRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let app_ns = if user_state.user_id == "system" {
            req.app.clone()
        } else {
            format!("{}-{}", user_state.user_id, req.app)
        };
        // TODO: With multi-server support, we need to keep track of how many deployments we are running
        resume_ns(self.kube_client.clone(), &app_ns)
            .await
            .map_err(|e| {
                tracing::error!("Failed to resume namespace {}: {:#?}", app_ns, e);
                Status::internal(format!("Failed to resume namespace: {}", e))
            })?;
        Ok(Response::new(Empty {}))
    }

    async fn remove_runnables(
        &self,
        request: Request<RemoveRunnablesRequest>,
    ) -> Result<Response<Empty>, Status> {
        // Remove everything that can create a pod
        // Deployments, StatefulSets, Jobs, CronJobs, Pods, ReplicaSets, DaemonSets
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let app_ns = if user_state.user_id == "system" {
            req.app.clone()
        } else {
            format!("{}-{}", user_state.user_id, req.app)
        };

        let client = self.kube_client.clone();
        nirvati::for_each_type_parallel_result!(remove_all_from_ns, Deployment, StatefulSet, Job, CronJob, ReplicaSet, DaemonSet, Pod; (client.clone(), &app_ns))
            .map_err(|e| Status::internal(format!("Failed to remove runnables: {}", e)))?;
        Ok(Response::new(Empty {}))
    }

    async fn take_snapshot(
        &self,
        request: Request<TakeSnapshotRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        let Some(user_state) = req.user_state else {
            return Err(Status::invalid_argument("User state is required"));
        };
        let app_ns = if user_state.user_id == "system" {
            req.app.clone()
        } else {
            format!("{}-{}", user_state.user_id, req.app)
        };
        take_app_snapshot(self.kube_client.clone(), &app_ns, req.snapshot_id).await;
        Ok(Response::new(Empty {}))
    }

    async fn restore_snapshot(
        &self,
        request: Request<RestoreSnapshotRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        self.remove_runnables(Request::new(RemoveRunnablesRequest {
            app: req.app,
            user_state: req.user_state,
        }))
        .await?;

        todo!()
    }
}
