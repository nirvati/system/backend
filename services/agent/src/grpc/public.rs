// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use tonic::{Request, Response, Status};

use crate::apps::parser::context::get_processing_context;
use crate::grpc::public::defs::{LoadAppRequest, LoadAppResponse};
use crate::grpc::{expect_result, require_opt, ApiServer};
use defs::{
    GetAppDirRequest, GetAppDirResponse, GetAppMetadataRequest, GetAppMetadataResponse,
    GetParsingContextRequest, GetParsingContextResponse,
};

pub mod defs {
    tonic::include_proto!("nirvati_public");
}

impl From<defs::UserState> for crate::grpc::api::UserState {
    fn from(user_state: defs::UserState) -> Self {
        Self {
            user_id: user_state.user_id,
            installed_apps: user_state.installed_apps,
            app_settings: user_state.app_settings,
            user_display_name: user_state.user_display_name,
        }
    }
}

impl From<defs::UserState> for crate::plugins::api::UserState {
    fn from(user_state: defs::UserState) -> Self {
        Self {
            user_id: user_state.user_id,
            installed_apps: user_state.installed_apps,
            app_settings: user_state.app_settings,
            user_display_name: user_state.user_display_name,
        }
    }
}

#[tonic::async_trait]
impl defs::apps_public_server::AppsPublic for ApiServer {
    async fn get_app_metadata(
        &self,
        request: Request<GetAppMetadataRequest>,
    ) -> Result<Response<GetAppMetadataResponse>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let (app, _) = ApiServer::load_app(self, &req.app_id, &user_state.into(), None).await?;
        let app_metadata = app.get_metadata().clone();
        Ok(Response::new(GetAppMetadataResponse {
            app_metadata: expect_result!(
                serde_json::to_string(&app_metadata),
                "Failed to serialize app metadata"
            )?,
        }))
    }

    async fn get_app_dir(
        &self,
        request: Request<GetAppDirRequest>,
    ) -> Result<Response<GetAppDirResponse>, Status> {
        let req = request.into_inner();
        let apps_root = self.main_apps_root.join(&req.user_id);
        let app_dir = apps_root.join(&req.app_id);
        let compressed = expect_result!(
            crate::utils::tar::compress_dir(&app_dir).await,
            "Failed to compress app dir"
        )?;
        Ok(Response::new(GetAppDirResponse {
            app_dir: compressed,
        }))
    }

    async fn get_parsing_context(
        &self,
        request: Request<GetParsingContextRequest>,
    ) -> Result<Response<GetParsingContextResponse>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let additional_metadata = expect_result!(
            serde_json::from_str(&req.additional_metadata),
            "Failed to deserialize additional metadata",
            Status::invalid_argument
        )?;
        let parsing_context = expect_result!(
            get_processing_context(
                #[cfg(not(feature = "test-parser"))]
                self.kube_client.clone(),
                &req.app_id,
                &req.permissions,
                additional_metadata,
                &user_state.into(),
            )
            .await,
            "Failed to serialize parsing context"
        )?;
        Ok(Response::new(GetParsingContextResponse {
            parsing_context: expect_result!(
                serde_json::to_string(&parsing_context.into_json()),
                "Failed to serialize parsing context"
            )?,
        }))
    }

    async fn load_app(
        &self,
        request: Request<LoadAppRequest>,
    ) -> Result<Response<LoadAppResponse>, Status> {
        let req = request.into_inner();
        let user_state = require_opt!(req.user_state.clone())?;
        let (app, _) = ApiServer::load_app(self, &req.app_id, &user_state.into(), None).await?;
        Ok(Response::new(LoadAppResponse {
            app_repr: expect_result!(
                serde_json::to_string(&app),
                "Failed to serialize app metadata"
            )?,
        }))
    }
}
