// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use super::{expect_result, ApiServer};
use crate::grpc::api::upgrades_server::Upgrades;
use crate::grpc::api::{Empty, KubeDistro, KubeVersion, VersionInfo};
use crate::system_management::k8s_updates::{
    get_distro, get_installed_version, get_installed_version_raw, get_latest_release_up_to_channel,
    upgrade, Distro,
};
use tonic::{async_trait, Request, Response, Status};

#[async_trait]
impl Upgrades for ApiServer {
    async fn get_kube_version(
        &self,
        _request: Request<Empty>,
    ) -> Result<Response<KubeVersion>, Status> {
        let distro = expect_result!(
            get_distro(&self.kube_client).await,
            "Failed to check what Kubernetes distribution is installed"
        )?;
        Ok(Response::new(KubeVersion {
            distro: match distro {
                None => i32::from(KubeDistro::Unknown),
                Some(Distro::K3s) => i32::from(KubeDistro::K3s),
                Some(Distro::Rke2) => i32::from(KubeDistro::Rke2),
            },
            version: expect_result!(
                get_installed_version_raw(&self.kube_client).await,
                "Failed to get Kubernetes version"
            )?
            .to_string(),
        }))
    }

    async fn get_latest_kube_version(
        &self,
        _request: Request<Empty>,
    ) -> Result<Response<VersionInfo>, Status> {
        let installed = expect_result!(
            get_installed_version(&self.kube_client).await,
            "Failed to get installed Kubernetes version"
        )?;
        let latest = expect_result!(
            get_latest_release_up_to_channel(&installed, "stable").await,
            "Failed to get latest Kubernetes version"
        )?;
        Ok(Response::new(VersionInfo {
            version: latest.to_string(),
        }))
    }

    async fn update_kube(&self, request: Request<VersionInfo>) -> Result<Response<Empty>, Status> {
        let version_to_install = expect_result!(
            request.into_inner().version.parse(),
            "Failed to parse version",
            Status::invalid_argument
        )?;
        expect_result!(
            upgrade(&self.kube_client, &version_to_install).await,
            "Failed to install update!"
        )?;
        Ok(Response::new(Empty {}))
    }
}
