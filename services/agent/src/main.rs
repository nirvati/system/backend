// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use base64::engine::general_purpose::STANDARD;
use base64::Engine;
use k8s_openapi::api::core::v1::Secret;
use kube::{Api, Client};
use tonic::transport::Server;

use nirvati_agent::grpc::api::apps_server::AppsServer;
use nirvati_agent::grpc::api::citadel_compat_server::CitadelCompatServer;
use nirvati_agent::grpc::api::entropy_server::EntropyServer;
use nirvati_agent::grpc::api::https_server::HttpsServer;
use nirvati_agent::grpc::api::ingress_server::IngressServer;
use nirvati_agent::grpc::api::manager_server::ManagerServer;
use nirvati_agent::grpc::api::middlewares_server::MiddlewaresServer;
use nirvati_agent::grpc::api::node_server::NodeServer;
use nirvati_agent::grpc::api::proxies_server::ProxiesServer;
use nirvati_agent::grpc::api::services_server::ServicesServer;
use nirvati_agent::grpc::api::setup_server::SetupServer;
use nirvati_agent::grpc::api::tailscale_server::TailscaleServer;
use nirvati_agent::grpc::api::upgrades_server::UpgradesServer;
use nirvati_agent::grpc::api::users_server::UsersServer;
use nirvati_agent::grpc::public::defs::apps_public_server;
use nirvati_agent::grpc::ApiServer;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    #[cfg(feature = "dotenvy")]
    dotenvy::dotenv().expect("Failed to load .env file");
    // Take Nirvati path and from env
    let apps_path = std::env::var("APPS_PATH").expect("APPS_PATH not set");
    let nirvati_seed = std::env::var("NIRVATI_SEED").expect("NIRVATI_SEED not set");
    let default_apps_branch =
        std::env::var("NIRVATI_APP_STORE_VERSION").unwrap_or("stable".to_string());
    let socket_path = std::env::var("TAILSCALE_SOCKET").expect("TAILSCALE_SOCKET not set");

    let kube_client = Client::try_default()
        .await
        .expect("Failed to create kube client");

    let api: Api<Secret> = Api::namespaced(kube_client.clone(), "nirvati");
    let pubkey = api.get("jwt-keys").await.expect("Failed to get jwt-pubkey");
    let pubkey = pubkey.data.unwrap();
    let pubkey = pubkey.get("public_key").expect("Failed to get public_key");

    unsafe {
        std::env::set_var("JWT_PUBKEY_BASE64", STANDARD.encode(&pubkey.0));
    }

    let api_server = ApiServer {
        main_apps_root: apps_path.clone().into(),
        kube_client: api.into_client(),
        nirvati_seed: nirvati_seed.clone(),
        default_apps_branch,
        tailscale_socket: socket_path,
    };
    let bind_address = std::env::var("BIND_ADDRESS").unwrap_or("[::]:8080".to_string());
    tracing::info!("Starting server on {}", bind_address);
    let address = bind_address.parse().expect("Failed to parse bind address");
    let api_server_clone = api_server.clone();
    let server_1 = tokio::spawn(async move {
        Server::builder()
            .add_service(AppsServer::new(api_server.clone()))
            .add_service(EntropyServer::new(api_server.clone()))
            .add_service(IngressServer::new(api_server.clone()))
            .add_service(ManagerServer::new(api_server.clone()))
            .add_service(MiddlewaresServer::new(api_server.clone()))
            .add_service(NodeServer::new(api_server.clone()))
            .add_service(SetupServer::new(api_server.clone()))
            .add_service(TailscaleServer::new(api_server.clone()))
            .add_service(UpgradesServer::new(api_server.clone()))
            .add_service(UsersServer::new(api_server.clone()))
            .add_service(ServicesServer::new(api_server.clone()))
            .add_service(ProxiesServer::new(api_server.clone()))
            .add_service(HttpsServer::new(api_server.clone()))
            .add_service(CitadelCompatServer::new(api_server.clone()))
            .serve(address)
            .await
    });

    let public_api_bind_address =
        std::env::var("PUBLIC_API_BIND_ADDRESS").unwrap_or("[::]:8081".to_string());
    tracing::info!("Starting public server on {}", public_api_bind_address);
    let public_address = public_api_bind_address
        .parse()
        .expect("Failed to parse public bind address");
    let server_2 = tokio::spawn(async move {
        Server::builder()
            .add_service(apps_public_server::AppsPublicServer::new(api_server_clone))
            .serve(public_address)
            .await
    });
    let (server1_res, server2_res) =
        tokio::try_join!(server_1, server_2).expect("Failed to start servers");
    server1_res.expect("Failed to start private server");
    server2_res.expect("Failed to start public server");
}
