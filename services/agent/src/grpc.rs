// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::path::PathBuf;

use crate::apps::generator::internal::ResolverCtx;
use kube::Client;
use tonic::Status;

use crate::apps::parser::load_app;
use nirvati_apps::internal::types::InternalAppRepresentation;

use crate::grpc::api::UserState;

pub mod apps;
pub mod ingress;
pub mod manager;
pub mod public;
pub mod setup;

pub mod citadel;
pub mod entropy;
pub mod https;
pub mod middlewares;
pub mod node;
pub mod proxies;
pub mod services;
pub mod tailscale;
pub mod upgrades;
pub mod users;

pub mod api {
    tonic::include_proto!("nirvati_agent");
}

impl From<UserState> for crate::plugins::api::UserState {
    fn from(user_state: UserState) -> Self {
        crate::plugins::api::UserState {
            user_id: user_state.user_id,
            installed_apps: user_state.installed_apps,
            app_settings: user_state.app_settings,
            user_display_name: user_state.user_display_name,
        }
    }
}

impl From<&UserState> for crate::plugins::api::UserState {
    fn from(user_state: &UserState) -> Self {
        crate::plugins::api::UserState {
            user_id: user_state.user_id.clone(),
            installed_apps: user_state.installed_apps.clone(),
            app_settings: user_state.app_settings.clone(),
            user_display_name: user_state.user_display_name.clone(),
        }
    }
}

#[derive(Clone)]
pub struct ApiServer {
    pub main_apps_root: PathBuf,
    pub kube_client: Client,
    pub nirvati_seed: String,
    pub default_apps_branch: String,
    pub tailscale_socket: String,
}

macro_rules! expect_result {
    ($expr:expr) => {
        $expr.map_err(|err| Status::internal(err.to_string()))
    };
    ($expr:expr, $type:path) => {
        $expr.map_err(|err| $type(err.to_string()))
    };
    ($expr:expr, $msg:expr) => {
        $expr.map_err(|err| {
            tracing::error!("{}: {:#?}", $msg, err);
            Status::internal($msg)
        })
    };
    ($expr:expr, $msg:expr, $type:path) => {
        $expr.map_err(|err| {
            tracing::error!("{}: {:#?}", $msg, err);
            $type($msg)
        })
    };
}

macro_rules! require_opt {
    ($expr:expr) => {
        $expr.ok_or_else(|| {
            Status::invalid_argument(format!(
                "A required field was not set: {}",
                stringify!($expr)
            ))
        })
    };
    ($expr:expr, $msg:expr) => {
        $expr.ok_or_else(|| Status::invalid_argument($msg))
    };
    ($expr:expr, $type:path) => {
        $expr.ok_or_else(|| {
            $type(format!(
                "A required field was not set: {}",
                stringify!($expr)
            ))
        })
    };
    ($expr:expr, $msg:expr, $type:path) => {
        $expr.ok_or_else(|| $type($msg))
    };
}

use crate::apps::files::app_exists;
pub(crate) use expect_result;
pub(crate) use require_opt;

impl ApiServer {
    async fn load_app(
        &self,
        app_id: &str,
        user_state: &UserState,
        init_domain: Option<String>,
    ) -> Result<(InternalAppRepresentation, ResolverCtx), Status> {
        let apps_root = self.main_apps_root.join(&user_state.user_id);
        if !app_exists(&apps_root, app_id) {
            return Err(Status::not_found("App does not exist!"));
        }
        let resolver_ctx = ResolverCtx::new(
            self.main_apps_root.clone(),
            user_state.clone().into(),
            self.nirvati_seed.clone(),
            #[cfg(not(feature = "test-parser"))]
            self.kube_client.clone(),
        );
        Ok((
            expect_result!(
                load_app(
                    #[cfg(not(feature = "test-parser"))]
                    self.kube_client.clone(),
                    &self.main_apps_root,
                    app_id,
                    &apps_root.join(app_id),
                    user_state.clone().into(),
                    &self.nirvati_seed,
                    init_domain,
                    &resolver_ctx,
                )
                .await,
                "Failed to load app"
            )?,
            resolver_ctx,
        ))
    }
}
