#!/usr/bin/env bash

NIRVATI_VERSION=$1
GITHUB_TOKEN=$2

if [ -z "${NIRVATI_VERSION}" ] || [ -z "${GITHUB_TOKEN}" ]; then
  echo "Usage: $0 <nirvati-version> <github-token>"
  exit 1
fi

if [ "${NIRVATI_VERSION}" == "main" ]|| [ "${NIRVATI_VERSION}" == "master" ]; then
  exit 0
fi

./generate-image-bundle.sh ${NIRVATI_VERSION}

ARCH=$(uname -m)
if [ "${ARCH}" == "aarch64" ]; then
  ARCH="arm64"
fi
if [ "${ARCH}" == "x86_64" ]; then
  ARCH="amd64"
fi

RELEASE_ID=$(curl -s -H "Authorization: token ${GITHUB_TOKEN}" -H "Accept: application/vnd.github.v3+json" https://api.github.com/repos/nirvati/releases/releases | jq -r ".[] | select(.tag_name == \"${NIRVATI_VERSION}\") | .id")

mv nirvati-images-$(uname -m).tar.xz nirvati-images-${NIRVATI_VERSION}-${ARCH}.tar.xz
curl -X POST -H "Authorization: token ${GITHUB_TOKEN}" -H "Accept: application/vnd.github.v3+json" -H "Content-Type: application/octet-stream" --data-binary @nirvati-images-${NIRVATI_VERSION}-${ARCH}.tar.xz https://uploads.github.com/repos/nirvati/releases/releases/${RELEASE_ID}/assets?name=nirvati-images-${NIRVATI_VERSION}-${ARCH}.tar.xz
