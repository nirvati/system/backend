#!/usr/bin/env bash

if [[ $1 == */* ]]; then
  export image=$1
else
  export image="nirvati/$1"
fi

export IMAGE_DIGEST_ARM64=$(docker pull --platform linux/arm64 "harbor.nirvati.org/$image:${2}-arm64" | grep "Digest: " | sed "s/Digest: //")
export IMAGE_DIGEST_AMD64=$(docker pull --platform linux/amd64 "harbor.nirvati.org/$image:${2}-amd64" | grep "Digest: " | sed "s/Digest: //")
docker manifest create "harbor.nirvati.org/$image:${2}" "harbor.nirvati.org/$image:${2}-arm64@${IMAGE_DIGEST_ARM64}" "harbor.nirvati.org/$image:${2}-amd64@${IMAGE_DIGEST_AMD64}"
docker manifest annotate "harbor.nirvati.org/$image:${2}" "harbor.nirvati.org/$image:${2}-arm64@${IMAGE_DIGEST_ARM64}" --arch arm64 --os linux
docker manifest annotate "harbor.nirvati.org/$image:${2}" "harbor.nirvati.org/$image:${2}-amd64@${IMAGE_DIGEST_AMD64}" --arch amd64 --os linux
docker manifest push "harbor.nirvati.org/$image:${2}"
export IMAGE_DIGEST=$(docker manifest inspect "harbor.nirvati.org/$image:${2}" | jq -r ".manifests[0].digest")
cosign sign "harbor.nirvati.org/$image:${2}@$IMAGE_DIGEST"
