#!/usr/bin/env bash

NIRVATI_VERSION=$1

if [ -z "${NIRVATI_VERSION}" ]; then
  echo "Usage: $0 <nirvati-version>"
  exit 1
fi

IMAGES=$(./generate-image-list.sh ${NIRVATI_VERSION})

for IMAGE in ${IMAGES}; do
  echo "Pulling ${IMAGE}"
  docker pull "${IMAGE}"
done

# Save the images
docker save -o nirvati-images-$(uname -m).tar ${IMAGES}
# Compress the images
xz -v --extreme -9 nirvati-images-$(uname -m).tar
