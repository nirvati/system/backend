#!/usr/bin/env bash

NIRVATI_VERSION=$1

if [ -z "${NIRVATI_VERSION}" ]; then
  echo "Usage: $0 <nirvati-version>"
  exit 1
fi

K3S_VERSION=$(curl "https://update.k3s.io/v1-release/channels/stable" -s -L -I -o /dev/null -w '%{url_effective}' | sed "s/https:\/\/github.com\/k3s-io\/k3s\/releases\/tag\///")
K3S_IMAGE_LIST_SRC="https://github.com/k3s-io/k3s/releases/download/${K3S_VERSION}/k3s-images.txt"
K3S_IMAGE_LIST=$(curl -sL "${K3S_IMAGE_LIST_SRC}")
# Remove the lines containing "klipper-lb", "local-path-provisioner" and "coredns" from the image list
K3S_IMAGE_LIST=$(echo "${K3S_IMAGE_LIST}" | grep -v "klipper-lb" | grep -v "local-path-provisioner" | grep -v "coredns")

NIRVATI_VERSION_DASHED=$(echo "${NIRVATI_VERSION}" | sed 's/\./-/g')

NIRVATI_IMAGES=$(cat <<EOF
harbor.nirvati.org/nirvati/init:${NIRVATI_VERSION}
harbor.nirvati.org/nirvati/agent:${NIRVATI_VERSION}
harbor.nirvati.org/nirvati/api:${NIRVATI_VERSION}
harbor.nirvati.org/nirvati/bg-updater:${NIRVATI_VERSION}
harbor.nirvati.org/nirvati/dns:${NIRVATI_VERSION}
harbor.nirvati.org/nirvati/migrations:${NIRVATI_VERSION}
harbor.nirvati.org/nirvati/dashboard:${NIRVATI_VERSION_DASHED}
harbor.nirvati.org/nirvati/app-data-loader:v0.1.0-alpha.13
EOF
)


download_yml_image_list() {
  local yml_url=$1
  local yml_image_list
  yml_image_list=$(curl -sL "${yml_url}" | grep "image: " | awk '{print $2}' | sort | uniq | sed "s/\"//g")
  echo "${yml_image_list}"
}

CERT_MANAGER_IMAGES=$(download_yml_image_list "https://gitlab.com/nirvati/apps/essentials/-/raw/stable/v1/cert-manager/custom.yml?ref_type=heads")
COREDNS_IMAGES=$(download_yml_image_list "https://gitlab.com/nirvati/apps/essentials/-/raw/stable/v1/coredns/custom.yml?ref_type=heads")
METALLB_IMAGES=$(download_yml_image_list "https://gitlab.com/nirvati/apps/essentials/-/raw/stable/v1/metallb/custom.yml?ref_type=heads")
SYSTEM_UPGRADE_CONTROLLER_IMAGES=$(download_yml_image_list "https://gitlab.com/nirvati/apps/essentials/-/raw/stable/v1/system-upgrade-controller/custom.yml?ref_type=heads")

LONGHORN_CUSTOM_YML=$(curl -sL "https://gitlab.com/nirvati/apps/essentials/-/raw/stable/v1/longhorn/custom.yml?ref_type=heads")
LONGHORN_IMAGES=$(echo "${LONGHORN_CUSTOM_YML}" | grep "image:" | awk '{print $2}' | sed "s/\"//g")
OTHER_LONGHORN_IMAGES=$(echo "${LONGHORN_CUSTOM_YML}" | grep "\- \"longhornio/" | sed "s/- \"//g" | sed "s/\"//g" | awk '{print $1}')
LONGHORN_IMAGES=$(printf "%s\n%s" "${LONGHORN_IMAGES}" "${OTHER_LONGHORN_IMAGES}" | sort | uniq | grep -v '^$')

echo "${K3S_IMAGE_LIST}"
echo
echo "${NIRVATI_IMAGES}"
echo
echo "${COREDNS_IMAGES}"
echo
echo "${CERT_MANAGER_IMAGES}"
echo
echo "${METALLB_IMAGES}"
echo
echo "${SYSTEM_UPGRADE_CONTROLLER_IMAGES}"
echo
echo "${LONGHORN_IMAGES}"
echo
# Hardcoded currently
echo "quay.io/cilium/cilium-envoy:v1.29.7-39a2a56bbd5b3a591f69dbca51d3e30ef97e0e51"
echo "quay.io/cilium/cilium:v1.16.1"
echo "quay.io/cilium/operator-generic:v1.16.1"
