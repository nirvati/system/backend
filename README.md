<!--
SPDX-FileCopyrightText: 2024 The Nirvati Developers

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Nirvati

Nirvati is a modern server management software built on top of Kubernetes and PostgreSQL.
This repository contains Nirvati's backend components, which are built in Rust.

The Nirvati frontend, which interacts with this repo's GraphQL API, is maintained [separately](https://gitlab.com/nirvati/system/dashboard) and built using [Nuxt](https://nuxt.com).

## Shared Components

- [Common](shared/common): Code shared between various components of Nirvati, could also be useful for plugin developers.
- [Apps Core](shared/apps): Types & shared code for our app system. Especially useful for plugin developers.
- [Database definitions](shared/db): Prisma client for our database system. Not intended to be used outside Nirvati.
- [Audit logs module](shared/audit-logs): Utils for Nirvati's audit logs (WIP)
- [SaaS module](shared/saas): Utils for SaaS products utilizing Nirvati (WIP)

## Services

- [Agent](./services/agent/): Microservice responsible for performing various administrative tasks, as well as parsing & installing apps and managing their domains. Only intended to be used by the GraphQL API.
- [GraphQL API](./services/api): Interface the dashboard uses to interact with Nirvati. Keeps track of system state, users and permissions in the database, and interacts with the various system components on behalf of the user.
- [Background updater](./services/background-updater/): Regularly downloads new apps added to the various app stores, and keeps the definitions for apps that aren't yet installed up-to-date.
- [Init](./services/init/): Container used to bootstrap new Nirvati installations on an existing Kubernetes cluster.
- [Migrations](./services/migrations): Performs post-update changes.

## Plugins

- [Bitcoin plugin](./plugins/bitcoin): Adds various Bitcoin-related features
- [LND plugin](./plugins/bitcoin): Adds various LND-related features
- [Tor plugin](./plugins/tor): Adds various Tor-related features
- [Tipi plugin](./plugins/tipi): Adds compatibility with [Tipi](https://runtipi.io) apps (WIP)
