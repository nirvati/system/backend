#!/usr/bin/env bash

NIRVATI_VERSION=$1
GITHUB_TOKEN=$2

if [ -z "${NIRVATI_VERSION}" ] || [ -z "${GITHUB_TOKEN}" ]; then
  echo "Usage: $0 <nirvati-version> <github-token>"
  exit 1
fi

if [ "${NIRVATI_VERSION}" == "main" ] || [ "${NIRVATI_VERSION}" == "master" ]; then
  exit 0
fi

curl -X POST -H "Authorization: token ${GITHUB_TOKEN}" -H "Accept: application/vnd.github.v3+json" https://api.github.com/repos/nirvati/releases/releases -d "{\"tag_name\":\"${NIRVATI_VERSION}\",\"name\":\"${NIRVATI_VERSION}\",\"body\":\"Nirvati ${NIRVATI_VERSION} release\",\"draft\":false,\"prerelease\":false}"
