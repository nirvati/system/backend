use nirvati_plugin_bitcoin::plugin::PluginState;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let plugin_state = PluginState {
        pw_seed: std::env::var("PW_SEED").unwrap(),
        btc_network: std::env::var("BTC_NETWORK").unwrap(),
        is_mock: std::env::var("IS_MOCK").unwrap_or_default() == "true",
    };
    let addr = "[::]:50051".parse().unwrap();
    tonic::transport::Server::builder()
        .add_service(
            nirvati_plugin_common::context_plugin_server::ContextPluginServer::new(plugin_state),
        )
        .serve(addr)
        .await
        .unwrap();
}
