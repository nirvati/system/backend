use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct BitcoinRpcAccount {
    pub name: String,
}

#[derive(Serialize, Deserialize)]
pub struct Settings {
    pub rpc_accounts: Vec<BitcoinRpcAccount>,
}

#[derive(Serialize, Deserialize)]
pub struct Context {
    pub network: String,
    pub account_passwords: HashMap<String, String>,
}
