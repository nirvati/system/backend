use crate::types::{Context, Settings};
use nirvati_plugin_common::context_plugin_server::ContextPlugin;
use nirvati_plugin_common::{
    AppContext, GetContextRequest, RegisterAppRequest, RegisterAppResponse, UnregisterAppRequest,
    UnregisterAppResponse,
};
use slugify::slugify;
use std::collections::HashMap;
use std::path::Path;
use tonic::{Request, Response, Status};

pub struct PluginState {
    pub pw_seed: String,
    pub btc_network: String,
    pub is_mock: bool,
}

impl PluginState {
    fn read_accounts(&self) -> Result<HashMap<String, String>, Status> {
        if self.is_mock {
            panic!("This function should not be called in mock mode");
        }
        let accounts_file = Path::new("/data/accounts.json");
        let current_accounts: HashMap<String, String> = if accounts_file.exists() {
            let mut accounts_file = std::fs::File::open(accounts_file)
                .map_err(|e| Status::internal(format!("Failed to open accounts file: {}", e)))?;
            serde_json::from_reader(&mut accounts_file)
                .map_err(|e| Status::internal(format!("Failed to read accounts file: {}", e)))?
        } else {
            HashMap::new()
        };
        Ok(current_accounts)
    }
}

#[tonic::async_trait]
impl ContextPlugin for PluginState {
    async fn register_app(
        &self,
        request: Request<RegisterAppRequest>,
    ) -> Result<Response<RegisterAppResponse>, Status> {
        let req = request.into_inner();
        let config: Settings = serde_json::from_str(&req.additional_data).map_err(|e| {
            tracing::debug!(
                "Failed to parse additional_data {}: {}",
                &req.additional_data,
                e
            );
            Status::invalid_argument(format!("Failed to parse additional_data: {}", e))
        })?;
        let app_id = req.app_id.clone();
        let user_name = req.user_state.unwrap().user_id.clone();
        let id_prefix = format!("{}-{}", app_id, user_name);
        let accounts: HashMap<String, String> = config
            .rpc_accounts
            .into_iter()
            .map(|account| {
                let pw = nirvati::utils::derive_entropy(
                    &format!("{}-{}", id_prefix, slugify!(&account.name)),
                    &self.pw_seed,
                );
                (format!("{}-{}", &id_prefix, slugify!(&account.name)), pw)
            })
            .collect();
        if !self.is_mock {
            let mut current_accounts = self.read_accounts()?;
            current_accounts.retain(|k, _| !k.starts_with(&(id_prefix.clone() + "-")));
            current_accounts.extend(accounts);
            let accounts_file = Path::new("/data/accounts.json");
            let mut accounts_file = std::fs::File::create(accounts_file)
                .map_err(|e| Status::internal(format!("Failed to create accounts file: {}", e)))?;
            serde_json::to_writer(&mut accounts_file, &current_accounts)
                .map_err(|e| Status::internal(format!("Failed to write accounts file: {}", e)))?;
        }
        Ok(Response::new(RegisterAppResponse {}))
    }

    async fn unregister_app(
        &self,
        request: Request<UnregisterAppRequest>,
    ) -> Result<Response<UnregisterAppResponse>, Status> {
        let req = request.into_inner();
        let app_id = req.app_id.clone();
        let user_name = req.user_state.unwrap().user_id.clone();
        let id_prefix = format!("{}-{}", app_id, user_name);
        if !self.is_mock {
            let mut current_accounts = self.read_accounts()?;
            current_accounts.retain(|k, _| !k.starts_with(&(id_prefix.clone() + "-")));
            let accounts_file = Path::new("/data/accounts.json");
            let mut accounts_file = std::fs::File::create(accounts_file)
                .map_err(|e| Status::internal(format!("Failed to create accounts file: {}", e)))?;
            serde_json::to_writer(&mut accounts_file, &current_accounts)
                .map_err(|e| Status::internal(format!("Failed to write accounts file: {}", e)))?;
        }
        Ok(Response::new(UnregisterAppResponse {}))
    }

    async fn get_context(
        &self,
        request: Request<GetContextRequest>,
    ) -> Result<Response<AppContext>, Status> {
        let req = request.into_inner();
        let config: Settings = serde_json::from_str(&req.additional_data).map_err(|e| {
            Status::invalid_argument(format!("Failed to parse additional_data: {}", e))
        })?;
        let app_id = req.app_id.clone();
        let user_name = req.user_state.unwrap().user_id.clone();
        let id_prefix = format!("{}-{}", app_id, user_name);
        let accounts: HashMap<String, String> = config
            .rpc_accounts
            .into_iter()
            .map(|account| {
                let pw = nirvati::utils::derive_entropy(
                    &format!("{}-{}", id_prefix, slugify!(&account.name)),
                    &self.pw_seed,
                );
                (account.name, pw)
            })
            .collect();
        let context = Context {
            network: self.btc_network.clone(),
            account_passwords: accounts,
        };
        Ok(Response::new(AppContext {
            context: serde_json::to_string(&context)
                .map_err(|e| Status::internal(format!("Failed to serialize context: {}", e)))?,
        }))
    }
}
