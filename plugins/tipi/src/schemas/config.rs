use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub name: String,
    pub available: bool,
    pub port: u16,
    pub id: String,
    pub tipi_version: u32,
    pub version: String,
    pub categories: Vec<String>,
    pub description: String,
    pub short_desc: String,
    pub author: String,
    pub source: String,
    #[serde(default)]
    pub form_fields: Vec<FormField>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub deprecated: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub exposable: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub no_gui: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub https: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub uid: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub gid: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub force_expose: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub generate_vapid_keys: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub requirements: Option<Requirements>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub supported_architectures: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url_suffix: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub min_tipi_version: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dynamic_config: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created_at: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub updated_at: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub website: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Requirements {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ports: Option<Vec<u16>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct FormField {
    #[serde(rename = "type")]
    pub field_type: String,
    pub label: String,
    pub env_variable: String,
    pub placeholder: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub max: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub min: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub required: Option<bool>,
}
