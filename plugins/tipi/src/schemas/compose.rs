#![allow(clippy::redundant_closure_call)]
#![allow(clippy::needless_lifetimes)]
#![allow(clippy::match_single_binding)]
#![allow(clippy::clone_on_copy)]

pub mod command;

use nirvati_apps::utils::{StringLike, StringOrNumber};
use serde::{Deserialize, Serialize};

#[doc = r" Error types."]
pub mod error {
    #[doc = r" Error from a TryFrom or FromStr implementation."]
    pub struct ConversionError(std::borrow::Cow<'static, str>);
    impl std::error::Error for ConversionError {}
    impl std::fmt::Display for ConversionError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
            std::fmt::Display::fmt(&self.0, f)
        }
    }
    impl std::fmt::Debug for ConversionError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
            std::fmt::Debug::fmt(&self.0, f)
        }
    }
    impl From<&'static str> for ConversionError {
        fn from(value: &'static str) -> Self {
            Self(value.into())
        }
    }
    impl From<String> for ConversionError {
        fn from(value: String) -> Self {
            Self(value.into())
        }
    }
}
#[doc = "Compose"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"title\": \"Compose\","]
#[doc = "  \"type\": \"object\","]
#[doc = "  \"required\": ["]
#[doc = "    \"$schema\","]
#[doc = "    \"services\""]
#[doc = "  ],"]
#[doc = "  \"properties\": {"]
#[doc = "    \"$schema\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"services\": {"]
#[doc = "      \"type\": \"array\","]
#[doc = "      \"items\": {"]
#[doc = "        \"type\": \"object\","]
#[doc = "        \"required\": ["]
#[doc = "          \"image\","]
#[doc = "          \"name\""]
#[doc = "        ],"]
#[doc = "        \"properties\": {"]
#[doc = "          \"addPorts\": {"]
#[doc = "            \"type\": \"array\","]
#[doc = "            \"items\": {"]
#[doc = "              \"type\": \"object\","]
#[doc = "              \"required\": ["]
#[doc = "                \"containerPort\","]
#[doc = "                \"hostPort\""]
#[doc = "              ],"]
#[doc = "              \"properties\": {"]
#[doc = "                \"containerPort\": {"]
#[doc = "                  \"type\": \"number\""]
#[doc = "                },"]
#[doc = "                \"hostPort\": {"]
#[doc = "                  \"type\": \"number\""]
#[doc = "                },"]
#[doc = "                \"interface\": {"]
#[doc = "                  \"type\": \"string\""]
#[doc = "                },"]
#[doc = "                \"tcp\": {"]
#[doc = "                  \"type\": \"boolean\""]
#[doc = "                },"]
#[doc = "                \"udp\": {"]
#[doc = "                  \"type\": \"boolean\""]
#[doc = "                }"]
#[doc = "              },"]
#[doc = "              \"additionalProperties\": false"]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"capAdd\": {"]
#[doc = "            \"type\": \"array\","]
#[doc = "            \"items\": {"]
#[doc = "              \"type\": \"string\""]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"capDrop\": {"]
#[doc = "            \"type\": \"array\","]
#[doc = "            \"items\": {"]
#[doc = "              \"type\": \"string\""]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"command\": {"]
#[doc = "            \"anyOf\": ["]
#[doc = "              {"]
#[doc = "                \"anyOf\": ["]
#[doc = "                  {"]
#[doc = "                    \"type\": \"string\""]
#[doc = "                  }"]
#[doc = "                ]"]
#[doc = "              },"]
#[doc = "              {"]
#[doc = "                \"anyOf\": ["]
#[doc = "                  {"]
#[doc = "                    \"type\": \"array\","]
#[doc = "                    \"items\": {"]
#[doc = "                      \"type\": \"string\""]
#[doc = "                    }"]
#[doc = "                  }"]
#[doc = "                ]"]
#[doc = "              }"]
#[doc = "            ]"]
#[doc = "          },"]
#[doc = "          \"dependsOn\": {"]
#[doc = "            \"anyOf\": ["]
#[doc = "              {"]
#[doc = "                \"type\": \"array\","]
#[doc = "                \"items\": {"]
#[doc = "                  \"type\": \"string\""]
#[doc = "                }"]
#[doc = "              },"]
#[doc = "              {"]
#[doc = "                \"type\": \"object\","]
#[doc = "                \"additionalProperties\": {"]
#[doc = "                  \"type\": \"object\","]
#[doc = "                  \"required\": ["]
#[doc = "                    \"condition\""]
#[doc = "                  ],"]
#[doc = "                  \"properties\": {"]
#[doc = "                    \"condition\": {"]
#[doc = "                      \"type\": \"string\","]
#[doc = "                      \"enum\": ["]
#[doc = "                        \"service_healthy\","]
#[doc = "                        \"service_started\","]
#[doc = "                        \"service_completed_successfully\""]
#[doc = "                      ]"]
#[doc = "                    }"]
#[doc = "                  },"]
#[doc = "                  \"additionalProperties\": false"]
#[doc = "                }"]
#[doc = "              }"]
#[doc = "            ]"]
#[doc = "          },"]
#[doc = "          \"deploy\": {"]
#[doc = "            \"type\": \"object\","]
#[doc = "            \"required\": ["]
#[doc = "              \"resources\""]
#[doc = "            ],"]
#[doc = "            \"properties\": {"]
#[doc = "              \"resources\": {"]
#[doc = "                \"type\": \"object\","]
#[doc = "                \"required\": ["]
#[doc = "                  \"limits\","]
#[doc = "                  \"reservations\""]
#[doc = "                ],"]
#[doc = "                \"properties\": {"]
#[doc = "                  \"limits\": {"]
#[doc = "                    \"type\": \"object\","]
#[doc = "                    \"properties\": {"]
#[doc = "                      \"cpus\": {"]
#[doc = "                        \"type\": \"string\""]
#[doc = "                      },"]
#[doc = "                      \"memory\": {"]
#[doc = "                        \"type\": \"string\""]
#[doc = "                      },"]
#[doc = "                      \"pids\": {"]
#[doc = "                        \"type\": \"number\""]
#[doc = "                      }"]
#[doc = "                    },"]
#[doc = "                    \"additionalProperties\": false"]
#[doc = "                  },"]
#[doc = "                  \"reservations\": {"]
#[doc = "                    \"type\": \"object\","]
#[doc = "                    \"required\": ["]
#[doc = "                      \"devices\""]
#[doc = "                    ],"]
#[doc = "                    \"properties\": {"]
#[doc = "                      \"cpus\": {"]
#[doc = "                        \"type\": \"string\""]
#[doc = "                      },"]
#[doc = "                      \"devices\": {"]
#[doc = "                        \"type\": \"array\","]
#[doc = "                        \"items\": {"]
#[doc = "                          \"type\": \"object\","]
#[doc = "                          \"required\": ["]
#[doc = "                            \"capabilities\""]
#[doc = "                          ],"]
#[doc = "                          \"properties\": {"]
#[doc = "                            \"capabilities\": {"]
#[doc = "                              \"type\": \"array\","]
#[doc = "                              \"items\": {"]
#[doc = "                                \"type\": \"string\""]
#[doc = "                              }"]
#[doc = "                            },"]
#[doc = "                            \"count\": {"]
#[doc = "                              \"anyOf\": ["]
#[doc = "                                {"]
#[doc = "                                  \"type\": \"string\","]
#[doc = "                                  \"enum\": ["]
#[doc = "                                    \"all\""]
#[doc = "                                  ]"]
#[doc = "                                },"]
#[doc = "                                {"]
#[doc = "                                  \"type\": \"number\""]
#[doc = "                                }"]
#[doc = "                              ]"]
#[doc = "                            },"]
#[doc = "                            \"deviceIds\": {"]
#[doc = "                              \"type\": \"array\","]
#[doc = "                              \"items\": {"]
#[doc = "                                \"type\": \"string\""]
#[doc = "                              }"]
#[doc = "                            },"]
#[doc = "                            \"driver\": {"]
#[doc = "                              \"type\": \"string\""]
#[doc = "                            }"]
#[doc = "                          },"]
#[doc = "                          \"additionalProperties\": false"]
#[doc = "                        }"]
#[doc = "                      },"]
#[doc = "                      \"memory\": {"]
#[doc = "                        \"type\": \"string\""]
#[doc = "                      }"]
#[doc = "                    },"]
#[doc = "                    \"additionalProperties\": false"]
#[doc = "                  }"]
#[doc = "                },"]
#[doc = "                \"additionalProperties\": false"]
#[doc = "              }"]
#[doc = "            },"]
#[doc = "            \"additionalProperties\": false"]
#[doc = "          },"]
#[doc = "          \"devices\": {"]
#[doc = "            \"type\": \"array\","]
#[doc = "            \"items\": {"]
#[doc = "              \"type\": \"string\""]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"entrypoint\": {"]
#[doc = "            \"anyOf\": ["]
#[doc = "              {"]
#[doc = "                \"type\": \"string\""]
#[doc = "              },"]
#[doc = "              {"]
#[doc = "                \"type\": \"array\","]
#[doc = "                \"items\": {"]
#[doc = "                  \"type\": \"string\""]
#[doc = "                }"]
#[doc = "              }"]
#[doc = "            ]"]
#[doc = "          },"]
#[doc = "          \"environment\": {"]
#[doc = "            \"type\": \"object\","]
#[doc = "            \"additionalProperties\": {"]
#[doc = "              \"type\": ["]
#[doc = "                \"string\","]
#[doc = "                \"number\""]
#[doc = "              ]"]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"extraHosts\": {"]
#[doc = "            \"type\": \"array\","]
#[doc = "            \"items\": {"]
#[doc = "              \"type\": \"string\""]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"healthCheck\": {"]
#[doc = "            \"type\": \"object\","]
#[doc = "            \"required\": ["]
#[doc = "              \"test\""]
#[doc = "            ],"]
#[doc = "            \"properties\": {"]
#[doc = "              \"interval\": {"]
#[doc = "                \"type\": \"string\""]
#[doc = "              },"]
#[doc = "              \"retries\": {"]
#[doc = "                \"type\": \"number\""]
#[doc = "              },"]
#[doc = "              \"startInterval\": {"]
#[doc = "                \"type\": \"string\""]
#[doc = "              },"]
#[doc = "              \"startPeriod\": {"]
#[doc = "                \"type\": \"string\""]
#[doc = "              },"]
#[doc = "              \"test\": {"]
#[doc = "                \"type\": \"string\""]
#[doc = "              },"]
#[doc = "              \"timeout\": {"]
#[doc = "                \"type\": \"string\""]
#[doc = "              }"]
#[doc = "            },"]
#[doc = "            \"additionalProperties\": false"]
#[doc = "          },"]
#[doc = "          \"hostname\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          },"]
#[doc = "          \"image\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          },"]
#[doc = "          \"internalPort\": {"]
#[doc = "            \"type\": \"number\""]
#[doc = "          },"]
#[doc = "          \"isMain\": {"]
#[doc = "            \"type\": \"boolean\""]
#[doc = "          },"]
#[doc = "          \"logging\": {"]
#[doc = "            \"type\": \"object\","]
#[doc = "            \"required\": ["]
#[doc = "              \"driver\","]
#[doc = "              \"options\""]
#[doc = "            ],"]
#[doc = "            \"properties\": {"]
#[doc = "              \"driver\": {"]
#[doc = "                \"type\": \"string\""]
#[doc = "              },"]
#[doc = "              \"options\": {"]
#[doc = "                \"type\": \"object\","]
#[doc = "                \"additionalProperties\": {"]
#[doc = "                  \"type\": \"string\""]
#[doc = "                }"]
#[doc = "              }"]
#[doc = "            },"]
#[doc = "            \"additionalProperties\": false"]
#[doc = "          },"]
#[doc = "          \"name\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          },"]
#[doc = "          \"networkMode\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          },"]
#[doc = "          \"pid\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          },"]
#[doc = "          \"privileged\": {"]
#[doc = "            \"type\": \"boolean\""]
#[doc = "          },"]
#[doc = "          \"readOnly\": {"]
#[doc = "            \"type\": \"boolean\""]
#[doc = "          },"]
#[doc = "          \"securityOpt\": {"]
#[doc = "            \"type\": \"array\","]
#[doc = "            \"items\": {"]
#[doc = "              \"type\": \"string\""]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"shmSize\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          },"]
#[doc = "          \"stdinOpen\": {"]
#[doc = "            \"type\": \"boolean\""]
#[doc = "          },"]
#[doc = "          \"stopGracePeriod\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          },"]
#[doc = "          \"stopSignal\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          },"]
#[doc = "          \"tty\": {"]
#[doc = "            \"type\": \"boolean\""]
#[doc = "          },"]
#[doc = "          \"ulimits\": {"]
#[doc = "            \"type\": \"object\","]
#[doc = "            \"required\": ["]
#[doc = "              \"nofile\","]
#[doc = "              \"nproc\""]
#[doc = "            ],"]
#[doc = "            \"properties\": {"]
#[doc = "              \"nofile\": {"]
#[doc = "                \"anyOf\": ["]
#[doc = "                  {"]
#[doc = "                    \"type\": \"number\""]
#[doc = "                  },"]
#[doc = "                  {"]
#[doc = "                    \"type\": \"object\","]
#[doc = "                    \"required\": ["]
#[doc = "                      \"hard\","]
#[doc = "                      \"soft\""]
#[doc = "                    ],"]
#[doc = "                    \"properties\": {"]
#[doc = "                      \"hard\": {"]
#[doc = "                        \"type\": \"number\""]
#[doc = "                      },"]
#[doc = "                      \"soft\": {"]
#[doc = "                        \"type\": \"number\""]
#[doc = "                      }"]
#[doc = "                    },"]
#[doc = "                    \"additionalProperties\": false"]
#[doc = "                  }"]
#[doc = "                ]"]
#[doc = "              },"]
#[doc = "              \"nproc\": {"]
#[doc = "                \"anyOf\": ["]
#[doc = "                  {"]
#[doc = "                    \"type\": \"number\""]
#[doc = "                  },"]
#[doc = "                  {"]
#[doc = "                    \"type\": \"object\","]
#[doc = "                    \"required\": ["]
#[doc = "                      \"hard\","]
#[doc = "                      \"soft\""]
#[doc = "                    ],"]
#[doc = "                    \"properties\": {"]
#[doc = "                      \"hard\": {"]
#[doc = "                        \"type\": \"number\""]
#[doc = "                      },"]
#[doc = "                      \"soft\": {"]
#[doc = "                        \"type\": \"number\""]
#[doc = "                      }"]
#[doc = "                    },"]
#[doc = "                    \"additionalProperties\": false"]
#[doc = "                  }"]
#[doc = "                ]"]
#[doc = "              }"]
#[doc = "            },"]
#[doc = "            \"additionalProperties\": false"]
#[doc = "          },"]
#[doc = "          \"user\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          },"]
#[doc = "          \"volumes\": {"]
#[doc = "            \"type\": \"array\","]
#[doc = "            \"items\": {"]
#[doc = "              \"type\": \"object\","]
#[doc = "              \"required\": ["]
#[doc = "                \"containerPath\","]
#[doc = "                \"hostPath\""]
#[doc = "              ],"]
#[doc = "              \"properties\": {"]
#[doc = "                \"containerPath\": {"]
#[doc = "                  \"type\": \"string\""]
#[doc = "                },"]
#[doc = "                \"hostPath\": {"]
#[doc = "                  \"type\": \"string\""]
#[doc = "                },"]
#[doc = "                \"readOnly\": {"]
#[doc = "                  \"type\": \"boolean\""]
#[doc = "                }"]
#[doc = "              },"]
#[doc = "              \"additionalProperties\": false"]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"workingDir\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          }"]
#[doc = "        },"]
#[doc = "        \"additionalProperties\": false"]
#[doc = "      }"]
#[doc = "    }"]
#[doc = "  },"]
#[doc = "  \"additionalProperties\": false"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct Compose {
    #[serde(rename = "$schema")]
    pub schema: String,
    pub services: Vec<ComposeServicesItem>,
}
impl From<&Compose> for Compose {
    fn from(value: &Compose) -> Self {
        value.clone()
    }
}
#[doc = "ComposeServicesItem"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"type\": \"object\","]
#[doc = "  \"required\": ["]
#[doc = "    \"image\","]
#[doc = "    \"name\""]
#[doc = "  ],"]
#[doc = "  \"properties\": {"]
#[doc = "    \"addPorts\": {"]
#[doc = "      \"type\": \"array\","]
#[doc = "      \"items\": {"]
#[doc = "        \"type\": \"object\","]
#[doc = "        \"required\": ["]
#[doc = "          \"containerPort\","]
#[doc = "          \"hostPort\""]
#[doc = "        ],"]
#[doc = "        \"properties\": {"]
#[doc = "          \"containerPort\": {"]
#[doc = "            \"type\": \"number\""]
#[doc = "          },"]
#[doc = "          \"hostPort\": {"]
#[doc = "            \"type\": \"number\""]
#[doc = "          },"]
#[doc = "          \"interface\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          },"]
#[doc = "          \"tcp\": {"]
#[doc = "            \"type\": \"boolean\""]
#[doc = "          },"]
#[doc = "          \"udp\": {"]
#[doc = "            \"type\": \"boolean\""]
#[doc = "          }"]
#[doc = "        },"]
#[doc = "        \"additionalProperties\": false"]
#[doc = "      }"]
#[doc = "    },"]
#[doc = "    \"capAdd\": {"]
#[doc = "      \"type\": \"array\","]
#[doc = "      \"items\": {"]
#[doc = "        \"type\": \"string\""]
#[doc = "      }"]
#[doc = "    },"]
#[doc = "    \"capDrop\": {"]
#[doc = "      \"type\": \"array\","]
#[doc = "      \"items\": {"]
#[doc = "        \"type\": \"string\""]
#[doc = "      }"]
#[doc = "    },"]
#[doc = "    \"command\": {"]
#[doc = "      \"anyOf\": ["]
#[doc = "        {"]
#[doc = "          \"anyOf\": ["]
#[doc = "            {"]
#[doc = "              \"type\": \"string\""]
#[doc = "            }"]
#[doc = "          ]"]
#[doc = "        },"]
#[doc = "        {"]
#[doc = "          \"anyOf\": ["]
#[doc = "            {"]
#[doc = "              \"type\": \"array\","]
#[doc = "              \"items\": {"]
#[doc = "                \"type\": \"string\""]
#[doc = "              }"]
#[doc = "            }"]
#[doc = "          ]"]
#[doc = "        }"]
#[doc = "      ]"]
#[doc = "    },"]
#[doc = "    \"dependsOn\": {"]
#[doc = "      \"anyOf\": ["]
#[doc = "        {"]
#[doc = "          \"type\": \"array\","]
#[doc = "          \"items\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          }"]
#[doc = "        },"]
#[doc = "        {"]
#[doc = "          \"type\": \"object\","]
#[doc = "          \"additionalProperties\": {"]
#[doc = "            \"type\": \"object\","]
#[doc = "            \"required\": ["]
#[doc = "              \"condition\""]
#[doc = "            ],"]
#[doc = "            \"properties\": {"]
#[doc = "              \"condition\": {"]
#[doc = "                \"type\": \"string\","]
#[doc = "                \"enum\": ["]
#[doc = "                  \"service_healthy\","]
#[doc = "                  \"service_started\","]
#[doc = "                  \"service_completed_successfully\""]
#[doc = "                ]"]
#[doc = "              }"]
#[doc = "            },"]
#[doc = "            \"additionalProperties\": false"]
#[doc = "          }"]
#[doc = "        }"]
#[doc = "      ]"]
#[doc = "    },"]
#[doc = "    \"deploy\": {"]
#[doc = "      \"type\": \"object\","]
#[doc = "      \"required\": ["]
#[doc = "        \"resources\""]
#[doc = "      ],"]
#[doc = "      \"properties\": {"]
#[doc = "        \"resources\": {"]
#[doc = "          \"type\": \"object\","]
#[doc = "          \"required\": ["]
#[doc = "            \"limits\","]
#[doc = "            \"reservations\""]
#[doc = "          ],"]
#[doc = "          \"properties\": {"]
#[doc = "            \"limits\": {"]
#[doc = "              \"type\": \"object\","]
#[doc = "              \"properties\": {"]
#[doc = "                \"cpus\": {"]
#[doc = "                  \"type\": \"string\""]
#[doc = "                },"]
#[doc = "                \"memory\": {"]
#[doc = "                  \"type\": \"string\""]
#[doc = "                },"]
#[doc = "                \"pids\": {"]
#[doc = "                  \"type\": \"number\""]
#[doc = "                }"]
#[doc = "              },"]
#[doc = "              \"additionalProperties\": false"]
#[doc = "            },"]
#[doc = "            \"reservations\": {"]
#[doc = "              \"type\": \"object\","]
#[doc = "              \"required\": ["]
#[doc = "                \"devices\""]
#[doc = "              ],"]
#[doc = "              \"properties\": {"]
#[doc = "                \"cpus\": {"]
#[doc = "                  \"type\": \"string\""]
#[doc = "                },"]
#[doc = "                \"devices\": {"]
#[doc = "                  \"type\": \"array\","]
#[doc = "                  \"items\": {"]
#[doc = "                    \"type\": \"object\","]
#[doc = "                    \"required\": ["]
#[doc = "                      \"capabilities\""]
#[doc = "                    ],"]
#[doc = "                    \"properties\": {"]
#[doc = "                      \"capabilities\": {"]
#[doc = "                        \"type\": \"array\","]
#[doc = "                        \"items\": {"]
#[doc = "                          \"type\": \"string\""]
#[doc = "                        }"]
#[doc = "                      },"]
#[doc = "                      \"count\": {"]
#[doc = "                        \"anyOf\": ["]
#[doc = "                          {"]
#[doc = "                            \"type\": \"string\","]
#[doc = "                            \"enum\": ["]
#[doc = "                              \"all\""]
#[doc = "                            ]"]
#[doc = "                          },"]
#[doc = "                          {"]
#[doc = "                            \"type\": \"number\""]
#[doc = "                          }"]
#[doc = "                        ]"]
#[doc = "                      },"]
#[doc = "                      \"deviceIds\": {"]
#[doc = "                        \"type\": \"array\","]
#[doc = "                        \"items\": {"]
#[doc = "                          \"type\": \"string\""]
#[doc = "                        }"]
#[doc = "                      },"]
#[doc = "                      \"driver\": {"]
#[doc = "                        \"type\": \"string\""]
#[doc = "                      }"]
#[doc = "                    },"]
#[doc = "                    \"additionalProperties\": false"]
#[doc = "                  }"]
#[doc = "                },"]
#[doc = "                \"memory\": {"]
#[doc = "                  \"type\": \"string\""]
#[doc = "                }"]
#[doc = "              },"]
#[doc = "              \"additionalProperties\": false"]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"additionalProperties\": false"]
#[doc = "        }"]
#[doc = "      },"]
#[doc = "      \"additionalProperties\": false"]
#[doc = "    },"]
#[doc = "    \"devices\": {"]
#[doc = "      \"type\": \"array\","]
#[doc = "      \"items\": {"]
#[doc = "        \"type\": \"string\""]
#[doc = "      }"]
#[doc = "    },"]
#[doc = "    \"entrypoint\": {"]
#[doc = "      \"anyOf\": ["]
#[doc = "        {"]
#[doc = "          \"type\": \"string\""]
#[doc = "        },"]
#[doc = "        {"]
#[doc = "          \"type\": \"array\","]
#[doc = "          \"items\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          }"]
#[doc = "        }"]
#[doc = "      ]"]
#[doc = "    },"]
#[doc = "    \"environment\": {"]
#[doc = "      \"type\": \"object\","]
#[doc = "      \"additionalProperties\": {"]
#[doc = "        \"type\": ["]
#[doc = "          \"string\","]
#[doc = "          \"number\""]
#[doc = "        ]"]
#[doc = "      }"]
#[doc = "    },"]
#[doc = "    \"extraHosts\": {"]
#[doc = "      \"type\": \"array\","]
#[doc = "      \"items\": {"]
#[doc = "        \"type\": \"string\""]
#[doc = "      }"]
#[doc = "    },"]
#[doc = "    \"healthCheck\": {"]
#[doc = "      \"type\": \"object\","]
#[doc = "      \"required\": ["]
#[doc = "        \"test\""]
#[doc = "      ],"]
#[doc = "      \"properties\": {"]
#[doc = "        \"interval\": {"]
#[doc = "          \"type\": \"string\""]
#[doc = "        },"]
#[doc = "        \"retries\": {"]
#[doc = "          \"type\": \"number\""]
#[doc = "        },"]
#[doc = "        \"startInterval\": {"]
#[doc = "          \"type\": \"string\""]
#[doc = "        },"]
#[doc = "        \"startPeriod\": {"]
#[doc = "          \"type\": \"string\""]
#[doc = "        },"]
#[doc = "        \"test\": {"]
#[doc = "          \"type\": \"string\""]
#[doc = "        },"]
#[doc = "        \"timeout\": {"]
#[doc = "          \"type\": \"string\""]
#[doc = "        }"]
#[doc = "      },"]
#[doc = "      \"additionalProperties\": false"]
#[doc = "    },"]
#[doc = "    \"hostname\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"image\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"internalPort\": {"]
#[doc = "      \"type\": \"number\""]
#[doc = "    },"]
#[doc = "    \"isMain\": {"]
#[doc = "      \"type\": \"boolean\""]
#[doc = "    },"]
#[doc = "    \"logging\": {"]
#[doc = "      \"type\": \"object\","]
#[doc = "      \"required\": ["]
#[doc = "        \"driver\","]
#[doc = "        \"options\""]
#[doc = "      ],"]
#[doc = "      \"properties\": {"]
#[doc = "        \"driver\": {"]
#[doc = "          \"type\": \"string\""]
#[doc = "        },"]
#[doc = "        \"options\": {"]
#[doc = "          \"type\": \"object\","]
#[doc = "          \"additionalProperties\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          }"]
#[doc = "        }"]
#[doc = "      },"]
#[doc = "      \"additionalProperties\": false"]
#[doc = "    },"]
#[doc = "    \"name\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"networkMode\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"pid\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"privileged\": {"]
#[doc = "      \"type\": \"boolean\""]
#[doc = "    },"]
#[doc = "    \"readOnly\": {"]
#[doc = "      \"type\": \"boolean\""]
#[doc = "    },"]
#[doc = "    \"securityOpt\": {"]
#[doc = "      \"type\": \"array\","]
#[doc = "      \"items\": {"]
#[doc = "        \"type\": \"string\""]
#[doc = "      }"]
#[doc = "    },"]
#[doc = "    \"shmSize\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"stdinOpen\": {"]
#[doc = "      \"type\": \"boolean\""]
#[doc = "    },"]
#[doc = "    \"stopGracePeriod\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"stopSignal\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"tty\": {"]
#[doc = "      \"type\": \"boolean\""]
#[doc = "    },"]
#[doc = "    \"ulimits\": {"]
#[doc = "      \"type\": \"object\","]
#[doc = "      \"required\": ["]
#[doc = "        \"nofile\","]
#[doc = "        \"nproc\""]
#[doc = "      ],"]
#[doc = "      \"properties\": {"]
#[doc = "        \"nofile\": {"]
#[doc = "          \"anyOf\": ["]
#[doc = "            {"]
#[doc = "              \"type\": \"number\""]
#[doc = "            },"]
#[doc = "            {"]
#[doc = "              \"type\": \"object\","]
#[doc = "              \"required\": ["]
#[doc = "                \"hard\","]
#[doc = "                \"soft\""]
#[doc = "              ],"]
#[doc = "              \"properties\": {"]
#[doc = "                \"hard\": {"]
#[doc = "                  \"type\": \"number\""]
#[doc = "                },"]
#[doc = "                \"soft\": {"]
#[doc = "                  \"type\": \"number\""]
#[doc = "                }"]
#[doc = "              },"]
#[doc = "              \"additionalProperties\": false"]
#[doc = "            }"]
#[doc = "          ]"]
#[doc = "        },"]
#[doc = "        \"nproc\": {"]
#[doc = "          \"anyOf\": ["]
#[doc = "            {"]
#[doc = "              \"type\": \"number\""]
#[doc = "            },"]
#[doc = "            {"]
#[doc = "              \"type\": \"object\","]
#[doc = "              \"required\": ["]
#[doc = "                \"hard\","]
#[doc = "                \"soft\""]
#[doc = "              ],"]
#[doc = "              \"properties\": {"]
#[doc = "                \"hard\": {"]
#[doc = "                  \"type\": \"number\""]
#[doc = "                },"]
#[doc = "                \"soft\": {"]
#[doc = "                  \"type\": \"number\""]
#[doc = "                }"]
#[doc = "              },"]
#[doc = "              \"additionalProperties\": false"]
#[doc = "            }"]
#[doc = "          ]"]
#[doc = "        }"]
#[doc = "      },"]
#[doc = "      \"additionalProperties\": false"]
#[doc = "    },"]
#[doc = "    \"user\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"volumes\": {"]
#[doc = "      \"type\": \"array\","]
#[doc = "      \"items\": {"]
#[doc = "        \"type\": \"object\","]
#[doc = "        \"required\": ["]
#[doc = "          \"containerPath\","]
#[doc = "          \"hostPath\""]
#[doc = "        ],"]
#[doc = "        \"properties\": {"]
#[doc = "          \"containerPath\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          },"]
#[doc = "          \"hostPath\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          },"]
#[doc = "          \"readOnly\": {"]
#[doc = "            \"type\": \"boolean\""]
#[doc = "          }"]
#[doc = "        },"]
#[doc = "        \"additionalProperties\": false"]
#[doc = "      }"]
#[doc = "    },"]
#[doc = "    \"workingDir\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    }"]
#[doc = "  },"]
#[doc = "  \"additionalProperties\": false"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ComposeServicesItem {
    #[serde(rename = "addPorts", default, skip_serializing_if = "Vec::is_empty")]
    pub add_ports: Vec<ComposeServicesItemAddPortsItem>,
    #[serde(rename = "capAdd", default, skip_serializing_if = "Vec::is_empty")]
    pub cap_add: Vec<String>,
    #[serde(rename = "capDrop", default, skip_serializing_if = "Vec::is_empty")]
    pub cap_drop: Vec<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub command: Option<command::Command>,
    #[serde(rename = "dependsOn", default, skip_serializing_if = "Option::is_none")]
    pub depends_on: Option<ComposeServicesItemDependsOn>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub deploy: Option<ComposeServicesItemDeploy>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub devices: Vec<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub entrypoint: Option<command::Command>,
    #[serde(default, skip_serializing_if = "std::collections::HashMap::is_empty")]
    pub environment: std::collections::HashMap<String, StringLike>,
    #[serde(rename = "extraHosts", default, skip_serializing_if = "Vec::is_empty")]
    pub extra_hosts: Vec<String>,
    #[serde(
        rename = "healthCheck",
        default,
        skip_serializing_if = "Option::is_none"
    )]
    pub health_check: Option<ComposeServicesItemHealthCheck>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub hostname: Option<String>,
    pub image: String,
    #[serde(
        rename = "internalPort",
        default,
        skip_serializing_if = "Option::is_none"
    )]
    pub internal_port: Option<u16>,
    #[serde(rename = "isMain", default, skip_serializing_if = "Option::is_none")]
    pub is_main: Option<bool>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub logging: Option<ComposeServicesItemLogging>,
    pub name: String,
    #[serde(
        rename = "networkMode",
        default,
        skip_serializing_if = "Option::is_none"
    )]
    pub network_mode: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub pid: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub privileged: Option<bool>,
    #[serde(rename = "readOnly", default, skip_serializing_if = "Option::is_none")]
    pub read_only: Option<bool>,
    #[serde(rename = "securityOpt", default, skip_serializing_if = "Vec::is_empty")]
    pub security_opt: Vec<String>,
    #[serde(rename = "shmSize", default, skip_serializing_if = "Option::is_none")]
    pub shm_size: Option<StringOrNumber>,
    #[serde(rename = "stdinOpen", default, skip_serializing_if = "Option::is_none")]
    pub stdin_open: Option<bool>,
    #[serde(
        rename = "stopGracePeriod",
        default,
        skip_serializing_if = "Option::is_none"
    )]
    pub stop_grace_period: Option<String>,
    #[serde(
        rename = "stopSignal",
        default,
        skip_serializing_if = "Option::is_none"
    )]
    pub stop_signal: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub tty: Option<bool>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub ulimits: Option<ComposeServicesItemUlimits>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub user: Option<String>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub volumes: Vec<ComposeServicesItemVolumesItem>,
    #[serde(
        rename = "workingDir",
        default,
        skip_serializing_if = "Option::is_none"
    )]
    pub working_dir: Option<String>,

    // Ignored properties
    pub restart: Option<String>,
}
impl From<&ComposeServicesItem> for ComposeServicesItem {
    fn from(value: &ComposeServicesItem) -> Self {
        value.clone()
    }
}
#[doc = "ComposeServicesItemAddPortsItem"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"type\": \"object\","]
#[doc = "  \"required\": ["]
#[doc = "    \"containerPort\","]
#[doc = "    \"hostPort\""]
#[doc = "  ],"]
#[doc = "  \"properties\": {"]
#[doc = "    \"containerPort\": {"]
#[doc = "      \"type\": \"number\""]
#[doc = "    },"]
#[doc = "    \"hostPort\": {"]
#[doc = "      \"type\": \"number\""]
#[doc = "    },"]
#[doc = "    \"interface\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"tcp\": {"]
#[doc = "      \"type\": \"boolean\""]
#[doc = "    },"]
#[doc = "    \"udp\": {"]
#[doc = "      \"type\": \"boolean\""]
#[doc = "    }"]
#[doc = "  },"]
#[doc = "  \"additionalProperties\": false"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ComposeServicesItemAddPortsItem {
    #[serde(rename = "containerPort")]
    pub container_port: u16,
    #[serde(rename = "hostPort")]
    pub host_port: u16,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub interface: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub tcp: Option<bool>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub udp: Option<bool>,
}
impl From<&ComposeServicesItemAddPortsItem> for ComposeServicesItemAddPortsItem {
    fn from(value: &ComposeServicesItemAddPortsItem) -> Self {
        value.clone()
    }
}
#[doc = "ComposeServicesItemCommand"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"anyOf\": ["]
#[doc = "    {"]
#[doc = "      \"anyOf\": ["]
#[doc = "        {"]
#[doc = "          \"type\": \"string\""]
#[doc = "        }"]
#[doc = "      ]"]
#[doc = "    },"]
#[doc = "    {"]
#[doc = "      \"anyOf\": ["]
#[doc = "        {"]
#[doc = "          \"type\": \"array\","]
#[doc = "          \"items\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          }"]
#[doc = "        }"]
#[doc = "      ]"]
#[doc = "    }"]
#[doc = "  ]"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum ComposeServicesItemCommand {
    Variant0(String),
    Variant1(Vec<String>),
}
impl From<&ComposeServicesItemCommand> for ComposeServicesItemCommand {
    fn from(value: &ComposeServicesItemCommand) -> Self {
        value.clone()
    }
}
impl From<Vec<String>> for ComposeServicesItemCommand {
    fn from(value: Vec<String>) -> Self {
        Self::Variant1(value)
    }
}
#[doc = "ComposeServicesItemDependsOn"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"anyOf\": ["]
#[doc = "    {"]
#[doc = "      \"type\": \"array\","]
#[doc = "      \"items\": {"]
#[doc = "        \"type\": \"string\""]
#[doc = "      }"]
#[doc = "    },"]
#[doc = "    {"]
#[doc = "      \"type\": \"object\","]
#[doc = "      \"additionalProperties\": {"]
#[doc = "        \"type\": \"object\","]
#[doc = "        \"required\": ["]
#[doc = "          \"condition\""]
#[doc = "        ],"]
#[doc = "        \"properties\": {"]
#[doc = "          \"condition\": {"]
#[doc = "            \"type\": \"string\","]
#[doc = "            \"enum\": ["]
#[doc = "              \"service_healthy\","]
#[doc = "              \"service_started\","]
#[doc = "              \"service_completed_successfully\""]
#[doc = "            ]"]
#[doc = "          }"]
#[doc = "        },"]
#[doc = "        \"additionalProperties\": false"]
#[doc = "      }"]
#[doc = "    }"]
#[doc = "  ]"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum ComposeServicesItemDependsOn {
    Variant0(Vec<String>),
    Variant1(std::collections::HashMap<String, ComposeServicesItemDependsOnVariant1Value>),
}
impl From<&ComposeServicesItemDependsOn> for ComposeServicesItemDependsOn {
    fn from(value: &ComposeServicesItemDependsOn) -> Self {
        value.clone()
    }
}
impl From<Vec<String>> for ComposeServicesItemDependsOn {
    fn from(value: Vec<String>) -> Self {
        Self::Variant0(value)
    }
}
impl From<std::collections::HashMap<String, ComposeServicesItemDependsOnVariant1Value>>
    for ComposeServicesItemDependsOn
{
    fn from(
        value: std::collections::HashMap<String, ComposeServicesItemDependsOnVariant1Value>,
    ) -> Self {
        Self::Variant1(value)
    }
}
#[doc = "ComposeServicesItemDependsOnVariant1Value"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"type\": \"object\","]
#[doc = "  \"required\": ["]
#[doc = "    \"condition\""]
#[doc = "  ],"]
#[doc = "  \"properties\": {"]
#[doc = "    \"condition\": {"]
#[doc = "      \"type\": \"string\","]
#[doc = "      \"enum\": ["]
#[doc = "        \"service_healthy\","]
#[doc = "        \"service_started\","]
#[doc = "        \"service_completed_successfully\""]
#[doc = "      ]"]
#[doc = "    }"]
#[doc = "  },"]
#[doc = "  \"additionalProperties\": false"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ComposeServicesItemDependsOnVariant1Value {
    pub condition: ComposeServicesItemDependsOnVariant1ValueCondition,
}
impl From<&ComposeServicesItemDependsOnVariant1Value>
    for ComposeServicesItemDependsOnVariant1Value
{
    fn from(value: &ComposeServicesItemDependsOnVariant1Value) -> Self {
        value.clone()
    }
}
#[doc = "ComposeServicesItemDependsOnVariant1ValueCondition"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"type\": \"string\","]
#[doc = "  \"enum\": ["]
#[doc = "    \"service_healthy\","]
#[doc = "    \"service_started\","]
#[doc = "    \"service_completed_successfully\""]
#[doc = "  ]"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, Ord, PartialEq, PartialOrd, Serialize)]
pub enum ComposeServicesItemDependsOnVariant1ValueCondition {
    #[serde(rename = "service_healthy")]
    ServiceHealthy,
    #[serde(rename = "service_started")]
    ServiceStarted,
    #[serde(rename = "service_completed_successfully")]
    ServiceCompletedSuccessfully,
}
impl From<&ComposeServicesItemDependsOnVariant1ValueCondition>
    for ComposeServicesItemDependsOnVariant1ValueCondition
{
    fn from(value: &ComposeServicesItemDependsOnVariant1ValueCondition) -> Self {
        value.clone()
    }
}
impl ToString for ComposeServicesItemDependsOnVariant1ValueCondition {
    fn to_string(&self) -> String {
        match *self {
            Self::ServiceHealthy => "service_healthy".to_string(),
            Self::ServiceStarted => "service_started".to_string(),
            Self::ServiceCompletedSuccessfully => "service_completed_successfully".to_string(),
        }
    }
}
impl std::str::FromStr for ComposeServicesItemDependsOnVariant1ValueCondition {
    type Err = self::error::ConversionError;
    fn from_str(value: &str) -> Result<Self, self::error::ConversionError> {
        match value {
            "service_healthy" => Ok(Self::ServiceHealthy),
            "service_started" => Ok(Self::ServiceStarted),
            "service_completed_successfully" => Ok(Self::ServiceCompletedSuccessfully),
            _ => Err("invalid value".into()),
        }
    }
}
impl std::convert::TryFrom<&str> for ComposeServicesItemDependsOnVariant1ValueCondition {
    type Error = self::error::ConversionError;
    fn try_from(value: &str) -> Result<Self, self::error::ConversionError> {
        value.parse()
    }
}
impl std::convert::TryFrom<&String> for ComposeServicesItemDependsOnVariant1ValueCondition {
    type Error = self::error::ConversionError;
    fn try_from(value: &String) -> Result<Self, self::error::ConversionError> {
        value.parse()
    }
}
impl std::convert::TryFrom<String> for ComposeServicesItemDependsOnVariant1ValueCondition {
    type Error = self::error::ConversionError;
    fn try_from(value: String) -> Result<Self, self::error::ConversionError> {
        value.parse()
    }
}
#[doc = "ComposeServicesItemDeploy"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"type\": \"object\","]
#[doc = "  \"required\": ["]
#[doc = "    \"resources\""]
#[doc = "  ],"]
#[doc = "  \"properties\": {"]
#[doc = "    \"resources\": {"]
#[doc = "      \"type\": \"object\","]
#[doc = "      \"required\": ["]
#[doc = "        \"limits\","]
#[doc = "        \"reservations\""]
#[doc = "      ],"]
#[doc = "      \"properties\": {"]
#[doc = "        \"limits\": {"]
#[doc = "          \"type\": \"object\","]
#[doc = "          \"properties\": {"]
#[doc = "            \"cpus\": {"]
#[doc = "              \"type\": \"string\""]
#[doc = "            },"]
#[doc = "            \"memory\": {"]
#[doc = "              \"type\": \"string\""]
#[doc = "            },"]
#[doc = "            \"pids\": {"]
#[doc = "              \"type\": \"number\""]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"additionalProperties\": false"]
#[doc = "        },"]
#[doc = "        \"reservations\": {"]
#[doc = "          \"type\": \"object\","]
#[doc = "          \"required\": ["]
#[doc = "            \"devices\""]
#[doc = "          ],"]
#[doc = "          \"properties\": {"]
#[doc = "            \"cpus\": {"]
#[doc = "              \"type\": \"string\""]
#[doc = "            },"]
#[doc = "            \"devices\": {"]
#[doc = "              \"type\": \"array\","]
#[doc = "              \"items\": {"]
#[doc = "                \"type\": \"object\","]
#[doc = "                \"required\": ["]
#[doc = "                  \"capabilities\""]
#[doc = "                ],"]
#[doc = "                \"properties\": {"]
#[doc = "                  \"capabilities\": {"]
#[doc = "                    \"type\": \"array\","]
#[doc = "                    \"items\": {"]
#[doc = "                      \"type\": \"string\""]
#[doc = "                    }"]
#[doc = "                  },"]
#[doc = "                  \"count\": {"]
#[doc = "                    \"anyOf\": ["]
#[doc = "                      {"]
#[doc = "                        \"type\": \"string\","]
#[doc = "                        \"enum\": ["]
#[doc = "                          \"all\""]
#[doc = "                        ]"]
#[doc = "                      },"]
#[doc = "                      {"]
#[doc = "                        \"type\": \"number\""]
#[doc = "                      }"]
#[doc = "                    ]"]
#[doc = "                  },"]
#[doc = "                  \"deviceIds\": {"]
#[doc = "                    \"type\": \"array\","]
#[doc = "                    \"items\": {"]
#[doc = "                      \"type\": \"string\""]
#[doc = "                    }"]
#[doc = "                  },"]
#[doc = "                  \"driver\": {"]
#[doc = "                    \"type\": \"string\""]
#[doc = "                  }"]
#[doc = "                },"]
#[doc = "                \"additionalProperties\": false"]
#[doc = "              }"]
#[doc = "            },"]
#[doc = "            \"memory\": {"]
#[doc = "              \"type\": \"string\""]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"additionalProperties\": false"]
#[doc = "        }"]
#[doc = "      },"]
#[doc = "      \"additionalProperties\": false"]
#[doc = "    }"]
#[doc = "  },"]
#[doc = "  \"additionalProperties\": false"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ComposeServicesItemDeploy {
    pub resources: ComposeServicesItemDeployResources,
}
impl From<&ComposeServicesItemDeploy> for ComposeServicesItemDeploy {
    fn from(value: &ComposeServicesItemDeploy) -> Self {
        value.clone()
    }
}
#[doc = "ComposeServicesItemDeployResources"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"type\": \"object\","]
#[doc = "  \"required\": ["]
#[doc = "    \"limits\","]
#[doc = "    \"reservations\""]
#[doc = "  ],"]
#[doc = "  \"properties\": {"]
#[doc = "    \"limits\": {"]
#[doc = "      \"type\": \"object\","]
#[doc = "      \"properties\": {"]
#[doc = "        \"cpus\": {"]
#[doc = "          \"type\": \"string\""]
#[doc = "        },"]
#[doc = "        \"memory\": {"]
#[doc = "          \"type\": \"string\""]
#[doc = "        },"]
#[doc = "        \"pids\": {"]
#[doc = "          \"type\": \"number\""]
#[doc = "        }"]
#[doc = "      },"]
#[doc = "      \"additionalProperties\": false"]
#[doc = "    },"]
#[doc = "    \"reservations\": {"]
#[doc = "      \"type\": \"object\","]
#[doc = "      \"required\": ["]
#[doc = "        \"devices\""]
#[doc = "      ],"]
#[doc = "      \"properties\": {"]
#[doc = "        \"cpus\": {"]
#[doc = "          \"type\": \"string\""]
#[doc = "        },"]
#[doc = "        \"devices\": {"]
#[doc = "          \"type\": \"array\","]
#[doc = "          \"items\": {"]
#[doc = "            \"type\": \"object\","]
#[doc = "            \"required\": ["]
#[doc = "              \"capabilities\""]
#[doc = "            ],"]
#[doc = "            \"properties\": {"]
#[doc = "              \"capabilities\": {"]
#[doc = "                \"type\": \"array\","]
#[doc = "                \"items\": {"]
#[doc = "                  \"type\": \"string\""]
#[doc = "                }"]
#[doc = "              },"]
#[doc = "              \"count\": {"]
#[doc = "                \"anyOf\": ["]
#[doc = "                  {"]
#[doc = "                    \"type\": \"string\","]
#[doc = "                    \"enum\": ["]
#[doc = "                      \"all\""]
#[doc = "                    ]"]
#[doc = "                  },"]
#[doc = "                  {"]
#[doc = "                    \"type\": \"number\""]
#[doc = "                  }"]
#[doc = "                ]"]
#[doc = "              },"]
#[doc = "              \"deviceIds\": {"]
#[doc = "                \"type\": \"array\","]
#[doc = "                \"items\": {"]
#[doc = "                  \"type\": \"string\""]
#[doc = "                }"]
#[doc = "              },"]
#[doc = "              \"driver\": {"]
#[doc = "                \"type\": \"string\""]
#[doc = "              }"]
#[doc = "            },"]
#[doc = "            \"additionalProperties\": false"]
#[doc = "          }"]
#[doc = "        },"]
#[doc = "        \"memory\": {"]
#[doc = "          \"type\": \"string\""]
#[doc = "        }"]
#[doc = "      },"]
#[doc = "      \"additionalProperties\": false"]
#[doc = "    }"]
#[doc = "  },"]
#[doc = "  \"additionalProperties\": false"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ComposeServicesItemDeployResources {
    pub limits: ComposeServicesItemDeployResourcesLimits,
    pub reservations: ComposeServicesItemDeployResourcesReservations,
}
impl From<&ComposeServicesItemDeployResources> for ComposeServicesItemDeployResources {
    fn from(value: &ComposeServicesItemDeployResources) -> Self {
        value.clone()
    }
}
#[doc = "ComposeServicesItemDeployResourcesLimits"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"type\": \"object\","]
#[doc = "  \"properties\": {"]
#[doc = "    \"cpus\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"memory\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"pids\": {"]
#[doc = "      \"type\": \"number\""]
#[doc = "    }"]
#[doc = "  },"]
#[doc = "  \"additionalProperties\": false"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ComposeServicesItemDeployResourcesLimits {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub cpus: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub memory: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub pids: Option<f64>,
}
impl From<&ComposeServicesItemDeployResourcesLimits> for ComposeServicesItemDeployResourcesLimits {
    fn from(value: &ComposeServicesItemDeployResourcesLimits) -> Self {
        value.clone()
    }
}
#[doc = "ComposeServicesItemDeployResourcesReservations"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"type\": \"object\","]
#[doc = "  \"required\": ["]
#[doc = "    \"devices\""]
#[doc = "  ],"]
#[doc = "  \"properties\": {"]
#[doc = "    \"cpus\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"devices\": {"]
#[doc = "      \"type\": \"array\","]
#[doc = "      \"items\": {"]
#[doc = "        \"type\": \"object\","]
#[doc = "        \"required\": ["]
#[doc = "          \"capabilities\""]
#[doc = "        ],"]
#[doc = "        \"properties\": {"]
#[doc = "          \"capabilities\": {"]
#[doc = "            \"type\": \"array\","]
#[doc = "            \"items\": {"]
#[doc = "              \"type\": \"string\""]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"count\": {"]
#[doc = "            \"anyOf\": ["]
#[doc = "              {"]
#[doc = "                \"type\": \"string\","]
#[doc = "                \"enum\": ["]
#[doc = "                  \"all\""]
#[doc = "                ]"]
#[doc = "              },"]
#[doc = "              {"]
#[doc = "                \"type\": \"number\""]
#[doc = "              }"]
#[doc = "            ]"]
#[doc = "          },"]
#[doc = "          \"deviceIds\": {"]
#[doc = "            \"type\": \"array\","]
#[doc = "            \"items\": {"]
#[doc = "              \"type\": \"string\""]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"driver\": {"]
#[doc = "            \"type\": \"string\""]
#[doc = "          }"]
#[doc = "        },"]
#[doc = "        \"additionalProperties\": false"]
#[doc = "      }"]
#[doc = "    },"]
#[doc = "    \"memory\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    }"]
#[doc = "  },"]
#[doc = "  \"additionalProperties\": false"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ComposeServicesItemDeployResourcesReservations {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub cpus: Option<String>,
    pub devices: Vec<ComposeServicesItemDeployResourcesReservationsDevicesItem>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub memory: Option<String>,
}
impl From<&ComposeServicesItemDeployResourcesReservations>
    for ComposeServicesItemDeployResourcesReservations
{
    fn from(value: &ComposeServicesItemDeployResourcesReservations) -> Self {
        value.clone()
    }
}
#[doc = "ComposeServicesItemDeployResourcesReservationsDevicesItem"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"type\": \"object\","]
#[doc = "  \"required\": ["]
#[doc = "    \"capabilities\""]
#[doc = "  ],"]
#[doc = "  \"properties\": {"]
#[doc = "    \"capabilities\": {"]
#[doc = "      \"type\": \"array\","]
#[doc = "      \"items\": {"]
#[doc = "        \"type\": \"string\""]
#[doc = "      }"]
#[doc = "    },"]
#[doc = "    \"count\": {"]
#[doc = "      \"anyOf\": ["]
#[doc = "        {"]
#[doc = "          \"type\": \"string\","]
#[doc = "          \"enum\": ["]
#[doc = "            \"all\""]
#[doc = "          ]"]
#[doc = "        },"]
#[doc = "        {"]
#[doc = "          \"type\": \"number\""]
#[doc = "        }"]
#[doc = "      ]"]
#[doc = "    },"]
#[doc = "    \"deviceIds\": {"]
#[doc = "      \"type\": \"array\","]
#[doc = "      \"items\": {"]
#[doc = "        \"type\": \"string\""]
#[doc = "      }"]
#[doc = "    },"]
#[doc = "    \"driver\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    }"]
#[doc = "  },"]
#[doc = "  \"additionalProperties\": false"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ComposeServicesItemDeployResourcesReservationsDevicesItem {
    pub capabilities: Vec<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub count: Option<ComposeServicesItemDeployResourcesReservationsDevicesItemCount>,
    #[serde(rename = "deviceIds", default, skip_serializing_if = "Vec::is_empty")]
    pub device_ids: Vec<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub driver: Option<String>,
}
impl From<&ComposeServicesItemDeployResourcesReservationsDevicesItem>
    for ComposeServicesItemDeployResourcesReservationsDevicesItem
{
    fn from(value: &ComposeServicesItemDeployResourcesReservationsDevicesItem) -> Self {
        value.clone()
    }
}
#[doc = "ComposeServicesItemDeployResourcesReservationsDevicesItemCount"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"anyOf\": ["]
#[doc = "    {"]
#[doc = "      \"type\": \"string\","]
#[doc = "      \"enum\": ["]
#[doc = "        \"all\""]
#[doc = "      ]"]
#[doc = "    },"]
#[doc = "    {"]
#[doc = "      \"type\": \"number\""]
#[doc = "    }"]
#[doc = "  ]"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum ComposeServicesItemDeployResourcesReservationsDevicesItemCount {
    Variant0(ComposeServicesItemDeployResourcesReservationsDevicesItemCountVariant0),
    Variant1(f64),
}
impl From<&ComposeServicesItemDeployResourcesReservationsDevicesItemCount>
    for ComposeServicesItemDeployResourcesReservationsDevicesItemCount
{
    fn from(value: &ComposeServicesItemDeployResourcesReservationsDevicesItemCount) -> Self {
        value.clone()
    }
}
impl std::str::FromStr for ComposeServicesItemDeployResourcesReservationsDevicesItemCount {
    type Err = self::error::ConversionError;
    fn from_str(value: &str) -> Result<Self, self::error::ConversionError> {
        if let Ok(v) = value.parse() {
            Ok(Self::Variant0(v))
        } else if let Ok(v) = value.parse() {
            Ok(Self::Variant1(v))
        } else {
            Err("string conversion failed for all variants".into())
        }
    }
}
impl std::convert::TryFrom<&str>
    for ComposeServicesItemDeployResourcesReservationsDevicesItemCount
{
    type Error = self::error::ConversionError;
    fn try_from(value: &str) -> Result<Self, self::error::ConversionError> {
        value.parse()
    }
}
impl std::convert::TryFrom<&String>
    for ComposeServicesItemDeployResourcesReservationsDevicesItemCount
{
    type Error = self::error::ConversionError;
    fn try_from(value: &String) -> Result<Self, self::error::ConversionError> {
        value.parse()
    }
}
impl std::convert::TryFrom<String>
    for ComposeServicesItemDeployResourcesReservationsDevicesItemCount
{
    type Error = self::error::ConversionError;
    fn try_from(value: String) -> Result<Self, self::error::ConversionError> {
        value.parse()
    }
}
impl ToString for ComposeServicesItemDeployResourcesReservationsDevicesItemCount {
    fn to_string(&self) -> String {
        match self {
            Self::Variant0(x) => x.to_string(),
            Self::Variant1(x) => x.to_string(),
        }
    }
}
impl From<ComposeServicesItemDeployResourcesReservationsDevicesItemCountVariant0>
    for ComposeServicesItemDeployResourcesReservationsDevicesItemCount
{
    fn from(value: ComposeServicesItemDeployResourcesReservationsDevicesItemCountVariant0) -> Self {
        Self::Variant0(value)
    }
}
impl From<f64> for ComposeServicesItemDeployResourcesReservationsDevicesItemCount {
    fn from(value: f64) -> Self {
        Self::Variant1(value)
    }
}
#[doc = "ComposeServicesItemDeployResourcesReservationsDevicesItemCountVariant0"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"type\": \"string\","]
#[doc = "  \"enum\": ["]
#[doc = "    \"all\""]
#[doc = "  ]"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, Ord, PartialEq, PartialOrd, Serialize)]
pub enum ComposeServicesItemDeployResourcesReservationsDevicesItemCountVariant0 {
    #[serde(rename = "all")]
    All,
}
impl From<&ComposeServicesItemDeployResourcesReservationsDevicesItemCountVariant0>
    for ComposeServicesItemDeployResourcesReservationsDevicesItemCountVariant0
{
    fn from(
        value: &ComposeServicesItemDeployResourcesReservationsDevicesItemCountVariant0,
    ) -> Self {
        value.clone()
    }
}
impl ToString for ComposeServicesItemDeployResourcesReservationsDevicesItemCountVariant0 {
    fn to_string(&self) -> String {
        match *self {
            Self::All => "all".to_string(),
        }
    }
}
impl std::str::FromStr for ComposeServicesItemDeployResourcesReservationsDevicesItemCountVariant0 {
    type Err = self::error::ConversionError;
    fn from_str(value: &str) -> Result<Self, self::error::ConversionError> {
        match value {
            "all" => Ok(Self::All),
            _ => Err("invalid value".into()),
        }
    }
}
impl std::convert::TryFrom<&str>
    for ComposeServicesItemDeployResourcesReservationsDevicesItemCountVariant0
{
    type Error = self::error::ConversionError;
    fn try_from(value: &str) -> Result<Self, self::error::ConversionError> {
        value.parse()
    }
}
impl std::convert::TryFrom<&String>
    for ComposeServicesItemDeployResourcesReservationsDevicesItemCountVariant0
{
    type Error = self::error::ConversionError;
    fn try_from(value: &String) -> Result<Self, self::error::ConversionError> {
        value.parse()
    }
}
impl std::convert::TryFrom<String>
    for ComposeServicesItemDeployResourcesReservationsDevicesItemCountVariant0
{
    type Error = self::error::ConversionError;
    fn try_from(value: String) -> Result<Self, self::error::ConversionError> {
        value.parse()
    }
}
#[doc = "ComposeServicesItemEntrypoint"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"anyOf\": ["]
#[doc = "    {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    {"]
#[doc = "      \"type\": \"array\","]
#[doc = "      \"items\": {"]
#[doc = "        \"type\": \"string\""]
#[doc = "      }"]
#[doc = "    }"]
#[doc = "  ]"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum ComposeServicesItemEntrypoint {
    Variant0(String),
    Variant1(Vec<String>),
}
impl From<&ComposeServicesItemEntrypoint> for ComposeServicesItemEntrypoint {
    fn from(value: &ComposeServicesItemEntrypoint) -> Self {
        value.clone()
    }
}
impl From<Vec<String>> for ComposeServicesItemEntrypoint {
    fn from(value: Vec<String>) -> Self {
        Self::Variant1(value)
    }
}
#[doc = "ComposeServicesItemHealthCheck"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"type\": \"object\","]
#[doc = "  \"required\": ["]
#[doc = "    \"test\""]
#[doc = "  ],"]
#[doc = "  \"properties\": {"]
#[doc = "    \"interval\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"retries\": {"]
#[doc = "      \"type\": \"number\""]
#[doc = "    },"]
#[doc = "    \"startInterval\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"startPeriod\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"test\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"timeout\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    }"]
#[doc = "  },"]
#[doc = "  \"additionalProperties\": false"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ComposeServicesItemHealthCheck {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub interval: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub retries: Option<f64>,
    #[serde(
        rename = "startInterval",
        default,
        skip_serializing_if = "Option::is_none"
    )]
    pub start_interval: Option<String>,
    #[serde(
        rename = "startPeriod",
        default,
        skip_serializing_if = "Option::is_none"
    )]
    pub start_period: Option<String>,
    pub test: String,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub timeout: Option<String>,
}
impl From<&ComposeServicesItemHealthCheck> for ComposeServicesItemHealthCheck {
    fn from(value: &ComposeServicesItemHealthCheck) -> Self {
        value.clone()
    }
}
#[doc = "ComposeServicesItemLogging"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"type\": \"object\","]
#[doc = "  \"required\": ["]
#[doc = "    \"driver\","]
#[doc = "    \"options\""]
#[doc = "  ],"]
#[doc = "  \"properties\": {"]
#[doc = "    \"driver\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"options\": {"]
#[doc = "      \"type\": \"object\","]
#[doc = "      \"additionalProperties\": {"]
#[doc = "        \"type\": \"string\""]
#[doc = "      }"]
#[doc = "    }"]
#[doc = "  },"]
#[doc = "  \"additionalProperties\": false"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ComposeServicesItemLogging {
    pub driver: String,
    pub options: std::collections::HashMap<String, String>,
}
impl From<&ComposeServicesItemLogging> for ComposeServicesItemLogging {
    fn from(value: &ComposeServicesItemLogging) -> Self {
        value.clone()
    }
}
#[doc = "ComposeServicesItemUlimits"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"type\": \"object\","]
#[doc = "  \"required\": ["]
#[doc = "    \"nofile\","]
#[doc = "    \"nproc\""]
#[doc = "  ],"]
#[doc = "  \"properties\": {"]
#[doc = "    \"nofile\": {"]
#[doc = "      \"anyOf\": ["]
#[doc = "        {"]
#[doc = "          \"type\": \"number\""]
#[doc = "        },"]
#[doc = "        {"]
#[doc = "          \"type\": \"object\","]
#[doc = "          \"required\": ["]
#[doc = "            \"hard\","]
#[doc = "            \"soft\""]
#[doc = "          ],"]
#[doc = "          \"properties\": {"]
#[doc = "            \"hard\": {"]
#[doc = "              \"type\": \"number\""]
#[doc = "            },"]
#[doc = "            \"soft\": {"]
#[doc = "              \"type\": \"number\""]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"additionalProperties\": false"]
#[doc = "        }"]
#[doc = "      ]"]
#[doc = "    },"]
#[doc = "    \"nproc\": {"]
#[doc = "      \"anyOf\": ["]
#[doc = "        {"]
#[doc = "          \"type\": \"number\""]
#[doc = "        },"]
#[doc = "        {"]
#[doc = "          \"type\": \"object\","]
#[doc = "          \"required\": ["]
#[doc = "            \"hard\","]
#[doc = "            \"soft\""]
#[doc = "          ],"]
#[doc = "          \"properties\": {"]
#[doc = "            \"hard\": {"]
#[doc = "              \"type\": \"number\""]
#[doc = "            },"]
#[doc = "            \"soft\": {"]
#[doc = "              \"type\": \"number\""]
#[doc = "            }"]
#[doc = "          },"]
#[doc = "          \"additionalProperties\": false"]
#[doc = "        }"]
#[doc = "      ]"]
#[doc = "    }"]
#[doc = "  },"]
#[doc = "  \"additionalProperties\": false"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ComposeServicesItemUlimits {
    pub nofile: ComposeServicesItemUlimitsNofile,
    pub nproc: ComposeServicesItemUlimitsNproc,
}
impl From<&ComposeServicesItemUlimits> for ComposeServicesItemUlimits {
    fn from(value: &ComposeServicesItemUlimits) -> Self {
        value.clone()
    }
}
#[doc = "ComposeServicesItemUlimitsNofile"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"anyOf\": ["]
#[doc = "    {"]
#[doc = "      \"type\": \"number\""]
#[doc = "    },"]
#[doc = "    {"]
#[doc = "      \"type\": \"object\","]
#[doc = "      \"required\": ["]
#[doc = "        \"hard\","]
#[doc = "        \"soft\""]
#[doc = "      ],"]
#[doc = "      \"properties\": {"]
#[doc = "        \"hard\": {"]
#[doc = "          \"type\": \"number\""]
#[doc = "        },"]
#[doc = "        \"soft\": {"]
#[doc = "          \"type\": \"number\""]
#[doc = "        }"]
#[doc = "      },"]
#[doc = "      \"additionalProperties\": false"]
#[doc = "    }"]
#[doc = "  ]"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum ComposeServicesItemUlimitsNofile {
    Variant0(f64),
    Variant1 { hard: f64, soft: f64 },
}
impl From<&ComposeServicesItemUlimitsNofile> for ComposeServicesItemUlimitsNofile {
    fn from(value: &ComposeServicesItemUlimitsNofile) -> Self {
        value.clone()
    }
}
impl From<f64> for ComposeServicesItemUlimitsNofile {
    fn from(value: f64) -> Self {
        Self::Variant0(value)
    }
}
#[doc = "ComposeServicesItemUlimitsNproc"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"anyOf\": ["]
#[doc = "    {"]
#[doc = "      \"type\": \"number\""]
#[doc = "    },"]
#[doc = "    {"]
#[doc = "      \"type\": \"object\","]
#[doc = "      \"required\": ["]
#[doc = "        \"hard\","]
#[doc = "        \"soft\""]
#[doc = "      ],"]
#[doc = "      \"properties\": {"]
#[doc = "        \"hard\": {"]
#[doc = "          \"type\": \"number\""]
#[doc = "        },"]
#[doc = "        \"soft\": {"]
#[doc = "          \"type\": \"number\""]
#[doc = "        }"]
#[doc = "      },"]
#[doc = "      \"additionalProperties\": false"]
#[doc = "    }"]
#[doc = "  ]"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum ComposeServicesItemUlimitsNproc {
    Variant0(f64),
    Variant1 { hard: f64, soft: f64 },
}
impl From<&ComposeServicesItemUlimitsNproc> for ComposeServicesItemUlimitsNproc {
    fn from(value: &ComposeServicesItemUlimitsNproc) -> Self {
        value.clone()
    }
}
impl From<f64> for ComposeServicesItemUlimitsNproc {
    fn from(value: f64) -> Self {
        Self::Variant0(value)
    }
}
#[doc = "ComposeServicesItemVolumesItem"]
#[doc = r""]
#[doc = r" <details><summary>JSON schema</summary>"]
#[doc = r""]
#[doc = r" ```json"]
#[doc = "{"]
#[doc = "  \"type\": \"object\","]
#[doc = "  \"required\": ["]
#[doc = "    \"containerPath\","]
#[doc = "    \"hostPath\""]
#[doc = "  ],"]
#[doc = "  \"properties\": {"]
#[doc = "    \"containerPath\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"hostPath\": {"]
#[doc = "      \"type\": \"string\""]
#[doc = "    },"]
#[doc = "    \"readOnly\": {"]
#[doc = "      \"type\": \"boolean\""]
#[doc = "    }"]
#[doc = "  },"]
#[doc = "  \"additionalProperties\": false"]
#[doc = "}"]
#[doc = r" ```"]
#[doc = r" </details>"]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ComposeServicesItemVolumesItem {
    #[serde(rename = "containerPath")]
    pub container_path: String,
    #[serde(rename = "hostPath")]
    pub host_path: String,
    #[serde(rename = "readOnly", default, skip_serializing_if = "Option::is_none")]
    pub read_only: Option<bool>,
}
impl From<&ComposeServicesItemVolumesItem> for ComposeServicesItemVolumesItem {
    fn from(value: &ComposeServicesItemVolumesItem) -> Self {
        value.clone()
    }
}
