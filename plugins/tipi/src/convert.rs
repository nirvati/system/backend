use crate::schemas::compose::command::Command;
use crate::schemas::compose::Compose;
use crate::schemas::config::Config;
use anyhow::bail;
use nirvati::utils::derive_entropy;
use nirvati_apps::internal::{
    CommonComponentConfig, Container, Deployment, DnsConfig, EnvVar, HostVolume, LonghornVolume,
    NetworkPolicy, NetworkPolicyEgress, NetworkPolicyIngress, PodConfig, Ports, Runnable, Service,
    ServiceType, Volume,
};
use nirvati_apps::metadata;
use nirvati_apps::metadata::{MultiLanguageItem, PreloadType, Runtime, SaasCompatibility};
use nirvati_apps::utils::StringLike;
use nirvati_plugin_common::UserState;
use semver::Version;
use slugify::slugify;
use std::collections::{BTreeMap, HashMap};
use serde_json::Value;

macro_rules! map(
    { $($key:expr => $value:expr),+ } => {
        {
            let mut m = ::std::collections::HashMap::new();
            $(
                m.insert($key, $value);
            )+
            m
        }
     };
);

macro_rules! langmap(
    { $($key:expr => $value:expr),+ } => {
        {
            let mut m = ::std::collections::HashMap::new();
            $(
                m.insert($key.to_string(), $value.to_string());
            )+
            MultiLanguageItem::from_hashmap(m)
        }
     };
);

fn get_category_names() -> HashMap<&'static str, MultiLanguageItem> {
    map! {
        "ai" => langmap! {
            "en" => "AI",
            "ja" => "AI",
            "fi" => "AI",
            "no" => "AI",
            "da" => "AI",
            "uk" => "ШІ",
            "tr" => "Yapay Zeka",
            "zh" => "AI",
            "el" => "Τεχνητή Νοημοσύνη",
            "ar" => "الذكاء الإصطناعي",
            "sv" => "AI",
            "he" => "AI",
            "vi" => "Trí tuệ nhân tạo",
            "ko" => "AI",
            "pt" => "AI",
            "fr" => "IA",
            "hu" => "AI",
            "cs" => "AI",
            "es" => "IA",
            "sr" => "AI",
            "af" => "AI",
            "ro" => "IA",
            "de" => "KI",
            "ca" => "IA",
            "nl" => "AI",
            "it" => "AI",
            "pl" => "SI",
            "ru" => "ИИ"
        },
        "automation" => langmap! {
            "en" => "Automation",
            "ja" => "自動化",
            "fi" => "Automation",
            "no" => "Automation",
            "da" => "Automation",
            "uk" => "Автоматизація",
            "tr" => "Otomasyon",
            "zh" => "自動化",
            "el" => "Αυτοματισμός",
            "ar" => "الوضع التلقائي",
            "sv" => "Automatisering",
            "he" => "Automation",
            "vi" => "Tự động hóa",
            "ko" => "자동화",
            "pt" => "Automação",
            "fr" => "Automatisation",
            "hu" => "Automatizálás",
            "cs" => "Automation",
            "es" => "Automatización",
            "sr" => "Automation",
            "af" => "Automation",
            "ro" => "Automatizare",
            "de" => "Automatisierung",
            "ca" => "Automatització",
            "nl" => "Automatisering",
            "it" => "Automazione",
            "pl" => "Automatyzacja",
            "ru" => "Автоматизация"
        },
        "books" => langmap! {
            "en" => "Books",
            "ja" => "ブック",
            "fi" => "Books",
            "no" => "Books",
            "da" => "Books",
            "uk" => "Книги",
            "tr" => "Kitaplar",
            "zh" => "書籍",
            "el" => "Βιβλία",
            "ar" => "كتب",
            "sv" => "Böcker",
            "he" => "Books",
            "vi" => "Sách",
            "ko" => "도서",
            "pt" => "Livros",
            "fr" => "Livres",
            "hu" => "Könyvek",
            "cs" => "Books",
            "es" => "Libros",
            "sr" => "Books",
            "af" => "Books",
            "ro" => "Cărți",
            "de" => "Bücher",
            "ca" => "Llibres",
            "nl" => "Boeken",
            "it" => "Libri",
            "pl" => "Książki",
            "ru" => "Книги"
        },
        "data" => langmap! {
            "en" => "Data",
            "ja" => "データ",
            "fi" => "Data",
            "no" => "Data",
            "da" => "Data",
            "uk" => "Дані",
            "tr" => "Veri",
            "zh" => "資料",
            "el" => "Δεδομένα",
            "ar" => "بيانات",
            "sv" => "Data",
            "he" => "Data",
            "vi" => "Dữ liệu",
            "ko" => "데이터",
            "pt" => "Dados",
            "fr" => "Données",
            "hu" => "Adat",
            "cs" => "Data",
            "es" => "Datos",
            "sr" => "Data",
            "af" => "Data",
            "ro" => "Informații",
            "de" => "Daten",
            "ca" => "Dades",
            "nl" => "Gegevens",
            "it" => "Data",
            "pl" => "Dane",
            "ru" => "Данные"
        },
        "development" => langmap! {
            "en" => "Development",
            "ja" => "開発",
            "fi" => "Development",
            "no" => "Development",
            "da" => "Development",
            "uk" => "Розробка",
            "tr" => "Geliştirme",
            "zh" => "開發",
            "el" => "Ανάπτυξη",
            "ar" => "برمجة",
            "sv" => "Utveckling",
            "he" => "Development",
            "vi" => "Phát triển",
            "ko" => "개발",
            "pt" => "Desenvolvimento",
            "fr" => "Développement",
            "hu" => "Fejlesztés",
            "cs" => "Development",
            "es" => "Desarrollo",
            "sr" => "Development",
            "af" => "Development",
            "ro" => "Dezvoltare",
            "de" => "Entwicklung",
            "ca" => "Desenvolupament",
            "nl" => "Ontwikkeling",
            "it" => "Sviluppo",
            "pl" => "Rozwój",
            "ru" => "Разработка"
        },
        "featured" => langmap! {
            "en" => "Featured",
            "ja" => "おすすめ",
            "fi" => "Featured",
            "no" => "Featured",
            "da" => "Featured",
            "uk" => "Рекомедновані",
            "tr" => "Öne Çıkanlar",
            "zh" => "精選",
            "el" => "�Προτεινόμενα",
            "ar" => "المعروضة",
            "sv" => "Utvalda",
            "he" => "Featured",
            "vi" => "Nôi bât",
            "ko" => "추천",
            "pt" => "Destaques",
            "fr" => "À l'affiche",
            "hu" => "Ajánlott",
            "cs" => "Featured",
            "es" => "Destacados",
            "sr" => "Featured",
            "af" => "Featured",
            "ro" => "Recomandări",
            "de" => "Empfohlen",
            "ca" => "Destacat",
            "nl" => "Uitgelicht",
            "it" => "In evidenza",
            "pl" => "Wyróżnione",
            "ru" => "Избранное"
        },
        "finance" => langmap! {
            "en" => "Finance",
            "ja" => "金融",
            "fi" => "Finance",
            "no" => "Finance",
            "da" => "Finance",
            "uk" => "Фінанси",
            "tr" => "Finans",
            "zh" => "金融",
            "el" => "Οικονομικά",
            "ar" => "مالية",
            "sv" => "Finans",
            "he" => "Finance",
            "vi" => "Tài chính",
            "ko" => "금융",
            "pt" => "Finanças",
            "fr" => "Finance",
            "hu" => "Pénzügyek",
            "cs" => "Finance",
            "es" => "Finanzas",
            "sr" => "Finance",
            "af" => "Finance",
            "ro" => "Finanțe",
            "de" => "Finanzen",
            "ca" => "Finances",
            "nl" => "Financiën",
            "it" => "Finanza",
            "pl" => "Finanse",
            "ru" => "Финансы"
        },
        "gaming" => langmap! {
            "en" => "Gaming",
            "ja" => "ゲーミング",
            "fi" => "Gaming",
            "no" => "Gaming",
            "da" => "Gaming",
            "uk" => "Ігри",
            "tr" => "Oyun",
            "zh" => "遊戲",
            "el" => "Παιχνίδια",
            "ar" => "اللعب",
            "sv" => "Spel",
            "he" => "Gaming",
            "vi" => "Trò chơi",
            "ko" => "게임",
            "pt" => "Jogos",
            "fr" => "Jeux",
            "hu" => "Játék",
            "cs" => "Gaming",
            "es" => "Gaming",
            "sr" => "Gaming",
            "af" => "Gaming",
            "ro" => "Gaming",
            "de" => "Spiele",
            "ca" => "Jocs",
            "nl" => "Gaming",
            "it" => "Videogiochi",
            "pl" => "Rozgrywka",
            "ru" => "Игры"
        },
        "media" => langmap! {
            "en" => "Media",
            "ja" => "メディア",
            "fi" => "Media",
            "no" => "Media",
            "da" => "Media",
            "uk" => "Медіа",
            "tr" => "Medya",
            "zh" => "媒體",
            "el" => "Πολυμέσα",
            "ar" => "وسائط",
            "sv" => "Medier",
            "he" => "Media",
            "vi" => "Phương tiện truyền thông",
            "ko" => "미디어",
            "pt" => "Multimédia",
            "fr" => "Médias",
            "hu" => "Média",
            "cs" => "Media",
            "es" => "Multimedia",
            "sr" => "Media",
            "af" => "Media",
            "ro" => "Media",
            "de" => "Medien",
            "ca" => "Medis",
            "nl" => "Media",
            "it" => "Media",
            "pl" => "Media",
            "ru" => "Медиа"
        },
        "music" => langmap! {
            "en" => "Music",
            "ja" => "ミュージック",
            "fi" => "Music",
            "no" => "Music",
            "da" => "Music",
            "uk" => "Музика",
            "tr" => "Müzik",
            "zh" => "音樂",
            "el" => "Μουσική",
            "ar" => "موسيقى",
            "sv" => "Musik",
            "he" => "Music",
            "vi" => "Âm nhạc",
            "ko" => "음악",
            "pt" => "Música",
            "fr" => "Musique",
            "hu" => "Zene",
            "cs" => "Music",
            "es" => "Música",
            "sr" => "Music",
            "af" => "Music",
            "ro" => "Muzică",
            "de" => "Musik",
            "ca" => "Música",
            "nl" => "Muziek",
            "it" => "Musica",
            "pl" => "Muzyka",
            "ru" => "Музыка"
        },
        "network" => langmap! {
            "en" => "Network",
            "ja" => "ネットワーク",
            "fi" => "Network",
            "no" => "Network",
            "da" => "Network",
            "uk" => "Мережа",
            "tr" => "Ağ",
            "zh" => "網路",
            "el" => "Δίκτυο",
            "ar" => "شبكة",
            "sv" => "Nätverk",
            "he" => "Network",
            "vi" => "Mạng",
            "ko" => "네트워크",
            "pt" => "Rede",
            "fr" => "Réseau",
            "hu" => "Hálózat",
            "cs" => "Network",
            "es" => "Red",
            "sr" => "Network",
            "af" => "Network",
            "ro" => "Rețea",
            "de" => "Netzwerk",
            "ca" => "Xarxa",
            "nl" => "Netwerk",
            "it" => "Rete",
            "pl" => "Sieć",
            "ru" => "Сеть"
        },
        "photography" => langmap! {
            "en" => "Photography",
            "ja" => "写真撮影",
            "fi" => "Photography",
            "no" => "Photography",
            "da" => "Photography",
            "uk" => "Фотографії",
            "tr" => "Fotoğrafçılık",
            "zh" => "攝影",
            "el" => "Φωτογράφηση",
            "ar" => "الفوتوغرافي",
            "sv" => "Fotografi",
            "he" => "Photography",
            "vi" => "Nhiếp ảnh",
            "ko" => "사진",
            "pt" => "Fotografias",
            "fr" => "Photographie",
            "hu" => "Fényképészet",
            "cs" => "Photography",
            "es" => "Fotografía",
            "sr" => "Photography",
            "af" => "Photography",
            "ro" => "Fotografie",
            "de" => "Fotografie",
            "ca" => "Fotografia",
            "nl" => "Fotografie",
            "it" => "Fotografia",
            "pl" => "Fotografia",
            "ru" => "Фотография"
        },
        "security" => langmap! {
            "en" => "Security",
            "ja" => "セキュリティ",
            "fi" => "Security",
            "no" => "Security",
            "da" => "Security",
            "uk" => "Безпека",
            "tr" => "Güvenlik",
            "zh" => "安全性",
            "el" => "Ασφάλεια",
            "ar" => "حماية",
            "sv" => "Säkerhet",
            "he" => "Security",
            "vi" => "Bảo mật",
            "ko" => "보안",
            "pt" => "Segurança",
            "fr" => "Sécurité",
            "hu" => "Biztonság",
            "cs" => "Security",
            "es" => "Seguridad",
            "sr" => "Security",
            "af" => "Security",
            "ro" => "Securitate",
            "de" => "Sicherheit",
            "ca" => "Seguretat",
            "nl" => "Beveiliging",
            "it" => "Sicurezza",
            "pl" => "Bezpieczeństwo",
            "ru" => "Безопасность"
        },
        "social" => langmap! {
            "en" => "Social",
            "ja" => "ソーシャル",
            "fi" => "Social",
            "no" => "Social",
            "da" => "Social",
            "uk" => "Соціальні мережі",
            "tr" => "Sosyal",
            "zh" => "社交",
            "el" => "Κοινωνικά",
            "ar" => "وسائل التواصل",
            "sv" => "Social",
            "he" => "Social",
            "vi" => "Mạng xã hội",
            "ko" => "소셜",
            "pt" => "Social",
            "fr" => "Réseaux Sociaux",
            "hu" => "Közösségi",
            "cs" => "Social",
            "es" => "Social",
            "sr" => "Social",
            "af" => "Social",
            "ro" => "Social",
            "de" => "Soziale Medien",
            "ca" => "Social",
            "nl" => "Sociaal",
            "it" => "Social",
            "pl" => "Społeczność",
            "ru" => "Общение"
        },
        "utilities" => langmap! {
            "en" => "Utilities",
            "ja" => "ユーティリティ",
            "fi" => "Utilities",
            "no" => "Utilities",
            "da" => "Utilities",
            "uk" => "Утиліти",
            "tr" => "Araçlar",
            "zh" => "實用工具",
            "el" => "Βοηθήματα",
            "ar" => "أدوات",
            "sv" => "Verktyg",
            "he" => "Utilities",
            "vi" => "Tiện ích",
            "ko" => "유틸리티",
            "pt" => "Utilitários",
            "fr" => "Utilitaires",
            "hu" => "Segédprogramok",
            "cs" => "Utilities",
            "es" => "Utilidades",
            "sr" => "Utilities",
            "af" => "Utilities",
            "ro" => "Utilități",
            "de" => "Dienstprogramme",
            "ca" => "Utilitats",
            "nl" => "Hulpmiddelen",
            "it" => "Utilità",
            "pl" => "Narzędzia",
            "ru" => "Инструменты"
        }
    }
}

pub fn convert_to_nirvati(
    app_id: &str,
    nirvati_seed: &str,
    mut config: Config,
    compose: Compose,
    init_domain: Option<String>,
    user_state: UserState,
) -> anyhow::Result<nirvati_apps::internal::InternalAppRepresentation> {
    let categories = get_category_names();
    let mut metadata = metadata::Metadata {
        id: config.id,
        name: MultiLanguageItem::from_string(config.name),
        version: Version::new(0, 1, 0),
        display_version: config.version.clone(),
        category: if config.categories.is_empty() {
            MultiLanguageItem::from_string("Uncategorized".to_string())
        } else {
            let tmp = config.categories.remove(0);
            if let Some(cat) = categories.get(tmp.as_str()) {
                cat.clone()
            } else {
                MultiLanguageItem::from_string(tmp)
            }
        },
        tagline: MultiLanguageItem::from_string(config.short_desc),
        developers: Default::default(),
        description: MultiLanguageItem::from_string(config.description),
        dependencies: vec![],
        base_permissions: vec![],
        has_permissions: vec![],
        exposes_permissions: vec![],
        repos: BTreeMap::from([("Source code".to_string(), config.source)]),
        support: config.website.unwrap_or_default(),
        gallery: vec![],
        icon: Some(format!("https://raw.githubusercontent.com/runtipi/runtipi-appstore/master/apps/{}/metadata/logo.jpg", app_id)),
        path: config.url_suffix,
        default_username: None,
        default_password: None,
        implements: None,
        release_notes: Default::default(),
        license: "Unknown".to_string(),
        settings: Default::default(),
        volumes: BTreeMap::from([(
            "data".to_string(),
            metadata::Volume {
                minimum_size: 1024 * 1024 * 1024,
                recommended_size: 5 * 1024 * 1024 * 1024,
                name: MultiLanguageItem::from_string("App data".to_string()),
                description: MultiLanguageItem::from_string(
                    "All data generated by the app".to_string(),
                ),
            },
        )]),
        ports: vec![],
        exported_data: None,
        runtime: Runtime::Kubernetes,
        scope: Default::default(),
        allowed_scopes: vec![],
        default_scope: Default::default(),
        store_plugins: vec![],
        ui_module: None,
        supports_ingress: false,
        can_be_protected: false,
        saas_compatibility: SaasCompatibility::Unknown,
        raw_ingress: false,
        post_install_redirect: None,
        components: vec![],
        is_citadel: false,
        preload_type: Some(PreloadType::MainVolumeFromSubdir("data".to_string())),
    };

    let mut random_vars = BTreeMap::new();
    let mut field_vars: BTreeMap<String, String> = BTreeMap::new();

    let parsed_settings = if let Some(settings) = user_state.app_settings.get(app_id) {
        let parsed_settings: serde_json::Value = serde_json::from_str(settings)?;
        // Get the map
        if let serde_json::Value::Object(map) = parsed_settings {
            map
        } else {
            bail!("Settings must be an object")
        }
    } else {
        serde_json::Map::new()
    };

    for field in config.form_fields {
        match field.field_type.as_str() {
            "password" | "email" | "text" | "url" | "ip" => {
                metadata.settings.0.insert(
                    field.env_variable.clone(),
                    nirvati_apps::metadata::Setting::String {
                        name: MultiLanguageItem::from_string(field.label),
                        description: MultiLanguageItem::from_string("".to_string()),
                        default: None,
                        max_len: None,
                        string_type: match field.field_type.as_str() {
                            "password" => Some(nirvati_apps::metadata::StringType::Password),
                            "email" => Some(nirvati_apps::metadata::StringType::Email),
                            "url" => Some(nirvati_apps::metadata::StringType::Url),
                            "ip" => Some(nirvati_apps::metadata::StringType::Ip),
                            "text" => None,
                            _ => unreachable!(),
                        },
                        required: field.required.unwrap_or_default(),
                        placeholder: field.placeholder.map(MultiLanguageItem::from_string),
                    },
                );
                if let Some(val) = parsed_settings.get(&field.env_variable) {

                    field_vars.insert(field.env_variable.clone(), match &val {
                        Value::Null => "".to_string(),
                        Value::Bool(b) => b.to_string(),
                        Value::Number(n) => n.to_string(),
                        Value::String(s) => s.to_string(),
                        // Should not happen, but let's fail safely
                        Value::Array(_) => val.to_string(),
                        Value::Object(_) => val.to_string(),
                    });
                } else {
                    field_vars.insert(field.env_variable.clone(), "".to_string());
                }
            }
            "boolean" => {
                metadata.settings.0.insert(
                    field.env_variable.clone(),
                    nirvati_apps::metadata::Setting::Bool {
                        name: MultiLanguageItem::from_string(field.label),
                        description: MultiLanguageItem::from_string("".to_string()),
                        default: false,
                    },
                );
                if let Some(val) = parsed_settings.get(&field.env_variable) {
                    field_vars.insert(field.env_variable.clone(), val.to_string());
                } else {
                    field_vars.insert(field.env_variable.clone(), "false".to_string());
                }
            }
            "number" => {
                metadata.settings.0.insert(
                    field.env_variable.clone(),
                    nirvati_apps::metadata::Setting::Float {
                        name: MultiLanguageItem::from_string(field.label),
                        description: MultiLanguageItem::from_string("".to_string()),
                        default: None,
                        min: field.min.map(|v| v as f64),
                        max: field.max.map(|v| v as f64),
                        step_size: None,
                    },
                );
                if let Some(val) = parsed_settings.get(&field.env_variable) {
                    field_vars.insert(field.env_variable.clone(), val.to_string());
                } else {
                    field_vars.insert(field.env_variable.clone(), "0".to_string());
                }
            }
            "random" => {
                let truncate_len = field.min.unwrap_or(32);
                let mut hash =
                    derive_entropy(nirvati_seed, &field.env_variable);
                // Fail if truncate_len is more than 512
                if truncate_len > 512 {
                    bail!("A random field's minimum length can not be more than 512");
                }
                let mut i = 0;
                while hash.len() < truncate_len as usize {
                    hash = format!(
                        "{}{}",
                        hash,
                        derive_entropy(
                            nirvati_seed,
                            format!("{}:{}", field.env_variable, i).as_str(),
                        )
                    );
                    i += 1;
                }
                hash.truncate(truncate_len as usize);
                random_vars.insert(field.env_variable.clone(), hash);
            }
            other_type => {
                bail!("Unsupported field type: {}", other_type);
            }
        }
    }
    let mut services = BTreeMap::new();
    let mut containers = BTreeMap::new();
    let mut ingress = vec![];

    let mut uses_volume = false;
    for service in compose.services {
        if service.is_main.unwrap_or_default() {
            if let Some(internal_port) = service.internal_port {
                services.insert(
                    format!("{}-nirvati-ingress", service.name),
                    Service {
                        target_container: service.name.clone(),
                        r#type: ServiceType::ClusterIp,
                        ports: Ports {
                            udp: Default::default(),
                            tcp: BTreeMap::from([(internal_port, internal_port)]),
                        },
                        cluster_ip: None,
                        network_policy: None,
                    },
                );
                ingress.push(nirvati_apps::internal::Ingress {
                    target_service: Some(format!("{}-nirvati-ingress", service.name)),
                    target_port: Some(internal_port),
                    ..Default::default()
                });
                metadata.supports_ingress = true;
            }
        }
        if !service.extra_hosts.is_empty() {
            bail!("Extra hosts are not yet supported");
        }
        if !service.devices.is_empty() {
            bail!("Devices are not yet supported");
        }
        let shellexpander = |var: String| {
            if let Some(val) = random_vars.get(&var) {
                Ok(Some(val.clone()))
            } else if var == "TZ" {
                // TODO: Use the user's timezone
                Ok(Some("UTC".to_string()))
            } else if var == "APP_DOMAIN" {
                Ok(init_domain.clone())
            } else {
                match field_vars.get(&var) {
                    Some(val) => Ok(Some(val.clone())),
                    None => Err(std::io::Error::new(
                        std::io::ErrorKind::NotFound,
                        format!("Variable {} not found", var),
                    )),
                }
            }
        };
        containers.insert(
            service.name.clone(),
            Runnable::Deployment(Box::new(Deployment {
                container: Container {
                    cap_add: service.cap_add,
                    cap_drop: service.cap_drop,
                    image: service.image,
                    privileged: service.privileged.unwrap_or_default(),
                    restart: None,
                    stop_grace_period: service.stop_grace_period,
                    // TODO: Parse the user property
                    uid: None,
                    gid: None,
                    volumes: service
                        .volumes
                        .iter()
                        .map(|v| {
                            if v.host_path.starts_with("${APP_DATA_DIR}") {
                                let sub_path = v.host_path.replace("${APP_DATA_DIR}", "");
                                // Trim / from the start
                                let sub_path = sub_path.trim_start_matches('/');
                                uses_volume = true;
                                Ok(Volume::Longhorn(LonghornVolume {
                                    name: "data".to_string(),
                                    mount_path: v.container_path.clone(),
                                    sub_path: if sub_path.is_empty() {
                                        None
                                    } else {
                                        Some(sub_path.to_string())
                                    },
                                    is_readonly: false,
                                    from_app: None,
                                    from_ns: None,
                                }))
                            } else if v.host_path.starts_with("${ROOT_FOLDER_HOST}/media") {
                                let sub_path = v.host_path.replace("${ROOT_FOLDER_HOST}/media", "");
                                // Trim / from the start
                                let sub_path = sub_path.trim_start_matches('/');
                                Ok(Volume::Longhorn(LonghornVolume {
                                    name: "media".to_string(),
                                    mount_path: v.container_path.clone(),
                                    sub_path: if sub_path.is_empty() {
                                        None
                                    } else {
                                        Some(sub_path.to_string())
                                    },
                                    is_readonly: false,
                                    from_app: Some("tipi".to_string()),
                                    from_ns: None,
                                }))
                            } else if v.host_path == "/var/run/docker.sock" {
                                Err(anyhow::anyhow!(
                                    "This app uses Docker and can not be used on Nirvati"
                                ))
                            } else if v.host_path.starts_with("/") && !v.host_path.contains("$") {
                                Ok(Volume::Host(HostVolume {
                                    name: slugify!(&v.host_path),
                                    host_path: v.host_path.clone(),
                                    mount_path: v.container_path.clone(),
                                    is_readonly: false,
                                }))
                            } else {
                            Err(anyhow::anyhow!(
                                    "Only volumes starting with ${{APP_DATA_DIR}} are supported, encountered: {}",
                                    v.host_path
                                ))
                            }
                        })
                        .collect::<anyhow::Result<Vec<_>>>()?,
                    working_dir: service.working_dir,
                    shm_size: service.shm_size,
                    ulimits: service
                        .ulimits
                        .map(|ulimits| serde_json::to_value(ulimits).unwrap()),
                    host_network: service.network_mode.is_some_and(|mode| mode == "host"),
                    exposes: Default::default(),
                    fs_group: None,
                    service_account: None,
                    network_policy: None,
                    additional_labels: BTreeMap::from([(
                        "uses-compat-layer".to_string(),
                        "tipi".to_string(),
                    )]),
                    command: if let Some(cmd ) = service.command {
                        Some(
                            <Command as Into<Vec<String>>>::into(cmd).into_iter().map(|c| {
                                let res = shellexpand::env_with_context(&c, |v| shellexpander(v.to_string()))?;
                                let res_str = res.to_string();
                                Ok(res_str)
                            }).collect::<anyhow::Result<Vec<_>>>()?
                        )
                    } else {
                        None
                    },
                    entrypoint: if let Some(cmd ) = service.entrypoint {
                        Some(
                            <Command as Into<Vec<String>>>::into(cmd).into_iter().map(|c| {
                                let res = shellexpand::env_with_context(&c, |v| shellexpander(v.to_string()))?;
                                let res_str = res.to_string();
                                Ok(res_str)
                            }).collect::<anyhow::Result<Vec<_>>>()?
                        )
                    } else {
                        None
                    },
                    environment: service
                            .environment
                            .into_iter()
                            .map(|(k, v)| {
                                match v {
                                    StringLike::String(ref val) => {
                                        let res = shellexpand::env_with_context(val, |v| shellexpander(v.to_string()))?;
                                        let res_str = res.to_string();
                                        Ok((k, EnvVar::StringLike(StringLike::String(res_str))))
                                    }
                                    StringLike::Int(_) | StringLike::Bool(_) | StringLike::Float(_) => Ok((k, EnvVar::StringLike(v)))
                                }

                            })
                        .collect::<anyhow::Result<BTreeMap<_, _>>>()?,
                    hostname: service.hostname,
                    has_permissions: vec![],
                },
                middlewares: Default::default(),
            })),
        );
        services.insert(
            service.name.clone(),
            Service {
                target_container: service.name.clone(),
                r#type: ServiceType::ClusterIp,
                ports: Ports {
                    udp: Default::default(),
                    tcp: Default::default(),
                },
                cluster_ip: Some("None".to_string()),
                network_policy: None,
            },
        );
        if !service.add_ports.is_empty() {
            services.insert(
                format!("{}-public", service.name),
                Service {
                    target_container: service.name.clone(),
                    r#type: ServiceType::LoadBalancer,
                    ports: Ports {
                        tcp: service
                            .add_ports
                            .iter()
                            .filter_map(|port| {
                                let is_tcp = port.tcp.is_some_and(|tcp| tcp)
                                    || ((port.udp.is_none_or(|udp| !udp)) && port.tcp.is_none());
                                if is_tcp {
                                    Some((port.host_port, port.container_port))
                                } else {
                                    None
                                }
                            })
                            .collect(),
                        udp: service
                            .add_ports
                            .iter()
                            .filter_map(|port| {
                                let is_udp = port.udp.is_some_and(|udp| udp);
                                if is_udp {
                                    Some((port.host_port, port.container_port))
                                } else {
                                    None
                                }
                            })
                            .collect(),
                    },
                    cluster_ip: Some("None".to_string()),
                    network_policy: None,
                },
            );
        }
    }

    if !uses_volume {
        metadata.volumes = BTreeMap::new();
        metadata.preload_type = None;
    }

    Ok(nirvati_apps::internal::InternalAppRepresentation::new(
        metadata,
        containers,
        services,
        ingress,
        vec![],
        BTreeMap::new(),
        vec![],
        Some(NetworkPolicy {
            ingress: NetworkPolicyIngress {
                allow_world: CommonComponentConfig {
                    allow: true,
                    allowed_ports: None,
                },
                allow_cluster: CommonComponentConfig {
                    allow: false,
                    allowed_ports: None,
                },
                allow_same_ns: CommonComponentConfig {
                    allow: true,
                    allowed_ports: None,
                },
                cidrs: vec![],
                pods: vec![PodConfig {
                    namespace: None,
                    app: None,
                    match_labels: Some(HashMap::from([(
                        "uses-compat-layer".to_string(),
                        "tipi".to_string(),
                    )])),
                    allowed_ports: None,
                    all_namespaces: true,
                }],
            },
            egress: NetworkPolicyEgress {
                allow_cluster: false,
                allow_same_ns: true,
                allow_kube_api: false,
                allow_world: true,
                allow_local_net: true,
                dns: Some(DnsConfig {
                    allow: true,
                    allowed_lookup_names: None,
                    allowed_lookup_pattern: None,
                }),
                fqdns: vec![],
                cidrs: vec![],
                services: vec![],
                pods: vec![PodConfig {
                    namespace: None,
                    app: None,
                    match_labels: Some(HashMap::from([(
                        "uses-compat-layer".to_string(),
                        "tipi".to_string(),
                    )])),
                    allowed_ports: None,
                    all_namespaces: true,
                }],
            },
        }),
        vec![],
    ))
}
