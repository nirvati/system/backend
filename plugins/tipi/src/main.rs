use nirvati_plugin_tipi::plugin::TipiPlugin;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let plugin_state = TipiPlugin;
    let addr = "[::]:50051".parse().unwrap();
    tonic::transport::Server::builder()
        .add_service(
            nirvati_plugin_common::runtime_plugin_server::RuntimePluginServer::new(plugin_state),
        )
        .serve(addr)
        .await
        .unwrap();
}
