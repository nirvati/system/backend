use flate2::read::GzDecoder;
use nirvati_plugin_common::runtime_plugin_server::RuntimePlugin;
use nirvati_plugin_common::{
    InstallAppRequest, InstallAppResponse, IsSupportedAppRequest, IsSupportedAppResponse,
    ParseAppRequest, ParseAppResponse, PauseAppRequest, PauseAppResponse, UninstallAppRequest,
    UninstallAppResponse, UnpauseAppRequest, UnpauseAppResponse,
};
use tar::Archive;
use tempfile::TempDir;
use tonic::{Request, Response, Status};

pub struct TipiPlugin;

macro_rules! ok_or_unsupported {
    ($res:expr) => {
        match $res {
            Ok(res) => res,
            Err(err) => {
                tracing::error!("Error: {}", err);
                return Ok(Response::new(IsSupportedAppResponse { supported: false }));
            }
        }
    };
}

#[tonic::async_trait]
impl RuntimePlugin for TipiPlugin {
    async fn is_supported_app(
        &self,
        request: Request<IsSupportedAppRequest>,
    ) -> Result<Response<IsSupportedAppResponse>, Status> {
        let req = request.into_inner();
        let tmp_dir = TempDir::new()?;
        let tmp_dir_path = tmp_dir.path();
        let archive = GzDecoder::new(req.app_directory.as_slice());
        let mut archive = Archive::new(archive);
        archive.unpack(tmp_dir_path)?;

        let docker_compose_path = tmp_dir_path.join("docker-compose.json");
        let config_path = tmp_dir_path.join("config.json");
        if !docker_compose_path.exists() || !config_path.exists() {
            return Ok(Response::new(IsSupportedAppResponse { supported: false }));
        }
        let docker_compose = ok_or_unsupported!(std::fs::read_to_string(docker_compose_path));
        let config = ok_or_unsupported!(std::fs::read_to_string(config_path));
        let _docker_compose: crate::schemas::compose::Compose =
            ok_or_unsupported!(serde_json::from_str(&docker_compose));
        let _config: crate::schemas::config::Config =
            ok_or_unsupported!(serde_json::from_str(&config));
        Ok(Response::new(IsSupportedAppResponse { supported: true }))
    }

    async fn parse_app(
        &self,
        request: Request<ParseAppRequest>,
    ) -> Result<Response<ParseAppResponse>, Status> {
        let req = request.into_inner();
        let tmp_dir = TempDir::new()?;
        let tmp_dir_path = tmp_dir.path();
        let archive = GzDecoder::new(req.app_directory.as_slice());
        let mut archive = Archive::new(archive);
        archive.unpack(tmp_dir_path)?;

        let docker_compose_path = tmp_dir_path.join("docker-compose.json");
        let config_path = tmp_dir_path.join("config.json");
        let docker_compose = std::fs::read_to_string(docker_compose_path)?;
        let config = std::fs::read_to_string(config_path)?;
        let docker_compose: crate::schemas::compose::Compose =
            serde_json::from_str(&docker_compose)
                .map_err(|_| Status::invalid_argument("Invalid docker-compose.json"))?;
        let config: crate::schemas::config::Config = serde_json::from_str(&config)
            .map_err(|_| Status::invalid_argument("Invalid config.json"))?;
        let Some(user_state) = req.user_state else {
            return Err(Status::invalid_argument("User state not provided"));
        };
        let converted = crate::convert::convert_to_nirvati(
            &req.app_id,
            &req.app_seed_base,
            config,
            docker_compose,
            req.initial_domain,
            user_state,
        )
        .map_err(|e| Status::internal(e.to_string()))?;
        let converted = serde_json::to_string(&converted).unwrap();
        Ok(Response::new(ParseAppResponse {
            parsed_app: converted,
        }))
    }

    async fn install_app(
        &self,
        _request: Request<InstallAppRequest>,
    ) -> Result<Response<InstallAppResponse>, Status> {
        Ok(Response::new(InstallAppResponse {}))
    }

    async fn uninstall_app(
        &self,
        _request: Request<UninstallAppRequest>,
    ) -> Result<Response<UninstallAppResponse>, Status> {
        Ok(Response::new(UninstallAppResponse {}))
    }

    async fn pause_app(
        &self,
        _request: Request<PauseAppRequest>,
    ) -> Result<Response<PauseAppResponse>, Status> {
        Ok(Response::new(PauseAppResponse {}))
    }

    async fn unpause_app(
        &self,
        _request: Request<UnpauseAppRequest>,
    ) -> Result<Response<UnpauseAppResponse>, Status> {
        Ok(Response::new(UnpauseAppResponse {}))
    }
}
