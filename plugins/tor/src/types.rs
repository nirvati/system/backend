use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

#[derive(Serialize, Deserialize)]
pub struct OnionService {
    pub target_svc: String,
    pub target_ns: Option<String>,
    pub ports: BTreeMap<u16, Port>,
    pub insecure_single_hop: Option<bool>,
    pub store_name_in_secret: Option<OnionServiceStoreNameInSecret>,
}

#[derive(Serialize, Deserialize)]
pub struct OnionServiceStoreNameInSecret {
    pub key: String,
    pub name: String,
}

#[derive(Serialize, Deserialize)]
#[serde(untagged)]
pub enum Port {
    Number(u16),
    WithSvc(PortWithSvc),
}

#[derive(Serialize, Deserialize)]
pub struct PortWithSvc {
    pub target_svc: String,
    pub target_ns: Option<String>,
    pub target_port: u16,
}

impl Port {
    pub fn get_target_svc(&self) -> Option<&str> {
        match self {
            Port::Number(_) => None,
            Port::WithSvc(p) => Some(&p.target_svc),
        }
    }

    pub fn get_target_ns(&self) -> Option<&str> {
        match self {
            Port::Number(_) => None,
            Port::WithSvc(p) => p.target_ns.as_deref(),
        }
    }

    pub fn get_target_port(&self) -> u16 {
        match self {
            Port::Number(p) => *p,
            Port::WithSvc(p) => p.target_port,
        }
    }
}
