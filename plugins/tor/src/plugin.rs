use crate::types::OnionService;
use k8s_crds_arti_controller::{
    OnionServiceKeySecret, OnionServiceRoutes, OnionServiceRoutesTarget, OnionServiceSpec,
};
use kube::api::ObjectMeta;
use nirvati_plugin_common::custom_resource_plugin_server::CustomResourcePlugin;
use nirvati_plugin_common::{ConvertCrRequest, ConvertCrResponse};
use std::collections::BTreeMap;
use tonic::{Request, Response, Status};

pub struct PluginState {}

#[tonic::async_trait]
impl CustomResourcePlugin for PluginState {
    async fn generate_c_rs(
        &self,
        request: Request<ConvertCrRequest>,
    ) -> Result<Response<ConvertCrResponse>, Status> {
        let request = request.into_inner();
        let inner = request.resource;
        let parsed: BTreeMap<String, OnionService> = serde_yaml::from_str(&inner).unwrap();
        let mut result = vec![];
        let mut has_cluster_egress = false;
        for (svc_name, onion_service) in parsed {
            has_cluster_egress = has_cluster_egress || onion_service.target_ns.is_some();
            result.push(k8s_crds_arti_controller::OnionService {
                spec: OnionServiceSpec {
                    key_secret: OnionServiceKeySecret {
                        name: format!("onion-svc-{}-keys", svc_name),
                        namespace: None,
                    },
                    routes: onion_service
                        .ports
                        .into_iter()
                        .map(|(exposed_port, target_port)| {
                            OnionServiceRoutes {
                                source_port: exposed_port,
                                target: OnionServiceRoutesTarget {
                                    dns_name: None,
                                    ip: None,
                                    ns: target_port
                                        .get_target_ns()
                                        .map(|ns| ns.to_string())
                                        .or(onion_service.target_ns.clone()),
                                    port: target_port.get_target_port(),
                                    // Unwrapping and then wrapping in Some may be less optimal, but it makes it clear here that it is never None
                                    svc: Some(
                                        target_port
                                            .get_target_svc()
                                            .map(|svc| svc.to_string())
                                            .unwrap_or_else(|| onion_service.target_svc.clone()),
                                    ),
                                },
                            }
                        })
                        .collect(),
                    store_name_in_secret: onion_service.store_name_in_secret.map(|store| {
                        k8s_crds_arti_controller::OnionServiceStoreNameInSecret {
                            key: store.key,
                            name: store.name,
                            namespace: None,
                        }
                    }),
                },
                // Place metadata here to avoid a clone on svc_name
                metadata: ObjectMeta {
                    name: Some(svc_name),
                    ..Default::default()
                },
                status: None,
            });
        }
        Ok(Response::new(ConvertCrResponse {
            resources: serde_yaml::to_string(&result).unwrap(),
            added_permissions: if has_cluster_egress {
                vec!["builtin/network/cluster-egress".to_string()]
            } else {
                vec![]
            },
        }))
    }
}
