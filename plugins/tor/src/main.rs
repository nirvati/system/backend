use nirvati_plugin_tor::plugin::PluginState;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let plugin_state = PluginState {};
    let addr = "[::]:50051".parse().unwrap();
    tonic::transport::Server::builder()
        .add_service(
            nirvati_plugin_common::custom_resource_plugin_server::CustomResourcePluginServer::new(
                plugin_state,
            ),
        )
        .serve(addr)
        .await
        .unwrap();
}
