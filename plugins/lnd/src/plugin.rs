use crate::types::{Context, Settings};
use lnd_grpc_rust::lnrpc::{BakeMacaroonRequest, DeleteMacaroonIdRequest};
use nirvati_plugin_common::context_plugin_server::ContextPlugin;
use nirvati_plugin_common::{
    AppContext, GetContextRequest, RegisterAppRequest, RegisterAppResponse, UnregisterAppRequest,
    UnregisterAppResponse,
};
use sha2::Digest;
use std::collections::HashMap;
use tokio::sync::Mutex;
use tonic::{Request, Response, Status};

pub struct PluginState {
    pub btc_network: String,
    pub client: Option<Mutex<lnd_grpc_rust::LndClient>>,
    pub tls_cert: String,
}

#[tonic::async_trait]
impl ContextPlugin for PluginState {
    async fn register_app(
        &self,
        _request: Request<RegisterAppRequest>,
    ) -> Result<Response<RegisterAppResponse>, Status> {
        Ok(Response::new(RegisterAppResponse {}))
    }

    async fn unregister_app(
        &self,
        request: Request<UnregisterAppRequest>,
    ) -> Result<Response<UnregisterAppResponse>, Status> {
        let req = request.into_inner();
        let app_id = req.app_id.clone();
        let user_name = req.user_state.unwrap().user_id.clone();
        let id_prefix = format!("{}-{}", app_id, user_name);
        let prefix_hash = sha2::Sha256::digest(id_prefix.as_bytes());
        let key_id = u64::from_be_bytes([
            prefix_hash[0],
            prefix_hash[1],
            prefix_hash[2],
            prefix_hash[3],
            prefix_hash[4],
            prefix_hash[5],
            prefix_hash[6],
            prefix_hash[7],
        ]);

        if let Some(client) = self.client.as_ref() {
            let mut client = client.lock().await;
            client
                .lightning()
                .delete_macaroon_id(DeleteMacaroonIdRequest {
                    root_key_id: key_id,
                })
                .await
                .map_err(|e| {
                    tracing::error!("Failed to delete macaroon: {}", e);
                    Status::internal("Failed to delete macaroon")
                })?;
        }
        Ok(Response::new(UnregisterAppResponse {}))
    }

    async fn get_context(
        &self,
        request: Request<GetContextRequest>,
    ) -> Result<Response<AppContext>, Status> {
        let req = request.into_inner();
        let config: Settings = serde_json::from_str(&req.additional_data).map_err(|e| {
            Status::invalid_argument(format!("Failed to parse additional_data: {}", e))
        })?;
        let app_id = req.app_id.clone();
        let user_name = req.user_state.unwrap().user_id.clone();
        let id_prefix = format!("{}-{}", app_id, user_name);
        let prefix_hash = sha2::Sha256::digest(id_prefix.as_bytes());
        let key_id = u64::from_be_bytes([
            prefix_hash[0],
            prefix_hash[1],
            prefix_hash[2],
            prefix_hash[3],
            prefix_hash[4],
            prefix_hash[5],
            prefix_hash[6],
            prefix_hash[7],
        ]);
        let mut macaroons = HashMap::new();

        if let Some(client) = self.client.as_ref() {
            let mut client = client.lock().await;
            for (macaroon_id, macaroon) in config.macaroons {
                let macaroon = match client
                    .lightning()
                    .bake_macaroon(BakeMacaroonRequest {
                        permissions: macaroon.permissions.into_iter().map(|p| p.into()).collect(),
                        root_key_id: key_id,
                        allow_external_permissions: false,
                    })
                    .await
                {
                    Ok(resp) => resp.into_inner().macaroon,
                    Err(e) => {
                        tracing::error!("Failed to bake macaroon: {}", e);
                        return Err(Status::internal("Failed to bake macaroon"));
                    }
                };
                macaroons.insert(macaroon_id, macaroon);
            }
        } else {
            for (macaroon_id, _macaroon) in config.macaroons {
                let macaroon = format!("placeholder_macaroon_{}", macaroon_id);
                macaroons.insert(macaroon_id, macaroon);
            }
        }

        Ok(Response::new(AppContext {
            context: serde_json::to_string(&Context {
                network: self.btc_network.clone(),
                macaroons,
                tls_cert: self.tls_cert.clone(),
            })
            .map_err(|e| {
                tracing::error!("Failed to serialize context: {}", e);
                Status::internal("Failed to serialize context")
            })?,
        }))
    }
}
