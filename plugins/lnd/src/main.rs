use nirvati_plugin_lnd::plugin::PluginState;
use std::fs;
use tokio::sync::Mutex;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let plugin_state = if std::env::var("IS_MOCK").is_ok_and(|mock| mock == "true") {
        PluginState {
            btc_network: std::env::var("BTC_NETWORK").unwrap(),
            client: None,
            tls_cert: "placeholder_cert".to_string(),
        }
    } else {
        let cert_bytes = fs::read("/data/tls.cert").expect("FailedToReadTlsCertFile");
        let mac_bytes = fs::read(
            "/data/data/chain/bitcoin/".to_owned()
                + &std::env::var("BTC_NETWORK").unwrap()
                + "/admin.macaroon",
        )
        .expect("FailedToReadMacaroonFile");

        // Convert the bytes to a hex string
        let cert = hex::encode(&cert_bytes);
        let macaroon = hex::encode(&mac_bytes);
        let lnd_client =
            lnd_grpc_rust::connect(cert.clone(), macaroon, std::env::var("LND_URL").unwrap())
                .await
                .unwrap();
        PluginState {
            btc_network: std::env::var("BTC_NETWORK").unwrap(),
            client: Some(Mutex::new(lnd_client)),
            tls_cert: cert,
        }
    };
    let addr = "[::]:50051".parse().unwrap();
    tonic::transport::Server::builder()
        .add_service(
            nirvati_plugin_common::context_plugin_server::ContextPluginServer::new(plugin_state),
        )
        .serve(addr)
        .await
        .unwrap();
}
