use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct Macaroon {
    pub permissions: Vec<MacaroonPermission>,
}

#[derive(Serialize, Deserialize)]
pub struct Settings {
    pub macaroons: HashMap<String, Macaroon>,
}

#[derive(Serialize, Deserialize)]
pub struct Context {
    pub network: String,
    pub macaroons: HashMap<String, String>,
    pub tls_cert: String,
}

#[derive(Serialize, Deserialize)]
pub struct MacaroonPermission {
    /// The entity a permission grants access to.
    pub entity: String,
    /// The action that is granted.
    pub action: String,
}

impl From<MacaroonPermission> for lnd_grpc_rust::lnrpc::MacaroonPermission {
    fn from(permission: MacaroonPermission) -> Self {
        lnd_grpc_rust::lnrpc::MacaroonPermission {
            entity: permission.entity,
            action: permission.action,
        }
    }
}
