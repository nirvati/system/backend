use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

#[derive(Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
struct Schema {
    data: Vec<SchemaInner>,
    links: BTreeMap<String, String>,
    #[serde(rename = "resourceType")]
    resource_type: SchemaTypeShouldBeSchema,
    r#type: SchemaTypeShouldBeCollection,
}

#[derive(Serialize, Deserialize, Debug)]
enum CollectionMethod {
    #[serde(rename = "GET")]
    Get,
    #[serde(rename = "POST")]
    Post,
    #[serde(rename = "PUT")]
    Put,
    #[serde(rename = "DELETE")]
    Delete,
}

#[derive(Serialize, Deserialize, Debug)]
enum SchemaTypeShouldBeSchema {
    #[serde(rename = "schema")]
    Schema,
}

#[derive(Serialize, Deserialize, Debug)]
enum SchemaTypeShouldBeCollection {
    #[serde(rename = "collection")]
    Collection,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
struct SchemaInner {
    actions: BTreeMap<String, String>,
    id: String,
    #[serde(rename = "collectionMethods")]
    collection_methods: Vec<CollectionMethod>,
    #[serde(rename = "resourceMethods")]
    resource_methods: Vec<CollectionMethod>,
    #[serde(rename = "resourceActions", default)]
    resource_actions: BTreeMap<String, serde_json::Value>,
    links: BTreeMap<String, String>,
    #[serde(rename = "pluralName")]
    plural_name: String,
    #[serde(rename = "type")]
    schema_type: SchemaTypeShouldBeSchema,
    #[serde(rename = "resourceFields")]
    resource_fields: BTreeMap<String, Field>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
struct Field {
    #[serde(rename = "type")]
    field_type: String,
    #[serde(default)]
    create: bool,
    #[serde(default)]
    required: bool,
    default: Option<serde_json::Value>,
    #[serde(default)]
    nullable: bool,
    #[serde(default)]
    unique: bool,
    #[serde(default)]
    update: bool,
}

fn capitalize_first_letter(s: &str) -> String {
    s[0..1].to_uppercase() + &s[1..]
}

fn type_to_rust_type(field_type: &str) -> String {
    if let Some(inner_type) = field_type.strip_prefix("map[") {
        let inner_type = type_to_rust_type(&inner_type[..inner_type.len() - 1]);
        format!("HashMap<String, {}>", inner_type)
    } else if let Some(inner_type) = field_type.strip_prefix("array[") {
        let inner_type = type_to_rust_type(&inner_type[..inner_type.len() - 1]);
        format!("Vec<{}>", inner_type)
    } else if field_type == "string" {
        "String".to_string()
    } else if field_type == "int" {
        "i32".to_string()
    } else if field_type == "bool" {
        "bool".to_string()
    } else if let Some(inner_type) = field_type.strip_prefix("v1.") {
        "k8s_openapi::api::core::v1::".to_string() + inner_type
    } else if field_type.starts_with(".") {
        panic!("Unsupported field type: {}", field_type);
    } else {
        capitalize_first_letter(field_type)
    }
}

fn camel_to_snake(s: &str) -> String {
    let mut result = String::new();
    for (i, c) in s.chars().enumerate() {
        if c.is_uppercase() && i != 0 {
            result.push('_');
        }
        result.push(c.to_ascii_lowercase());
    }
    result.replace("u_r_l", "url")
}

fn main() {
    // Load the first argument (JSON file) and parse it
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: {} <json_file>", args[0]);
        std::process::exit(1);
    }

    let json_data = std::fs::read_to_string(&args[1]).expect("Unable to read file");
    let schema: Schema = serde_json::from_str(&json_data).expect("Unable to parse JSON");
    println!(
        r#"// SPDX-FileCopyrightText: Longhorn (Cloud Native Computing Foundation)
// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use std::collections::HashMap;

use serde::{{Deserialize, Serialize}};
use serde_aux::prelude::*;"#
    );
    for property in schema.data {
        let mut struct_name = property.plural_name[0..1].to_uppercase()
            + &property.plural_name[1..(property.plural_name.len() - 1)];
        if struct_name.ends_with("Statuse") {
            struct_name = struct_name[0..struct_name.len() - 1].to_string();
        }
        if struct_name == "InstanceProcesse" {
            continue;
        }
        println!("#[derive(Debug, Clone, Serialize, Deserialize)]");
        println!("pub struct {} {{", struct_name);
        for (mut field_name, field) in property.resource_fields {
            if field_name == "type" {
                field_name = "r#type".to_string();
            };
            let output_field_name = camel_to_snake(&field_name);
            let mut field_type = type_to_rust_type(&field.field_type);
            if output_field_name != field_name
                || field_type.starts_with("HashMap<")
                || field_type.starts_with("Vec<")
            {
                print!("    #[serde(");
                if output_field_name != field_name {
                    print!("rename = \"{}\",", field_name);
                }
                if field_type.starts_with("HashMap<") || field_type.starts_with("Vec<") {
                    print!("deserialize_with = \"deserialize_default_from_null\",");
                }
                println!(")]");
            }
            if struct_name.ends_with("Status")
                && !field_type.starts_with("HashMap")
                && !field_type.starts_with("Option")
            {
                field_type = format!("Option<{}>", field_type);
            }
            if struct_name == "Volume"
                && (field_name == "offlineReplicaRebuilding"
                    || field_name == "offlineReplicaRebuildingRequired")
            {
                field_type = format!("Option<{}>", field_type);
            }

            println!("    pub {}: {},", output_field_name, field_type);
        }
        println!("}}");
    }
}
