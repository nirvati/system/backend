VERSION 0.8

IMPORT github.com/earthly/lib/rust:3.0.3 AS rust

FROM rust:bookworm

WORKDIR /nirvati

RUN apt-get update && apt-get install -y pkg-config curl cmake make protobuf-compiler libparted-dev build-essential libseccomp-dev libclang-dev llvm-dev && rm -rf /var/lib/apt/lists/*

RUN curl -L https://github.com/rui314/mold/releases/download/v2.31.0/mold-2.31.0-$(uname -m)-linux.tar.gz -o - | tar -C /usr/local --strip-components=1 --no-overwrite-dir -xzf -

ARG --required PUSH_TAG
ARG --required TARGETARCH

build:
    DO rust+INIT --keep_fingerprints=true
    COPY --keep-ts --dir plugins services shared dev-tools crds Cargo.lock Cargo.toml .
    ENV RUSTFLAGS="-C link-arg=-fuse-ld=mold"
    WORKDIR /nirvati
    DO rust+CARGO --args="build --release
        --bin nirvati-api
        --bin nirvati-bg-update
        --bin nirvati-agent
        --bin nirvati-dns
        --bin nirvati-init
        --bin nirvati-telemetry
        --bin nirvati-plugin-lnd
        --bin nirvati-plugin-tor
        --bin nirvati-plugin-bitcoin
        --bin nirvati-plugin-tipi
        --bin nirvati-migrations
        --bin app-data-loader
        --bin citadel-api
    " --output="release/[^/\.]+"
    SAVE ARTIFACT target/release/app-data-loader app-data-loader
    SAVE ARTIFACT target/release/nirvati-agent nirvati-agent
    SAVE ARTIFACT target/release/nirvati-api nirvati-api
    SAVE ARTIFACT target/release/nirvati-bg-update nirvati-bg-update
    SAVE ARTIFACT target/release/nirvati-dns nirvati-dns
    SAVE ARTIFACT target/release/nirvati-init nirvati-init
    SAVE ARTIFACT target/release/nirvati-telemetry nirvati-telemetry
    SAVE ARTIFACT target/release/nirvati-plugin-tor nirvati-plugin-tor
    SAVE ARTIFACT target/release/nirvati-plugin-bitcoin nirvati-plugin-bitcoin
    SAVE ARTIFACT target/release/nirvati-plugin-lnd nirvati-plugin-lnd
    SAVE ARTIFACT target/release/nirvati-plugin-tipi nirvati-plugin-tipi
    SAVE ARTIFACT target/release/nirvati-migrations nirvati-migrations
    SAVE ARTIFACT target/release/citadel-api citadel-api

docker-api:
    ARG --required PUSH_TAG
    ARG --required TARGETARCH
    FROM debian:bookworm-slim
    RUN apt-get update && apt-get install -y libssl3 ca-certificates && rm -rf /var/lib/apt/lists/*
    COPY +build/nirvati-api /nirvati-api
    ENTRYPOINT ["/nirvati-api"]
    SAVE IMAGE --push harbor.nirvati.org/nirvati/api:$PUSH_TAG-$TARGETARCH

docker-agent:
    ARG --required PUSH_TAG
    ARG --required TARGETARCH
    FROM debian:bookworm-slim
    RUN apt-get update && apt-get install -y ca-certificates libseccomp2 libclang1 && rm -rf /var/lib/apt/lists/*
    COPY +build/nirvati-agent /nirvati-agent
    ENTRYPOINT ["/nirvati-agent"]
    SAVE IMAGE --push harbor.nirvati.org/nirvati/agent:$PUSH_TAG-$TARGETARCH

docker-bg-update:
    ARG --required PUSH_TAG
    ARG --required TARGETARCH
    FROM debian:bookworm-slim
    RUN apt-get update && apt-get install -y libssl3 && rm -rf /var/lib/apt/lists/*
    COPY +build/nirvati-bg-update /nirvati-bg-update
    ENTRYPOINT ["/nirvati-bg-update"]
    SAVE IMAGE --push harbor.nirvati.org/nirvati/bg-updater:$PUSH_TAG-$TARGETARCH

docker-telemetry:
    ARG --required PUSH_TAG
    ARG --required TARGETARCH
    FROM debian:bookworm-slim
    RUN apt-get update && apt-get install -y ca-certificates libssl3 && rm -rf /var/lib/apt/lists/*
    COPY +build/nirvati-telemetry /nirvati-telemetry
    ENTRYPOINT ["/nirvati-telemetry"]
    SAVE IMAGE --push harbor.nirvati.org/nirvati/telemetry:$PUSH_TAG-$TARGETARCH

docker-dns:
    ARG --required PUSH_TAG
    ARG --required TARGETARCH
    FROM debian:bookworm-slim
    RUN apt-get update && apt-get install -y libssl3 && rm -rf /var/lib/apt/lists/*
    COPY +build/nirvati-dns /nirvati-dns
    ENTRYPOINT ["/nirvati-dns"]
    SAVE IMAGE --push harbor.nirvati.org/nirvati/dns:$PUSH_TAG-$TARGETARCH

docker-init:
    ARG --required PUSH_TAG
    ARG --required TARGETARCH
    FROM debian:bookworm-slim
    RUN apt update && \
        apt install -y ca-certificates libatomic1 libclang1 libparted2 btrfs-progs udev libseccomp2 curl openssl && \
        curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash && \
        apt purge -y curl && \
        apt autoremove -y --purge && \
        apt clean && \
        rm -rf /var/lib/apt/lists/*
    COPY +build/nirvati-init /nirvati-init
    ENTRYPOINT ["/nirvati-init"]
    SAVE IMAGE --push harbor.nirvati.org/nirvati/init:$PUSH_TAG-$TARGETARCH

docker-plugin-tor:
    ARG --required PUSH_TAG
    ARG --required TARGETARCH
    FROM debian:bookworm-slim
    COPY +build/nirvati-plugin-tor /nirvati-plugin-tor
    ENTRYPOINT ["/nirvati-plugin-tor"]
    SAVE IMAGE --push harbor.nirvati.org/nirvati/nirvati-plugin-tor:$PUSH_TAG-$TARGETARCH

docker-plugin-bitcoin:
    ARG --required PUSH_TAG
    ARG --required TARGETARCH
    FROM debian:bookworm-slim
    COPY +build/nirvati-plugin-bitcoin /nirvati-plugin-bitcoin
    ENTRYPOINT ["/nirvati-plugin-bitcoin"]
    SAVE IMAGE --push harbor.nirvati.org/nirvati/nirvati-plugin-bitcoin:$PUSH_TAG-$TARGETARCH

docker-plugin-lnd:
    ARG --required PUSH_TAG
    ARG --required TARGETARCH
    FROM debian:bookworm-slim
    RUN apt-get update && apt-get install -y libssl3 && rm -rf /var/lib/apt/lists/*
    COPY +build/nirvati-plugin-lnd /nirvati-plugin-lnd
    ENTRYPOINT ["/nirvati-plugin-lnd"]
    SAVE IMAGE --push harbor.nirvati.org/nirvati/nirvati-plugin-lnd:$PUSH_TAG-$TARGETARCH

docker-plugin-tipi:
    ARG --required PUSH_TAG
    ARG --required TARGETARCH
    FROM debian:bookworm-slim
    COPY +build/nirvati-plugin-tipi /nirvati-plugin-tipi
    ENTRYPOINT ["/nirvati-plugin-tipi"]
    SAVE IMAGE --push harbor.nirvati.org/nirvati/nirvati-plugin-tipi:$PUSH_TAG-$TARGETARCH

docker-migrations:
    ARG --required PUSH_TAG
    ARG --required TARGETARCH
    FROM debian:bookworm-slim
    RUN apt-get update && apt-get install -y libssl3 ca-certificates curl && \
        curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash && \
        apt purge -y curl && \
        apt autoremove -y --purge && \
        apt clean && \
        rm -rf /var/lib/apt/lists/*
    COPY +build/nirvati-migrations /nirvati-migrations
    ENTRYPOINT ["/nirvati-migrations"]
    SAVE IMAGE --push harbor.nirvati.org/nirvati/migrations:$PUSH_TAG-$TARGETARCH

docker-app-data-loader:
    ARG --required PUSH_TAG
    ARG --required TARGETARCH
    FROM debian:bookworm-slim
    COPY +build/app-data-loader /app-data-loader
    ENTRYPOINT ["/app-data-loader"]
    SAVE IMAGE --push harbor.nirvati.org/nirvati/app-data-loader:$PUSH_TAG-$TARGETARCH

docker-citadel-api:
    ARG --required PUSH_TAG
    ARG --required TARGETARCH
    FROM debian:bookworm-slim
    RUN apt-get update && apt-get install -y ca-certificates libssl3 && rm -rf /var/lib/apt/lists/*
    COPY +build/citadel-api /citadel-api
    ENTRYPOINT ["/citadel-api"]
    SAVE IMAGE --push harbor.nirvati.org/citadel/api:$PUSH_TAG-$TARGETARCH
 
docker-all:
    BUILD +docker-api
    BUILD +docker-agent
    BUILD +docker-bg-update
    BUILD +docker-dns
    BUILD +docker-init
    BUILD +docker-telemetry
    BUILD +docker-plugin-tor
    BUILD +docker-plugin-bitcoin
    BUILD +docker-plugin-tipi
    BUILD +docker-plugin-lnd
    BUILD +docker-migrations
    BUILD +docker-app-data-loader
    BUILD +docker-citadel-api
